//
//  UITabBar+Badge.h
//  519
//
//  Created by 陈 on 16/9/3.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (Badge)

- (void)showBadgeOnItemIndex:(int)index value:(NSString*)value;   //显示小红点

- (void)hideBadgeOnItemIndex:(int)index; //隐藏小红点

@end
