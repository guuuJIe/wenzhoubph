//
//  ImageBrowserView.h
//  519
//
//  Created by Macmini on 2019/3/9.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageBrowserView : UIView<UIScrollViewDelegate>

// 图片数组

@property (nonatomic, strong) NSArray * imageArr;

// 创建图片浏览器

- (instancetype)initWithFrame:(CGRect)frame ImageArr:(NSArray *)imags andTag:(NSInteger)index;

// 显示图片浏览器

- (void)show;

@end

NS_ASSUME_NONNULL_END
