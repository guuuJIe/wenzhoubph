//
//  ImageBrowserView.m
//  519
//
//  Created by Macmini on 2019/3/9.
//  Copyright © 2019 519. All rights reserved.
//

#import "ImageBrowserView.h"
@interface ImageBrowserView()
@property (nonatomic,strong)UILabel *numlabel;
@end
@implementation ImageBrowserView

- (instancetype)initWithFrame:(CGRect)frame ImageArr:(NSArray *)imags andTag:(NSInteger)index{
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupChildControlWithArr:imags andTag:index];
    }
    
    return self;
}


- (void)setupChildControlWithArr:(NSArray *)arr andTag:(NSInteger)index{
    _imageArr = arr;
    
//    [self.numlabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(-60);
//        make.top.equalTo(40*AdapterHeightScal);
//    }];
    
//    [self addSubview:self.numlabel];
   
    
    UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0 , Screen_Width, Screen_Height)];
    
    scrollView.delegate = self;
    
    scrollView.scrollEnabled = YES;
    
    scrollView.pagingEnabled = YES;
    
    scrollView.backgroundColor = [UIColor whiteColor];
    
    scrollView.showsHorizontalScrollIndicator = NO;
    
    scrollView.showsVerticalScrollIndicator = NO;
    
    [self addSubview:scrollView];
    
//    [scrollView addSubview:self.numlabel];
//    [self.numlabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(scrollView).offset(45*AdapterHeightScal);
//        make.right.equalTo(-20);
//    }];
     self.numlabel.text = [NSString stringWithFormat:@"1/%lu",(unsigned long)arr.count];
    scrollView.contentSize = CGSizeMake( Screen_Width * arr.count , 0); // 内容视图大小
    
//    scrollView.contentOffset = CGPointMake(Screen_Width * (index-1), 0);   // 偏移量
    
    for ( int i = 0 ; i < arr.count ; i++ ) {
        
        UIScrollView *sc = [[UIScrollView alloc] initWithFrame:CGRectMake(Screen_Width  * i, Screen_Width/2-30,  Screen_Width , Screen_Width*0.8)];
        
        sc.backgroundColor = [UIColor clearColor];
        
        sc.maximumZoomScale = 2.0;
        
        sc.minimumZoomScale = 1.0;
        
        sc.decelerationRate = 0.2;
        
        sc.delegate = self;
        
        sc.tag = 1 + i;
        
        [scrollView addSubview:sc];
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.8)];
        
        [img sd_setImageWithURL:[NSURL URLWithString:arr[i]] placeholderImage:[UIImage new]];
        
        img.userInteractionEnabled = YES;
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenPics:)];
        
        tap.numberOfTapsRequired = 1;
        
        tap.numberOfTouchesRequired = 1;
        
        [sc addGestureRecognizer:tap];
        
        UITapGestureRecognizer * twoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makePicBigger:)];
        
        twoTap.numberOfTapsRequired = 2;
        
        img.userInteractionEnabled = YES;
        
        [img addGestureRecognizer:twoTap];
        
        img.contentMode = UIViewContentModeScaleAspectFit;
        
        img.tag = 1000 + i;
        
        img.userInteractionEnabled = YES;
        
        [sc addSubview:img];
        
        sc.contentSize = CGSizeMake(Screen_Width , 0);
        
        //   双击没有识别到的时候识别单击手势
        
        [tap requireGestureRecognizerToFail:twoTap];
        
    }
    
}

- (void)show{
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    UIWindow *window=[UIApplication sharedApplication].keyWindow;
    
    [window addSubview:self];
}

- (void)hiddenPics:(UITapGestureRecognizer *)tap{
    
    [self removeFromSuperview];
   
    [UIApplication sharedApplication].statusBarHidden = NO;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    BLog(@"%f",scrollView.contentOffset.x);
//    CGFloat OffsetX = scrollView.contentOffset.x;
//    int num = Screen_Width/OffsetX;
//    self.numlabel.text = [NSString stringWithFormat:@"%f/%lu",num+1.00,(unsigned long)self.imageArr.count];
}

- (void)makePicBigger:(UITapGestureRecognizer *)tap{
    
//    UIScrollView * sc = (UIScrollView *)[tap.view superview];
//
//    CGFloat zoomScale = sc.zoomScale;
//
//    zoomScale = (zoomScale ==  1.0) ? 3.0 : 1.0;
//
//    CGRect zoomRect = [self zoomRectForScale:zoomScale withCenter:[tap locationInView:tap.view]];
    
//    [sc zoomToRect:zoomRect animated:YES];
    
}
    

- (UILabel *)numlabel
{
    if (!_numlabel) {
        _numlabel = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width-70 , 45*AdapterHeightScal, 50, 50)];
        
        _numlabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        _numlabel.textColor = [UIColor whiteColor];
        _numlabel.layer.cornerRadius = 25;
        _numlabel.textAlignment = NSTextAlignmentCenter;
        _numlabel.layer.masksToBounds = true;
//        [self addSubview:_numlabel];
    }
    return _numlabel;
}


@end
