//
//  UIScrollView+EmptyDataSet.m
//  MallApp
//
//  Created by Mac on 2020/2/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UIScrollView+EmptyDataSet.h"

static char const * const kEmptyDataSetSource =     "emptyDataSetSource";
//static char const * const kEmptyDataSetDelegate =   "emptyDataSetDelegate";

@interface DZNWeakObjectContainer : NSObject

@property (nonatomic, readonly, weak) id weakObject;

- (instancetype)initWithWeakObject:(id)object;

@end

@implementation UIScrollView (EmptyDataSet)

- (BOOL)dzn_canDisplay
{
    if (self.emptyDataSetSource && [self.emptyDataSetSource conformsToProtocol:@protocol(DZNEmptyDataSetSource)]) {
        if ([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]] || [self isKindOfClass:[UIScrollView class]]) {
            return YES;
        }
    }
    
    return NO;
}

- (id<DZNEmptyDataSetSource>)emptyDataSetSource
{
    DZNWeakObjectContainer *container = objc_getAssociatedObject(self, kEmptyDataSetSource);
    return container.weakObject;
}



- (void)setEmptyDataSetSource:(id<DZNEmptyDataSetSource>)datasource
{
    if (!datasource || [self dzn_canDisplay]) {
//        [self dzn_invalidate];
        return;
    }
    
    objc_setAssociatedObject(self, kEmptyDataSetSource, [[DZNWeakObjectContainer alloc] initWithWeakObject:datasource], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // We add method sizzling for injecting -dzn_reloadData implementation to the native -reloadData implementation
//    [self swizzleIfPossible:@selector(reloadData)];
//
//    // Exclusively for UITableView, we also inject -dzn_reloadData to -endUpdates
//    if ([self isKindOfClass:[UITableView class]]) {
//        [self swizzleIfPossible:@selector(endUpdates)];
//    }
}


- (void)willMoveToSuperview:(UIView *)newSuperview{
    [super willMoveToSuperview:newSuperview];
    if (newSuperview.superview) {
        UIView *view = [self customeView];
        if (view) {
            [view removeFromSuperview];
            view = nil;
        }
    }
}

- (NSInteger)itemsCount{
    NSInteger items = 0;
    // UIScollView doesn't respond to 'dataSource' so let's exit
    if (![self respondsToSelector:@selector(dataSource)]) {
        return items;
    }
    // UITableView support
    if ([self isKindOfClass:[UITableView class]]) {
        
        UITableView *tableView = (UITableView *)self;
        id <UITableViewDataSource> dataSource = tableView.dataSource;
        
        NSInteger sections = 1;
        
        if (dataSource && [dataSource respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
            sections = [dataSource numberOfSectionsInTableView:tableView];
        }
        
        if (dataSource && [dataSource respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) {
            for (NSInteger section = 0; section < sections; section++) {
                items += [dataSource tableView:tableView numberOfRowsInSection:section];
            }
        }
    }
    // UICollectionView support
    else if ([self isKindOfClass:[UICollectionView class]]) {
        
        UICollectionView *collectionView = (UICollectionView *)self;
        id <UICollectionViewDataSource> dataSource = collectionView.dataSource;

        NSInteger sections = 1;
        
        if (dataSource && [dataSource respondsToSelector:@selector(numberOfSectionsInCollectionView:)]) {
            sections = [dataSource numberOfSectionsInCollectionView:collectionView];
        }
        
        if (dataSource && [dataSource respondsToSelector:@selector(collectionView:numberOfItemsInSection:)]) {
            for (NSInteger section = 0; section < sections; section++) {
                items += [dataSource collectionView:collectionView numberOfItemsInSection:section];
            }
        }
    }
    
    
    
    return items;
}


- (void)reloadEmptyDataSet
{
    [self gj_reloadEmptyDataSet];
}

- (void)gj_reloadEmptyDataSet{
    if ([self dzn_canDisplay]) {
        if ([self itemsCount] == 0) {
            UIView *view = [self customeView];
            view.userInteractionEnabled = true;
            [view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
            if (self.hidden) {
                self.hidden = false;
            }
            if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
                [self insertSubview:view atIndex:0];
            }
            else {
                [self addSubview:view];
            }
            
        }
    }
    
//    if ([self netWorkChangeEvent] >= 1) {
//
//    }else{
//
//    }

}

- (void)reloadNetUnworkDataSet{
    UIView *view = [self customeView];
    view.userInteractionEnabled = true;
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    if (view.hidden) {
        view.hidden = false;
    }
    self.hidden = true;
    if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
        [self.superview insertSubview:view atIndex:0];
    }
    else {
        [self.superview addSubview:view];
    }
}

- (void)reloadNetworkDataSet{
    UIView *view = [self customeView];
//    view.userInteractionEnabled = true;
//    [view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    if (view) {
        view.hidden = true;
    }
    
    if (self.hidden) {
        self.hidden = false;
    }


}

- (UIView *)customeView{
    if (self.emptyDataSetSource && [self.emptyDataSetSource respondsToSelector:@selector(customViewForEmptyDataSet:)]) {
        UIView *view = [self.emptyDataSetSource customViewForEmptyDataSet:self];
        return view;
    }
    return nil;
}

- (void)click:(id)sender{
    if (self.emptyDataSetSource && [self.emptyDataSetSource respondsToSelector:@selector(emptyDataSet:didTapView:)]) {
        [self.emptyDataSetSource emptyDataSet:self didTapView:sender];
    }
}



@end




#pragma mark - DZNWeakObjectContainer

@implementation DZNWeakObjectContainer

- (instancetype)initWithWeakObject:(id)object
{
    self = [super init];
    if (self) {
        _weakObject = object;
    }
    return self;
}

@end
