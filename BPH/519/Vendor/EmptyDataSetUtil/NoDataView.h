//
//  NoDataView.h
//  MallApp
//
//  Created by Mac on 2020/2/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoDataView : UIView
+(instancetype)initView;
@end

NS_ASSUME_NONNULL_END
