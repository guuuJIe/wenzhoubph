//
//  PayHelper.m
//  carcool
//
//  Created by 朱子涵 on 15/10/11.
//  Copyright © 2015年 ttsoft. All rights reserved.
//

#import "PayHelper.h"
#import "PayConfig.h"
#import "AliPayOrder.h"
#import "DataSigner.h"
#import "payRequsestHandler.h"
#import "PayHelper.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>

//支付结果回调页面
#define ZFB_NOTIFY_URL      @"http://www.519wz.cn/callback/payment/aliapp_notify.php"

@implementation PayHelper

+ (instancetype)sharedManager {
    static PayHelper *shared_manager = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];
    });
    return shared_manager;
}

-(void)ZFBPay:(NSString *)orderId price:(NSString *)price title:(NSString *)title info:(NSString *)info{
    //支付宝支付
    /*
     *商户的唯一的parnter和seller。
     *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
     */
    
    /*============================================================================*/
    /*=======================需要填写商户app申请的===================================*/
    /*============================================================================*/
    NSString *partner = ZFB_PartnerID;
    NSString *seller = ZFB_SellerID;
    NSString *privateKey = ZFB_PrivateKey;
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    AliPayOrder *order = [[AliPayOrder alloc] init];
    order.partner = partner;
    order.seller = seller;
    order.tradeNO = orderId; //订单ID（由商家自行制定）
    order.productName = info; //商品标题
    order.productDescription =title; //商品描述
    //        order.amount =[self.needMoney.text substringToIndex:self.needMoney.text.length-1]; //商品价格
    order.amount =price; //商品价格
    order.notifyURL = ZFB_NOTIFY_URL; //回调URL
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"wyjzfb";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            if(self.zfbPayBlock){
                self.zfbPayBlock(resultDic);
            }
        }];
    }
    
}

-(void)WXPay:(NSString *)prepayId{
    payRequsestHandler *req = [payRequsestHandler alloc] ;
    [req init:APP_ID mch_id:MCH_ID];
    [req setKey:PARTNER_ID];
    //    double priceD=price.doubleValue *100;
    NSMutableDictionary *dict = [req sendPay_demo:prepayId];
    if(dict == nil){
        //错误提示
        NSString *debug = [req getDebugifo];
        NSLog(@"%@\n\n",debug);
    }else{
        NSLog(@"%@\n\n",[req getDebugifo]);
        NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
        
        //调起微信支付
        PayReq* req             = [[PayReq alloc] init];
        req.openID              = [dict objectForKey:@"appid"];
        req.partnerId           = [dict objectForKey:@"partnerid"];
        req.prepayId            = [dict objectForKey:@"prepayid"];
        req.nonceStr            = [dict objectForKey:@"noncestr"];
        req.timeStamp           = stamp.intValue;
        req.package             = [dict objectForKey:@"package"];
        req.sign                = [dict objectForKey:@"sign"];
        
        Boolean result = [WXApi sendReq:req];
        if(result){
            
        }else{
            
        }
        NSLog(@"%hhu\n\n",result);
    }
    
}



@end
