//
//  ZFBConfig.pch
//  carcool
//
//  Created by yons on 15/9/22.
//  Copyright (c) 2015年 ttsoft. All rights reserved.
//

#ifndef carcool_ZFBConfig_pch
#define carcool_ZFBConfig_pch
//支付宝－－－－－－－－－－－－－－－－－－－－－－－－－
//合作身份者id，以2088开头的16位纯数字
#define ZFB_PartnerID @"2088021233320460"
//收款支付宝账号
#define ZFB_SellerID  @"wuyaojiu519@126.com"

//安全校验码（MD5）密钥，以数字和字母组成的32位字符
#define MD5_KEY @""

//商户私钥，自助生成
#define ZFB_PrivateKey @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANQTQW5cSjXn8DvxQNWkpxi2Esav8d7yk2fTZuhVXpyjqqD5zCBESiB6BqJYWwSUkVOq0HMVtIahBp4rrU8QCjlI0tmvCbgkoFW50hA1va9fZ/lX4VVH1w8nP2F2PMy+Rd/Gor1WhdVt9EC8mU5DvYEBe8e74oA+Mm//vmGAOzwHAgMBAAECgYEAsOgjI7lu2krRVl9ocw83qGBxhzZL8Guv1OM/K73S/51zf3cA9UWiBDdmg1RrEIYSOJX77pSDk+6NQ7IxjwtVSnCspBiVqnuMg4kIsPWk4MMs3hbQcYTsK4EhFL5g08sMl804Y5aKt5bPqBg8WA/TY+VXpa6WdVkhx6AdGVEttvECQQD0XM2Mmv9eCX91Tq/f+XeaQ9Z8MjKmCdDFYOIya3D78ZJNd7WsBmQfvWljk8EzY1RzENG2BNDXpp7REeQV8MfvAkEA3izQuBDDNmh2qs02rQjXYDrpZdwKg6fiBM+tSNvCCjBJ2YmnmtjCpFtEwWb84cVi6ieSYMj+E7VHdFcKy3d1aQJAA1VYwHhEz4WVpYYDRCzlAse2H0+X/UfY/zhJHaL1kZpEAqaFZelYaE5vJ5qWYhsSxdcO5Uq/hpGRTQzPbbUxJQJAB/5ozBaeT3jWvDKCAxvMVAQKncWbAz2pHi8ytuphQuiV36PaSfH+ntdB1AuWkbD+bBpcY+sXaTIejgsLsmJr8QJBAM+9Kif69/32qWG56qIMMB9wsvOTpXQ0ckCcmAUb2cipWNSEVcOuTosAZaDnHx8Pvz1sRmWOzGaa5ws5a4bCkUk="

//支付宝公钥
#define ZFB_PublicKey @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB"



//微信－－－－－－－－－－－－－－－－－－－－－－－－－
#define APP_ID          @"wxd8b6de3105de6063"               //APPID
#define APP_SECRET      @"d4624c36b6795d1d99dcf0547af5443d" //appsecret
//商户号，填写商户对应参数
#define MCH_ID          @"1288959501"
//商户API密钥，填写相应参数
#define PARTNER_ID      @"4wChgUuFZFCnVhYZhncCaYodxzrHtwyj"
//支付结果回调页面
#define WX_NOTIFY_URL      @"http://519wz.cn/shop.php?ctl=payment&act=notify&class_name=WxApp&is_wap=1"
//获取服务器端支付数据地址（商户自定义）
#define SP_URL          @"http://wxpay.weixin.qq.com/pub_v2/app/app_pay.php"

#endif
