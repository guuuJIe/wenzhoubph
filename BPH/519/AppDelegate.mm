//
//  AppDelegate.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import "AppDelegate.h"
#import "payRequsestHandler.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApiManager.h"
#import "AdvertiseView.h"
#import "MainVC.h"
#import "BNCoreServices.h"
#import "MainTabBarController.h"
#import "TableVC.h"

@interface AppDelegate ()<UMSocialUIDelegate,WXApiDelegate,UNUserNotificationCenterDelegate,AdvertiseDissmissDelegate,BMKLocationAuthDelegate>


@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    
    
    //设置header  useragent头部 加上这段代码可以去掉网页多余的头部 底部
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString *oldAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];

    NSString *newAgent = [oldAgent stringByAppendingString:@"IOS_bOpIhUi90_"];

    //注册useraagent
    NSDictionary *dictionary=[[NSDictionary alloc]initWithObjectsAndKeys:newAgent,@"UserAgent" ,nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    
    //获取设备产品信息
    struct utsname systemInfo;
    uname(&systemInfo);
    NSLog(@"设备信息：%@", [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding]);
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    //设置窗口对象
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.window.rootViewController = [[MainTabBarController alloc]init];
    //设置背景色
//    self.window.backgroundColor = [UIColor whiteColor];
    
    [[UITabBar appearance] setTranslucent:NO];
    //显示窗口
    [self.window makeKeyAndVisible];
    
    [WXApi registerApp:WX_APPID withDescription:@"wechat"];
//
//    [[QYSDK sharedSDK] registerAppId:QYKEY appName:@"泊啤汇供应链"];
//    [[[QYSDK sharedSDK]conversationManager] setDelegate:self];
    
    [UMUtil initUM:launchOptions];
    //iOS10必须加下面这段代码。
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    UNAuthorizationOptions types10=UNAuthorizationOptionBadge|  UNAuthorizationOptionAlert|UNAuthorizationOptionSound;
    [center requestAuthorizationWithOptions:types10     completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            //点击允许
            //这里可以添加一些自己的逻辑
        } else {
            //点击不允许
            //这里可以添加一些自己的逻辑
        }
    }];
    //设置NAV
    [self initNavBar];
    
#ifdef DEBUG
    [MobClick setLogEnabled:YES];
#else
    
#endif
    
    //百度定位
    [[BMKLocationAuth sharedInstance] checkPermisionWithKey:BAIDU_KEY authDelegate:self];
    BOOL ret = [[[BMKMapManager alloc]init]start:BAIDU_KEY  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    [BNCoreServices_Instance startServicesAsyn:^{
        [BNCoreServices_Instance authorizeNaviAppKey:BAIDU_KEY completion:^(BOOL suc) {
            NSLog(@"authorizeNaviAppKey ret = %d",suc);
        }];
//        [BNCoreServices_Instance authorizeTTSAppId:@"" apiKey:@"" secretKey:@"" completion:^(BOOL suc) {
//
//        }];
    } fail:^{
        
    }];
    
    NSString *homeDir = NSHomeDirectory();
    NSString* cachepath = [NSHomeDirectory()stringByAppendingString:@"Douments"];
    BLog(@"%@ == %@",homeDir,cachepath);
    
    // 1.判断沙盒中是否存在广告图片，如果存在，直接显示
    NSString *filePath = [self getFilePathWithImageName:[kUserDefaults valueForKey:adImageName]];
    NSLog(@"%@",filePath);
    BOOL isExist = [self isFileExistWithFilePath:filePath];
    if (isExist) {// 图片存在
        
        AdvertiseView *advertiseView = [[AdvertiseView alloc] initWithFrame:self.window.frame];
        advertiseView.filePath = filePath;
        advertiseView.delegate = self;
        [advertiseView show];
        
    }else{
        [self dissmiss];
    }
    // 2.无论沙盒中是否存在广告图片，都需要重新调用广告接口，判断广告是否更新
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self requestUrlImage];
    });
    
#if DEBUG
    
    // for iOS
    [[NSBundle  bundleWithPath:@"/Applications/InjectionIII.app/Contents/Resources/iOSInjection10.bundle"] load];
    
   
    
#endif
    [self netWorkChangeEvent];
    
    return YES;

    
}
-(void)dissmiss{
    [self setStatusBarBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0]];
}
//设置状态栏颜色
- (void)setStatusBarBackgroundColor:(UIColor *)color {
      UIView *statusBar;
      
      if (@available(iOS 13.0, *)) {
         UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
           statusBar = [[UIView alloc] initWithFrame:keyWindow.windowScene.statusBarManager.statusBarFrame];
      } else {
          // Fallback on earlier versions
          statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
      }
          
     
      if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
          statusBar.backgroundColor = color;
      }
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    return YES;
}
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
    }else{
        return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    }
    return YES;

}

//后台进入前台刷新定位
-(void)applicationWillEnterForeground:(UIApplication *)application{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshLocation" object:nil];
}

#pragma mark 友盟分享回调  UMSocialUIDelegate
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]
                  stringByReplacingOccurrencesOfString: @">" withString: @""]
                 stringByReplacingOccurrencesOfString: @" " withString: @""];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    //关闭友盟自带的弹出框
    [UMessage setAutoAlert:NO];
    [UMessage didReceiveRemoteNotification:userInfo];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        [self showChatViewController:userInfo];
    }
}
-(void)showChatViewController:(NSDictionary *)remoteNotificationInfo
{
    id object = [remoteNotificationInfo objectForKey:@"nim"]; //含有“nim”字段，就表示是七鱼的消息
    if (object)
    {
        //所有 UINavigationController 都先 popToRootViewController，
        //然后将聊天窗口 push 进某个 UINavigationController
        MainTabBarController *rootVc = (MainTabBarController *)_window.rootViewController;
        assert(rootVc.viewControllers.count == 2);
        UINavigationController *mainNav = rootVc.viewControllers[0];
        [mainNav popToRootViewControllerAnimated:NO];
        UINavigationController *settingNav = rootVc.viewControllers[1];
        [settingNav popToRootViewControllerAnimated:NO];
        [rootVc setSelectedIndex:1];
        assert(settingNav.viewControllers.count > 0);
        //        QYSettingViewController *settingViewController = settingNav.viewControllers[0];
        //        [settingViewController onChat];
    }
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
//    return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
        if ([url.host isEqualToString:@"safepay"]) {
            
            //跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
        }else if([url.host isEqualToString:@"pay"]){
            //跳转微信支付进行支付，处理支付结果
            [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
        }
        return YES;
    }
    return result;
}

//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于前台时的远程推送接受
        //关闭友盟自带的弹出框
        [UMessage setAutoAlert:NO];
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
    }else{
        //应用处于前台时的本地推送接受
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
    }else{
        //应用处于后台时的本地推送接受
    }
    
}


//打开推送消息后处理
- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {

}


#pragma mark 导航条
- (void)initNavBar {
    //设置Nav的背景色和title色
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    
    
    NSDictionary *textAttributes = nil;
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        [navigationBarAppearance setTintColor:[UIColor whiteColor]];//返回按钮的箭头颜色
        [[UITextField appearance] setTintColor:[UIColor colorWithHexString:@"0x3bbc79"]];//设置UITextField的光标颜色
        [[UITextView appearance] setTintColor:[UIColor colorWithHexString:@"0x3bbc79"]];//设置UITextView的光标颜色
        //        [[UISearchBar appearance] setBackgroundImage:[UIImage imageWithColor:kColorNAVBackground] forBarPosition:0 barMetrics:UIBarMetricsDefault];
        textAttributes = @{
                           NSFontAttributeName: FZLanTingHei_L(19),
                           NSForegroundColorAttributeName: APPTabColor,
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        [[UISearchBar appearance] setBackgroundImage:[UIImage imageWithColor:NavBarBGColor]];
        
        textAttributes = @{
                           FZLanTingHei_B(19),
                           UITextAttributeTextColor:  NavBarTitleColor,
                           UITextAttributeTextShadowColor: [UIColor clearColor],
                           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetZero],
                           };
#endif
    }
    [navigationBarAppearance setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:UIBarMetricsDefault];
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
    //消除下方阴影线
    [navigationBarAppearance setShadowImage:[UIImage imageWithColor:APPColor]];
    
}

#pragma mark 广告页部分
/**
 *  判断文件是否存在
 */
- (BOOL)isFileExistWithFilePath:(NSString *)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = FALSE;
    return [fileManager fileExistsAtPath:filePath isDirectory:&isDirectory];
}

/**
 *  初始化广告页面
 */
-(void)requestUrlImage{
    NSString * imgUrl = [NSString stringWithFormat:@"%@ctl=init",FONPort];
        [RequestData requestDataOfUrl:imgUrl success:^(NSDictionary *dic) {
            
            NSArray * arr =dic[@"start_page"];
            if (arr.count>0) {
                for (NSDictionary * dict in arr) {
                    
                    if (dict[@"img"]) {
                        NSArray *stringArr = [dict[@"img"] componentsSeparatedByString:@"/"];
                        NSString *imageName;
                        for (NSString * str in stringArr) {
                            if ([str rangeOfString:@"?"].location != NSNotFound) {
                                NSArray * imgNameArr = [str componentsSeparatedByString:@"?"];
                                imageName = imgNameArr.firstObject;
                                break;
                            }else{
                                imageName = stringArr.lastObject;
                            }
                        }
                        
                        
                        NSString * imageId;
                        NSString * typeStr;
                        
                        if ([dict[@"type"] isEqualToString:@"0"]) {
                            imageId =dict[@"data"][@"url"];
                        }
                        if ([dict[@"type"] isEqualToString:@"11"]) {
                            imageId = dict[@"data"][@"tid"];
                        }
                        if ([dict[@"type"] isEqualToString:@"12"]) {
                            imageId = dict[@"data"][@"cate_id"];
                        }
                        if ([dict[@"type"] isEqualToString:@"21"]) {
                            imageId = dict[@"data"][@"item_id"];
                        }
                        typeStr = dict[@"type"];
                        
                        
                        // 拼接沙盒路径
                        NSString *filePath = [self getFilePathWithImageName:imageName];
                        
                        NSString * adimagename = [kUserDefaults valueForKey:adImageName];
                        if (!adimagename) {
                            [kUserDefaults setObject:imageName forKey:adImageName];
                        }
                        
                        BOOL isExist = [self isFileExistWithFilePath:filePath];
                        if (!isExist){// 如果该图片不存在，则删除老图片，下载新图片
                            
                            [self downloadAdImageWithUrl:dict[@"img"] imageName:imageName ImageId:imageId withimageType:typeStr];
                            
                        }
                    }
                }
            
            }else{
                [self deleteOldImage];
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    
}

/**
 *  下载新图片
 */
- (void)downloadAdImageWithUrl:(NSString *)imageUrl imageName:(NSString *)imageName ImageId:(NSString *)imageId withimageType:(NSString *)type
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self deleteOldImage];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        UIImage *image = [UIImage imageWithData:data];
        
        NSString *filePath = [self getFilePathWithImageName:imageName]; // 保存文件的名称
        if ([UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES]) {// 保存成功
            NSLog(@"保存成功");
            
            [kUserDefaults setValue:imageName forKey:adImageName];
            [kUserDefaults setValue:imageId forKey:@"advImageId"];
            [kUserDefaults setValue:type forKey:@"advImageType"];
            [kUserDefaults synchronize];
            
        }else{
            NSLog(@"保存失败");
        }
        
    });
}

/**
 *  删除旧图片
 */
- (void)deleteOldImage
{
    NSString *imageName = [kUserDefaults valueForKey:adImageName];
    if (imageName) {
        NSString *filePath = [self getFilePathWithImageName:imageName];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:filePath error:nil];
    }
}

/**
 *  根据图片名拼接文件路径
 */
- (NSString *)getFilePathWithImageName:(NSString *)imageName
{
    if (imageName) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:imageName];
        return filePath;
    }
    
    return nil;
}


#pragma mark - 检测网络状态变化
-(NSInteger)netWorkChangeEvent
{
//    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    NSURL *url = [NSURL URLWithString:@"https://www.baidu.com"];
    __block NSInteger Type = 0;
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
//    self.netWorkStatesCode =AFNetworkReachabilityStatusUnknown;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//        self.netWorkStatesCode = status;
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
                BLog(@"当前使用的是流量模式");
                Type = 1;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                BLog(@"当前使用的是wifi模式");
                Type = 2;
                break;
            case AFNetworkReachabilityStatusNotReachable:
                BLog(@"断网了");
                Type = -1;
                break;
            case AFNetworkReachabilityStatusUnknown:
                BLog(@"变成了未知网络状态");
                Type = 0;
                break;
                
            default:
                break;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"netWorkChangeEventNotification" object:@(status)];
    }];
    [manager.reachabilityManager startMonitoring];
    return Type;
}

@end
