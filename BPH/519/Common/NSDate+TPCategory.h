//
//  NSDate+DateHelper.h
//  CommonLibrary
//
//  Created by rang on 13-1-7.
//  Copyright (c) 2014 cliff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TPCategory)

-(NSString*)toDateString;
-(NSString*)toDateTimeString;
-(NSString*)toDateString : (NSString*)format;

+(NSDate*)dateFromString : (NSString*)dateString;
+(NSDate*)dateFromString : (NSString*)dateString  withFormat : (NSString*)format;

-(NSDate*)addSeconds : (int)seconds;
-(NSDate*)addMinutes : (int)minutes;
-(NSDate*)addHours : (int)hours;
-(NSDate*)addDays : (int)days;
-(NSDate*)addMonths : (int)months;
-(NSDate*)addYears : (int)years;
+(int)DateDiffOfDays :(NSDate*)fromDate toDate:(NSDate*)toDate;

@end
