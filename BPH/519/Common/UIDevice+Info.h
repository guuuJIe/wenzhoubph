//
//  UIDevice+Info.h
//  CarCool_iOS
//
//  Created by cliff on 14-9-24.
//  Copyright (c) 2014年 Coding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Info)

+ (NSDictionary *)systemInfoDict;

@end
