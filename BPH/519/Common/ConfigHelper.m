#define kDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#import "ConfigHelper.h"

@interface ConfigHelper ()



@end

@implementation ConfigHelper



+(void)updateValue:(NSString*)itemName : (NSString*)itemValue{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:itemValue forKey:itemName];
    [userDefaults synchronize];
}

+(void)updateIntValue:(NSString*)itemName : (int)itemValue{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:[NSNumber numberWithInt:itemValue] forKey:itemName];
    [userDefaults synchronize];
}

+(void)updateObjValue:(NSString*)itemName : (id)itemValue{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:itemValue forKey:itemName];
    [userDefaults synchronize];
}

+(id)getValue:(NSString*)itemName{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id value = [userDefaults objectForKey : itemName ];
    return value;
}


+(int)getIntValue:(NSString*)itemName{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    return (int)[userDefaults integerForKey: itemName];
    
}


+(NSString*)getStringValue:(NSString*)itemName{
    id value = [self getValue:itemName];
    return [NSString stringWithFormat:@"%@", value];
}


+(NSAttributedString *)getSumString:(NSString *)string
                          withRange:(NSRange)range
                      withRangeFont:(UIFont *)rangeFont
                      withOtherFont:(UIFont *)otherFont
                 WithOtherFontColor:(UIColor *)otherColor
                     withRangeColor:(UIColor *)rangeColor{
    @try {
        if(string){
            NSMutableAttributedString *attributedString=[[NSMutableAttributedString alloc]initWithString:string];
            
            
            
            // 设置字体
            [attributedString addAttribute:NSFontAttributeName value:otherFont range:NSMakeRange(0, string.length)];
            [attributedString addAttribute:NSForegroundColorAttributeName value:otherColor range:NSMakeRange(0, string.length)];
            [attributedString addAttribute:NSStrokeColorAttributeName value:otherColor range:NSMakeRange(0, string.length)];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:@0 range:NSMakeRange(0, string.length)];
            
            
            [attributedString addAttribute:NSFontAttributeName value:rangeFont range:range];
            [attributedString addAttribute:NSForegroundColorAttributeName value:rangeColor range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:otherColor range:range];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:@0 range:range];
            return attributedString;
        }
        return [NSMutableAttributedString new];
    } @catch (NSException *exception) {
        return [NSMutableAttributedString new];
    } @finally {
        
    }
    
}

/**
 拨打电话
 @param phonenum 电话号码
 */
+ (void)dailNumber:(NSString *)phonenum{
    NSString *callPhone = [NSString stringWithFormat:@"telprompt://%@", phonenum];
    
    NSComparisonResult compare = [[UIDevice currentDevice].systemVersion compare:@"10.0"];
    
    if (compare == NSOrderedDescending || compare == NSOrderedSame) {
        
        /// 大于等于10.0系统使用此openURL方法
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone] options:@{} completionHandler:nil];
        
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone]];
        
    }
}

+ (void)hideTabBar:(UITabBarController *)tabbarcontroller
{
    // 隐藏tabbar
    [UIView animateWithDuration:UINavigationControllerHideShowBarDuration animations:^{
        CGFloat H = tabbarcontroller.tabBar.frame.size.height+50;

        for(UIView *view in tabbarcontroller.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x,
                                          view.frame.origin.y + H,
                                          view.frame.size.width,
                                          view.frame.size.height)];
            }
            else
            {
                [view setFrame:CGRectMake(view.frame.origin.x,
                                          view.frame.origin.y,
                                          view.frame.size.width,
                                          view.frame.size.height + H)];
            }
        }
    }];
    
   BLog(@"%f",tabbarcontroller.tabBar.origin.y);
}

+(BOOL)tabbarIsShow{
    return false;
}

+ (void)showTabBar:(UITabBarController *)tabbarcontroller
{
    // 显示tabbar
    
    BLog(@"%f",tabbarcontroller.tabBar.origin.y);
    
    CGFloat H = tabbarcontroller.tabBar.frame.size.height;
    
    if (H<tabbarcontroller.tabBar.frame.size.height+50) {
        
    }
    
    [UIView animateWithDuration:UINavigationControllerHideShowBarDuration animations:^{
        for(UIView *view in tabbarcontroller.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x,
                                          view.frame.origin.y - H,
                                          view.frame.size.width,
                                          view.frame.size.height)];
            }
            else
            {
                [view setFrame:CGRectMake(view.frame.origin.x,
                                          view.frame.origin.y,
                                          view.frame.size.width,
                                          view.frame.size.height - H)];
            }
        }
    }];
}
@end
