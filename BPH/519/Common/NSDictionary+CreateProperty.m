//
//  NSDictionary+CreateProperty.m
//  519
//
//  Created by Macmini on 2019/1/11.
//  Copyright © 2019 519. All rights reserved.
//

#import "NSDictionary+CreateProperty.h"

@implementation NSDictionary (CreateProperty)

- (void)createPrope{
    NSMutableString *codes = [NSMutableString new];
    [self enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        BLog(@"key:%@,obj:%@,type%@",key,obj,[obj class]);
        NSString *code;
        if ([obj isKindOfClass:[NSString class]]) {
            code = [NSString stringWithFormat:@"@property (nonatomic,strong)NSString *%@",key];
        }else if ([obj isKindOfClass:[NSArray class]]){
            code = [NSString stringWithFormat:@"@property (nonatomic,strong)NSArray *%@",key];
        }else if ([obj isKindOfClass:[NSDictionary class]]){
            code = [NSString stringWithFormat:@"@property (nonatomic,strong)NSDictionary *%@",key];
        }else if ([obj isKindOfClass:NSClassFromString(@"_NSCFBoolean")]){
            code = [NSString stringWithFormat:@"@property (nonatomic,assign)BOOL %@",key];
        }else if ([obj isKindOfClass:[NSNumber class]]){
            code = [NSString stringWithFormat:@"@property (nonatomic,assign)NSInteger %@",key];
        }
        
        [codes appendFormat:@"\n%@\n",code];
    }];
    
    BLog(@"%@",codes);
    
    
}

@end
