//
//  AFURLSessionManager+AFNetworkingVideoDownload.m
//  519
//
//  Created by Macmini on 2019/7/17.
//  Copyright © 2019 519. All rights reserved.
//

#import "AFURLSessionManager+AFNetworkingVideoDownload.h"

@implementation AFURLSessionManager (AFNetworkingVideoDownload)
- (NSURLSessionDownloadTask *)download:(NSString *)URLString
                              progress:(void (^)(NSProgress * _Nonnull))downloadProgress
                           destination:(NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
                     completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler
{
    
    NSURLSessionDownloadTask *dataTask = [self downloadTaskWithURL:[NSURL URLWithString:URLString] progress:downloadProgress destination:destination completionHandler:completionHandler];
    
    [dataTask resume];
    
    return dataTask;
}


- (NSURLSessionDownloadTask *)downloadTaskWithURL:(NSURL *)url
                                         progress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                                      destination:(NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
                                completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler{
    __block NSURLSessionDownloadTask *downloadTask = nil;
    

   
        downloadTask = [self.session downloadTaskWithURL:url];
  
 
    
    [self addDelegateForDownloadTask:downloadTask progress:downloadProgressBlock destination:destination completionHandler:completionHandler];

    
    
    
    return downloadTask;
}
@end
