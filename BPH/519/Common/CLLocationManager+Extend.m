//
//  CLLocationManager+Extend.m
//  519
//
//  Created by Macmini on 2019/1/17.
//  Copyright © 2019 519. All rights reserved.
//

#import "CLLocationManager+Extend.h"

@implementation CLLocationManager (Extend)

+ (void)load {
    
    
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0) {
        
        method_exchangeImplementations(class_getInstanceMethod(self.class, NSSelectorFromString(@"setAllowsBackgroundLocationUpdates:")),
                                       
                                       class_getInstanceMethod(self.class, @selector(swizzledSetAllowsBackgroundLocationUpdates:)));
        
    }
    
    
    
}


- (void)swizzledSetAllowsBackgroundLocationUpdates:(BOOL)allow {
    
    if (allow) {
        
        NSArray* backgroundModes  = [[NSBundle mainBundle].infoDictionary objectForKey:@"UIBackgroundModes"];
        
        
        
        if( backgroundModes && [backgroundModes containsObject:@"location"]) {
            
            [self swizzledSetAllowsBackgroundLocationUpdates:allow];
            
        }else{
            
            BLog(@"APP想设置后台定位，但APP的info.plist里并没有申请后台定位");
            
        }
        
    }else{
        
        [self swizzledSetAllowsBackgroundLocationUpdates:allow];
        
    }
    
}


@end
