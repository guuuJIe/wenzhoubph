//
//  NSString+StringHelper.m
//  CommonLibrary
//
//  Created by rang on 13-1-7.
//  Copyright (c) 2013年 rang. All rights reserved.
//

#import "NSString+TPCategory.h"
#import "NSData+TPCategory.h"
#import "NSDate+TPCategory.h"
#import <CommonCrypto/CommonDigest.h>
@implementation NSString (TPCategory)
//生成一个guid值
+(NSString*)createGUID{
    CFUUIDRef uuid_ref = CFUUIDCreate(NULL);
    CFStringRef uuid_string_ref= CFUUIDCreateString(NULL, uuid_ref);
    CFRelease(uuid_ref);
    NSString *uuid = [NSString stringWithString:(__bridge NSString*)uuid_string_ref];
    CFRelease(uuid_string_ref);
    return uuid;
}



+(NSString *)toUnix2ToTime:(NSString *)str{
    if([NSString isNULL:str]){
        return @"0000-00-00 00:00";
    }
    NSTimeInterval time=str.doubleValue+28800;//因为时差问题要加8小时 == 28800 sec
    NSDate*detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    NSLog(@"date:%@",[detaildate description]);
    //实例化一个NSDateFormatter对象
    NSDateFormatter*dateFormatter = [[NSDateFormatter alloc]init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *currentDateStr = [dateFormatter stringFromDate:detaildate];
    return currentDateStr;
}


-(BOOL)toBOOL{
    if([NSString isEmpty : self]) return NO;
    return [self boolValue];
}
-(int)toInt{
    if([NSString isEmpty : self]) return 0;
    return [self intValue];
}

-(int)toInt : (int)defaultValue{
    if([NSString isEmpty : self]) return defaultValue;
    return [self intValue];
}

-(long)toLng{
    
    if([NSString isEmpty : self]) return 0L;
    return (long)[self longLongValue];
}

-(float)toFloat{
    
    if([NSString isEmpty : self]) return 0.0F;
    return [self floatValue];
}

-(NSDate*)toDate{
    return [self toDate : @"yyyy-MM-dd HH:mm:ss"];
}

-(NSString*)unixToDateStr:(NSString *)formatterStr{
    NSTimeInterval _interval=self.doubleValue;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setLocale:[NSLocale currentLocale]];
    [_formatter setDateFormat:formatterStr];
    NSString *dateStr=[_formatter stringFromDate:date];
    return dateStr;
}

+(NSString*)toDateTimeString : (NSString*)str{
    if([NSString isEmpty:str]) return @"";
    NSDate* date = [str toDate];
    if(!date) return @"";
    return [date toDateTimeString];
    
}

+(NSString *)getHtml:(NSString *)str{
    return  [NSString stringWithFormat:@"<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"><style>img{max-width: 100%; width:auto; height:auto;}</style></head><body>%@</body></html>",str];
}

+(float) getHeight:(NSString *)value font:(UIFont *)font andWidth:(float)width
{
    CGSize sizeToFit = [value sizeWithFont:font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height+20;
}

+(CGSize)getCGSize:(NSString *)str font:(UIFont*)font{
    NSDictionary *attributes = @{NSFontAttributeName:font,};
    CGSize textSize = [str boundingRectWithSize:CGSizeMake(100, 100) options:NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    return textSize;
}


+(NSString*)toTime:(NSString *)timeStr formatterNow:(NSString *)formatterNow formatterChange:(NSString *)formatterChange{
    if([timeStr isEqualToString:@""]){
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:formatterNow];//设置源时间字符串的格式
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];//设置时区
    [formatter setTimeZone:timeZone];
    NSDate* date = [formatter dateFromString:timeStr];//将源时间字符串转化为NSDate
    NSDateFormatter* toformatter = [[NSDateFormatter alloc] init];
    [toformatter setDateStyle:NSDateFormatterMediumStyle];
    [toformatter setTimeStyle:NSDateFormatterShortStyle];
    [toformatter setDateFormat:formatterChange];//设置目标时间字符串的格式
    NSString *targetTime = [toformatter stringFromDate:date];//将时间转化成目标时间字符串
    return targetTime;
}

+(NSString*)toDateString : (NSString*)str{
    if([NSString isEmpty:str]) return @"";
    NSDate* date = [str toDate];
    if(!date) return @"";
    return [date toDateString];
    
}

+(NSString*)fromBOOL : (BOOL) value{
    return (value ? @"1" : @"0");
}

+(NSString*)fromInt : (int) value{
    return [NSString stringWithFormat: @"%d", value];
}

+(NSString*)fromLong : (long) value{
    return [NSString stringWithFormat: @"%ld", value];
}

+(NSString*)fromFloat : (float) value{
    return [NSString stringWithFormat: @"%f", value];
}


+(NSString*)fromFloat : (float)value digits:(int)fractionalDigits{
    return [NSString fromNumber:[NSNumber numberWithFloat:value] digits:fractionalDigits];
}


+(NSString*)fromNumber : (NSNumber*)value digits:(int)fractionalDigits{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setMaximumFractionDigits:fractionalDigits];
    NSString* r = [numberFormatter stringFromNumber: value];
    return r;
}

+(NSString*)fromNumber : (NSNumber*) value{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    NSString* r = [numberFormatter stringFromNumber:value];
    return r;
}



-(NSDate*)toDate : (NSString*)format{
    if([NSString isEmpty : self]) return nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString:self];
    return date;
}

+(NSString*)fromObject : (id) value{
    if(!value || value == [NSNull null]) return @"";
    return value;
}


-(int)compareTo : (NSString*)str{
    if([NSString isNULL : self] || [NSString isNULL : str]){
        if(![NSString isNULL : self]) return 1;
        if(![NSString isNULL : str]) return -1;
        return 0;
    }
    return (int)[self compare:str];
}


-(BOOL)equals : (NSString*)str{
    if([NSString isNULL : self] || [NSString isNULL : str]){
        if([NSString isNULL : self] && [NSString isNULL : str]) return YES;
        return NO;
    }
    
    return [self isEqualToString:str];
}

-(BOOL)equalsIgnoreCase : (NSString*)str{
    if([NSString isNULL : self] || [NSString isNULL : str]){
        if([NSString isNULL : self] && [NSString isNULL : str]) return YES;
        return NO;
    }
    return ([self compare:str options:NSCaseInsensitiveSearch | NSNumericSearch] == NSOrderedSame);
}

+(BOOL)isNULL : (NSString*)str{
    if(!str) return YES;
    if((NSNull*)str == [NSNull null]) return YES;
    if([str isEqualToString:@"NULL"]) return YES;
    if([str isEqualToString:@"(null)"]) return YES;
        if([str isEqualToString:@"<null>"]) return YES;
    if([str length] == 0) return YES;
    if([str isEqualToString:@"nil"]) return YES;
    if([str isEqualToString:@"false"]) return YES;
    if(str==0) return YES;
    if([str isEqualToString:@"0"]) return YES;
    return NO;
}

+(BOOL)isEmpty : (NSString*)str{
    if(!str) return YES;
    if((NSNull*)str == [NSNull null]) return YES;
    if([str length] == 0) return YES;
    return NO;
}

+(BOOL)isEmptyOfJOSN : (NSString*)str{
    if(str == nil || [str length] == 0) return YES;
    if([str isEqualToString:@"anyType{}"]) return YES;
    
    return NO;
}

+(BOOL)isAnyTypeOfJson : (NSString*)str{
    if(str == nil || [str length] == 0) return NO;
    if([str isEqualToString:@"anyType{}"]) return YES;
    
    return NO;
}

//判断是否为整形：
+(BOOL)isInt:(NSString*)str{
    NSScanner* scan = [NSScanner scannerWithString:str];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}
//判断是否为浮点形：
+(BOOL)isNumberic:(NSString*)str{
    NSScanner* scan = [NSScanner scannerWithString:str];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

//开始匹配区分大小写）
-(BOOL) startWith : (NSString*) prefix{
    if ( self != nil && prefix != nil ){
        if ( prefix.length > self.length ) {
            return NO;
        }
        if ([self hasPrefix:prefix]){
            return YES;
        }
    }
    return NO;
}

//结尾匹配（区分大小写）
-(BOOL) endWith : (NSString*) suffix {
    if ( self != nil && suffix != nil ){
        if ( [suffix length] > [self length] ) {
            return NO;
        }
        if ([self hasSuffix:suffix]){
            return YES;
        }
    }
    return NO;
}

-(NSString*)replace : (NSString*) oldStr : (NSString*) newStr{
    
    return [self stringByReplacingOccurrencesOfString:oldStr withString:newStr];
}

-(NSString*) subString : (int)start {
    int count = (int)self.length - start;
    return [self subString : start :count];
}

-(NSString*) subString : (int)start : (int)count{
    if(start + count <= self.length)
        return [self substringWithRange:NSMakeRange(start, count)];
    return nil;
}

-(NSArray*) split : (NSString*) schar{
    return [self componentsSeparatedByString:schar];
}


-(NSArray*) splitByChars : (NSCharacterSet*) schar{
    return [self componentsSeparatedByCharactersInSet:schar];
}


//向前查找字符串
-(NSInteger)indexOf:(NSString*)search{
    NSRange r=[self rangeOfString:search];
    if (r.location!=NSNotFound) {
        return r.location;
    }
    
    return -1;
}
//向后查找字符串
-(NSInteger)lastIndexOf:(NSString*)search{
    NSRange r=[self rangeOfString:search options:NSBackwardsSearch];
    if (r.location!=NSNotFound) {
        return r.location;
    }
    return -1;
}

-(NSInteger)indexOfChar:(unichar)searchChar{
    for(int i=0;i<[self length];i++){
        char c =[self characterAtIndex:i];
        if(c == searchChar)
            return i;
    }
    
    return -1;
}

-(NSInteger)indexOfChar:(unichar)searchChar : (int) startIndex{
    for(int i=startIndex;i<[self length];i++){
        unichar c =[self characterAtIndex:i];
        if(c == searchChar)
            return i;
    }
    
    return -1;
}

//去除字符串前后空格
-(NSString*)trim{
    if (self) {
        //whitespaceAndNewlineCharacterSet 去除前后空格与回车
        //whitespaceCharacterSet 除前后空格
        return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    return @"";
}

//读取资源图像的路径
+(NSString*)getResourceFileFullPath : (NSString*)fileName{
    NSString *imagePath = [[NSBundle mainBundle] resourcePath];
    //imagePath = [imagePath stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
    imagePath = [imagePath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    return [NSString stringWithFormat:@"file://%@/%@",imagePath,fileName];
}

//获取文本大小
-(CGSize)textSize:(UIFont*)f withWidth:(CGFloat)w{
    return  [self sizeWithFont:f constrainedToSize:CGSizeMake(w, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
}

/**
 *  计算文字的Size大小
 *
 *  @param text    文字内容
 *  @param font    文字字体
 *  @param maxSize 文字最大尺寸
 */
+(CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    if([text isKindOfClass:[NSNull class]]){
        text = @"";
    }
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}


- (NSString *) stringFromMD5{
    if([NSString isEmpty : self])
        return nil;
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (int)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    return outputString;
}

- (NSString *)SHA1Sum {
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    return [data SHA1Sum];
}


- (NSString *)SHA256Sum {
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    return [data SHA256Sum];
}

- (NSString *)AES256EncryptWithKey {
    return [self AES256EncryptWithKey : @"1qaz\\]'/"];
}
- (NSString *)AES256EncryptWithKey:(NSString *)key {
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptData = [data AES256EncryptWithKey:key];
    return [encryptData base64EncodedString];
    
}

- (NSString *)AES256DecryptWithKey {
    return [self AES256DecryptWithKey : @"1qaz\\]'/"];
}

- (NSString *)AES256DecryptWithKey:(NSString *)key {
    
    NSData *data = [NSData dataFromBase64String : self];
    NSData *decryptData =[data AES256DecryptWithKey:key];
    NSString *result = [[NSString alloc] initWithData:decryptData encoding : NSUTF8StringEncoding];
    return result;
}


//url字符串编码处理
-(NSString*)URLEncode{
    
    NSString *encodedString = ( NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                                     (CFStringRef)self,
                                                                                                     NULL,
                                                                                                     (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                     kCFStringEncodingUTF8));
    
    return encodedString;
    /***
     NSString * encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
     (CFStringRef)self,
     NULL,
     NULL,
     kCFStringEncodingUTF8);
     return encodedString;
     ***/
}
- (NSString *)URLEncodedParameterString {
    static CFStringRef toEscape = CFSTR(":/=,!$&'()*+;[]@#?");
    return ( NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                  ( CFStringRef)self,
                                                                                  NULL,
                                                                                  toEscape,
                                                                                  kCFStringEncodingUTF8));
}


- (NSString *)URLDecodedString {
    return ( NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
                                                                                                  ( CFStringRef)self,
                                                                                                  CFSTR(""),
                                                                                                  kCFStringEncodingUTF8));
}
- (BOOL) isEmail{
    
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject:[self lowercaseString]];
}

+ (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    long long vall;
    if([scan scanInt:&val]){
        return YES;
    }else if([scan scanLongLong:&vall]){
        return YES;
    }
    
    return NO;
}
- (BOOL) isPhone{
    
    NSString *phoneRegEx = @"(1[3-9][0-9])\\d{8}$";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    return [regExPredicate evaluateWithObject:[self lowercaseString]];
}

- (BOOL) isURLString{
    NSString *emailRegEx =@"^http(s)?://([\\w-]+.)+[\\w-]+(/[\\w-./?%&=]*)?$";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject:[self lowercaseString]];
}
- (NSString *) escapeHTML{
    NSMutableString *s = [NSMutableString string];
    
    int start = 0;
    int len = (int)[self length];
    NSCharacterSet *chs = [NSCharacterSet characterSetWithCharactersInString:@"<>&\""];
    
    while (start < len) {
        NSRange r = [self rangeOfCharacterFromSet:chs options:0 range:NSMakeRange(start, len-start)];
        if (r.location == NSNotFound) {
            [s appendString:[self substringFromIndex:start]];
            break;
        }
        
        if (start < r.location) {
            [s appendString:[self substringWithRange:NSMakeRange(start, r.location-start)]];
        }
        
        switch ([self characterAtIndex:r.location]) {
            case '<':
                [s appendString:@"&lt;"];
                break;
            case '>':
                [s appendString:@"&gt;"];
                break;
            case '"':
                [s appendString:@"&quot;"];
                break;
                //			case '…':
                //				[s appendString:@"&hellip;"];
                //				break;
            case '&':
                [s appendString:@"&amp;"];
                break;
        }
        
        start = (int)r.location + 1;
    }
    
    return s;
}

- (NSString *) unescapeHTML{
    NSMutableString *s = [NSMutableString string];
    NSMutableString *target = [self mutableCopy];
    NSCharacterSet *chs = [NSCharacterSet characterSetWithCharactersInString:@"&"];
    
    while ([target length] > 0) {
        NSRange r = [target rangeOfCharacterFromSet:chs];
        if (r.location == NSNotFound) {
            [s appendString:target];
            break;
        }
        
        if (r.location > 0) {
            [s appendString:[target substringToIndex:r.location]];
            [target deleteCharactersInRange:NSMakeRange(0, r.location)];
        }
        
        if ([target hasPrefix:@"&lt;"]) {
            [s appendString:@"<"];
            [target deleteCharactersInRange:NSMakeRange(0, 4)];
        } else if ([target hasPrefix:@"&gt;"]) {
            [s appendString:@">"];
            [target deleteCharactersInRange:NSMakeRange(0, 4)];
        } else if ([target hasPrefix:@"&quot;"]) {
            [s appendString:@"\""];
            [target deleteCharactersInRange:NSMakeRange(0, 6)];
        } else if ([target hasPrefix:@"&#39;"]) {
            [s appendString:@"'"];
            [target deleteCharactersInRange:NSMakeRange(0, 5)];
        } else if ([target hasPrefix:@"&amp;"]) {
            [s appendString:@"&"];
            [target deleteCharactersInRange:NSMakeRange(0, 5)];
        } else if ([target hasPrefix:@"&hellip;"]) {
            [s appendString:@"…"];
            [target deleteCharactersInRange:NSMakeRange(0, 8)];
        } else {
            [s appendString:@"&"];
            [target deleteCharactersInRange:NSMakeRange(0, 1)];
        }
    }
    
    return s;
}
#pragma mark - URL Escaping and Unescaping

- (NSString *)stringByEscapingForURLQuery {
    NSString *result = self;
    
    static CFStringRef leaveAlone = CFSTR(" ");
    static CFStringRef toEscape = CFSTR("\n\r:/=,!$&'()*+;[]@#?%");
    
    CFStringRef escapedStr = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, ( CFStringRef)self, leaveAlone,
                                                                     toEscape, kCFStringEncodingUTF8);
    
    if (escapedStr) {
        NSMutableString *mutable = [NSMutableString stringWithString:(__bridge  NSString *)escapedStr];
        CFRelease(escapedStr);
        
        [mutable replaceOccurrencesOfString:@" " withString:@"+" options:0 range:NSMakeRange(0, [mutable length])];
        result = mutable;
    }
    return result;
}


- (NSString *)stringByUnescapingFromURLQuery {
    NSString *deplussed = [self stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    return [deplussed stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (NSString*) stringByRemovingHTML{
    
    NSString *html = self;
    NSScanner *thescanner = [NSScanner scannerWithString:html];
    NSString *text = nil;
    
    while ([thescanner isAtEnd] == NO) {
        [thescanner scanUpToString:@"<" intoString:NULL];
        [thescanner scanUpToString:@">" intoString:&text];
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@" "];
    }
    return html;
}
- (BOOL) hasString:(NSString*)substring{
    return !([self rangeOfString:substring].location == NSNotFound);
    
}



- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size{
    CGSize resultSize = CGSizeZero;
    if (self.length <= 0) {
        return resultSize;
    }
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        resultSize = [self boundingRectWithSize:size
                                        options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                     attributes:@{NSFontAttributeName: font}
                                        context:nil].size;
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        resultSize = [self sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        //        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:self attributes:@{NSFontAttributeName:font}];
        //
        //        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        //        [paragraphStyle setLineSpacing:2.0];
        //
        //        [attributedStr addAttribute:NSParagraphStyleAttributeName
        //                              value:paragraphStyle
        //                              range:NSMakeRange(0, [self length])];
        //        resultSize = [attributedStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
        
#endif
    }
    resultSize = CGSizeMake(MIN(size.width, ceilf(resultSize.width)), MIN(size.height, ceilf(resultSize.height)));
    //    if ([self containsEmoji]) {
    //        resultSize.height += 10;
    //    }
    return resultSize;
}

- (CGFloat)getHeightWithFont:(UIFont *)font constrainedToSize:(CGSize)size{
    return [self getSizeWithFont:font constrainedToSize:size].height;
}
- (CGFloat)getWidthWithFont:(UIFont *)font constrainedToSize:(CGSize)size{
    return [self getSizeWithFont:font constrainedToSize:size].width;
}


#pragma mark Json

//返回数组
+(id)getJSONObject : (NSString*)jsonString{
    
    NSData *jsonData = [jsonString 	dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves  error:&error];
    //[jsonData release];
    if (jsonObject != nil && error == nil){
        return jsonObject;
    }
    else if(error){
        NSLog(@"jsonerror->%@",error);
    }
    
    return nil;
    
}

#pragma mark - Base64 Encoding

- (NSString *)base64EncodedString  {
    if ([self length] == 0) {
        return nil;
    }
    
    return [[self dataUsingEncoding:NSUTF8StringEncoding] base64EncodedString];
}

+ (NSString *)stringWithBase64String:(NSString *)base64String {
    return [[NSString alloc] initWithData:[NSData dataFromBase64String:base64String] encoding:NSUTF8StringEncoding];
}

//判断是否为手机号码
+ (NSString *)valiMobile:(NSString *)mobile{
    if (mobile.length < 11)
    {
        return @"手机号长度只能是11位";
    }else{
        /**
         * 移动号段正则表达式
         */
        NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
        /**
         * 联通号段正则表达式
         */
        NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
        /**
         * 电信号段正则表达式
         */
        NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";
        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
        BOOL isMatch1 = [pred1 evaluateWithObject:mobile];
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
        BOOL isMatch2 = [pred2 evaluateWithObject:mobile];
        NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
        BOOL isMatch3 = [pred3 evaluateWithObject:mobile];
        
        if (isMatch1 || isMatch2 || isMatch3) {
            return nil;
        }else{
            return @"请输入正确的电话号码";
        }
    }
    return nil;
}

+(NSString *)getHtmlData:(NSString *)str{
    NSString *html=@"<html><head><meta name=\'viewport\' content=\'width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=no\'><style>img{max-width: 100%;}</style></head><body>";
    NSString *html1=@"</body></html>";
    NSString *retusl=[NSString stringWithFormat:@"%@%@%@",html,str,html1];
    return retusl;
}

//url解析
+(NSString *)urlKeyValue:(NSString *)CS webaddress:(NSString *)webaddress;
{
    NSError *error;
    NSString *regTags=[[NSString alloc] initWithFormat:@"(^|&|\\?)+%@=+([^&]*)(&|$)",CS];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regTags
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    // 执行匹配的过程
    // NSString *webaddress=@"http://www.baidu.com/dd/adb.htm?adc=e12&xx=lkw&dalsjd=12";
    NSArray *matches = [regex matchesInString:webaddress
                                      options:0
                                        range:NSMakeRange(0, [webaddress length])];
    for (NSTextCheckingResult *match in matches) {
        //NSRange matchRange = [match range];
        //NSString *tagString = [webaddress substringWithRange:matchRange];  // 整个匹配串
        //        NSRange r1 = [match rangeAtIndex:1];
        //        if (!NSEqualRanges(r1, NSMakeRange(NSNotFound, 0))) {    // 由时分组1可能没有找到相应的匹配，用这种办法来判断
        //            //NSString *tagName = [webaddress substringWithRange:r1];  // 分组1所对应的串
        //            return @"";
        //        }
        
        NSString *tagValue = [webaddress substringWithRange:[match rangeAtIndex:2]];  // 分组2所对应的串
        //    NSLog(@"分组2所对应的串:%@\n",tagValue);
        return tagValue;
    }
    return @"";
}



@end
