//
//  AFURLSessionManager+AFNetworkingVideoDownload.h
//  519
//
//  Created by Macmini on 2019/7/17.
//  Copyright © 2019 519. All rights reserved.
//

#import "AFURLSessionManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface AFURLSessionManager (AFNetworkingVideoDownload)
- (NSURLSessionDownloadTask *)download:(NSString *)URLString
                              progress:(void (^)(NSProgress * _Nonnull))downloadProgress
                           destination:(NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
                     completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;

- (void)addDelegateForDownloadTask:(NSURLSessionDownloadTask *)downloadTask
                          progress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                       destination:(NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
                 completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;
@end

NS_ASSUME_NONNULL_END
