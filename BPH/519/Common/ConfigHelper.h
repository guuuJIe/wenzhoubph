
#import <Foundation/Foundation.h>

@interface ConfigHelper : NSObject


+(void)updateValue:(NSString*)itemName : (NSString*)itemValue;
+(void)updateIntValue:(NSString*)itemName : (int)itemValue;
+(void)updateObjValue:(NSString*)itemName : (id)itemValue;

+(id)getValue:(NSString*)itemName;
+(int)getIntValue:(NSString*)itemName;
+(NSString*)getStringValue:(NSString*)itemName;

/**
 字符串 ~> 两种不同 大小 不同 颜色
 
 @param string 所有文字
 @param range 范围
 @param rangeFont 范围内的字体
 @param otherFont 其他字体
 @param otherColor 其他的颜色
 @param rangeColor 范围内的颜色
 @return 两种不同 大小 不同 颜色
 */
+(NSAttributedString *)getSumString:(NSString *)string
                          withRange:(NSRange)range
                      withRangeFont:(UIFont *)rangeFont
                      withOtherFont:(UIFont *)otherFont
                 WithOtherFontColor:(UIColor *)otherColor
                     withRangeColor:(UIColor *)rangeColor;

/**
 拨打电话
 @param phonenum 电话号码
 */
+ (void)dailNumber:(NSString *)phonenum;

+ (void)hideTabBar:(UITabBarController *)tabbarcontroller;

+(BOOL)tabbarIsShow;

+ (void)showTabBar:(UITabBarController *)tabbarcontroller;
@end
