//
//  RequestData.m
//  519
//
//  Created by Macmini on 16/11/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RequestData.h"
#import "base64.h"
#import "AFURLSessionManager+AFNetworkingVideoDownload.h"

@implementation RequestData

+(void)requestDataUrl:(NSString *)urlString success:(void (^)(id))success failure:(void (^)(NSError *))failure{
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    // 设置超时时间
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    NSDictionary *dic=manager.requestSerializer.HTTPRequestHeaders;
    NSString *oldStr=[dic valueForKey:@"User-Agent"];
    if (![oldStr containsString:@"IOS_bOpIhUi90_"]) {
        NSString *newStr=[oldStr stringByAppendingString:@"IOS_bOpIhUi90_"];
        [manager.requestSerializer setValue:newStr forHTTPHeaderField:@"User-Agent"];
    }
    
    NSLog(@"url -- %@",urlString);
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    

    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *base64Decoded = [[NSString alloc]
                                   initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSData * jsondatastring = [Base64 decodeString:base64Decoded];
        
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsondatastring options:NSJSONReadingMutableLeaves error:nil];
//        NSLog(@"%@",dic);
        success(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(error);
    }];
    
   
}

+(void)requestDataOfUrl:(NSString *)urlstring success:(void (^)(NSDictionary * dic))success failure:(void (^)(NSError * error))failure{
    
    [RequestData requestDataUrl:urlstring success:success failure:failure];
    
}

+(void)downloadDataUrl:(NSString *)urlString success:(void (^)(id response,NSString *fliepath))success failure:(void (^)(NSError *))failure{
    
   AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager download:urlString progress:^(NSProgress * _Nonnull progress) {
        
       
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //相对路径
        NSURL *documentDirectoryURL = [[NSFileManager defaultManager]URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:targetPath create:NO error:nil];
        NSString *string = [NSString stringWithContentsOfURL:documentDirectoryURL encoding:NSUnicodeStringEncoding error:nil];
        NSLog(@"%@", string);
        //绝对路径
        return [documentDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
   
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nonnull filePath, NSError * _Nonnull error) {
        //获取已经下载路径和文件
        NSString *string = NSSearchPathForDirectoriesInDomains(NSUserDirectory, NSUserDomainMask, YES).lastObject;
        
        NSString *path = [string stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filePath]];
        
     
        if (error) {
            failure(error);
        }else{
            success(filePath,response.suggestedFilename);
        }
    }];
}
@end
