//
//  NewLoginVC.m
//  519
//
//  Created by Macmini on 2018/12/4.
//  Copyright © 2018年 519. All rights reserved.
//

#import "NewLoginVC.h"
#import "WXApiManager.h"
#import "AddInfoVC.h"
#import "GetVerifyCodeVC.h"

@interface NewLoginVC ()<WXApiManagerDelegate,UITextFieldDelegate>

@property(nonatomic,strong)UITextField *zhdlNameText;
@property(nonatomic,strong)UIView *zhdlLine;
@property(nonatomic,strong)UIButton *loginBtn;
@property(nonatomic,strong)UIButton *accountLoginBtn;
@end

@implementation NewLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    self.title = @"登录";
    [WXApiManager sharedManager].delegate = self;
    [self initBar];
    [self setupUI];
}

- (void)setupUI{
    self.view.backgroundColor = APPBGColor;
    [self.zhdlNameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(80);
        make.left.equalTo(15);
        make.right.equalTo(-15);
        make.height.mas_equalTo(30);
    }];
   
    
    self.zhdlLine = [UIView new];
    [self.view addSubview:self.zhdlLine];
    self.zhdlLine.backgroundColor=APPThreeColor;
    [self.zhdlLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.zhdlNameText);
        make.top.equalTo(self.zhdlNameText.mas_bottom).offset(5);
        make.height.mas_equalTo(1);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(35);
        make.right.equalTo(-35);
        make.height.equalTo(50);
        make.top.equalTo(self.zhdlLine.mas_bottom).offset(45);
    }];
    
    [self.accountLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.mas_equalTo(-BottomAreaHeight-20);
    }];
    
    if (![WXApi isWXAppInstalled]) {
        [self.loginBtn setTitle:@"获取验证码" forState:0];
        self.loginBtn.backgroundColor = RGB(255, 142, 2);
        [self.loginBtn setImage:[UIImage imageNamed:@""] forState:0];
       
    }else{
        self.loginBtn.backgroundColor = RGB(2, 142, 45);
        [self.loginBtn setTitle:@"微信登入" forState:0];
        [self.loginBtn setImage:[UIImage imageNamed:@"wechat"] forState:0];
        [self.zhdlNameText addTarget:self action:@selector(textField1TextChange:) forControlEvents:UIControlEventEditingChanged];
    }
    
}

-(void)textField1TextChange:(UITextField *)textField{
    NSLog(@"textField1 - 输入框内容改变,当前内容为: %@",textField.text);
    
    if (textField.text.length >= 11) {
        self.zhdlNameText.text = [textField.text substringToIndex:11];
        self.loginBtn.backgroundColor = RGB(255, 142, 2);
        [self.loginBtn setTitle:@"获取验证码" forState:0];
        [self.loginBtn setImage:[UIImage imageNamed:@""] forState:0];
       
    }else if(textField.text.length < 11){
        
        self.loginBtn.backgroundColor = RGB(2, 142, 45);
        [self.loginBtn setTitle:@"微信登入" forState:0];
        
        [self.loginBtn setImage:[UIImage imageNamed:@"wechat"] forState:0];
    }
}

- (void)login{
    if ([self.loginBtn.currentTitle isEqualToString:@"微信登入"]) {
        [self umLogin:UMShareToWechatSession];
    }else if ([self.loginBtn.currentTitle isEqualToString:@"获取验证码"]){
        //获取验证码
//        NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=2",FONPort,self.zhdlNameText.text];
//        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
//
//            if ([dic[@"status"] isEqual:@1]) {
//                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
                 GetVerifyCodeVC *VC = [[GetVerifyCodeVC alloc] init];
                 VC.tel = self.zhdlNameText.text;
                 [self tabHidePushVC:VC];
//            }else{
//                [MBProgressHUD showError:dic[@"info"] toView:self.view];
//            }
//            
//        } failure:^(NSError *error) {
//            NSLog(@"%@",error);
//        }];
//        
    }
}

#pragma mark----微信登入------
-(void)umLogin:(NSString*)type{
    
    if (![WXApi isWXAppInstalled]) {
        [MBProgressHUD showError:@"你当前未安装微信" toView:self.view];
        return;
    }
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"123" ;
    req.openID = WX_APPID;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApiManager sharedManager].delegate = self;
    [WXApi sendReq:req];
    
    
}
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    [self getWeiXinOpenId:response.code];
}
//通过code获取access_token，openid，unionid
- (void)getWeiXinOpenId:(NSString *)code{
    [MBProgressHUD showHUD];
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WX_APPID,WX_SSECRET,code];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data){
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSString *openID = dic[@"openid"];
                NSString * access_token=  dic[@"access_token"];
                [self requestUserdata:access_token withopenid:openID];
            }
        });
    });
}
-(void)requestUserdata:(NSString *)access_token withopenid:(NSString *)openid{
    
    NSString * userdataurl = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",access_token,openid];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:userdataurl];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data){
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                [self requestIsLogin:dic[@"unionid"] nickname:dic[@"nickname"] withheadimgurl:dic[@"headimgurl"]];
                
            }
        });
    });
}
//判断是否已经绑定手机号
-(void)requestIsLogin:(NSString *)unionid nickname:(NSString *)nickname  withheadimgurl:(NSString *)headimgurl{
    NSString * url = [NSString stringWithFormat:@"%@ctl=synclogin&act=weixin&unionid=%@",FONPort,unionid];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        
        
        NSString * status = [NSString stringWithFormat:@"%@",dic[@"status"]];
        //0是去补充信息 1是直接登录 -1不能登录提示错误信息
        if ([status isEqualToString:@"0"]) {
            [MBProgressHUD dissmiss];
            AddInfoVC * vc = [AddInfoVC new];
            vc.nickIcon = headimgurl;
            vc.userName = nickname;
            vc.unionid = unionid;
            [self tabHidePushVC:vc];
        }else if([status isEqualToString:@"-1"]){
             [MBProgressHUD dissmiss];
             [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }else{
            [MBProgressHUD dissmiss];
            
            [[NSUserDefaults standardUserDefaults]setObject:nickname forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults]setObject:headimgurl forKey:@"headimgurl"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"mobile"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
//            [self popVC];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)accountLogin{
    [self tabHidePushVC:[[LoginVC alloc]init]];
}


- (UITextField *)zhdlNameText{
    if (!_zhdlNameText) {
        self.zhdlNameText = [[UITextField alloc] init];
        self.zhdlNameText.placeholder = @"输入手机号码登入";
        self.zhdlNameText.textColor=APPFourColor;
        self.zhdlNameText.tintColor=APPColor;
        self.zhdlNameText.delegate = self;
        //输入框光标的颜色为白色
        self.zhdlNameText.tintColor = [UIColor blackColor];
        self.zhdlNameText.keyboardType = UIKeyboardTypeNumberPad;
//        [self.zhdlNameText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
        self.zhdlNameText.font=[UIFont boldSystemFontOfSize:30];
//        [self.zhdlNameText setValue:[UIFont boldSystemFontOfSize:28] forKeyPath:@"_placeholderLabel.font"];
        [self.view addSubview:self.zhdlNameText];
    }
    return _zhdlNameText;
}

- (UIButton *)loginBtn{
    if (!_loginBtn) {
        _loginBtn = [UIButton new];
        [_loginBtn setTitle:@"微信登入" forState:0];
        _loginBtn.layer.cornerRadius = 10;
        _loginBtn.layer.masksToBounds = true;
        _loginBtn.backgroundColor = RGB(2, 142, 45);
        [_loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.loginBtn];
        [_loginBtn.titleLabel setFont:[UIFont systemFontOfSize:18]];
        [_loginBtn setImage:[UIImage imageNamed:@"wechat"] forState:0];
        _loginBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        _loginBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    }
    return _loginBtn;
}

- (UIButton *)accountLoginBtn{
    if (!_accountLoginBtn) {
        _accountLoginBtn = [UIButton new];
        [_accountLoginBtn setTitle:@"账号登录" forState:0];
        [_accountLoginBtn setTitleColor:APPColor forState:0];
        [_accountLoginBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_accountLoginBtn addTarget:self action:@selector(accountLogin) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_accountLoginBtn];
    }
    return _accountLoginBtn;
}


@end
