//
//  GetVerifyCodeVC.m
//  519
//
//  Created by Macmini on 2018/12/4.
//  Copyright © 2018年 519. All rights reserved.
//

#import "GetVerifyCodeVC.h"
#import "YN_PassWordView.h"
#import "TelView.h"
@interface GetVerifyCodeVC ()
@property(nonatomic,strong)YN_PassWordView *passView;
@property(nonatomic,strong)UIButton *dxdlSendSMSBtn;
@property(nonatomic,assign)int sendSMSTimeCount;
@property(nonatomic,strong)NSTimer *sendSMSTime;
@property(nonatomic,strong)NSString *code;
@property(nonatomic,strong)TelView *telView;
@end

@implementation GetVerifyCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"获取验证码";
    self.view.backgroundColor = APPBGColor;
    [self setupUI];
    [self sendSMS];
    
}

-(void)setupUI{
    
    self.telView = [[TelView alloc] init];
    [self.view addSubview:self.telView];
    self.telView.tellbl.text = self.tel;
    WeakSelf(self);
    self.telView.editBlock = ^{
        [weakself popVC];
    };
    [self.telView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(15);
        make.centerX.equalTo(self.view);
        make.width.equalTo(145);
        
    }];
    
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(35, 100, Screen_Width-35*2, 200/6);
//    self.passView.backgroundColor= APPBGColor;
    self.passView.showType = 5;//五种样式
    self.passView.num = 4;//框框个数
    [self.passView show];
    [self.view addSubview:self.passView];
    [self.passView.textF becomeFirstResponder];
  
    self.passView.textBlock = ^(NSString *str) {
        NSLog(@"str----%@",str);
        NSString *urlString  = [NSString stringWithFormat:@"%@&ctl=user&act=dophlogin_auto&mobile=%@&sms_verify=%@",FONPort,weakself.tel,str];
        [weakself requestDataOfData:urlString withUserEmail:weakself.tel];
        self->_code = str;
        
    };
    self.dxdlSendSMSBtn=[UIButton new];
    [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:APPFourColor forState:UIControlStateDisabled];
    self.dxdlSendSMSBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.dxdlSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    self.dxdlSendSMSBtn.backgroundColor = RGB(247, 43, 44);
    self.dxdlSendSMSBtn.layer.cornerRadius = 10;
    
    [self.view addSubview:self.dxdlSendSMSBtn];
    
    [self.dxdlSendSMSBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passView.mas_bottom).offset(50);
        make.left.equalTo(35);
        make.right.equalTo(-35);
        make.height.equalTo(50);
    }];

}

-(void)requestDataOfData:(NSString *)urlString withUserEmail:(NSString *)email{
    [RequestData requestDataOfUrl:urlString success:^(NSDictionary *dic) {
        
        if([dic[@"status"] isEqual:@1]){
            [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_name"] forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"mobile"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

- (void)sendSMS{
    if(self.sendSMSTimeCount!=0){
        return;
    }
    self.sendSMSTimeCount=60;
//    self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
   
        NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=4",FONPort,self.self.tel];
        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
            
            if ([dic[@"status"] isEqual:@1]) {
                self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            }else{
//                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
   
}
-(void)changeSMSTime{
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.dxdlSendSMSBtn.backgroundColor = RGB(247, 43, 44);
        self.dxdlSendSMSBtn.enabled = true;
    }else{
       self.dxdlSendSMSBtn.backgroundColor = RGB(224, 224, 224);
        self.dxdlSendSMSBtn.enabled = false;
        [self.dxdlSendSMSBtn setTitle:[NSString stringWithFormat:@"重新发送(%is)",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
