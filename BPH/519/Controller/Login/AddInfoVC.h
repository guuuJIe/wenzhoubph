//
//  AddInfoVC.h
//  519
//
//  Created by Macmini on 17/1/7.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@interface AddInfoVC : BaseVC

@property(nonatomic,copy)NSString * userName;
@property(nonatomic,copy)NSString * nickIcon;
@property(nonatomic,copy)NSString * unionid;
@end
