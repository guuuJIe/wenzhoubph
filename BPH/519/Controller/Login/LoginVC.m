//  LoginVC.m
//  519
//
//  Created by 陈 on 16/9/7.
//  Copyright © 2016年 519. All rights reserved.
//

#import "LoginVC.h"
#import "PasswordVC.h"
#import "RegisterVC.h"
#import "AddInfoVC.h"
#import "WXApiManager.h"
@interface LoginVC()<WXApiManagerDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;
@property(nonatomic,strong)UISegmentedControl *segment;
@property(nonatomic,strong)UIView *zhdlView;
@property(nonatomic,strong)UIImageView *zhdlNameImg;
@property(nonatomic,strong)UITextField *zhdlNameText;
@property(nonatomic,strong)UIView *zhdlLine;
@property(nonatomic,strong)UIImageView *zhdlPwdImg;
@property(nonatomic,strong)UITextField *zhdlPwdText;
@property(nonatomic,strong)UIButton *zhdlShowPwdBtn;

@property(nonatomic,strong)UIView *dxdlView;
@property(nonatomic,strong)UIImageView *dxdlNameImg;
@property(nonatomic,strong)UITextField *dxdlNameText;
@property(nonatomic,strong)UIView *dxdlLine;
@property(nonatomic,strong)UIImageView *dxdlPwdImg;
@property(nonatomic,strong)UITextField *dxdlPwdText;
@property(nonatomic,strong)UIButton *dxdlSendSMSBtn;
@property(nonatomic,assign)int sendSMSTimeCount;
@property(nonatomic,strong)NSTimer *sendSMSTime;

@property(nonatomic,strong)UIButton *wjmmBtn;

@property(nonatomic,strong)UIButton *dlBtn;
@property(nonatomic,strong)UIButton *zcBtn;

@property(nonatomic,strong)UIView * thirdView;
@property(nonatomic,strong)UIButton *wxBtn;

@end

@implementation LoginVC

-(void)viewDidLoad{
    [super viewDidLoad];
    self.title = @"登录";
    [WXApiManager sharedManager].delegate = self;
    [self initView];
    [self initBar];
}


-(void)initView{
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, Screen_Height-self.barView.frame.size.height-self.statusBarView.frame.size.height)];
    self.scrollView.backgroundColor=APPBGColor;
    
//    self.segment=[[UISegmentedControl alloc]initWithItems:[[NSArray alloc]initWithObjects:@"帐号登录",@"短信登录", nil]];
//    self.segment.backgroundColor=[UIColor clearColor];
//    self.segment.frame=CGRectMake(Default_Space, Default_Space*2, Screen_Width-Default_Space*2, 35);
//    self.segment.tintColor=APPColor;
//    self.segment.selectedSegmentIndex = 0;
//    [self.segment setApportionsSegmentWidthsByContent:NO];
//    [self. segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor clearColor],UITextAttributeTextShadowColor,APPColor,UITextAttributeTextColor,[UIFont systemFontOfSize:FONT_SIZE_L],UITextAttributeFont ,nil] forState:UIControlStateNormal];
//    [self. segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: APPColor,UITextAttributeTextShadowColor,[UIColor whiteColor],UITextAttributeTextColor,[UIFont systemFontOfSize:FONT_SIZE_L],UITextAttributeFont ,nil] forState:UIControlStateSelected];
//    [self.segment addTarget:self action:@selector(segmentOnSelectItemClick:) forControlEvents:UIControlEventValueChanged];
    
    self.zhdlView=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space*2, self.scrollView.frame.size.width-Default_Space*2, 100)];
    self.zhdlView.backgroundColor=[UIColor whiteColor];
    self.zhdlView.layer.cornerRadius=5;
    self.zhdlView.layer.masksToBounds=YES;
    
    self.zhdlNameImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 15, 20, 20)];
    self.zhdlNameImg.image=[UIImage imageNamed:@"user_login_name_icon"];
    
    self.zhdlNameText=[[UITextField alloc]initWithFrame:CGRectMake(self.zhdlNameImg.frame.origin.x+self.zhdlNameImg.frame.size.width+Default_Space, Default_Space, self.zhdlView.frame.size.width-Default_Space-self.zhdlNameImg.frame.size.width-Default_Space-Default_Space, 30)];
    self.zhdlNameText.placeholder=@"手机号";
    //[self.zhdlNameText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.zhdlNameText.textColor=APPFourColor;
    self.zhdlNameText.tintColor=APPColor;
    self.zhdlNameText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.zhdlPwdImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 65, 20, 20)];
    self.zhdlPwdImg.image=[UIImage imageNamed:@"user_login_pwd_icon"];
    
    self.zhdlPwdText=[[UITextField alloc]initWithFrame:CGRectMake(self.zhdlPwdImg.frame.origin.x+self.zhdlPwdImg.frame.size.width+Default_Space, 60, self.zhdlView.frame.size.width-Default_Space-self.zhdlPwdImg.frame.size.width-Default_Space-Default_Space-20-Default_Space, 30)];
    self.zhdlPwdText.placeholder=@"密码";
    //[self.zhdlPwdText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.zhdlPwdText.textColor=APPFourColor;
    self.zhdlPwdText.tintColor=APPColor;
    self.zhdlPwdText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.zhdlPwdText.secureTextEntry=YES;
    
    self.zhdlShowPwdBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.zhdlPwdText.frame.size.width+self.zhdlPwdText.frame.origin.x+Default_Space,65, 20, 20)];
    [self.zhdlShowPwdBtn setBackgroundImage:[UIImage imageNamed:@"user_login_show_0_icon"] forState:UIControlStateNormal];
    [self.zhdlShowPwdBtn setBackgroundImage:[UIImage imageNamed:@"user_login_show_1_icon"] forState:UIControlStateSelected];
    self.zhdlShowPwdBtn.selected=NO;
    [self.zhdlShowPwdBtn addTarget:self action:@selector(showPwd) forControlEvents:UIControlEventTouchUpInside];
    
    self.zhdlLine=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, self.zhdlView.frame.size.height/2, self.zhdlView.frame.size.width-Default_Space*2, Default_Line)];
    self.zhdlLine.backgroundColor=APPBGColor;
    
    [self.zhdlView addSubview:self.zhdlNameImg];
    [self.zhdlView addSubview:self.zhdlNameText];
    [self.zhdlView addSubview:self.zhdlLine];
    [self.zhdlView addSubview:self.zhdlPwdImg];
    [self.zhdlView addSubview:self.zhdlPwdText];
    [self.zhdlView addSubview:self.zhdlShowPwdBtn];
    
    self.dxdlView=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, self.segment.frame.origin.y+self.segment.frame.size.height+Default_Space*2, self.scrollView.frame.size.width-Default_Space*2, 100)];
    self.dxdlView.backgroundColor=[UIColor whiteColor];
    self.dxdlView.layer.cornerRadius=5;
    self.dxdlView.layer.masksToBounds=YES;
    self.dxdlView.hidden=YES;
    
    self.dxdlNameImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 15, 20, 20)];
    self.dxdlNameImg.image=[UIImage imageNamed:@"user_login_name_icon"];
    
    self.dxdlNameText=[[UITextField alloc]initWithFrame:CGRectMake(self.dxdlNameImg.frame.origin.x+self.dxdlNameImg.frame.size.width+Default_Space, Default_Space, self.dxdlView.frame.size.width-Default_Space-self.dxdlNameImg.frame.size.width-Default_Space-Default_Space, 30)];
    self.dxdlNameText.placeholder=@"手机号";
//    [self.dxdlNameText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.dxdlNameText.textColor=APPFourColor;
    self.dxdlNameText.tintColor=APPColor;
    self.dxdlNameText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.dxdlPwdImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 65, 20, 20)];
    self.dxdlPwdImg.image=[UIImage imageNamed:@"user_login_sms_icon"];
    
    self.dxdlPwdText=[[UITextField alloc]initWithFrame:CGRectMake(self.dxdlPwdImg.frame.origin.x+self.dxdlPwdImg.frame.size.width+Default_Space, 60, self.dxdlView.frame.size.width-Default_Space-self.dxdlPwdImg.frame.size.width-Default_Space-Default_Space-80-Default_Space, 30)];
    self.dxdlPwdText.placeholder=@"验证码";
//    [self.dxdlPwdText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.dxdlPwdText.textColor=APPFourColor;
    self.dxdlPwdText.tintColor=APPColor;
    self.dxdlPwdText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.dxdlSendSMSBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.dxdlPwdText.frame.size.width+self.dxdlPwdText.frame.origin.x+Default_Space,65, 80, 20)];
    [self.dxdlSendSMSBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:APPFourColor forState:UIControlStateNormal];
    self.dxdlSendSMSBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.dxdlSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    
    self.dxdlLine=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, self.dxdlView.frame.size.height/2, self.dxdlView.frame.size.width-Default_Space*2, Default_Line)];
    self.dxdlLine.backgroundColor=APPBGColor;
    
    [self.dxdlView addSubview:self.dxdlNameImg];
    [self.dxdlView addSubview:self.dxdlNameText];
    [self.dxdlView addSubview:self.dxdlLine];
    [self.dxdlView addSubview:self.dxdlPwdImg];
    [self.dxdlView addSubview:self.dxdlPwdText];
    [self.dxdlView addSubview:self.dxdlSendSMSBtn];
    
    
    NSString *wjmmStr=@"忘记密码";
    UIFont *wjmmFont=[UIFont systemFontOfSize:FONT_SIZE_L];
    CGFloat wjmmWidth=[NSString sizeWithText:wjmmStr font:wjmmFont maxSize:Max_Size].width;
    self.wjmmBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.scrollView.frame.size.width-Default_Space-wjmmWidth, self.zhdlView.frame.size.height+self.zhdlView.frame.origin.y+Default_Space*2, wjmmWidth, 20)];
    [self.wjmmBtn setTitle:wjmmStr forState:UIControlStateNormal];
    [self.wjmmBtn setTitleColor:six9 forState:UIControlStateNormal];
    self.wjmmBtn.titleLabel.font=wjmmFont;
    [self.wjmmBtn addTarget:self action:@selector(goWJMM) forControlEvents:UIControlEventTouchUpInside];
    
    self.dlBtn=[[UIButton alloc] initWithFrame:CGRectMake(Default_Space, self.wjmmBtn.frame.origin.y+self.wjmmBtn.frame.size.height+Default_Space*2, self.scrollView.frame.size.width-Default_Space*2,40)];
    [self.dlBtn setTitle:@"登录" forState:UIControlStateNormal];
    self.dlBtn.backgroundColor=APPColor;
    [self.dlBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.dlBtn.layer.cornerRadius=5;
    self.dlBtn.layer.masksToBounds=YES;
    self.dlBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.dlBtn addTarget:self action:@selector(goDL) forControlEvents:UIControlEventTouchUpInside];
    
    self.zcBtn=[[UIButton alloc] initWithFrame:CGRectMake(Default_Space, self.dlBtn.frame.origin.y+self.dlBtn.frame.size.height+Default_Space*2, self.scrollView.frame.size.width-Default_Space*2,40)];
    [self.zcBtn setTitle:@"注册" forState:UIControlStateNormal];
    self.zcBtn.backgroundColor=[UIColor whiteColor];
    [self.zcBtn setTitleColor:APPFourColor forState:UIControlStateNormal];
    self.zcBtn.layer.cornerRadius=5;
    self.zcBtn.layer.masksToBounds=YES;
    self.zcBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.zcBtn addTarget:self action:@selector(goZC) forControlEvents:UIControlEventTouchUpInside];
    
    self.thirdView = [[UIView alloc]initWithFrame:CGRectMake(0, self.zcBtn.frame.origin.y+self.zcBtn.frame.size.height+Default_Space*3, Screen_Width, 100)];
    
    
    
//    UIView *dsfdlLeftLine=[[UIView alloc]initWithFrame:CGRectMake(0, Default_Space*3, (self.thirdView.frame.size.width-100)/2, Default_Line)];
//    dsfdlLeftLine.backgroundColor=APPThreeColor;
//    
//    UIView *dsfdlRightLine=[[UIView alloc]initWithFrame:CGRectMake((self.thirdView.frame.size.width-100)/2+100, Default_Space*3, (self.scrollView.frame.size.width-100)/2, Default_Line)];
//    dsfdlRightLine.backgroundColor=APPThreeColor;
//    
//    UILabel *dsfdlLabel=[[UILabel alloc]initWithFrame:CGRectMake(dsfdlLeftLine.frame.size.width, Default_Space*2, 100, 20)];
//    dsfdlLabel.text=@"第三方登录";
//    dsfdlLabel.textAlignment=NSTextAlignmentCenter;
//    dsfdlLabel.textColor=APPThreeColor;
//    dsfdlLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
//    
//    
//    self.wxBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width/2-25, dsfdlLabel.frame.origin.y+dsfdlLabel.frame.size.height+Default_Space*2, 50, 50)];
//    [self.wxBtn setBackgroundImage:[UIImage imageNamed:@"user_login_wx_icon"] forState:UIControlStateNormal];
//        [self.wxBtn addTarget:self action:@selector(wxLogin) forControlEvents:UIControlEventTouchUpInside];
//    [self.thirdView addSubview:dsfdlLeftLine];
//    [self.thirdView addSubview:dsfdlLabel];
//    [self.thirdView addSubview:dsfdlRightLine];
//    [self.thirdView addSubview:self.wxBtn];
    
    
    [self.scrollView addSubview:self.segment];
    [self.scrollView addSubview:self.zhdlView];
    [self.scrollView addSubview:self.dxdlView];
    [self.scrollView addSubview:self.wjmmBtn];
    [self.scrollView addSubview:self.dlBtn];
    [self.scrollView addSubview:self.zcBtn];
    [self.scrollView addSubview:self.thirdView];
    
    
    
    [self.view addSubview: self.scrollView];
    
    [self showData];
}

-(void)showData{
    BOOL isWx = [WXApi isWXAppInstalled];
    if (!isWx) {
        self.thirdView.hidden = YES;
        
    }
}

#pragma mark 忘记密码
-(void)goWJMM{

    PasswordVC *vc=[[PasswordVC alloc]init];
    [self tabHidePushVC:vc];
}

#pragma mark 登录
-(void)goDL{
    NSLog(@"%ld",(long)self.segment.selectedSegmentIndex);
    NSString * urlString;
    if (self.segment.selectedSegmentIndex == 0 && self.zhdlNameText.text.length > 0 && self.zhdlPwdText.text.length > 0 ) {
        urlString = [NSString stringWithFormat:@"%@&ctl=user&act=dologin&user_key=%@&user_pwd=%@",FONPort,self.zhdlNameText.text,self.zhdlPwdText.text];
        [self requestDataOfData:urlString withUserEmail:self.zhdlNameText.text];
    }
    if (self.segment.selectedSegmentIndex == 1 && self.dxdlNameText.text.length > 0 && self.dxdlPwdText.text.length > 0 ) {
        urlString = [NSString stringWithFormat:@"%@&ctl=user&act=dophlogin&mobile=%@&sms_verify=%@",FONPort,self.dxdlNameText.text,self.dxdlPwdText.text];
        [self requestDataOfData:urlString withUserEmail:self.dxdlNameText.text];
    }
}

-(void)requestDataOfData:(NSString *)urlString withUserEmail:(NSString *)email{
    [RequestData requestDataOfUrl:urlString success:^(NSDictionary *dic) {
        
        if([dic[@"status"] isEqual:@1]){
            [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_name"] forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"mobile"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}
#pragma mark 注册
-(void)goZC{
    RegisterVC *vc=[[RegisterVC alloc]init];
    [self tabHidePushVC:vc];
}

#pragma mark 第三方登录
-(void)wxLogin{

    [self umLogin:UMShareToWechatSession];
}


-(void)umLogin:(NSString*)type{
    
    
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"123" ;
    req.openID = WX_APPID;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApiManager sharedManager].delegate = self;
    [WXApi sendReq:req];
    
    
}
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    [self getWeiXinOpenId:response.code];
}
//通过code获取access_token，openid，unionid
- (void)getWeiXinOpenId:(NSString *)code{
    [MBProgressHUD showHUD];
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WX_APPID,WX_SSECRET,code];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data){
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSString *openID = dic[@"openid"];
                NSString * access_token=  dic[@"access_token"];
                [self requestUserdata:access_token withopenid:openID];
            }
        });
    });
}
-(void)requestUserdata:(NSString *)access_token withopenid:(NSString *)openid{
    
    NSString * userdataurl = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",access_token,openid];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:userdataurl];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data){
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                [self requestIsLogin:dic[@"unionid"] nickname:dic[@"nickname"] withheadimgurl:dic[@"headimgurl"]];
                
            }
        });
    });
}
//判断是否已经绑定手机号
-(void)requestIsLogin:(NSString *)unionid nickname:(NSString *)nickname  withheadimgurl:(NSString *)headimgurl{
    NSString * url = [NSString stringWithFormat:@"%@ctl=synclogin&act=weixin&unionid=%@",FONPort,unionid];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        
        
        NSString * status = [NSString stringWithFormat:@"%@",dic[@"status"]];
        if (![status isEqualToString:@"1"]) {
            [MBProgressHUD dissmiss];
            AddInfoVC * vc = [AddInfoVC new];
            vc.nickIcon = headimgurl;
            vc.userName = nickname;
            vc.unionid = unionid;
            [self tabHidePushVC:vc];
        }else{
            [MBProgressHUD dissmiss];
            
            [[NSUserDefaults standardUserDefaults]setObject:nickname forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults]setObject:headimgurl forKey:@"headimgurl"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"mobile"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
            [self popVC];
        }
        
    } failure:^(NSError *error) {
        
    }];
}



#pragma mark 发送验证码
-(void)sendSMS{
    if(self.sendSMSTimeCount!=0){
        return;
    }
    self.sendSMSTimeCount=60;
    
    if (self.dxdlNameText.text.length>0) {
        NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=2",FONPort,self.dxdlNameText.text];
        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
            
            if ([dic[@"status"] isEqual:@1]) {
                self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
    
    
}

-(void)changeSMSTime{
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.dxdlSendSMSBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    }else{
        [self.dxdlSendSMSBtn setTitle:[NSString stringWithFormat:@"%i秒",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
}

#pragma mark 显示密码
-(void)showPwd{
    if(!self.zhdlShowPwdBtn.selected){
        self.zhdlPwdText.secureTextEntry=NO;
        self.zhdlShowPwdBtn.selected=YES;
    }else{
        self.zhdlPwdText.secureTextEntry=YES;
        self.zhdlShowPwdBtn.selected=NO;
    }
    
}

#pragma  mark on click
//-(void)segmentOnSelectItemClick:(UISegmentedControl *)seg{
//    if(seg.selectedSegmentIndex==0){
//        // 帐号
//        self.zhdlView.hidden=NO;
//        self.dxdlView.hidden=YES;
//        
//    }else{
//        //短信
//        self.zhdlView.hidden=YES;
//        self.dxdlView.hidden=NO;
//        
//    }
//}


@end
