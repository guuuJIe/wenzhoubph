//
//  RegisterVC.m
//  519
//
//  Created by 陈 on 16/9/7.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RegisterVC.h"

@interface RegisterVC()
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;

@property(nonatomic,strong)UIView *pwdView;
@property(nonatomic,strong)UIImageView *pwdNameImg;
@property(nonatomic,strong)UITextField *pwdNameText;

@property(nonatomic,strong)UIImageView *pwdSMSImg;
@property(nonatomic,strong)UITextField *pwdSMSText;
@property(nonatomic,strong)UIButton *pwdSendSMSBtn;
@property(nonatomic,assign)int sendSMSTimeCount;
@property(nonatomic,strong)NSTimer *sendSMSTime;

@property(nonatomic,strong)UIImageView *pwdPwdImg;
@property(nonatomic,strong)UITextField *pwdPwdText;
@property(nonatomic,strong)UIButton *pwdShowPwdBtn;

@property(nonatomic,strong)UIButton *submitBtn;


@end

@implementation RegisterVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"注册";
    [self initView];
    [self initBar];
}


-(void)initView{
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, Screen_Height-self.barView.frame.size.height-self.statusBarView.frame.size.height)];
    self.scrollView.backgroundColor=APPBGColor;
    
    self.pwdView=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space*2, self.scrollView.frame.size.width-Default_Space*2, 150)];
    self.pwdView.backgroundColor=[UIColor whiteColor];
    self.pwdView.layer.cornerRadius=5;
    self.pwdView.layer.masksToBounds=YES;
    
    self.pwdNameImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 15, 20, 20)];
    self.pwdNameImg.image=[UIImage imageNamed:@"user_login_name_icon"];
    
    self.pwdNameText=[[UITextField alloc]initWithFrame:CGRectMake(self.pwdNameImg.frame.origin.x+self.pwdNameImg.frame.size.width+Default_Space, Default_Space, self.pwdView.frame.size.width-Default_Space-self.pwdNameImg.frame.size.width-Default_Space-Default_Space, 30)];
    self.pwdNameText.placeholder=@"手机号";
    [self.pwdNameText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.pwdNameText.textColor=APPFourColor;
    self.pwdNameText.tintColor=APPColor;
    self.pwdNameText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    UIView *pwdLine=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, 50, self.pwdView.frame.size.width-Default_Space*2, Default_Line)];
    pwdLine.backgroundColor=APPBGColor;
    
    self.pwdSMSImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 65, 20, 20)];
    self.pwdSMSImg.image=[UIImage imageNamed:@"user_login_sms_icon"];
    
    self.pwdSMSText=[[UITextField alloc]initWithFrame:CGRectMake(self.pwdSMSImg.frame.origin.x+self.pwdSMSImg.frame.size.width+Default_Space, 60, self.pwdView.frame.size.width-Default_Space-self.pwdSMSImg.frame.size.width-Default_Space-Default_Space-80-Default_Space, 30)];
    self.pwdSMSText.placeholder=@"验证码";
    [self.pwdSMSText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.pwdSMSText.textColor=APPFourColor;
    self.pwdSMSText.tintColor=APPColor;
    self.pwdSMSText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.pwdSendSMSBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.pwdSMSText.frame.size.width+self.pwdSMSText.frame.origin.x+Default_Space,65, 80, 20)];
    [self.pwdSendSMSBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.pwdSendSMSBtn setTitleColor:APPFourColor forState:UIControlStateNormal];
    self.pwdSendSMSBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.pwdSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *pwdLine1=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, 100, self.pwdView.frame.size.width-Default_Space*2, Default_Line)];
    pwdLine1.backgroundColor=APPBGColor;
    
    self.pwdPwdImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 115, 20, 20)];
    self.pwdPwdImg.image=[UIImage imageNamed:@"user_login_pwd_icon"];
    
    self.pwdPwdText=[[UITextField alloc]initWithFrame:CGRectMake(self.pwdPwdImg.frame.origin.x+self.pwdPwdImg.frame.size.width+Default_Space, 115, self.pwdView.frame.size.width-Default_Space-self.pwdPwdImg.frame.size.width-Default_Space-Default_Space-20-Default_Space, 30)];
    self.pwdPwdText.placeholder=@"登录密码";
    [self.pwdPwdText setValue:six9 forKeyPath:@"_placeholderLabel.textColor"];
    self.pwdPwdText.textColor=APPFourColor;
    self.pwdPwdText.tintColor=APPColor;
    self.pwdPwdText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.pwdPwdText.secureTextEntry=YES;
    
    self.pwdShowPwdBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.pwdPwdText.frame.size.width+self.pwdPwdText.frame.origin.x+Default_Space,110, 20, 20)];
    [self.pwdShowPwdBtn setBackgroundImage:[UIImage imageNamed:@"user_login_show_0_icon"] forState:UIControlStateNormal];
    [self.pwdShowPwdBtn setBackgroundImage:[UIImage imageNamed:@"user_login_show_1_icon"] forState:UIControlStateSelected];
    self.pwdShowPwdBtn.selected=NO;
    [self.pwdShowPwdBtn addTarget:self action:@selector(showPwd) forControlEvents:UIControlEventTouchUpInside];
    
    [self.pwdView addSubview:self.pwdNameImg];
    [self.pwdView addSubview:self.pwdNameText];
    [self.pwdView addSubview:self.pwdSMSImg];
    [self.pwdView addSubview:self.pwdSMSText];
    [self.pwdView addSubview:self.pwdSendSMSBtn];
    [self.pwdView addSubview:self.pwdPwdImg];
    [self.pwdView addSubview:self.pwdPwdText];
    [self.pwdView addSubview:self.pwdShowPwdBtn];
    [self.pwdView addSubview:pwdLine];
    [self.pwdView addSubview:pwdLine1];
    
    self.submitBtn=[[UIButton alloc] initWithFrame:CGRectMake(Default_Space, self.pwdView.frame.origin.y+self.pwdView.frame.size.height+Default_Space*2, self.scrollView.frame.size.width-Default_Space*2,40)];
    [self.submitBtn setTitle:@"注册并登陆" forState:UIControlStateNormal];
    self.submitBtn.backgroundColor=APPColor;
    [self.submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.submitBtn.layer.cornerRadius=5;
    self.submitBtn.layer.masksToBounds=YES;
    self.submitBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.submitBtn addTarget:self action:@selector(goSubmit) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.pwdView];
    [self.scrollView addSubview:self.submitBtn];
    
    [self.view addSubview:self.scrollView];
}

#pragma mark 提交
-(void)goSubmit{
    
    if (self.pwdNameText.text.length>0 && self.pwdPwdText.text.length>0 && self.pwdSMSText.text.length>0) {
        NSString * reginUrl = [NSString stringWithFormat:@"%@ctl=user&act=dophlogin&mobile=%@&sms_verify=%@&user_pwd=%@&is_reg=1",FONPort,self.pwdNameText.text,self.pwdSMSText.text,self.pwdPwdText.text];
        [RequestData requestDataOfUrl:reginUrl success:^(NSDictionary *dic) {
            
            NSString * info = dic[@"info"];
            if ([dic[@"status"] isEqual:@1]) {
                [MBProgressHUD showSuccess:info toView:self.view];
                [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_name"] forKey:@"user_name"];
                [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
                [[NSUserDefaults standardUserDefaults]setObject:self.pwdNameText.text forKey:@"email"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showError:info toView:self.view];
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

#pragma mark 发送验证码
-(void)sendSMS{
    if(self.sendSMSTimeCount!=0){
        return;
    }
    self.sendSMSTimeCount=60;
       if (self.pwdNameText.text.length > 0) {
        NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=1",FONPort,self.pwdNameText.text];
        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {

            if ([dic[@"status"] isEqual:@1]) {
                self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:NO];
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

-(void)changeSMSTime{
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.pwdSendSMSBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    }else{
        [self.pwdSendSMSBtn setTitle:[NSString stringWithFormat:@"%i秒",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
}

#pragma mark 显示密码
-(void)showPwd{
    if(!self.pwdShowPwdBtn.selected){
        self.pwdPwdText.secureTextEntry=NO;
        self.pwdShowPwdBtn.selected=YES;
    }else{
        self.pwdPwdText.secureTextEntry=YES;
        self.pwdShowPwdBtn.selected=NO;
    }
    
}
@end
