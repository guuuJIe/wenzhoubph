//
//  CollectVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "CollectVC.h"
#import "GoodListCC.h"
#import "GoodInfoVC.h"
@interface CollectVC()<UICollectionViewDataSource,UICollectionViewDelegate,RemoveCollectGoodsDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,assign)CGFloat collectionWidth;
@property(nonatomic,assign)CGFloat collectionHeight;


@property(nonatomic,strong)NSMutableArray * collectIconMuArr;
@property(nonatomic,strong)NSMutableArray * collectNameMuArr;
@property(nonatomic,strong)NSMutableArray * collectPriceMuArr;
@property(nonatomic,strong)NSMutableArray * collectOldPriceMuArr;
@property(nonatomic,strong)NSMutableArray * collectBuyCoundMuArr;
@property(nonatomic,strong)NSMutableArray * collectIdsMuArr;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)NSInteger collectIndex;
@end

@implementation CollectVC


-(void)viewDidLoad{
    [super viewDidLoad];
    _index = 1;
    _collectIconMuArr = [NSMutableArray new];
    _collectNameMuArr = [NSMutableArray new];
    _collectIdsMuArr = [NSMutableArray new];
    _collectPriceMuArr = [NSMutableArray new];
    _collectOldPriceMuArr = [NSMutableArray new];
    _collectBuyCoundMuArr = [NSMutableArray new];
    
    
    self.title = @"我的收藏";
    [self initView];
    [self initBar];
    [self requeateDataForCollect];
}

-(void)requeateDataForCollect{
    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    
    NSString * collectUrl = [NSString stringWithFormat:@"%@ctl=uc_collect&page=%ld&email=%@&pwd=%@",FONPort,(long)_index,email,pwd];
    
    [RequestData requestDataOfUrl:collectUrl success:^(NSDictionary *dic) {
        
        NSArray * listArr = dic[@"goods_list"];
        if (listArr.count>0) {
            for (NSDictionary *  subdic in listArr) {
                [_collectIconMuArr addObject:subdic[@"icon"]];
                [_collectNameMuArr addObject:subdic[@"sub_name"]];
                [_collectIdsMuArr addObject:subdic[@"id"]];
                [_collectPriceMuArr addObject:subdic[@"current_price"]];
                [_collectOldPriceMuArr addObject:subdic[@"origin_price"]];
                [_collectBuyCoundMuArr addObject:subdic[@"buy_count"]];
            }
            [self showData];
        }else{
            if (_index == 1) {
                [self showData];
            }else{
                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

-(void)initView{
    self.collectionWidth=(Screen_Width-Default_Space*3)/2;
    self.collectionHeight=self.collectionWidth+70;
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake(self.collectionWidth,self.collectionHeight);
    layout.minimumInteritemSpacing =0;
    layout.minimumLineSpacing = Default_Space;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height,Screen_Width, Screen_Height-self.barView.frame.origin.y-self.barView.frame.size.height) collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[GoodListCC class] forCellWithReuseIdentifier:[GoodListCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.showsVerticalScrollIndicator=NO;
    
    
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
//    header.lastUpdatedTimeLabel.hidden = YES;
    self.collectionView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer= [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    self.collectionView.mj_footer = footer;
    
    [self.view addSubview:self.collectionView];
    
//    [self loadData];
}

-(void)loadData{
    _index=1;
    [_collectIconMuArr removeAllObjects];
    [_collectNameMuArr removeAllObjects];
    [_collectIdsMuArr removeAllObjects];
    [_collectPriceMuArr removeAllObjects];
    [_collectOldPriceMuArr removeAllObjects];
    [_collectBuyCoundMuArr removeAllObjects];
    [self requeateDataForCollect];
}
-(void)refreshData{
    _index++;
    [self requeateDataForCollect];
}
-(void)showData{
    
    [self.collectionView.mj_header endRefreshing];
    [self.collectionView.mj_footer endRefreshing];

    [self.collectionView reloadData];
}

#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _collectIdsMuArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    GoodListCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[GoodListCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[GoodListCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    cell.delegate = self;
    [cell showDataForCollectVCName:_collectNameMuArr[indexPath.row] icon:_collectIconMuArr[indexPath.row] withPrice:_collectPriceMuArr[indexPath.row] oldprice:_collectOldPriceMuArr[indexPath.row]  withCound:_collectBuyCoundMuArr[indexPath.row] withcount:indexPath.row];
    
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionWidth,self.collectionHeight);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(Default_Space, Default_Space, Default_Space, Default_Space);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodInfoVC * vc = [GoodInfoVC new];
    vc.goodsId = _collectIdsMuArr[indexPath.row];
    [self tabHidePushVC:vc];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)removeCollectGoodsIds:(NSInteger)index{
    _collectIndex = index;
    UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"是否删除" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [MBProgressHUD showHUD];
        
        NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
        
        NSString * scUrl = [NSString stringWithFormat:@"%@ctl=deal&act=add_collect&id=%@&email=%@&pwd=%@",FONPort,_collectIdsMuArr[_collectIndex],email,pwd];
        [RequestData requestDataOfUrl:scUrl success:^(NSDictionary *dic) {
           
        } failure:^(NSError *error) {
            [MBProgressHUD showError:@"网络错误" toView:self.view];
        }];
        
        [_collectIconMuArr removeObjectAtIndex:_collectIndex];
        [_collectNameMuArr removeObjectAtIndex:_collectIndex];
        [_collectIdsMuArr removeObjectAtIndex:_collectIndex];
        [_collectPriceMuArr removeObjectAtIndex:_collectIndex];
        [_collectOldPriceMuArr removeObjectAtIndex:_collectIndex];
        [_collectBuyCoundMuArr removeObjectAtIndex:_collectIndex];
        [_collectionView reloadData];
        [MBProgressHUD dissmiss];
    }
    
}

@end
