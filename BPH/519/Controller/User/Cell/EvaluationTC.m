//
//  EvaluationTC.m
//  519
//
//  Created by Macmini on 16/12/21.
//  Copyright © 2016年 519. All rights reserved.
//

#import "EvaluationTC.h"

@interface EvaluationTC()<UITextViewDelegate>
@property(nonatomic,strong)UIImageView * iconImg;
@property(nonatomic,strong)UILabel * nameLabel;

@property(nonatomic,assign)BOOL iskeyHide;

@property(nonatomic,copy)NSString * oldpointStr;
@property(nonatomic,strong)NSMutableArray * muarr;

@property(nonatomic,strong)UIView * pointView;
@property(nonatomic,strong)UITextView * evaluaTextView;
@property(nonatomic,strong)UILabel * ecaluatelabel;
@property(nonatomic,strong)UIView * isEvaluationView;

@end

@implementation EvaluationTC


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _muarr = [NSMutableArray new];
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = APPBGColor;
    UIView * goodsInfoView = [[UIView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 100)];
    goodsInfoView.backgroundColor = [UIColor whiteColor];
    [self addSubview:goodsInfoView];
    UITapGestureRecognizer * viewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewtap)];
    [goodsInfoView addGestureRecognizer:viewTap];
    
    _iconImg = [[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 80, 80)];
    _iconImg.contentMode = UIViewContentModeScaleAspectFit;
    [goodsInfoView addSubview:_iconImg];
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, Default_Space, Screen_Width-120, 40)];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.numberOfLines = 0   ;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    [goodsInfoView addSubview:_nameLabel];
    
    /***************************************/
    
    _pointView = [[UIView alloc]initWithFrame:CGRectMake(0, goodsInfoView.frame.size.height+goodsInfoView.frame.origin.y+1, Screen_Width, 40)];
    _pointView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_pointView];
    UILabel * pointLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 40, 20)];
    pointLabel.text = @"评分:";
    pointLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    pointLabel.textAlignment = NSTextAlignmentLeft;
    pointLabel.textColor = APPFourColor;
    [_pointView addSubview:pointLabel];
    
    for (NSInteger i = 0; i<5; i++) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(50+i*30, 5, 30, 30)];
        [btn setImage:[UIImage imageNamed:@"rating_icon_0"] forState:(UIControlStateNormal)];
        btn.tag = 500+i;
        [btn addTarget:self action:@selector(pointBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [_pointView addSubview:btn];
    }
    
    
    _evaluaTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, _pointView.frame.size.height+_pointView.frame.origin.y+1 , Screen_Width, 100)];
    _evaluaTextView.delegate = self;
    _evaluaTextView.editable = YES;
    _evaluaTextView.textColor = APPFourColor;
    _evaluaTextView.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _evaluaTextView.scrollEnabled = NO;
    _evaluaTextView.returnKeyType = UIReturnKeyDone;
    [self addSubview:_evaluaTextView];
    
    _ecaluatelabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, _evaluaTextView.frame.size.width, 20)];
    _ecaluatelabel.enabled = NO;
    _ecaluatelabel.text = @"说点什么~";
    _ecaluatelabel.textColor= [UIColor grayColor];
    _ecaluatelabel.font = [UIFont systemFontOfSize:15];
    [_evaluaTextView addSubview:_ecaluatelabel];
    
    
    _isEvaluationView = [[UIView alloc]initWithFrame:CGRectMake(0, goodsInfoView.frame.size.height+goodsInfoView.frame.origin.y+1, Screen_Width,141)];
    _isEvaluationView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_isEvaluationView];
    UILabel * evalabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, _isEvaluationView.frame.size.width, 20)];
    evalabel.text = @"评价已完成";
    evalabel.enabled = NO;
    evalabel.textAlignment = NSTextAlignmentLeft;
    evalabel.textColor = APPThreeColor;
    evalabel.font = [UIFont systemFontOfSize:15];
    [_isEvaluationView addSubview:evalabel];
}

-(void)viewtap{
    [_evaluaTextView resignFirstResponder];
}

-(void)pointBtnClick:(UIButton *)btn{
    _isbtnClick = YES;
    for (NSInteger i = 0; i<5; i++) {
        UIButton * subbtn = [self viewWithTag:500+i];
        if (subbtn.tag <= btn.tag) {
            [subbtn setImage:[UIImage imageNamed:@"rating_icon_1"] forState:(UIControlStateNormal)];
        }else{
            [subbtn setImage:[UIImage imageNamed:@"rating_icon_0"] forState:(UIControlStateNormal)];
        }
    }
    _pointStr = [NSString stringWithFormat:@"%ld",btn.tag-500+1];
    [self passData];
}

-(void)setModel:(DealOrderModel *)model{
    _model=model;
    _ids = model.ids;
    [_iconImg sd_setImageWithURL:[NSURL URLWithString:model.deal_icon]];
    _nameLabel.text = model.sub_name;
    
    NSString * dp_id = [NSString stringWithFormat:@"%@",model.dp_id];
    if (![dp_id isEqualToString:@"0"]) {
        _pointView.hidden = YES;
        _evaluaTextView.hidden = YES;
    }else{
        _isEvaluationView.hidden = YES;
    }
}

//进入编辑模式
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _ecaluatelabel.hidden = YES;
    _iskeyHide = NO;
    [self.delegate keyUp:textView with:_index];
    return YES;
}

//退出编辑模式
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (textView.text.length==0) {
        _isTextView = NO;
        _ecaluatelabel.hidden = NO;
    }else{
        _isTextView = YES;
        _ecaluatelabel.hidden = YES;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.delegate keydown];
        
    }];
    if (textView.text) {
        _contents = textView.text;
    }
    
    return YES;
}

//已经结束/退出编辑模式
- (void)textViewDidEndEditing:(UITextView *)textView{
    [self passData];
    return;
    
}

//监听输入的文字
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
        
    }
    
    return YES;
}


-(void)passData{
    if (_pointStr && _contents) {
        [self.delegate passDataForIds:_ids point:_pointStr withContent:_contents];
    }
   
}

+(NSString *)getID{
    return @"EvaluationTC";
}
@end
