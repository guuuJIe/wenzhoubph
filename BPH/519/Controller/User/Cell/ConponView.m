//
//  ConponView.m
//  519
//
//  Created by Macmini on 2018/12/15.
//  Copyright © 2018年 519. All rights reserved.
//

#import "ConponView.h"
#import "UnderLineButton.h"
@interface ConponView()
@property (nonatomic , strong) UnderLineButton *selectedBtn;
@end
@implementation ConponView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = YES;
        [self setuplayout];
    }
    return self;
}

- (void)setuplayout{
    UIView *lastView;
    NSArray *arr = @[@"未使用",@"已使用",@"已过期"];
    for (int i = 0; i<arr.count; i++) {
        UnderLineButton *button = [UnderLineButton new];
        button.lineView.backgroundColor = RGB(244, 44, 45);
        button.l_width = 50;
        [button setTitle:arr[i] forState:0];
        [button setTitleColor:RGB(91, 91, 91) forState:0];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [button setTitleColor:RGB(244, 44, 45) forState:UIControlStateSelected];
        button.tag = i+200;
        [self addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.equalTo(self).offset(1);
            }else{
                make.left.equalTo(lastView.mas_right);
            }
            make.top.equalTo(self).offset(3);
            make.width.mas_equalTo(Screen_Width/3);
            make.bottom.equalTo(self).offset(0*AdapterScal);
        }];
        lastView = button;
    }
    
   
        UIView *line = [UIView new];
        line.backgroundColor = UIColore5e5;
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(0);
            make.right.equalTo(self).offset(0);
            make.height.mas_equalTo(1);
            make.top.equalTo(lastView.mas_bottom).offset(0);
        }];
    UnderLineButton *button = [self viewWithTag:200];
    [self click:button];
}

- (void)click:(UnderLineButton *)btn{
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
    if (self.clickBlock) {
        self.clickBlock(btn.tag);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
