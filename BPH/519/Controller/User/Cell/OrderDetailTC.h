//
//  OrderDetailTC.h
//  519
//
//  Created by Macmini on 16/12/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "GoodsDetailModel.h"
@interface OrderDetailTC : BaseTC
@property(nonatomic,strong)GoodsDetailModel * model;

+(NSString *)getID;
@end
