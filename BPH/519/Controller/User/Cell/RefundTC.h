//
//  RefundTC.h
//  519
//
//  Created by Macmini on 16/12/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "DealOrderModel.h"

@protocol keyBoredUpDelegate <NSObject>

-(void)keyUp:(UITextView *)textview with:(NSInteger)index;
-(void)keydown;

//string
-(void)addBtnRefundId:(NSString *)refundId withContent:(NSString *)content;
-(void)removeRefundId:(NSString *)refundId withContent:(NSString *)content;
@end


@interface RefundTC : BaseTC
@property(nonatomic,strong)DealOrderModel * model;
@property(nonatomic,assign)id<keyBoredUpDelegate>delegate;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,copy)NSString * refundId;

+(NSString *)getID;



@end
