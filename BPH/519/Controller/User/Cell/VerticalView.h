//
//  VerticalView.h
//  519
//
//  Created by Macmini on 2018/12/15.
//  Copyright © 2018年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerticalView : UIView
{
    NSInteger _lineLength;
    NSInteger _lineSpacing;
    UIColor *_lineColor;
    CGFloat _height;
}
- (instancetype)initWithFrame:(CGRect)frame withLineLength:(NSInteger)lineLength withLineSpacing:(NSInteger)lineSpacing withLineColor:(UIColor *)lineColor;
@end
