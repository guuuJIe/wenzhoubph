
//
//  AddressTC.m
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddressTC.h"

@interface AddressTC()

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel * phoneLabe;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,strong)UILabel * defaultLabel;
@property(nonatomic,strong)UIImageView *delectimg;
@property(nonatomic,strong)UIImageView * editImg;
@property(nonatomic,strong)UIButton * delectButton;
@property(nonatomic,strong)UIButton * editBtn;

@property(nonatomic,strong)UIView *line;

@end

@implementation AddressTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];

    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width/2, 20)];
    self.titleLabel.textColor = APPFourColor;
    self.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    self.phoneLabe = [[UILabel alloc]initWithFrame:CGRectMake(self.titleLabel.frame.size.width+self.titleLabel.frame.origin.x, Default_Space, Screen_Width/2-Default_Space*3, 20)];
    self.phoneLabe.textColor = APPFourColor;
    self.phoneLabe.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    self.phoneLabe.textAlignment = NSTextAlignmentRight;
    
    self.infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space,self.titleLabel.frame.size.height+self.titleLabel.frame.origin.y+Default_Space, Screen_Width-Default_Space*3-30, 40)];
    self.infoLabel.textColor=APPThreeColor;
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];

    _defueltBtn = [[UIButton alloc] initWithFrame:CGRectMake(Default_Space, self.infoLabel.frame.size.height+self.infoLabel.frame.origin.y+Default_Space,70, 20)];
    [_defueltBtn setTitle:@"默认" forState:(UIControlStateSelected)];
    [_defueltBtn setTitle:@"设置默认" forState:(UIControlStateNormal)];
    [_defueltBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    _defueltBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    _defueltBtn.layer.borderColor = APPFourColor.CGColor;
    _defueltBtn.layer.borderWidth = 1;
    _defueltBtn.layer.cornerRadius = 10;
    _defueltBtn.layer.masksToBounds = YES;
    [_defueltBtn addTarget:self action:@selector(setDefault:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    _renewBtn = [[UIButton alloc]init];
    [_renewBtn setTitle:@"地址需要更新" forState:(UIControlStateNormal)];
    [_renewBtn setTitleColor:APPColor forState:(UIControlStateNormal)];
    _renewBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    _renewBtn.layer.borderColor = APPColor.CGColor;
    _renewBtn.layer.borderWidth = 1;
    _renewBtn.layer.cornerRadius = 10;
    _renewBtn.layer.masksToBounds = YES;
    [_renewBtn addTarget:self action:@selector(editimageViewTap) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    UIView * editView = [[UIView alloc]initWithFrame:CGRectMake(Screen_Width-90, _defueltBtn.frame.origin.y-10, 40, 40)];
    
    [self addSubview:editView];
    UIImageView * editImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 20)];
    editImgView.image = [UIImage imageNamed:@"edit_icon"];
    [editView addSubview:editImgView];
    UITapGestureRecognizer * editimgViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editimageViewTap)];
    [editView addGestureRecognizer:editimgViewTap];
    
    
    UIView * deleteView = [[UIView alloc]initWithFrame:CGRectMake(Screen_Width-45, _defueltBtn.frame.origin.y-10, 40, 40)];
    
    [self addSubview:deleteView];
    UIImageView * deleImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 20)];
    deleImgView.image = [UIImage imageNamed:@"delect_icon"];
    [deleteView addSubview:deleImgView];
    UITapGestureRecognizer * imgViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewTap:)];
    [deleImgView addGestureRecognizer:imgViewTap];
    
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0, [AddressTC getHeight]-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
  
    [self addSubview:self.titleLabel];
    [self addSubview:self.defaultLabel];
    [self addSubview:self.infoLabel];
    [self addSubview:self.line];
    [self addSubview:_phoneLabe];
    [self addSubview:_defueltBtn];
    [self addSubview:_editBtn];
    [self addSubview:_renewBtn];

}




-(void)editimageViewTap{
    NSLog(@"edit");
    NSString * regionstr = [NSString stringWithFormat:@"%@ %@ %@",_model.region_lv2_name,_model.region_lv3_name,_model.region_lv4_name];
    [self.delegate editImgOnClicksWithIndex:self.cellIndex Ids:_ids name:_model.consignee phone:_model.mobile address:_model.address region:regionstr isdefault:_model.is_default];
}
-(void)imageViewTap:(UITapGestureRecognizer *)tap{
    NSLog(@"delect");
    [self.delegate delectOnClickWithIndex:self.cellIndex withIds:_ids];
}
-(void)setDefault:(UIButton *)btn{
    [self.delegate setDefaultIndex:_cellIndex withIds:_ids];
}

-(void)setModel:(AddressModel *)model{
    _model = model;
    _titleLabel.text = model.consignee;
    _infoLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",model.region_lv2_name,model.region_lv3_name,model.region_lv4_name,model.address];
    
    if([model.is_default isEqualToString:@"1"]){
        _defueltBtn.selected = YES;
        _defueltBtn.layer.borderColor = APPColor.CGColor;
        [_defueltBtn setTitleColor:APPColor forState:(UIControlStateNormal)];
        _defueltBtn.size = CGSizeMake(50,20);
        
    }else{
        _defueltBtn.selected = NO;
        _defueltBtn.layer.borderColor = APPFourColor.CGColor;
        [_defueltBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
        _defueltBtn.size = CGSizeMake(70,20);
        
    }
    if ([model.xpoint isEqualToString:@"0"]) {
        _renewBtn.frame = CGRectMake(85, _defueltBtn.frame.origin.y, 100, 20);
        
    }else{
        _renewBtn.hidden = YES;
    }
    _phoneLabe.text = model.mobile;
    _ids = model.ids;
}



+(CGFloat)getHeight{
    return 120;
}

+(NSString *)getID{
    return @"AddressTC";
}
@end
