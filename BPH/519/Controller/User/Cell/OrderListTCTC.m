//
//  OrderListTCTC.m
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderListTCTC.h"

@interface OrderListTCTC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *name;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UILabel *info;
@property(nonatomic,strong)UILabel *count;
@end

@implementation OrderListTCTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.img=[[UIImageView alloc]init];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.name=[[UILabel alloc]init];
    self.name.textColor=APPFourColor;
    self.name.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.name.numberOfLines=0;
    self.name.lineBreakMode=NSLineBreakByTruncatingTail;
    
    self.price=[[UILabel alloc]init];
    self.price.textColor=APPFourColor;
    self.price.textAlignment = NSTextAlignmentRight;
    self.price.font=[UIFont systemFontOfSize:FONT_SIZE_M];

    self.count=[[UILabel alloc]init];
    self.count.textColor=APPThreeColor;
    self.count.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    


    [self addSubview:self.img];
    [self addSubview:self.name];
    [self addSubview:self.price];
    [self addSubview:self.info];
    [self addSubview:self.count];
}

-(void)setModel:(DealOrderModel *)model{
    
    [_img makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(0);
        make.top.equalTo(self).with.offset(10);
        make.width.and.height.mas_equalTo(100);
    }];
    [_name makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img.mas_right).with.offset(0);
        make.top.equalTo(self.contentView).with.offset(5);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(Screen_Width-190);
    }];
    [_price makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_name.mas_right).with.offset(Default_Space);
        make.right.equalTo(self.mas_right).with.offset(-5);
        make.centerY.equalTo(_name.mas_centerY);
        make.height.mas_equalTo(20);
    }];
    [_count makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_img.mas_centerY);
        make.right.equalTo(self.contentView).with.offset(-5);
        make.height.mas_equalTo(40);
    }];

    _model = model;
    [_img sd_setImageWithURL:[NSURL URLWithString:model.deal_icon]];
    self.name.text=model.name;
    
    NSString * goodsprice ;
    NSInteger score = [NSString stringWithFormat:@"%@",model.return_score].integerValue;
    if (score > 0) {
        goodsprice = [NSString stringWithFormat:@"%ld积分",score];
    }else{
        if (goodsprice.length>5) {
            goodsprice =[NSString stringWithFormat:@"￥%.2lf",[model.total_price doubleValue]];
        }else{
            goodsprice = [NSString stringWithFormat:@"￥%@",model.total_price];
        }
    }
    
    self.price.text=goodsprice;
    
    self.info.text=model.attr_str;
    self.count.text=[NSString stringWithFormat:@"x%@",model.number];
    
}


-(void)showData{
    [self.img sd_setImageWithURL:[[NSURL alloc]initWithString:@"http://img.519wz.cn/public/attachment/201604/09/16/5708bdf5ef18c.jpg"]];
    
    NSString *nameStr=@"捷克百威啤酒500ml";
    NSString *priceStr=@"￥1000.0";
    NSString *infoStr=@"500ml,酒精度：5.0%vol、";
    NSString *counStr=@"x10";
    
    self.name.frame=CGRectMake(self.img.frame.size.width+Default_Space*2, self.img.frame.origin.y, Screen_Width-self.img.frame.size.width-Default_Space*2-[NSString sizeWithText:priceStr font:self.price.font maxSize:Max_Size].width-Default_Space*2, 40);
    self.name.text=nameStr;
    
    self.price.frame = CGRectMake(self.name.frame.size.width+self.name.frame.origin.x + Default_Space, self.img.frame.origin.y, [NSString sizeWithText:priceStr font:self.price.font maxSize:Max_Size].width, 20);
    self.price.text=priceStr;
    
    self.info.frame=CGRectMake(self.img.frame.size.width+Default_Space*2, self.name.frame.origin.y + self.name.frame.size.height, Screen_Width-self.img.frame.size.width-Default_Space*2-[NSString sizeWithText:counStr font:self.count.font maxSize:Max_Size].width-Default_Space*2, 40);
    self.info.text=infoStr;
    
    self.count.frame = CGRectMake(self.info.frame.size.width + self.info.frame.origin.x + Default_Space, self.name.frame.origin.y+self.name.frame.size.height, [NSString sizeWithText:counStr font:self.count.font maxSize:Max_Size].width, 40);
    self.count.text=counStr;
}

+(NSString *)getID{
    return @"OrderListTCTC";
}
@end
