//
//  RefundTC.m
//  519
//
//  Created by Macmini on 16/12/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RefundTC.h"

@interface RefundTC()<UITextViewDelegate>
@property(nonatomic,strong)UIView *refundView;
@property(nonatomic,strong)UIButton * isYesBtn;
@property(nonatomic,strong)UIImageView * refundImg;
@property(nonatomic,strong)UILabel * refundNameLabel;
@property(nonatomic,strong)UILabel * refundPriceLabel;
@property(nonatomic,strong)UILabel * refundNumberLabel;
@property(nonatomic,strong)UILabel * refundAttrLabel;
@property(nonatomic,strong)UITextView * whyTextView;
@property(nonatomic,strong)UILabel * norefundLabel;
@property(nonatomic,strong)UIView * lineView;
@property(nonatomic,strong)UIView * lineView2;
@property(nonatomic,copy)NSString * placeholder;
@property(nonatomic,copy)NSString * refundContent;

@property(nonatomic,assign)BOOL iskeyboard;
@end

@implementation RefundTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _iskeyboard = NO;
        for (UIView * subview in self.subviews) {
            if ([subview isKindOfClass:[UIButton class]]) {
                [subview removeFromSuperview];
            }
        }
        [self initView];
    }
    return self;
}

-(void)initView{
    
    _lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];;
    _lineView.backgroundColor = APPBGColor;
    [self addSubview:_lineView];
    
    _refundView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, Screen_Width, 120)];
    _refundView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_refundView];
    UITapGestureRecognizer * refundTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(refundBtn)];
    [_refundView addGestureRecognizer:refundTap];
    
    _isYesBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, _refundView.frame.size.height/2-10, 25, 25)];
    _isYesBtn.tag = 6500+_index;
    [_isYesBtn setImage:[UIImage imageNamed:@"ic_cb_normal"] forState:(UIControlStateNormal)];
    _isYesBtn.selected = NO;
    [_isYesBtn addTarget:self action:@selector(isBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    _refundImg = [[UIImageView alloc]initWithFrame:CGRectMake(_isYesBtn.frame.size.width+_isYesBtn.frame.origin.x, 10, 100, 100)];
    _refundImg.contentMode = UIViewContentModeScaleAspectFit;
    
    _refundNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(_refundImg.frame.size.width+_refundImg.frame.origin.x, 10, Screen_Width-30-100-60, 40)];
    _refundNameLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _refundNameLabel.textColor = APPFourColor;
    _refundNameLabel.textAlignment = NSTextAlignmentLeft;
    _refundNameLabel.numberOfLines = 0;
    
    _refundPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-70, 20,60, 20)];
    _refundPriceLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _refundPriceLabel.textColor = APPColor;
    _refundPriceLabel.textAlignment = NSTextAlignmentRight;
    
    _refundAttrLabel = [[UILabel alloc]initWithFrame:CGRectMake(_refundImg.frame.size.width+_refundImg.frame.origin.x, _refundNameLabel.frame.size.height+_refundNameLabel.frame.origin.y+10, Screen_Width/2, 40)];
    _refundAttrLabel.textAlignment = NSTextAlignmentLeft;
    _refundAttrLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _refundAttrLabel.textColor = APPThreeColor;
    _refundAttrLabel.numberOfLines = 0;
    
    _refundNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-50, _refundAttrLabel.frame.origin.y+10, 40, 20)];
    _refundNumberLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _refundNumberLabel.textAlignment = NSTextAlignmentRight;
    _refundNumberLabel.textColor = APPThreeColor;
    
    [_refundView addSubview:_isYesBtn];
    [_refundView addSubview:_refundImg];
    [_refundView addSubview:_refundNumberLabel];
    [_refundView addSubview:_refundPriceLabel];
    [_refundView addSubview:_refundNameLabel];
    [_refundView addSubview:_refundAttrLabel];
    
    _lineView2 = [[UIView alloc]initWithFrame:CGRectMake(0, _refundView.frame.size.height+_refundView.frame.origin.y, Screen_Width, 5)];
    _lineView2.backgroundColor = APPBGColor;
    [self addSubview:_lineView2];
    
    _whyTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, self.refundView.frame.size.height+self.refundView.frame.origin.y+2, Screen_Width, 100)];;
    _whyTextView.editable = YES;
    _whyTextView.delegate = self;
    _whyTextView.textColor = APPFourColor;
    _whyTextView.returnKeyType = UIReturnKeyDone;	
    _whyTextView.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _whyTextView.scrollEnabled = NO;
    [self addSubview:_whyTextView];
    
    _norefundLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, self.refundView.frame.size.height+self.refundView.frame.origin.y+2, Screen_Width, 50)];
    _norefundLabel.textColor = APPThreeColor;
    _norefundLabel.text = @"退款原因";
    _norefundLabel.font = [UIFont systemFontOfSize:FONT_SIZE_L];
    _norefundLabel.textAlignment = NSTextAlignmentLeft;
    _norefundLabel.hidden = NO;
    [self addSubview:_norefundLabel];
    
    
}

-(void)setModel:(DealOrderModel *)model{
    _model = model;
    _refundId = model.ids;
    if(model.name.length>0){
        _refundNameLabel.text = model.name;
    }else{
        _refundNameLabel.text = model.sub_name;
    }
    _refundAttrLabel.text = model.attr_str;
    [_refundImg sd_setImageWithURL:[NSURL URLWithString:model.deal_icon]];
    _refundPriceLabel.text = [NSString stringWithFormat:@"￥%@",model.unit_price];
    _refundNumberLabel.text = [NSString stringWithFormat:@"x%@",model.number];
    
    NSString * keyi_refund = [NSString stringWithFormat:@"%@",model.keyi_refund];
    
    if ([keyi_refund isEqualToString:@"1"]) {
        _whyTextView.hidden = NO;
        _isYesBtn.hidden = NO;
        _norefundLabel.hidden = NO;
    }
    if ([keyi_refund isEqualToString:@"0"]) {
        _norefundLabel.hidden = NO;
        _norefundLabel.text = model.refund_status_tip;
        _norefundLabel.textColor = APPFourColor;
        _whyTextView.hidden = YES;
        _isYesBtn.hidden = YES;
    }
    
    
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    _iskeyboard = YES;
    _norefundLabel.hidden = YES;
//    if([textView.text isEqualToString:@"退款原因"]){
//        textView.text = @"";
//        textView.textColor = APPFourColor;
//    }
    [self.delegate keyUp:textView with:_index];
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    _iskeyboard = NO;
    
    if ([textView.text isEqualToString:@""]) {
        _norefundLabel.hidden = NO;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.delegate keydown];
        
    }];
    
    if (textView.text.length>0) {
        _refundContent = textView.text;
    }
    [self addrefundid];
    return YES;
}
-(void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"%f",textView.frame.origin.y);
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_whyTextView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
        
    }
    
    return YES;
}

-(void)isBtnClick:(UIButton *)btn{
    
    if (btn.selected == YES) {
        btn.selected = NO;
        [self removeRefundId];
        [_isYesBtn setImage:[UIImage imageNamed:@"ic_cb_normal"] forState:(UIControlStateNormal)];
    }else{
        [self addrefundid];
        [_isYesBtn setImage:[UIImage imageNamed:@"ic_cb_checked"] forState:(UIControlStateNormal)];
        btn.selected = YES;
    }

}
-(void)refundBtn{
    NSLog(@"_refund %@",_refundId);
    if (_iskeyboard == YES) {
        _iskeyboard = NO;
        [_whyTextView resignFirstResponder];
    }else{
        if (_isYesBtn.selected == YES) {
            _isYesBtn.selected = NO;
            [self removeRefundId];
            [_isYesBtn setImage:[UIImage imageNamed:@"ic_cb_normal"] forState:(UIControlStateNormal)];
        }else{
            [self addrefundid];
            [_isYesBtn setImage:[UIImage imageNamed:@"ic_cb_checked"] forState:(UIControlStateNormal)];
            _isYesBtn.selected = YES;
        }
    }
}

//second way
-(void)addrefundid{
    [self.delegate addBtnRefundId:_refundId withContent:_refundContent];
}
-(void)removeRefundId{
    [self.delegate removeRefundId:_refundId withContent:_refundContent];
}

+(NSString *)getID{
    return @"RefundTC";
}

@end
