//
//  OrderListTC.m
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderListTC.h"
#import "OrderListTCTC.h"
#import "DealOrderModel.h"

@interface OrderListTC()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property(nonatomic,strong)UILabel *state;
@property(nonatomic,strong)UILabel *time;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UIView *line1;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UIView *btnView;

@property(nonatomic,strong)UIView *marLine;
@property(nonatomic,strong)NSMutableArray * dealOrderItem;
@property(nonatomic,strong)NSMutableArray * allDealOrderMuArr;
@property(nonatomic,copy)NSString * orderId;
@property(nonatomic,copy)NSString * orderIds;
@property(nonatomic,copy)NSString * status;
@property(nonatomic,strong)NSArray * dealArr;
@end

@implementation OrderListTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _allDealOrderMuArr = [NSMutableArray new];
        if (_dealOrderItem) {
            [_dealOrderItem removeAllObjects];
        }else{
            _dealOrderItem = [NSMutableArray new];
        }
        
        [self initView];
    }
    
    return  self;
}



-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    
    self.marLine=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Space)];
    self.marLine.backgroundColor=APPBGColor;
    
    self.state=[[UILabel alloc]init];
    self.state.font=[UIFont systemFontOfSize:FONT_SIZE_M];
        self.state.textColor=APPFourColor;
    self.time=[[UILabel alloc]init];
    self.time.textColor=APPThreeColor;
    self.time.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.time.textAlignment=NSTextAlignmentRight;
    
    self.line=[[UIView alloc]init];
    self.line.backgroundColor=APPBGColor;
    
    self.tableView=[[UITableView alloc]init];
    [self.tableView registerClass:[OrderListTCTC class] forCellReuseIdentifier:[OrderListTCTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.scrollEnabled=NO;
    self.tableView.showsVerticalScrollIndicator=NO;
    
    self.line1=[[UIView alloc]init];
    self.line1.backgroundColor=APPBGColor;

    self.price=[[UILabel alloc] init];
    self.price.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.btnView=[[UIView alloc]init];
    
    [self addSubview:self.marLine];
    [self addSubview:self.state];
    [self addSubview:self.time];
    [self addSubview:self.line];
    [self addSubview:self.tableView];
    [self addSubview:self.line1];
    [self addSubview:self.price];
    [self addSubview:self.btnView];
    
}



-(void)setModel:(OrderListModel *)model{
    _model = model;
    _orderId = model.ids;
    _status = model.status;
    _dealArr = model.deal_order_item;
    
    self.state.text=model.status;
    self.time.text=model.create_time;
    
    NSString * price = [NSString stringWithFormat:@"%@",model.total_price];
    NSInteger score = [NSString stringWithFormat:@"%@",model.return_total_score].integerValue ;
    if (score > 0) {
        price = [NSString stringWithFormat:@"%ld积分",score];
    }else{
        if (price.length>5) {
            price =[NSString stringWithFormat:@"￥%.2lf",[model.total_price doubleValue]];
        }else{
            price = [NSString stringWithFormat:@"￥%@",model.total_price];
        }
    }
    self.price.text =price;
    
    for (UIView *itemView in self.btnView.subviews) {
        [itemView removeFromSuperview];
    }
   
    if (_dealOrderItem) {
        [_dealOrderItem removeAllObjects];
    }else{
        _dealOrderItem = [NSMutableArray new];
    }
    for (NSDictionary * subdic in model.deal_order_item) {
        DealOrderModel * model = [DealOrderModel new];
        [model setValuesForKeysWithDictionary:subdic];
        [_dealOrderItem addObject:model];
        
    }
    
    [_allDealOrderMuArr addObject:_dealOrderItem];
   
    NSString * pay_status = [NSString stringWithFormat:@"%@",model.pay_status];
    NSString * items_refund_status = [NSString stringWithFormat:@"%@",model.items_refund_status];
    NSString * order_status = [NSString stringWithFormat:@"%@",model.order_status];
    NSString * keyi_dianpin = [NSString stringWithFormat:@"%@",model.keyi_dianpin];
    
    
    NSArray * btnTitleArray;
    if ([pay_status isEqualToString:@"0"]) {
        
        if ([model.wuliu_btn isEqual:@1]) {
            btnTitleArray=[[NSArray alloc] initWithObjects:@"删除订单",@"立即支付",@"查看物流", nil];
        }else{
           btnTitleArray=[[NSArray alloc] initWithObjects:@"删除订单",@"立即支付", nil];
        }
    }else
    if([pay_status isEqualToString:@"2"] && [items_refund_status isEqualToString:@"1"] && [order_status isEqualToString:@"0"] ){
        if ([model.wuliu_btn isEqual:@1]) {
            btnTitleArray=[[NSArray alloc] initWithObjects:@"我要退款",@"查看物流", nil];
        }else{
            btnTitleArray=[[NSArray alloc] initWithObjects:@"我要退款", nil];
        }
        
    }else
    if([keyi_dianpin isEqualToString:@"1"] && [order_status isEqualToString:@"0"]){
        
        if ([model.wuliu_btn isEqual:@1]) {
            btnTitleArray=[[NSArray alloc] initWithObjects:@"去评价",@"查看物流", nil];
        }else{
            btnTitleArray=[[NSArray alloc] initWithObjects:@"去评价", nil];
        }
    }else{
        if ([model.wuliu_btn isEqual:@1]) {
             btnTitleArray=[[NSArray alloc] initWithObjects:@"查看物流", nil];
        }
    }
    
    
    
    
    CGFloat allWidth=Screen_Width/3*2-Default_Space*2;
    CGFloat item_y = 0;
    for (int i=0;i<btnTitleArray.count;i++) {
        NSString *itemTitle=[btnTitleArray objectAtIndex:i];
        UIFont *itemFont=[UIFont systemFontOfSize:FONT_SIZE_X];
        CGFloat itemWidth=[NSString sizeWithText:itemTitle font:itemFont maxSize:Max_Size].width+Default_Space*2;
        
        
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(allWidth-(item_y + itemWidth), 0,itemWidth, 30)];
        [btn setBackgroundColor:APPColor];
        [btn setTitle:itemTitle forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.layer.cornerRadius=3;
        btn.layer.masksToBounds=YES;
        btn.titleLabel.font=itemFont;
        
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.btnView addSubview:btn];
        item_y = item_y + itemWidth+10;
    }
    
    
    [self.state makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(Default_Space) ;
        make.top.equalTo(_marLine.mas_bottom).with.offset(Default_Space);
        make.height.mas_equalTo(20);
//        make.width.mas_equalTo(self.frame.size.width/3-Default_Space);
    }];
    [_time makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_marLine.mas_bottom).with.offset(Default_Space);
//        make.left.equalTo(_state.mas_right).with.offset(Default_Space);
        make.right.equalTo(self.contentView).with.offset(-Default_Space);
        make.height.mas_equalTo(20);
//        make.width.mas_equalTo(Screen_Width/3*2-20);
    }];
    [_line makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_state.mas_bottom).with.offset(5);
        make.left.and.right.equalTo(self.contentView).with.offset(0);
        make.height.mas_equalTo(1);
    }];
    [_tableView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line.mas_bottom).with.offset(0);
        make.left.equalTo(self.contentView).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-Default_Space);
        make.height.mas_equalTo(_dealOrderItem.count*120);
    }];
    [_line1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tableView.mas_bottom).with.offset(0);
        make.left.and.right.equalTo(self.contentView).with.offset(0);
        make.height.mas_equalTo(Default_Line);
    }];
    [self.price makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line1.mas_bottom).with.offset(Default_Space);
        make.left.equalTo(self.contentView).with.offset(Default_Space);
        make.width.mas_equalTo(Screen_Width/3);
        make.height.mas_equalTo(24);
    }];
    [_btnView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line1.mas_bottom).with.offset(Default_Space);
        make.right.equalTo(self.contentView).with.offset(-Default_Space);
        make.width.mas_equalTo(Screen_Width/3*2-Default_Space*2);
        make.height.mas_equalTo(40);
        
    }];
    [self.contentView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    
    [self.tableView reloadData];
    
}

-(void)showData{
    NSString *stateStr=@"待付款";

    self.state.frame=CGRectMake(Default_Space, Default_Space+self.marLine.frame.size.height,[NSString sizeWithText:stateStr font:self.state.font maxSize:Max_Size].width, 20);
    self.state.text=stateStr;

    self.time.frame=CGRectMake(self.state.frame.origin.x+self.state.frame.size.width+Default_Space, self.state.frame.origin.y, Screen_Width-Default_Space-self.state.frame.size.width-Default_Space-Default_Space, 20);
    self.time.text=@"2016/8/23 19:11:12";

    self.line.frame=CGRectMake(0, self.state.frame.origin.y+self.state.frame.size.height+Default_Space, Screen_Width, Default_Line);

    self.tableView.frame=CGRectMake(0, self.line.frame.size.height+self.line.frame.origin.y, Screen_Width, 2*100);
    [self.tableView reloadData];
    
    self.line1.frame=CGRectMake(0, self.tableView.frame.size.height+self.tableView.frame.origin.y, Screen_Width, Default_Line);

    NSMutableAttributedString *priceStr = [[NSMutableAttributedString alloc] initWithString:@"￥124.9"];
    [priceStr addAttribute:NSForegroundColorAttributeName value:APPFourColor range:NSMakeRange(0, 1)];
    [priceStr addAttribute:NSForegroundColorAttributeName value:APPColor range:NSMakeRange(1, priceStr.length-1)];
    self.price.frame=CGRectMake(Default_Space, self.line1.frame.size.height+self.line1.frame.origin.y+Default_Space, [NSString sizeWithText:priceStr.string font:self.price.font maxSize:Max_Size].width, 20);
    self.price.attributedText=priceStr;
    
    for (UIView *itemView in self.btnView.subviews) {
        [itemView removeFromSuperview];
    }
    
    NSArray *btnTitleArray=[[NSArray alloc] initWithObjects:@"立即支付",@"删除订单",@"去评价", nil];
    CGFloat allWidth=Default_Space;
    for (int i=0;i<btnTitleArray.count;i++) {
        NSString *itemTitle=[btnTitleArray objectAtIndex:i];
        UIFont *itemFont=[UIFont systemFontOfSize:FONT_SIZE_X];
        CGFloat itemWidth=[NSString sizeWithText:itemTitle font:itemFont maxSize:Max_Size].width+Default_Space*2;

        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(allWidth, 0,itemWidth, 24)];
        [btn setBackgroundColor:APPColor];
        [btn setTitle:itemTitle forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.layer.cornerRadius=3;
        btn.layer.masksToBounds=YES;
        btn.titleLabel.font=itemFont;
        allWidth=allWidth+itemWidth+Default_Space;
       
        [self.btnView addSubview:btn];
    }
    self.btnView.frame=CGRectMake(Screen_Width-allWidth, self.price.frame.origin.y-2, allWidth, 24);
    
}

-(void)btnClick:(UIButton *)btn{
//    self.orderBtnOnClickBlick(btn.titleLabel.text);
    NSLog(@"%@",btn.titleLabel.text);
    if ([btn.titleLabel.text isEqualToString:@"立即支付"]) {
        [self.delegate nowPay:_orderId];
    }else if([btn.titleLabel.text isEqualToString:@"删除订单"]){
        
        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"" message:@"是否删除订单" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertview show];
        
    }else if([btn.titleLabel.text isEqualToString:@"我要退款"]){
        [self.delegate refundFor:_orderId withrefund:_dealArr];
    }else if ([btn.titleLabel.text isEqualToString:@"去评价"]){
        [self.delegate goodsEvaluationwithGoodsData:_dealArr withEvaluationId:_orderId];
    }else if ([btn.titleLabel.text isEqualToString:@"客服"]){
        [self.delegate thisisKefu];
    }else if ([btn.titleLabel.text isEqualToString:@"查看物流"]){
        [self.delegate thisisWuli:_orderId];
    }else{
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        [self.delegate deleteOrderId:_orderId withindex:_index ];
    }
}



#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _model.deal_order_item.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderListTCTC *cell=[tableView dequeueReusableCellWithIdentifier:[OrderListTCTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[OrderListTCTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[OrderListTCTC getID]];
    }
    DealOrderModel * model = _dealOrderItem[indexPath.row];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate orderDetail:_orderId withTitle:_status withCellIndex:indexPath.row];
}

+(NSString *)getID{
    return @"OrderListTC";
}
@end
