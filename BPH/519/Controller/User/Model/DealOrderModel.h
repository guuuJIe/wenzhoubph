//
//  DealOrderModel.h
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealOrderModel : NSObject

@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * deal_id;
@property(nonatomic,copy)NSString * deal_icon;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * sub_name;
@property(nonatomic,copy)NSString * attr_str;
@property(nonatomic,copy)NSString * number;
@property(nonatomic,copy)NSString * order_id;
@property(nonatomic,copy)NSString * unit_price;
@property(nonatomic,copy)NSString * total_price;
@property(nonatomic,copy)NSString * consume_count;
@property(nonatomic,copy)NSString * dp_id;
@property(nonatomic,copy)NSString * delivery_status;
@property(nonatomic,copy)NSString * is_arrival;
@property(nonatomic,copy)NSString * is_refund;
@property(nonatomic,copy)NSString * keyi_refund;
@property(nonatomic,copy)NSString * refund_status;
@property(nonatomic,copy)NSString * refund_status_tip;
@property(nonatomic,copy)NSString * return_total_score;
@property(nonatomic,copy)NSString * return_score;

@end
