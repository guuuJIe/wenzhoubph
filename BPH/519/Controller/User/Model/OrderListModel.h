//
//  OrderListModel.h
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderListModel : NSObject

@property(nonatomic,copy)NSString * c;
@property(nonatomic,copy)NSString * consume_count;
@property(nonatomic,copy)NSString * create_time;
@property(nonatomic,copy)NSArray * deal_order_item;
@property(nonatomic,copy)NSString * delivery_status;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * order_sn;
@property(nonatomic,copy)NSString * total_price;
@property(nonatomic,copy)NSString * pay_amount;
@property(nonatomic,copy)NSString * order_status;
@property(nonatomic,copy)NSString * refund_status;
@property(nonatomic,copy)NSString * status;
@property(nonatomic,copy)NSString * order_id;
@property(nonatomic,copy)NSString * type;
@property(nonatomic,copy)NSString * keyi_dianpin;
@property(nonatomic,copy)NSString * items_refund_status;
@property(nonatomic,copy)NSString * pay_status;
@property(nonatomic,copy)NSString * return_total_score;
@property(nonatomic,copy)NSString * wuliu_btn;
@end
