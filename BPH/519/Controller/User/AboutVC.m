//
//  AboutVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AboutVC.h"

@interface AboutVC()
@property(nonatomic,strong)UIButton *backBtn;

@end

@implementation AboutVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
//    [self initBar];
    [self initView];
}

//-(void)initBar{
//    [super initBar];
//
//    [self barViewAddTitle:@"分享有礼"];
//
//    self.backBtn=[[UIButton alloc]init];
//    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"back_white_icon"] forState:UIControlStateNormal];
//    [self.backBtn setImage:[UIImage imageNamed:@"back_select"] forState:(UIControlStateHighlighted)];
//    [self.backBtn addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
//    [self barViewAddLeftBtn:self.backBtn];
//}

-(void)initView{
    UILabel * shareLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 30)];
    shareLabel.center = CGPointMake(Screen_Width/2, 115);
    shareLabel.text = @"敬请期待";
    shareLabel.textColor = APPFourColor;
    shareLabel.textAlignment = NSTextAlignmentCenter;
    shareLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    [self.view addSubview:shareLabel];
    
}
@end
