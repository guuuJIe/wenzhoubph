//
//  GoodsEvaluationVC.m
//  519
//
//  Created by Macmini on 16/12/17.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodsEvaluationVC.h"
#import "DealOrderModel.h"
#import "EvaluationTC.h"
@interface GoodsEvaluationVC ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UIScrollViewDelegate,PassDataForVCDelegate>
@property(nonatomic,strong)UIButton * backBtn;
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)NSMutableArray * dataArray;
@property(nonatomic,strong)UITextView *evaluaTextView;
@property(nonatomic,strong)UIScrollView * scrollView;
@property(nonatomic,strong)UIView * pointView;
@property(nonatomic,strong)EvaluationTC * cell;


@property(nonatomic,copy)NSString * idstr;
@property(nonatomic,copy)NSString * contentstr;
@property(nonatomic,copy)NSString * pointstr;

@property(nonatomic,copy)NSString * contentArrStr;
@property(nonatomic,copy)NSString * idArrStr;
@property(nonatomic,copy)NSString * pointArrStr;

@property(nonatomic,strong)NSArray * oldcontentArr;
@property(nonatomic,strong)NSArray * oldpointArr;

@property(nonatomic,strong)NSMutableArray * newidmuArr;
@property(nonatomic,strong)NSMutableArray * newcontentArr;
@property(nonatomic,strong)NSMutableArray * newpointArr;
@end

@implementation GoodsEvaluationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBar];
    _dataArray = [NSMutableArray new];
    
    _newcontentArr = [NSMutableArray new];
    _newidmuArr = [NSMutableArray new];
    _newpointArr = [NSMutableArray new];
    
    self.title = @"评价";
    self.view.backgroundColor = APPBGColor;
    [self initView];
    [self addGoodInfo];
    NSLog(@"%@",_goodsEvaluationDataArr);
    
}


-(void)addGoodInfo{
    for ( NSDictionary * dic in _goodsEvaluationDataArr) {
        _evaluationId = dic[@"id"];
        DealOrderModel * model = [DealOrderModel new];
        [model setValuesForKeysWithDictionary:dic];
        [_dataArray addObject:model];
    }
    [_tableView reloadData];
   
}

-(void)initView{
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-50)];
    _scrollView.backgroundColor = APPBGColor;
    _scrollView.bounces = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.scrollEnabled = YES;
    [self.view addSubview:_scrollView];
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, _scrollView.frame.size.height)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView  = [[UIView alloc]initWithFrame:CGRectZero];
    _tableView.backgroundColor = APPBGColor;
    _tableView.separatorStyle = NO;
    [_tableView registerClass:[EvaluationTC class] forCellReuseIdentifier:[EvaluationTC getID]];
    [_scrollView addSubview:_tableView];
    
    UIView *btnView = [[UIButton  alloc]init];
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        btnView.frame = CGRectMake(0, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-64, Screen_Width, 44);
    }else{
        btnView.frame = CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-44, Screen_Width, 44);
    }
    [self.view addSubview:btnView];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 1)];
    lineView.backgroundColor = APPBGColor;
    [btnView addSubview:lineView];
    
    UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, Screen_Width, 49)];
    [btn setTitle:@"评论" forState:(UIControlStateNormal)];
    [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    btn.backgroundColor = APPColor;
    btn.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_L];
    [btn addTarget:self action:@selector(evaluationBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [btnView addSubview:btn];
}

-(void)evaluationBtnClick:(UIButton *)btn{
    [MBProgressHUD showHUD];
    
    if (_newcontentArr.count>0) {
        [self submitevaluate];
    }else{
        [MBProgressHUD dissmiss];
         [MBProgressHUD showError:@"请给出评价" toView:self.view];
    }
    

}

//提交评价
-(void)submitevaluate{
    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    
    _evaluationId = [self returnJsonString:_newidmuArr];
    _contentstr = [self returnJsonString:_newcontentArr];
    _pointstr = [self returnJsonString:_newpointArr];
    
    NSString * evaluateUrl = [NSString stringWithFormat:@"%@ctl=dp&act=add_order_dp&ids=%@&contents=%@&points=%@&email=%@&pwd=%@",FONPort,_evaluationId,_contentstr,_pointstr,email,pwd];
    NSLog(@"evaluateurl -- %@",evaluateUrl);
  
    [RequestData requestDataOfUrl:evaluateUrl success:^(NSDictionary *dic) {
        
        if ([dic[@"status"] isEqual:@1]) {
            
             [[NSNotificationCenter defaultCenter]postNotificationName:@"refundAction" object:nil];
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self popVC];
            });
            
        }
        [MBProgressHUD dissmiss];
        [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
        
        
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
    }];
}
-(NSString *)returnJsonString:(NSArray *)arr{
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    return jsonStr;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _cell = [tableView dequeueReusableCellWithIdentifier:[EvaluationTC getID]];
    if (_cell == nil) {
        _cell = [[EvaluationTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[EvaluationTC getID]];
    }
    _cell.delegate = self;
    _cell.selectionStyle =  UITableViewCellSelectionStyleNone;
    DealOrderModel * model = _dataArray[indexPath.row];
    _cell.model = model;
    _cell.index = indexPath.row;
    return _cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_evaluaTextView isFirstResponder];
}


#pragma mark passdatadelegate

//new
-(void)passDataForIds:(NSString *)ids point:(NSString *)pointString withContent:(NSString *)contents{
    
    BOOL isnew = NO;
    NSInteger i = 0;
    for (NSInteger j = 0; j<_newidmuArr.count; j++) {
        if ([_newidmuArr[j] isEqualToString:ids]) {
            i = j;
            isnew = YES;
        }
    }
    if (isnew == YES) {
        [_newidmuArr removeObject:_newidmuArr[i]];
        [_newpointArr removeObject:_newpointArr[i]];
        [_newcontentArr removeObject:_newcontentArr[i]];
    }
    [_newpointArr addObject:pointString];
    [_newidmuArr addObject:ids];
    [_newcontentArr addObject:contents];
    
}

-(void)keyUp:(UITextView *)textview with:(NSInteger)index{
    
    self.navigationController.navigationBarHidden = NO;
    
    
    CGFloat rects = self.tableView.frame.size.height - (216 +50 +240*index+textview.frame.size.height);
    
    NSLog(@"aa%f",rects);
    
    if (rects <= 0) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = self.view.frame;
            
            frame.origin.y = rects;
            
            self.tableView.frame = CGRectMake(0, 0, Screen_Width, _tableView.frame.size.height-216-20);
            
        }];
    }
}
-(void)keydown{
    CGRect frame = self.tableView.frame;
    
    frame.origin.y = 64;
    
    self.tableView.frame = CGRectMake(0, 0, Screen_Width,Screen_Height-64-50);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
