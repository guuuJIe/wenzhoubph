//
//  OrderListVC.h
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface OrderListVC : BaseVC
@property(nonatomic,copy)NSString *typeTitle;
@property(nonatomic,copy)NSString * pushId;
/**
 *  isscore 1:积分  0:普通
 */
@property(nonatomic,assign)NSInteger isScore;
@end
