//
//  RefundVC.m
//  519
//
//  Created by Macmini on 16/12/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RefundVC.h"
#import "RefundTC.h"
#import "DealOrderModel.h"
@interface RefundVC ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,keyBoredUpDelegate,UIScrollViewDelegate>
@property(nonatomic,strong)UIButton * backBtn;
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)UIView * refundView;

@property(nonatomic,copy)NSString * email;
@property(nonatomic,copy)NSString * pwd;
@property(nonatomic,strong)NSMutableArray * dataMuArr;
@property(nonatomic,copy)NSString * item_ids;
@property(nonatomic,copy)NSString * itemidsstr;
@property(nonatomic,copy)NSString * contents;
@property(nonatomic,strong)NSMutableArray * idsArr;
@property(nonatomic,strong)NSMutableArray * contentArr;

@end

@implementation RefundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBar];
    _dataMuArr = [NSMutableArray new];
    self.idsArr = [NSMutableArray new];
    self.contentArr = [NSMutableArray new];
    
    _email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    _pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    
    self.title = @"退款申请";
    self.view.backgroundColor = APPBGColor;
    
    [self addData];
}

-(void)addData{
    NSString * refundUrl = [NSString stringWithFormat:@"%@&ctl=uc_order&act=refund&order_id=%@&email=%@&pwd=%@",FONPort,_orderId,_email,_pwd];
    WeakSelf(self);
    [RequestData requestDataOfUrl:refundUrl success:^(NSDictionary *dic) {
        StrongSelf(self);
        self.refundArr = dic[@"items"];
        for (NSDictionary * subdic in dic[@"items"]) {
            DealOrderModel * model = [DealOrderModel new];
            [model setValuesForKeysWithDictionary:subdic];
            [self.dataMuArr addObject:model];
        }
        [self initView];
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)initView{

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-50)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerClass:[RefundTC class] forCellReuseIdentifier:[RefundTC getID]];
    self.tableView.backgroundColor=APPBGColor;
    [self.view addSubview:_tableView];
    
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        _refundView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-64, Screen_Width, 44)];
    }else{
        _refundView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-44, Screen_Width, 44)];
    }
    
    
    _refundView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_refundView];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 0.5)];
    lineView.backgroundColor = APPBGColor;
    [_refundView addSubview:lineView];
    
    UIButton * sureBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, _refundView.frame.size.height)];
    [sureBtn setBackgroundColor:APPColor];
    [sureBtn setTitle:@"提交" forState:(UIControlStateNormal)];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_L];
    [sureBtn addTarget:self action:@selector(runUp) forControlEvents:(UIControlEventTouchUpInside)];
    [_refundView addSubview:sureBtn];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 240;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _refundArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RefundTC * cell = [tableView dequeueReusableCellWithIdentifier:[RefundTC getID]];
    if (cell == nil) {
        cell = [[RefundTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[RefundTC getID]];
    }
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    DealOrderModel * model = _dataMuArr[indexPath.row];
    cell.model = model;
    cell.index = indexPath.row;
    return cell;
}
-(void)runUp{
    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    [self changeArrayToJson];
    NSString * refundUrl = [NSString stringWithFormat:@"%@ctl=Uc_order&act=do_refund&item_ids=%@&contents=%@&email=%@&pwd=%@",FONPort,_itemidsstr,_contents,email,pwd];
   
    NSLog(@"--%ld,%@,%@",(unsigned long)_contents.length,_itemidsstr,_contents);
    if(_itemidsstr){
        if ([_contents rangeOfString:@"退款原因"].location != NSNotFound || _contents.length<5) {
            [MBProgressHUD showError:@"请输入退款理由" toView:self.view];
        }else{
            [RequestData requestDataOfUrl:refundUrl success:^(NSDictionary *dic) {
                if ([dic[@"status"] isEqual:@1]) {
                    [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"refundAction" object:nil];
                    double delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [self popVC];
                    });
                }else{
                    [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
            }];
        }
    }else{
        [MBProgressHUD showError:@"请选择商品" toView:self.view];
    }
}


#pragma mark refundDelegate
-(void)addBtnRefundId:(NSString *)refundId withContent:(NSString *)content{
    if (content) {
        [_contentArr addObject:content];
    }
    BOOL isId = NO;
    for (NSInteger i = 0; i<_idsArr.count; i++) {
        if ([_idsArr[i] isEqualToString:refundId]) {
            isId = YES;
        }
    }
    if (isId == NO) {
        [_idsArr addObject:refundId];
    }
}
-(void)removeRefundId:(NSString *)refundId withContent:(NSString *)content{
    [_contentArr removeObject:content];
    [_idsArr removeObject:refundId];
}

-(void)changeArrayToJson{
    
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:_idsArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    
    NSData *datas=[NSJSONSerialization dataWithJSONObject:_contentArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStrs=[[NSString alloc]initWithData:datas encoding:NSUTF8StringEncoding];
    
    _itemidsstr = jsonStr;
    _contents = jsonStrs;
}



-(void)keyUp:(UITextView *)textview with:(NSInteger)index{
    
    self.navigationController.navigationBarHidden = NO;
    
    
    CGFloat rects = self.tableView.frame.size.height - (216 +50 +240*index+textview.frame.size.height);
    
    NSLog(@"aa%f",rects);
    
    if (rects <= 0) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = self.view.frame;
            
            frame.origin.y = rects;
            
            self.tableView.frame = CGRectMake(0, 0, Screen_Width, self.tableView.frame.size.height-216-20);
            
        }];
    }
}
-(void)keydown{
    CGRect frame = self.tableView.frame;
    
    frame.origin.y = 64;
    
    self.tableView.frame = CGRectMake(0, 0, Screen_Width,Screen_Height-64-50);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
