//
//  OrderDetailVC.m
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderDetailVC.h"
#import "GoodsDetailModel.h"
#import "OrderDetailTC.h"
#import "AddressVC.h"
#import "OrderPayVC.h"
#import "RefundVC.h"
#import "GoodsEvaluationVC.h"
#import "WuLiuViewController.h"
#import "MainWebView.h"
@interface OrderDetailVC ()<UITableViewDelegate ,UITableViewDataSource,UIAlertViewDelegate>
@property(nonatomic,strong)UIScrollView * scrollView;
@property(nonatomic,strong)UIView * addressView;
@property(nonatomic,strong)UIImageView * img;
@property(nonatomic,strong)UILabel * addAddressLabel;
@property(nonatomic,strong)UILabel * nameLabel;
@property(nonatomic,strong)UILabel * phoneLabel;
@property(nonatomic,strong)UILabel * addressLabel;
@property(nonatomic,strong)NSMutableArray * goodsDetailMuArr;


@property(nonatomic,strong)UITableView * tableView;

@property(nonatomic,strong)UIView * priceView;
@property(nonatomic,strong)UILabel * payPriceLabel;
@property(nonatomic,strong)UILabel * totalPriceLabel;
@property(nonatomic,strong)UILabel * freightLabel;
@property(nonatomic,strong)UIView * line;
@property(nonatomic,strong)UIView * line2;

@property(nonatomic,strong)UIView * detailView;
@property(nonatomic,strong)NSArray * detailArray;

@property(nonatomic,strong)UIView * btnView;

@property(nonatomic,strong)UIButton *backBtn;
@property(nonatomic,copy)NSString * orderId;
@property(nonatomic,copy)NSString * email;
@property(nonatomic,copy)NSString * pwd;
@property(nonatomic,strong)NSArray * detailArr;
//优惠券
@property(nonatomic,strong)UILabel * youhuiquanLabel;

//商城
@property(nonatomic,strong)UIView * storeview;
@property(nonatomic,strong)UILabel * storename;
@property(nonatomic,strong)UILabel * storephone;
@property(nonatomic,strong)UILabel * storeaddress;

//订单状态
@property(nonatomic,copy)NSString *pay_status;
@property(nonatomic,copy)NSString *items_refund_status;
@property(nonatomic,copy)NSString *keyi_dianpin;
@property(nonatomic,copy)NSString *order_status;
@property(nonatomic,copy)NSString *order_wuliu;

@property(nonatomic,copy)NSString *order_sn;
//二维码
@property(nonatomic,strong)UIView * erwmView;
@property(nonatomic,strong)UIImageView * imageView;
@property(nonatomic,strong)UILabel *conponLabel;
@property(nonatomic,assign)NSInteger type;
@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [MBProgressHUD showHUD];
    
    _goodsDetailMuArr = [NSMutableArray new];
    
    self.title = @"订单详情";
    self.view.backgroundColor = APPBGColor;
    [self initView];
    [self initBar];
    [self requestdataForOrderDetail];
}

-(BOOL)navigationShouldPopOnBackButton{
    [self.navigationController popToRootViewControllerAnimated:YES];
    return NO;
}
-(void)requestdataForOrderDetail{
    _email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    _pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    
    NSString * orderDetail = [NSString stringWithFormat:@"%@ctl=uc_order&act=view&id=%@&email=%@&pwd=%@",FONPort,_orderDetailId,_email,_pwd];
    WeakSelf(self);
    [RequestData requestDataOfUrl:orderDetail success:^(NSDictionary *dic) {
        StrongSelf(self);
        if (dic[@"consignee"]) {
            self.nameLabel.text = [NSString stringWithFormat:@"联系人:%@",dic[@"consignee"]];
            self.addressLabel.text = dic[@"address"];
            self.phoneLabel.text = dic[@"mobile"];
            self.addAddressLabel.hidden = YES;
        }else{
            self.nameLabel.hidden = YES;
            self.addressLabel.hidden = YES;
            self.phoneLabel.hidden = YES;
            self.addAddressLabel.hidden = NO;
        }
        
        self.detailArr = dic[@"deal_order_item"];
        for ( NSDictionary * subdic in dic[@"deal_order_item"]) {
            GoodsDetailModel * model = [GoodsDetailModel new];
            [model setValuesForKeysWithDictionary:subdic];
            [self.goodsDetailMuArr addObject:model];
        }
        
        double total_score = [NSString stringWithFormat:@"%@",dic[@"return_total_score"]].doubleValue;
        if (fabs(total_score)>0) {
            self.totalPriceLabel.text = [NSString stringWithFormat:@"%.0lf积分",fabs(total_score)];
            self.payPriceLabel.text = [NSString stringWithFormat:@"%.0lf积分",fabs(total_score)];
            
        }else{
            self.totalPriceLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"deal_total_price"]];
            self.payPriceLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"total_price"]];
        }
        self.youhuiquanLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"youhui_fee"]];
        self.freightLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"delivery_fee"]];
        
        self.storename.text = dic[@"shop_info"][@"name"];
        self.storephone.text = dic[@"shop_info"][@"tel"];
        self.storeaddress.text = dic[@"shop_info"][@"address"];
        
        self.pay_status = [NSString stringWithFormat:@"%@",dic[@"pay_status"]];
        self.items_refund_status = [NSString stringWithFormat:@"%@",dic[@"items_refund_status"]];
        self.keyi_dianpin = [NSString stringWithFormat:@"%@",dic[@"keyi_dianpin"]];
        self.order_status = [NSString stringWithFormat:@"%@",dic[@"order_status"]];
        self.order_wuliu = [NSString stringWithFormat:@"%@",dic[@"wuliu_btn"]];
        
        NSString * payTimer;
        if ([dic[@"pay_time"] isEqual:[NSNull null]]){
            payTimer = @"未支付";
        }else{
            payTimer = [self timerStampString:dic[@"pay_time"]];
        }
        
        NSString * deleveryTimer;
        if ([dic[@"delivery_time"] isEqualToString:@"0"]) {
            deleveryTimer = @"未配送";
        }else{
            deleveryTimer = [self timerStampString:dic[@"delivery_timer"]];
        }
        
        NSString * createTimer = [self timerStampString:dic[@"create_time"]];
        self.order_sn = dic[@"order_sn"];
        self.type = [dic[@"order_type"] integerValue];
        if ([dic[@"order_type"] integerValue] == 3) {
//            self.erwmView.frame = CGRectMake(0, self.detailView.frame.size.height+self.detailView.frame.origin.y+Default_Space, Screen_Width, 180);
             self.detailArray = @[dic[@"order_id"],createTimer,payTimer,dic[@"delivery_name"],dic[@"memo"]];
            self.conponLabel.hidden = false;
            self.conponLabel.text = [NSString stringWithFormat:@"团购券:%@",dic[@"order_id"]];
//            self.conponLabel.width = 130;
//            self.conponLabel.center = CGPointMake(Screen_Width/2, 130+40/2);
            [self.conponLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.imageView);
                make.top.equalTo(self.imageView.mas_bottom).offset(8);
            }];
        }else{
             self.detailArray = @[dic[@"order_sn"],createTimer,payTimer,dic[@"delivery_name"],dic[@"memo"]];
            self.conponLabel.hidden = true;
            self.conponLabel.height = 0;
//            self.erwmView.frame = CGRectMake(0, self.detailView.frame.size.height+self.detailView.frame.origin.y+Default_Space, Screen_Width, 150);
//
        }
        
       
        
//        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width , self.erwmView.frame.size.height+self.erwmView.frame.origin.y+Default_Space) ;
//        self.scrollView.scrollEnabled = YES;
        self.orderId = [NSString stringWithFormat:@"%@",dic[@"order_id"]];
        [self showdata];
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        
    }];
}

-(NSString *)timerStampString:(NSString *)timer{
    NSString *publishString = @"";
    
    double publishLong = [timer doubleValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
    
    NSDate *publishDate = [NSDate dateWithTimeIntervalSince1970:publishLong+28800];
    
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:date];
    publishDate = [publishDate  dateByAddingTimeInterval: interval];
    
    publishString = [formatter stringFromDate:publishDate];
    
    return publishString;
}


-(void)initView{
    
//    head地址
    _addressView  = [[UIView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 72)];
    _addressView.backgroundColor = [UIColor whiteColor ];
    _img = [[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, (self.addressView.frame.size.height-20)/2, 20, 20)];
    _img.image = [UIImage imageNamed:@"location_black_icon"];
    
    self.addAddressLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,(self.addressView.frame.size.height-20)/2,self.addressView.frame.size.width-20-20-Default_Space*4,20)];
    self.addAddressLabel.text = @"请添加地址";
    self.addAddressLabel.hidden = YES;
    self.addAddressLabel.textColor = APPFourColor;
    self.addAddressLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.img.frame.size.width+_img.frame.origin.x+Default_Space, Default_Space, 150, 20)];
    self.nameLabel.textColor=APPFourColor;
    self.nameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-150, Default_Space, 120, 20)];
    self.phoneLabel.textAlignment = NSTextAlignmentRight;
    self.phoneLabel.textColor = APPFourColor;
    self.phoneLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.img.frame.size.width+self.img.frame.origin.x+Default_Space, self.nameLabel.frame.size.height+self.nameLabel.frame.origin.y+Default_Space, self.addressView.frame.size.width-20-20-Default_Space*4, 20)];
    self.addressLabel.textColor=APPThreeColor;
    self.addressLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];

    
    [_addressView addSubview:_img];
    [_addressView addSubview:_addAddressLabel];
    [_addressView addSubview:_nameLabel];
    [_addressView addSubview:_phoneLabel];
    [_addressView addSubview:_addressLabel];
    
    
    
   //商品详情
    _tableView = [[UITableView alloc]init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator=NO;
    self.tableView.scrollEnabled=NO;
    [_tableView registerClass:[OrderDetailTC class] forCellReuseIdentifier:[OrderDetailTC getID]];
    
    //价格
    _priceView = [[UIView alloc]init];
    _priceView.backgroundColor = [UIColor whiteColor];
    UILabel * totallabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 150, 20)];
    totallabel.text = @"应付总额:";
    totallabel.textAlignment = NSTextAlignmentLeft;
    totallabel.textColor = APPFourColor;
    totallabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _payPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-150, Default_Space, 140, 20)];
    _payPriceLabel.textAlignment = NSTextAlignmentRight;
    _payPriceLabel.textColor = APPColor;
    _payPriceLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _line = [[UIView alloc]initWithFrame:CGRectMake(0, totallabel.frame.size.height+totallabel.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    _line.backgroundColor = APPBGColor;
    
    UILabel * goodsAllPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, _line.frame.size.height+_line.frame.origin.y+Default_Space, 150, 20)];
    goodsAllPriceLabel.text = @"商品总额:";
    goodsAllPriceLabel.textAlignment = NSTextAlignmentLeft;
    goodsAllPriceLabel.textColor = APPFourColor;
    goodsAllPriceLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _totalPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-150, goodsAllPriceLabel.frame.origin.y, 140, 20)];
    _totalPriceLabel.textAlignment = NSTextAlignmentRight;
    _totalPriceLabel.textColor = APPFourColor;
    _totalPriceLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _line2 = [[UIView alloc]initWithFrame:CGRectMake(0, goodsAllPriceLabel.frame.size.height+goodsAllPriceLabel.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    _line2.backgroundColor = APPBGColor;
    UILabel * youhuiquanLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, _line2.frame.size.height+_line2.frame.origin.y+Default_Space, 150, 20)];
    youhuiquanLabel.text = @"优惠券:";
    youhuiquanLabel.textAlignment = NSTextAlignmentLeft;
    youhuiquanLabel.textColor = APPFourColor;
    youhuiquanLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _youhuiquanLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-150, youhuiquanLabel.frame.origin.y, 140, 20)];
    _youhuiquanLabel.textAlignment = NSTextAlignmentRight;
    _youhuiquanLabel.textColor = APPFourColor;
    _youhuiquanLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    UIView *line3 = [[UIView alloc]initWithFrame:CGRectMake(0, youhuiquanLabel.frame.size.height+youhuiquanLabel.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    line3.backgroundColor = APPBGColor;
    
    UILabel * freightTextlabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, line3.frame.size.height+line3.frame.origin.y+Default_Space, 150, 20)];
    freightTextlabel.text = @"运费";
    freightTextlabel.textAlignment = NSTextAlignmentLeft;
    freightTextlabel.textColor = APPFourColor;
    freightTextlabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _freightLabel = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width-150, freightTextlabel.frame.origin.y, 140, 20)];
    _freightLabel.textAlignment = NSTextAlignmentRight;
    _freightLabel.textColor = APPFourColor;
    _freightLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    
    [_priceView addSubview:totallabel];
    [_priceView addSubview:_payPriceLabel];
    [_priceView addSubview:_line];
    [_priceView addSubview:goodsAllPriceLabel];
    [_priceView addSubview:_totalPriceLabel];
    [_priceView addSubview:_line2];
    [_priceView addSubview:youhuiquanLabel];
    [_priceView addSubview:_youhuiquanLabel];
    [_priceView addSubview:line3];
   
    [_priceView addSubview:freightTextlabel];
    [_priceView addSubview:_freightLabel];
    
    
    //商城
    _storeview  = [[UIView alloc]init];
    _storeview.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(storePhoneClick)];
    [_storeview addGestureRecognizer:tap];
    
    _storename = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width/2, 40)];
    _storename.textColor = APPFourColor;
    _storename.textAlignment = NSTextAlignmentLeft;
    _storename.font = [UIFont systemFontOfSize:FONT_SIZE_XL];
    [_storeview addSubview:_storename];
    
    _storephone = [[UILabel alloc]initWithFrame:CGRectMake(Screen_Width/2, Default_Space, Screen_Width/2-10, 40)];
    _storephone.textColor = APPThreeColor;
    _storephone.textAlignment = NSTextAlignmentRight;
    _storephone.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    [_storeview addSubview:_storephone];
    
    _storeaddress = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, _storename.frame.size.height+_storename.frame.origin.y, Screen_Width-20, 40)];
    _storeaddress.textColor = APP9EColor;
    _storeaddress.textAlignment = NSTextAlignmentLeft;
    _storeaddress.numberOfLines = 0;
    _storeaddress.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    [_storeview addSubview:_storeaddress];
    
    //end
    _detailView = [[UIView alloc]init];
    _detailView.backgroundColor = [UIColor whiteColor];
    
    //
    _erwmView = [[UIView alloc]init];
    _erwmView.backgroundColor = [UIColor whiteColor];
    
    _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 130, 130)];
    _imageView.center = CGPointMake(Screen_Width/2, 150/2);
    [_erwmView addSubview:_imageView];
    

    
    _conponLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_imageView.frame), 150, 20)];
    _conponLabel.center = CGPointMake(Screen_Width/2-10, 130+30/2);
    _conponLabel.text = @"团购券:";
    _conponLabel.textAlignment = NSTextAlignmentLeft;
    _conponLabel.textColor = APP9EColor;
    _conponLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    [_erwmView addSubview:_conponLabel];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-50)];
    
    [self.view addSubview:_scrollView];
    
    [_scrollView addSubview:_addressView];
    [_scrollView addSubview:_tableView];
    [_scrollView addSubview:_priceView];
    [_scrollView addSubview:_storeview];
    [_scrollView addSubview:_detailView];
    [_scrollView addSubview:_erwmView];
    
    UIView * bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        bgView.frame = CGRectMake(0, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-64, Screen_Width, 64);
        _btnView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-64, Screen_Width, 44)];
    }else{
        bgView.frame = CGRectMake(0, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-44, Screen_Width, 44);
        _btnView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-44, Screen_Width, 44)];
    }
    
    
    _btnView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_btnView];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Line)];
    lineView.backgroundColor = APPThreeColor;
    [_btnView addSubview:lineView];

    [self showdata];
}

-(void)showdata{
    
    _tableView.frame = CGRectMake(0, _addressView.frame.size.height+_addressView.frame.origin.y+Default_Space, Screen_Width, 100*_goodsDetailMuArr.count);
    _priceView.frame = CGRectMake(0, _tableView.frame.size.height+_tableView.frame.origin.y+Default_Space, Screen_Width, 160);
    _storeview.frame = CGRectMake(0, _priceView.frame.size.height+_priceView.frame.origin.y+Default_Space, Screen_Width, 100);
    _detailView.frame = CGRectMake(0, _storeview.frame.size.height+_storeview.frame.origin.y+Default_Space, Screen_Width, 170);
 
    if (self.type == 3) {
         _erwmView.frame = CGRectMake(0, _detailView.frame.size.height+_detailView.frame.origin.y+Default_Space, Screen_Width, 180);
    }else{
         _erwmView.frame = CGRectMake(0, _detailView.frame.size.height+_detailView.frame.origin.y+Default_Space, Screen_Width, 150);
    }
    
    if ([self.pay_status isEqualToString:@"0"]) {
        _erwmView.frame = CGRectMake(0, _detailView.frame.size.height+_detailView.frame.origin.y+Default_Space, Screen_Width, 0);
        _imageView.hidden = true;
        _conponLabel.hidden = true;
    }
   
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width , _erwmView.frame.size.height+_erwmView.frame.origin.y+Default_Space) ;
    _scrollView.scrollEnabled = YES;
    
    NSArray * array = @[@"订单编号 : ",@"下单时间 : ",@"付款时间 : ",@"配送方式 : ",@"备注 : "];
    for (NSInteger i = 0; i<array.count; i++) {
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, i*30, _detailView.frame.size.width, 30)];
        [_detailView addSubview:view];
        
        UILabel * leftlabel = [[UILabel alloc]initWithFrame:CGRectMake(0, Default_Space, 75, 20)];
        leftlabel.text = array[i];
        leftlabel.textAlignment = NSTextAlignmentRight;
        leftlabel.textColor = APP9EColor;
        leftlabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
        [view addSubview:leftlabel];
        
        UILabel * rightLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftlabel.frame.size.width+leftlabel.frame.origin.x+5, Default_Space, view.frame.size.width-100-10, 20)];
        rightLabel.text = _detailArray[i];
        rightLabel.textAlignment = NSTextAlignmentLeft;
        rightLabel.textColor = APPThreeColor;
        rightLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
        [view addSubview:rightLabel];
    }
    
    
    
    [self boringErwm:_detailArray.firstObject];
    
    NSLog(@"model %@ %@ %@ %@",_pay_status,_items_refund_status,_order_status,_keyi_dianpin);
    NSArray * btnTitArr;
    if ([_pay_status isEqualToString:@"0"]) {
        btnTitArr=[[NSArray alloc] initWithObjects:@"删除订单",@"立即支付",@"联系客服", nil];
        if ([_order_wuliu isEqualToString:@"1"]) {
             btnTitArr=[[NSArray alloc] initWithObjects:@"删除订单",@"立即支付",@"联系客服", nil];
        }
    }else
    if([_pay_status isEqualToString:@"2"] && [_items_refund_status isEqualToString:@"1"] && [_order_status isEqualToString:@"0"] ){
        
        btnTitArr=[[NSArray alloc] initWithObjects:@"我要退款",@"联系客服", nil];
        if ([_order_wuliu isEqualToString:@"1"]) {
            btnTitArr=[[NSArray alloc] initWithObjects:@"我要退款",@"查看物流",@"联系客服", nil];
        }
    }else
    if([_keyi_dianpin isEqualToString:@"1"] && [_order_status isEqualToString:@"0"]){
        btnTitArr=[[NSArray alloc] initWithObjects:@"去评价",@"联系客服", nil];
        if ([_order_wuliu isEqualToString:@"1"]) {
            btnTitArr=[[NSArray alloc] initWithObjects:@"去评价",@"联系客服",@"查看物流", nil];
        }
    }else{
        if ([_order_wuliu isEqualToString:@"1"]) {
            btnTitArr=[[NSArray alloc] initWithObjects:@"联系客服",@"查看物流", nil];
        }else{
            btnTitArr=[[NSArray alloc] initWithObjects:@"联系客服", nil];
        }
        
    }
    
    for (NSInteger i = 0; i<btnTitArr.count; i++) {
        
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-80*(i+1), Default_Space, 70, 30)];
        [btn setTitle:btnTitArr[i] forState:(UIControlStateNormal)];
        btn.backgroundColor = APPColor;
        [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        btn.layer.cornerRadius = 5;
        btn.layer.masksToBounds = YES;
        btn.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
        [btn addTarget:self action:@selector(btnOnClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [_btnView addSubview:btn];
    }
    
    [_tableView reloadData];
}


-(void)clickAddressView{
    AddressVC * vc = [AddressVC new];
    vc.pushStr = @"0";
    __weak typeof (self)weself = self;
    vc.pushBlock = ^(NSString * name,NSString * phone,NSString * address,NSString * ids){
        weself.nameLabel.hidden = NO;
        weself.addressLabel.hidden = NO;
        weself.phoneLabel.hidden = NO;
        weself.addAddressLabel.hidden = YES;
        weself.nameLabel.text = name;
        weself.addressLabel.text = address;
        weself.phoneLabel.text = phone;
    };
    [self tabHidePushVC:vc];
}
-(void)btnOnClick:(UIButton *)btn{
    if ([btn.titleLabel.text isEqualToString:@"立即支付"]) {
        OrderPayVC * vc = [OrderPayVC new];
        vc.paytype = @"nowPay";
        vc.orderId = _orderId;
        [self tabHidePushVC:vc];
    }
    if ([btn.titleLabel.text isEqualToString:@"删除订单"]) {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"是否删除订单" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView show];
       
    }
    if ([btn.titleLabel.text isEqualToString:@"我要退款"]) {
        RefundVC * vc = [RefundVC new];
        vc.orderId = _orderId;
        vc.refundArr = _detailArr;
        [self tabHidePushVC:vc];
    }
    if ([btn.titleLabel.text isEqualToString:@"去评价"]) {
         NSLog(@"_detailArr -- %@",_detailArr);
        GoodsEvaluationVC * vc = [GoodsEvaluationVC new];
        vc.goodsEvaluationDataArr = _detailArr;
//        vc.evaluationId = _orderId;
        [self tabHidePushVC:vc];
    }
    if ([btn.titleLabel.text isEqualToString:@"联系客服"]) {
        MainWebView * vc = [[MainWebView alloc]init];
        vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=xiaomi&deal_id=%@&order_sn=%@",EMAIL,[[UIDevice currentDevice] model],@"",self.order_sn];
        [self tabHidePushVC:vc];
//        QYSource *source = [[QYSource alloc] init];
//        source.title =  EMAIL;
//        source.urlString = @"https://8.163.com/";
//        QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
//        sessionViewController.sessionTitle = @"泊啤汇客服";
//        sessionViewController.source = source;
//        sessionViewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:sessionViewController animated:YES];
//        if (EMAIL) {
//            sessionViewController.staffId = [SALES_OID intValue];
//        }else{
//            sessionViewController.groupId = 880528;
//        }
//        QYCommodityInfo *commodityInfo = [[QYCommodityInfo alloc] init];
//        commodityInfo.title = @"订单号";
//        commodityInfo.desc = self.order_sn;
//        commodityInfo.show = YES; //访客端是否要在消息中显示商品信息，YES代表显示,NO代表不显示
//        sessionViewController.commodityInfo = commodityInfo;
//        [[QYSDK sharedSDK] customUIConfig].bottomMargin = 2;
    }
    if ([btn.titleLabel.text isEqualToString:@"查看物流"]) {
        WuLiuViewController * vc = [[WuLiuViewController alloc]init];
        vc.orderId = _orderId;
        [self tabHidePushVC:vc];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _goodsDetailMuArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderDetailTC * cell = [tableView dequeueReusableCellWithIdentifier:[OrderDetailTC getID]];
    if (cell == nil) {
        cell = [[OrderDetailTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[OrderDetailTC getID]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GoodsDetailModel * model = _goodsDetailMuArr[indexPath.row];
    cell.model = model;
    
    return cell;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        if (alertView.tag == 4100) {
            NSString *allString = [NSString stringWithFormat:@"tel:%@",self.storephone.text];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
        }else{
             NSString * indexStr = [NSString stringWithFormat:@"%ld",(long)_index];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"deleteOrder" object:indexStr];
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [self deleteOrder];
            });
            [self popVC];
        }
    }
}
-(void)deleteOrder{
    NSString * deleteUrl = [NSString stringWithFormat:@"%@ctl=uc_order&act=cancel&id=%@&email=%@&pwd=%@",FONPort,_orderId,_email,_pwd];
    [RequestData requestDataOfUrl:deleteUrl success:^(NSDictionary *dic) {

        if (dic[@"info"]) {
            [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
        }
    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
}

-(void)storePhoneClick{
    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"是否拨打电话" message:self.storephone.text delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertview.tag = 4100;
    [alertview show];
   
}



//二维码生成
-(void)boringErwm:(NSString *)orderNumber{
    if (orderNumber) {
        
        // 1.创建滤镜
        CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        
        // 2.还原滤镜默认属性
        [filter setDefaults];
        
        // 3.设置需要生成二维码的数据到滤镜中
        // OC中要求设置的是一个二进制数据
        NSData *data = [orderNumber dataUsingEncoding:NSUTF8StringEncoding];
        [filter setValue:data forKeyPath:@"InputMessage"];
        
        // 4.从滤镜从取出生成好的二维码图片
        CIImage *ciImage = [filter outputImage];
        
        _imageView.layer.shadowOffset = CGSizeMake(0, 0.5);  // 设置阴影的偏移量
        _imageView.layer.shadowRadius = 1;  // 设置阴影的半径
        _imageView.layer.shadowColor = [UIColor blackColor].CGColor; // 设置阴影的颜色为黑色
        _imageView.layer.shadowOpacity = 0.3; // 设置阴影的不透明度
        
        _imageView.image = [self createNonInterpolatedUIImageFormCIImage:ciImage size: 500];
    }
}


- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)ciImage size:(CGFloat)widthAndHeight
{
    CGRect extentRect = CGRectIntegral(ciImage.extent);
    CGFloat scale = MIN(widthAndHeight / CGRectGetWidth(extentRect), widthAndHeight / CGRectGetHeight(extentRect));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extentRect) * scale;
    size_t height = CGRectGetHeight(extentRect) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    
    CIContext *context = [CIContext contextWithOptions:nil];
    
    CGImageRef bitmapImage = [context createCGImage:ciImage fromRect:extentRect];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extentRect, bitmapImage);
    
    // 保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    
    UIImage *newImage = [UIImage imageWithCGImage:scaledImage];
    CGImageRelease(scaledImage);
    return [self imageBlackToTransparent:newImage withRed:0.0 andGreen:0.0 andBlue:0.0];
}

#pragma mark - 图片透明度
void ProviderReleaseData (void *info, const void *data, size_t size){
    free((void*)data);
}
- (UIImage*)imageBlackToTransparent:(UIImage*)image withRed:(CGFloat)red andGreen:(CGFloat)green andBlue:(CGFloat)blue{
    const int imageWidth = image.size.width;
    const int imageHeight = image.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), image.CGImage);
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++){
        if ((*pCurPtr & 0xFFFFFF00) < 0x99999900)    // 将白色变成透明
        {
            // 改成下面的代码，会将图片转成想要的颜色
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = red; //0~255
            ptr[2] = green;
            ptr[1] = blue;
        }
        else
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[0] = 0;
        }
    }
    // 输出图片
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    // 清理空间
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    return resultUIImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
