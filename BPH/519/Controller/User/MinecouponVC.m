//
//  MinecouponVC.m
//  519
//
//  Created by Macmini on 2018/12/15.
//  Copyright © 2018年 519. All rights reserved.
//

#import "MinecouponVC.h"
#import "ConponView.h"
#import "MineConponTC.h"
#import "GoodInfoVC.h"
#import "GoodListVC.h"
#import "UITableView+Animated.h"
@interface MinecouponVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic , strong) ConponView *conponView;
@property (nonatomic , strong) UITableView *listTableView;
@property (nonatomic , assign) NSInteger index;
@property (nonatomic , strong) NSMutableArray *dataArr;
@property (nonatomic,assign)NSInteger pageIndex;
@end

@implementation MinecouponVC

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的优惠券";
    self.view.backgroundColor = RGB(240, 240, 240);
    _index = 0;
    _pageIndex = 1;
    [self setuplayout];
    [self.listTableView.mj_header beginRefreshing];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//         [self requestMineConponData];
//    });
   
}

- (void)setuplayout{
    [self.view addSubview:self.conponView];
    [self.conponView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.conponView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *rid = @"MineConponTC";
    MineConponTC *cell = [tableView dequeueReusableCellWithIdentifier:rid];
    if (!cell) {
        cell = [[MineConponTC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rid];
    }
    
//    if (tableView.animatedStyle != TABTableViewAnimationStart) {
         [cell setupdata:self.dataArr[indexPath.row]];
//    }
    
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *rowData = self.dataArr[indexPath.row];
   
    NSString *goodsId = [NSString stringWithFormat:@"%@",rowData[@"deal_ids"]];
    if ([goodsId integerValue] == 0) {
        GoodListVC *vc = [GoodListVC new];
        [self tabHidePushVC:vc];
    }else{
        GoodInfoVC *vc = [GoodInfoVC new];
        vc.goodsId = goodsId;
        [self tabHidePushVC:vc];
    }
    
//    if (self.clickBlock) {
//        self.clickBlock(data);
//    }
}

- (UITableView *)listTableView{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] init];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 60;
        [_listTableView registerClass:[MineConponTC class] forCellReuseIdentifier:@"MineConponTC"];
        _listTableView.backgroundColor = [UIColor clearColor];
        WeakSelf(self);
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        header.lastUpdatedTimeLabel.hidden = YES;
        _listTableView.mj_header = header;
       
        _listTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            weakself.pageIndex = weakself.pageIndex+1;
            [self requestMineConponData];
        }];
//        _listTableView.animatedStyle = TABTableViewAnimationStart;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (void)loadData{
    _pageIndex = 1;
    [self requestMineConponData];
}

- (void)requestMineConponData{
    NSString *requestUrl = [NSString stringWithFormat:@"%@ctl=uc_youhui&used=%ld&page=%ld",FONPort,(long)_index,(long)_pageIndex];
    WeakSelf(self);
    [RequestData requestDataOfUrl:requestUrl success:^(NSDictionary *dic) {
        if ([dic[@"status"] isEqual:@0]) {
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }else{
            NSArray *conponData = dic[@"item"];
            if (weakself.pageIndex == 1) {
                [weakself.dataArr removeAllObjects];
                
                self.dataArr = [NSMutableArray arrayWithArray:conponData];
                
            }else{
                if (conponData.count == 0) {
                    self.pageIndex -- ;
                    [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                }else{
                    [self.dataArr addObjectsFromArray:conponData];
                }
               
            }
//            // 停止动画,并刷新数据
//            self.listTableView.animatedStyle = TABTableViewAnimationEnd;
            [self.listTableView reloadData];
        }
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
    }];
    
    
}

- (ConponView *)conponView
{
    if (!_conponView) {
        _conponView = [ConponView new];
        WeakSelf(self);
        _conponView.clickBlock = ^(NSInteger tag) {
            BLog(@"%ld",(long)tag);
            weakself.index = tag - 200;
            [weakself.listTableView.mj_header beginRefreshing];
        };
    }
    return _conponView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
