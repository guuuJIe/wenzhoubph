//
//  UserInfoVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "UserInfoVC.h"
#import "PasswordVC.h"
#import "base64.h"
@interface UserInfoVC()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;

@property(nonatomic,strong)UIView *imgView;
@property(nonatomic,strong)UILabel *imgTitleLabel;
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UIImageView *imgRightImg;

@property(nonatomic,strong)UIActionSheet *imgSheet;

@property(nonatomic,strong)UIView *nameView;
@property(nonatomic,strong)UILabel *nameTitleLabel;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UIImageView *nameRightImg;

@property(nonatomic,strong)UIView *pwdView;
@property(nonatomic,strong)UILabel *pwdTitleLabel;
@property(nonatomic,strong)UIImageView *pwdRightImg;

@property(nonatomic,strong)UIView *phoneView;
@property(nonatomic,strong)UILabel *phoneTitleLabel;
@property(nonatomic,strong)UILabel *phoneLabel;
@property(nonatomic,strong)UIImageView *phoneRightImg;

@property (nonatomic,copy)NSString * emailStr;
@end

@implementation UserInfoVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.emailStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];

    
    self.title = @"个人资料";
    [self initView];
     [self initBar];
    [self requestDataForUserInfo];
}

-(void)requestDataForUserInfo{
    NSString * userInfoUrl = [NSString stringWithFormat:@"%@ctl=uc_account&email=%@&pwd=%@",FONPort,self.emailStr,self.infouserPwd];
    
    [RequestData requestDataOfUrl:userInfoUrl success:^(NSDictionary *dic) {
         self.nameLabel.text = [NSString stringWithFormat:@"%@",dic[@"user_info"][@"user_name"]];
        self.phoneLabel.text = [NSString stringWithFormat:@"%@",dic[@"user_info"][@"mobile"]];
        [self showData];
    } failure:^(NSError *error) {
        NSLog(@"userinfo %@",error);
    }];
}


-(void)initView{
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, Screen_Height- self.barView.frame.origin.y-self.barView.frame.size.height)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=APPBGColor;
    
    self.imgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 60)];
    self.imgView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer *imgViewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgViewOnClick)];
    [self.imgView addGestureRecognizer:imgViewTap];
    
    self.imgTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space*2, self.imgView.frame.size.width-Default_Space*2, 20)];
    self.imgTitleLabel.text=@"头像";
    self.imgTitleLabel.textColor=APPFourColor;
    self.imgTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.img=[[UIImageView alloc] initWithFrame:CGRectMake(self.imgView.frame.size.width-Default_Space-20-Default_Space-40, Default_Space, 40, 40)];
    self.img.layer.cornerRadius=self.img.frame.size.width/2;
    self.img.layer.masksToBounds=YES;
    
    self.imgRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.img.frame.origin.x+self.img.frame.size.width+Default_Space, Default_Space*2, 20, 20)];
    self.imgRightImg.image=[UIImage imageNamed:@"right_black_icon"];
    
    [self.imgView addSubview:self.img];
    [self.imgView addSubview:self.imgTitleLabel];
    [self.imgView addSubview:self.imgRightImg];
    
    self.imgSheet = [[UIActionSheet alloc]
                      initWithTitle:nil
                      delegate:self
                      cancelButtonTitle:@"取消"
                      destructiveButtonTitle:nil
                      otherButtonTitles:nil,nil];
    for (NSString *str in [[NSArray alloc] initWithObjects:@"相机拍照",@"相册选择", nil]){
        [self.imgSheet addButtonWithTitle:str];
    }
    self.imgSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    
    self.nameView=[[UIView alloc]initWithFrame:CGRectMake(0, self.imgView.frame.size.height+Default_Line, Screen_Width, 40)];
    self.nameView.backgroundColor=[UIColor whiteColor];
    
    NSString *nameTitleStr=@"昵称";
    UIFont *nameTitleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.nameTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, [NSString sizeWithText:nameTitleStr font:nameTitleFont maxSize:Max_Size].width, 20)];
    self.nameTitleLabel.text=nameTitleStr;
    self.nameTitleLabel.textColor=APPFourColor;
    self.nameTitleLabel.font=nameTitleFont;
//
    self.nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.nameTitleLabel.frame.size.width+self.nameTitleLabel.frame.origin.x+Default_Space, Default_Space, self.nameView.frame.size.width-self.nameTitleLabel.frame.size.width-Default_Space*2-Default_Space-Default_Space-20, 20)];
    self.nameLabel.textColor=APPFourColor;
    self.nameLabel.textAlignment=NSTextAlignmentRight;
    self.nameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.imgRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.nameLabel.frame.origin.x+self.nameLabel.frame.size.width+Default_Space, Default_Space, 20, 20)];
    self.imgRightImg.image=[UIImage imageNamed:@"right_black_icon"];

    [self.nameView addSubview:self.nameTitleLabel];
    [self.nameView addSubview:self.nameLabel];
    [self.nameView addSubview:self.imgRightImg];
    
    self.pwdView=[[UIView alloc]initWithFrame:CGRectMake(0, self.nameView.frame.size.height+self.nameView.frame.origin.y+Default_Line, Screen_Width, 40)];
    self.pwdView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer * pwdtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePwd)];
    [self.pwdView addGestureRecognizer:pwdtap];
    
    NSString *pwdTitleStr=@"修改密码";
    UIFont *pwdTitleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.pwdTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, [NSString sizeWithText:pwdTitleStr font:pwdTitleFont maxSize:Max_Size].width, 20)];
    self.pwdTitleLabel.text=pwdTitleStr;
    self.pwdTitleLabel.textColor=APPFourColor;
    self.pwdTitleLabel.font=pwdTitleFont;
    
    self.pwdRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.pwdView.frame.size.width-Default_Space-20, Default_Space, 20, 20)];
    self.pwdRightImg.image=[UIImage imageNamed:@"right_black_icon"];
    
    [self.pwdView addSubview:self.pwdTitleLabel];
    [self.pwdView addSubview:self.pwdRightImg];
    
    
    self.phoneView=[[UIView alloc]initWithFrame:CGRectMake(0, self.pwdView.frame.origin.y+self.pwdView.frame.size.height+Default_Line, Screen_Width, 40)];
    self.phoneView.backgroundColor=[UIColor whiteColor];
    
    NSString *phoneTitleStr=@"绑定手机号";
    UIFont *phoneTitleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.phoneTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, [NSString sizeWithText:phoneTitleStr font:phoneTitleFont maxSize:Max_Size].width, 20)];
    self.phoneTitleLabel.text=phoneTitleStr;
    self.phoneTitleLabel.textColor=APPFourColor;
    self.phoneTitleLabel.font=nameTitleFont;
    
    self.phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.phoneTitleLabel.frame.size.width+self.phoneTitleLabel.frame.origin.x+Default_Space, Default_Space, self.phoneView.frame.size.width-self.phoneTitleLabel.frame.size.width-Default_Space*2-Default_Space-Default_Space-20, 20)];
    self.phoneLabel.textColor=APPFourColor;
    self.phoneLabel.textAlignment=NSTextAlignmentRight;
    
    self.phoneLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.phoneRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.phoneLabel.frame.origin.x+self.phoneLabel.frame.size.width+Default_Space, Default_Space, 20, 20)];
    self.phoneRightImg.image=[UIImage imageNamed:@"right_black_icon"];
    
    [self.phoneView addSubview:self.phoneTitleLabel];
    [self.phoneView addSubview:self.phoneLabel];
    [self.phoneView addSubview:self.phoneRightImg];
    
    
    UIButton * backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.phoneView.frame)+10, Screen_Width-20, 30)];
    [backBtn setTitle:@"退出账号 " forState:(UIControlStateNormal)];
    [backBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [backBtn setBackgroundColor:APPColor];
    [backBtn addTarget:self action:@selector(backUser) forControlEvents:(UIControlEventTouchUpInside)];
    backBtn.layer.cornerRadius = 5;
    backBtn.layer.masksToBounds = YES;
    
    [self.scrollView addSubview:self.imgView];
    [self.scrollView addSubview:self.nameView];
    [self.scrollView addSubview:self.pwdView];
    [self.scrollView addSubview:self.phoneView];
    [self.scrollView addSubview:backBtn];
    [self.view addSubview:self.scrollView];
    
//    [self loadData];
}
-(void)backUser{
    [MBProgressHUD showHUD];
    NSString * loginOutUrl = [NSString stringWithFormat:@"%@&ctl=user&act=loginout",FONPort];
    
    [RequestData requestDataOfUrl:loginOutUrl success:^(NSDictionary *dic) {
        NSString * appDomain =[ [NSBundle mainBundle]bundleIdentifier];
        [[NSUserDefaults standardUserDefaults]removePersistentDomainForName: appDomain];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"loginOut" object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refershUserMessage" object:nil];
        
        //七鱼注销
//        [[QYSDK sharedSDK] logout:^(){}];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [MBProgressHUD dissmiss];
        
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}
-(void)loadData{

    [self showData];
}

-(void)showData{
    self.img.image = self.infoImage;

}


#pragma mark
-(void)imgViewOnClick{
    [self.imgSheet showInView:self.view];
}

-(void)changePwd{
    [self tabHidePushVC:[[PasswordVC alloc]init]];
}


#pragma mark UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        //拍照
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *picker = [UIImagePickerController new];
            picker.delegate = self;
            picker.allowsEditing = YES;
            //摄像头
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }if (buttonIndex == 2) {
        //进入图库
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController *picker = [UIImagePickerController new];
            picker.delegate = self;
            picker.allowsEditing = YES;
            //打开相册
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:nil];
            
        }
    }
}

//点击保存时调用的方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString* ,id>* )info{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    UIImage * image = [info objectForKey:UIImagePickerControllerEditedImage];
    [self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
}
//点击取消是调用的方法
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)saveImage:(UIImage *)image{
    self.img.image = image;
    
    NSString * uploadStr = [NSString stringWithFormat:@"%@ctl=uc_account&act=upload_avatar&email=%@&pwd=%@",FONPort,self.emailStr,self.infouserPwd];
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:uploadStr parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData * imageData;
        if (UIImagePNGRepresentation(self.img.image) == nil) {
            imageData = UIImageJPEGRepresentation(self.img.image, 1);
        }else{
            imageData = UIImagePNGRepresentation(self.img.image);
        }
        NSDateFormatter * formattter = [NSDateFormatter new];
        formattter.dateFormat = @"yyyyMMddHHmmss";
        NSString * str = [formattter stringFromDate:[NSDate date]];
        NSString * fileName = [NSString stringWithFormat:@"%@.jpg",str];
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpg"];
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *base64Decoded = [[NSString alloc]
                                   initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSData * jsondatastring = [Base64 decodeString:base64Decoded];
        
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsondatastring options:NSJSONReadingMutableLeaves error:nil];
        
        NSLog(@"yes -- %@",dic);
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refershUserMessage" object:nil];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"图片上传出错" message:[NSString stringWithFormat:@"%@",error] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        
        [alert show];
        
        NSLog(@"no -- %@",error);
        
    }];
}
@end
