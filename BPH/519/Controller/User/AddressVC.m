//
//  AddressVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddressVC.h"
#import "AddressTC.h"
#import "AddressEditVC.h"
#import "AddressModel.h"

@interface AddressVC()<UITableViewDataSource,UITableViewDelegate,AddressTCDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)UIButton *backBtn;
@property(nonatomic,strong)UIButton *barRightBtn;

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray * addressMuArray;
@property(nonatomic,copy)NSString * email;
@property(nonatomic,copy)NSString * pwd;
@property(nonatomic,copy)NSMutableArray * btnMuarr;

@property(nonatomic,assign)NSInteger removeinter;
@property(nonatomic,strong)NSString * removeIds;
@end

@implementation AddressVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    _addressMuArray = [NSMutableArray new];
    _btnMuarr = [NSMutableArray new];
    
    _email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    _pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    
    [self initBar];
    
    _barRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0,88,44)];
    [_barRightBtn setTitle:@"新增" forState:(UIControlStateNormal)];
    [_barRightBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    _barRightBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_barRightBtn addTarget:self action:@selector(barItemBtnOnClick) forControlEvents:(UIControlEventTouchUpInside)];
    _barRightBtn.contentHorizontalAlignment =UIControlContentHorizontalAlignmentRight;
    [_barRightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0,0,0,5 *Screen_Width/375.0)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_barRightBtn];
    
    [self initView];
    [self requestDataOfAddress];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(editUserInfo) name:@"editImageOnClick" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshAddress) name:@"saveAddress" object:nil];
}

-(void)editUserInfo{
    AddressEditVC *vc=[[AddressEditVC alloc] init];
    vc.typeStr=@"编辑地址";
    [self tabHidePushVC:vc];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"editImageOnClick" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"saveAddress" object:nil];
}

-(void)initBar{
    [super initBar];
    self.title = @"地址管理";
}

//请求地址
-(void)requestDataOfAddress{

    NSString * addressUrl = [NSString stringWithFormat:@"%@ctl=uc_address&email=%@&pwd=%@",FONPort,_email,_pwd];
    [RequestData requestDataOfUrl:addressUrl success:^(NSDictionary *dic) {
        if (dic[@"consignee_list"] != nil) {
            for (NSDictionary * subdic in dic[@"consignee_list"]) {
                AddressModel * model = [AddressModel new];
                [model setValuesForKeysWithDictionary:subdic];
                [self.addressMuArray addObject:model];
            }
        }
        [self showData];
        
    } failure:^(NSError *error) {
        
    }];
}

//-(void)initBar{
//    [super initBar];
//
//    [self barViewAddTitle:@"地址管理"];
//
//    self.backBtn=[[UIButton alloc]init];
//    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"back_white_icon"] forState:UIControlStateNormal];
//    [self.backBtn setImage:[UIImage imageNamed:@"back_select"] forState:(UIControlStateHighlighted)];
//    [self.backBtn addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
//    [self barViewAddLeftBtn:self.backBtn];
//
//    self.barRightBtn=[[UIButton alloc]init];
//    [self.barRightBtn setTitle:@"新增" forState:UIControlStateNormal];
//    self.barRightBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
//    self.barRightBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
//    [self.barRightBtn addTarget:self action:@selector(barItemBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
//    [self barViewAddRightBtn:self.barRightBtn];
//}

-(void)initView{
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight)];
    [self.tableView registerClass:[AddressTC class] forCellReuseIdentifier:[AddressTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.backgroundColor=APPBGColor;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshAddress)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    
    [self.view addSubview:self.tableView];
    
    
}

-(void)refreshAddress{
    [self.addressMuArray removeAllObjects];
    [self.btnMuarr removeAllObjects];
    [self requestDataOfAddress];
}

-(void)showData{
    [self.tableView.mj_header endRefreshing];
    
    [self.tableView reloadData];

}


#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.addressMuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressTC *cell=[tableView dequeueReusableCellWithIdentifier:[AddressTC getID] forIndexPath:indexPath];
    
    if(cell==nil){
        cell=[[AddressTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[AddressTC getID]];
    }
    cell.delegate=self;
    AddressModel * model = _addressMuArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = model;
    [_btnMuarr addObject:cell.defueltBtn];
    
    cell.cellIndex = indexPath.row;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [AddressTC getHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.pushStr isEqualToString:@"0"]) {
        
        AddressModel * model = _addressMuArray[indexPath.row];
        NSString * address = [NSString stringWithFormat:@"%@ %@ %@ %@",model.region_lv2_name,model.region_lv3_name,model.region_lv4_name,model.address];
        self.pushBlock(model.consignee,model.mobile,address,model.ids);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self setDefaultIndex:indexPath.row withIds:model.ids];
        });
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
//        AddressModel * model = _addressMuArray[indexPath.row];
//        AddressEditVC *vc=[[AddressEditVC alloc] init];
//        vc.typeStr=@"编辑地址";
//        vc.nameStr = model.consignee;
//        vc.phoneStr = model.mobile;
//        vc.isdefualt = model.is_default;
//        vc.regoinStr = [NSString stringWithFormat:@"%@ %@ %@",model.region_lv2_name,model.region_lv3_name,model.region_lv4_name];
//        vc.addressStr = model.address;
//        vc.ids = model.ids;
//        vc.isedit = @"1";
//        [self tabHidePushVC:vc];
    }
    
}


#pragma mark bar delegate
-(void)barItemBtnOnClick{
    AddressEditVC *vc=[[AddressEditVC alloc] init];
    vc.typeStr=@"添加地址";
    vc.isedit = @"0";
    [self tabHidePushVC:vc];
}



-(void)editImgOnClicksWithIndex:(NSInteger)cellIndex Ids:(NSString *)ids name:(NSString *)name phone:(NSString *)phone address:(NSString *)address region:(NSString *)region isdefault:(NSString *)isdefault{
    AddressEditVC *vc=[[AddressEditVC alloc] init];
    vc.typeStr=@"编辑地址";
    
    vc.nameStr = name;
    vc.phoneStr = phone;
    vc.isdefualt = isdefault;
    vc.regoinStr = region;
    vc.addressStr = address;
    vc.ids = ids;
    vc.isedit = @"1";
    
    [self tabHidePushVC:vc];
}
//删除地址
-(void)delectOnClickWithIndex:(NSInteger)cellIndex withIds:(NSString *)ids{
    _removeinter = cellIndex;
    _removeIds = ids;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否确定删除" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [_addressMuArray removeObjectAtIndex:_removeinter];
        [self.tableView reloadData];
        
        NSString * deleteUrl = [NSString stringWithFormat:@"%@ctl=uc_address&act=del&id=%@&email=%@&pwd=%@",FONPort,_removeIds,_email,_pwd];
        [RequestData requestDataOfUrl:deleteUrl success:^(NSDictionary *dic) {
            
            if ([dic[@"status"] isEqual:@1]) {
                [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD showError:@"网络错误" toView:self.view];
        }];
    }
}

-(void)setDefaultIndex:(NSInteger)cellIndex withIds:(NSString *)ids{
        NSLog(@"cellindex %d",cellIndex);
        for (NSInteger i = 0; i<_btnMuarr.count; i++) {
            UIButton * btn = _btnMuarr[i];
            if (i != cellIndex) {
                btn.selected = NO;
                btn.layer.borderColor = APPFourColor.CGColor;
                [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
                btn.size = CGSizeMake(70, 20);
            }else{
                btn.selected = YES;
                btn.layer.borderColor = APPColor.CGColor;
                [btn setTitleColor:APPColor forState:(UIControlStateNormal)];
                btn.size = CGSizeMake(50, 20);
            }
        }


    
    NSString * defUrl = [NSString stringWithFormat:@"%@ctl=uc_address&act=set_default&id=%@&email=%@&pwd=%@",FONPort,ids,_email,_pwd];
    [RequestData requestDataOfUrl:defUrl success:^(NSDictionary *dic) {
        
    } failure:^(NSError *error) {
        
    }];
}


@end
