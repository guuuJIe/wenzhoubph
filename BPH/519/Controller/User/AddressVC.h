//
//  AddressVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface AddressVC : BaseVC
@property(nonatomic,copy)void(^pushBlock)(NSString * name,NSString * phone,NSString * address,NSString * ids);
@property(nonatomic,copy)NSString * pushStr;
@end
