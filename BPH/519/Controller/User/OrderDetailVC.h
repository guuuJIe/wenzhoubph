//
//  OrderDetailVC.h
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface OrderDetailVC : BaseVC
@property(nonatomic,copy)NSString * orderDetailId;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,copy)NSString * vcName;

@end
