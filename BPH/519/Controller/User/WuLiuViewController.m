//
//  WuLiuViewController.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/7.
//  Copyright © 2018年 519. All rights reserved.
//

#import "WuLiuViewController.h"
#import "AddressModel.h"
@interface WuLiuViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UIView * headView;
@property (nonatomic,strong)UILabel * wuliuLabel;
@property (nonatomic,strong)UILabel * numberLabel;
@property (nonatomic,strong)NSMutableArray * dataScore;

@property (nonatomic,copy)NSString * numberStr;

@end

@implementation WuLiuViewController
-(NSMutableArray *)dataScore{
    if (!_dataScore) {
        _dataScore = [[NSMutableArray alloc]init];
    }
    return _dataScore;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"查看物流";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self crateHeadView];
    [self requestData];
}


-(void)crateHeadView{
    
    [self.view addSubview:self.tableView];
    
    
    _headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 120)];
    [self.view addSubview:_headView];
    
    UIImageView * iconImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 40, 40)];
    iconImg.image = [UIImage imageNamed:@"飞机"];
    [_headView addSubview:iconImg];
    
    _wuliuLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconImg.frame)+10, 10, Screen_Width-70, 20)];
    _wuliuLabel.textColor = APPFourColor;
    _wuliuLabel.textAlignment = NSTextAlignmentLeft;
    _wuliuLabel.font = [UIFont systemFontOfSize:14];
    [_headView addSubview:_wuliuLabel];
    
    _numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconImg.frame)+10, CGRectGetMaxY(_wuliuLabel.frame), Screen_Width/2, 20)];
    _numberLabel.textColor = APPFourColor;
    _numberLabel.textAlignment = NSTextAlignmentLeft;
    _numberLabel.font = [UIFont systemFontOfSize:14];
    [_headView addSubview:_numberLabel];
    
    UIButton * copyBtn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-70, CGRectGetMinY(_numberLabel.frame), 50, 20)];
    [copyBtn setTitle:@"复制" forState:(UIControlStateNormal)];
    [copyBtn addTarget:self action:@selector(copyNumber) forControlEvents:(UIControlEventTouchUpInside)];
    [copyBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    copyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    copyBtn.layer.cornerRadius = 5;
    copyBtn.layer.masksToBounds = YES;
    copyBtn.layer.borderWidth = 1;
    copyBtn.layer.borderColor = APPThreeColor.CGColor;
    [_headView addSubview:copyBtn];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_numberLabel.frame)+10, Screen_Width, 10)];
    lineView.backgroundColor = APPBGColor;
    [_headView addSubview:lineView];
    
    UILabel * titlelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lineView.frame)+20, 100, 20)];
    titlelabel.text = @"订单跟踪";
    titlelabel.textColor = APPFourColor;
    titlelabel.textAlignment = NSTextAlignmentLeft;
    titlelabel.font = [UIFont boldSystemFontOfSize:14];
    [_headView addSubview:titlelabel];
    _tableView.tableHeaderView = _headView;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.dataScore.count<1) {
        return 1;
    }
    return self.dataScore.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataScore.count>0) {
        WuliuModel * model = self.dataScore[indexPath.row];
        CGFloat height = [self returnContentHeight:model.context withWidth:Screen_Width-60];
        return  80+height;
    }
    return 200;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * wuliuIde = @"wuliuCellIde";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:wuliuIde];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:wuliuIde];
    }else{
        for (UIView * subView in cell.subviews) {
            [subView removeFromSuperview];
        }
    }
    if (self.dataScore.count<1) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 200)];
        label.numberOfLines = 0;
        label.text = @"暂无物流信息";
        label.textColor = APPFourColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:14];
        [cell addSubview:label];
    }else{
        WuliuModel * model = self.dataScore[indexPath.row];
        UIImageView * listImgV = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 20)];
        [cell addSubview:listImgV];
        
        UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(20, 31, 1, 70)];
        lineView.backgroundColor = APPBGColor;
        [cell addSubview:lineView];
        
        CGFloat height = [self returnContentHeight:model.context withWidth:Screen_Width-60];
        UILabel * contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, Screen_Width-60, height)];
        contentLabel.numberOfLines = 0;
        contentLabel.text = model.context;
        contentLabel.textColor = APPFourColor;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        contentLabel.font = [UIFont systemFontOfSize:14];
        [cell addSubview:contentLabel];
        
        UILabel * timelabel = [[UILabel alloc]initWithFrame:CGRectMake(40, CGRectGetMaxY(contentLabel.frame)+5, Screen_Width-60, 20)];
        timelabel.text = model.time;
        contentLabel.textColor = APPFourColor;
        timelabel.textAlignment = NSTextAlignmentLeft;
        timelabel.font = [UIFont systemFontOfSize:14];
        [cell addSubview:timelabel];
        if (indexPath.row == 0) {
            contentLabel.textColor = APPColor;
            timelabel.textColor = APPColor;
            listImgV.image = [UIImage imageNamed:@"对号"];
        }else{
            listImgV.frame = CGRectMake(18, 10, 4, 4);
            listImgV.backgroundColor = APPBGColor;
            listImgV.layer.cornerRadius = 2;
            listImgV.layer.masksToBounds = YES;
        }
    }
    
    
    return cell;
}


-(void)copyNumber{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.numberStr;
}


-(void)requestData{
    NSString * url = [NSString stringWithFormat:@"%@ctl=uc_order&act=check_delivery&order_id=%@",FONPort,_orderId];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        if ([dic[@"ok"] isEqual:@1]) {
            self.wuliuLabel.text = [NSString stringWithFormat:@"物流公司:%@",dic[@"name"]];
            self.numberStr = dic[@"number"];
            self.numberLabel.text = [NSString stringWithFormat:@"物流单号:%@",dic[@"number"]];
            for (NSDictionary * subdic in dic[@"data"]) {
                WuliuModel * model = [[WuliuModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [self.dataScore addObject:model];
            }
            [self.tableView reloadData];
            
            
        }
        else{
            [MBProgressHUD showError:dic[@"message"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

-(void)showNoteDate{
    
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-self.navigationController.navigationBar.frame.size.height) style:(UITableViewStylePlain)];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return _tableView;
}


-(CGFloat)returnContentHeight:(NSString *)contentStr withWidth:(CGFloat)width{
    CGSize strSize = [contentStr boundingRectWithSize:CGSizeMake(width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size;
    return strSize.height;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
