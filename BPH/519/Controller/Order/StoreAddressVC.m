//
//  StoreAddressVC.m
//  519
//
//  Created by Macmini on 16/12/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "StoreAddressVC.h"
#import "StoreAddressTC.h"
#import "StoreAddressModel.h"
#import "MapNavigatorVC.h"

@interface StoreAddressVC ()<UITableViewDelegate,UITableViewDataSource,BMKLocationManagerDelegate,LocationNavigatorDelegate>
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSMutableArray * dataMuArray;
@property(nonatomic,strong)UIButton *backBtn;
//定位
@property(nonatomic,strong)BMKLocationManager * locService;
@property(nonatomic,assign)float x;
@property(nonatomic,assign)float y;
@end

@implementation StoreAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择门店";
    [MBProgressHUD showHUD];
    [self isOpenLocation];
    [self initBar];
    [self initView];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_locService stopUpdatingLocation];
}

-(void)initView{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight)];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[StoreAddressTC class] forCellReuseIdentifier:[StoreAddressTC getId]];
    [self.view addSubview:self.tableView];
}

-(void)isOpenLocation{
    //判断定位是否可用
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
        //定位功能可用
        _locService = [[BMKLocationManager alloc]init];
        _locService.delegate = self;
        //设置返回位置的坐标系类型
        _locService.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        //设置距离过滤参数
        _locService.distanceFilter = kCLDistanceFilterNone;
        //设置预期精度参数
        _locService.desiredAccuracy = kCLLocationAccuracyBest;
        //设置应用位置类型
        _locService.activityType = CLActivityTypeAutomotiveNavigation;
        WS(ws);
        [_locService requestLocationWithReGeocode:0 withNetworkState:0 completionBlock:^(BMKLocation * _Nullable location, BMKLocationNetworkState state, NSError * _Nullable error) {
            [ws didUpDataLocation:location];
        }];
    }else if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        
        //定位不能用
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"stop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self requestDataForStoreAddress];
    }
}

-(void)requestDataForStoreAddress{
    if (self.dataMuArray) {
        [self.dataMuArray removeAllObjects];
    }else{
        self.dataMuArray = [NSMutableArray new];
    }
    if(!_x && !_y){
        _x = 0 ;
        _y = 0 ;
    }
    NSString * fwUrl = [NSString stringWithFormat:@"%@ctl=shop&act=index&city_shop_id=%@&xpoint=%f&ypoint=%f",FONPort,_storeCityId,_x,_y];
    [RequestData requestDataOfUrl:fwUrl success:^(NSDictionary *dic) {
       
        if ([dic[@"status"] isEqual:@1]) {
            for (NSDictionary * subdic in dic[@"shop_list"]) {
                StoreAddressModel * model = [[StoreAddressModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [self.dataMuArray addObject:model];
            }
            [MBProgressHUD dissmiss];
            [self.tableView reloadData];
        }else{
            [MBProgressHUD dissmiss];
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.dataMuArray.count>0) {
        return self.dataMuArray.count;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreAddressTC * cell = [tableView dequeueReusableCellWithIdentifier:[StoreAddressTC getId]];
    cell.delegate = self;
    if (cell == nil) {
        cell = [[StoreAddressTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[StoreAddressTC getId]];
    }
    cell.selectionStyle = UITableViewCellStyleDefault;
    if (self.dataMuArray.count>0) {
        StoreAddressModel * model = self.dataMuArray[indexPath.row];
        cell.model = model;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreAddressModel * model = self.dataMuArray[indexPath.row];
    NSString * ids = model.ids;
    NSString * name = model.name;
//    [self targetStoreLocation:model.xpoint Ypoint:model.ypoint withPhonenum:model.tel withStorename:model.name];
    NSLog(@"%@",model.ids);
    self.storeAddressBlock(ids,name);
    [self.navigationController popViewControllerAnimated:YES];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSDictionary * storeDic = @{@"xpoint":model.xpoint,@"ypoint":model.ypoint,@"shopName":name,@"tel":model.tel};
        [[NSUserDefaults standardUserDefaults]setObject:storeDic forKey:@"storeInfo"];
        [[NSUserDefaults standardUserDefaults]setObject:model.ids forKey:@"shop_id"];
        [[NSUserDefaults standardUserDefaults] setObject:model.xpoint forKey:@"xPoint"];
        [[NSUserDefaults standardUserDefaults] setObject:model.ypoint forKey:@"yPoint"];
        [[NSUserDefaults standardUserDefaults] setObject:model.name forKey:@"name"];
        [[NSUserDefaults standardUserDefaults] setObject:model.tel forKey:@"tel"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    });
}

#pragma mark location
-(void)targetStoreLocation:(NSString *)xpoint Ypoint:(NSString *)ypoint withPhonenum:(NSString *)phoneNum withStorename:(NSString *)storeName{
    NSDictionary*storedic = [[NSUserDefaults standardUserDefaults]objectForKey:@"storeInfo"];
    MapNavigatorVC *vc = [[MapNavigatorVC alloc] init];
    vc.xpoint = xpoint;
    vc.ypoint = ypoint;
    vc.phoneNum = phoneNum;
    vc.storeName = storeName;
    BaseNC *nav = [[BaseNC alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)didUpDataLocation:(BMKLocation*)location{
    if (!_x || !_y || _x == 0 || _y == 0) {
        _y = location.location.coordinate.latitude;
        _x = location.location.coordinate.longitude;
       
       
        [self requestDataForStoreAddress];
        _locService.delegate = nil;
    }
}

-(void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
