//
//  GoodstypeView.m
//  519
//
//  Created by Macmini on 2018/12/10.
//  Copyright © 2018年 519. All rights reserved.
//

#import "GoodstypeView.h"
@interface GoodstypeView()
@property (nonatomic , strong) UIButton *selectedBtn;
@end
@implementation GoodstypeView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setuplayout];
    }
    return self;
}

- (void)setuplayout{
    
}

- (void)setGoodsTypesArr:(NSArray *)goodsTypesArr{
    _goodsTypesArr = goodsTypesArr;
    UILabel * typeLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 100, 20)];
    typeLabel.textColor=APPFourColor;
    typeLabel.font=[UIFont boldSystemFontOfSize:FONT_SIZE_L];
    typeLabel.text = @"商品规格";
    typeLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:typeLabel];
    
    for (UIView *view in self.subviews) {
        if ([view  isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    CGFloat allWidth = 10;
    NSInteger index = 0;
    for (int i =0 ; i<goodsTypesArr.count; i++) {
        NSDictionary * dic = goodsTypesArr[i];
        CGFloat width = [NSString sizeWithText:dic[@"name"] font:[UIFont systemFontOfSize:14] maxSize:Max_Size].width;
        
        if (allWidth+width >= Screen_Width) {
            allWidth = 10;
            index++;
        }
        
        UIButton * typeBtn = [[UIButton alloc]initWithFrame:CGRectMake(allWidth, 40+index*45, width+10, 30)];
        [typeBtn setTitle:dic[@"name"] forState:(UIControlStateNormal)];
        [typeBtn setTitle:dic[@"name"] forState:(UIControlStateSelected)];
        [typeBtn addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
        typeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        typeBtn.tag = [dic[@"ref_id"] integerValue];
        typeBtn.layer.cornerRadius = 3;
        typeBtn.layer.masksToBounds = YES;
        typeBtn.layer.borderWidth = 1;
        typeBtn.layer.borderColor = APPThreeColor.CGColor;
        [typeBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
//        typeBtn.layer.borderColor = APPColor.CGColor;
        [typeBtn setTitleColor:APPColor forState:(UIControlStateSelected)];
//        if ([self.goodsId isEqualToString:[NSString stringWithFormat:@"%@",dic[@"ref_id"]]]) {
//            typeBtn.layer.borderColor = APPColor.CGColor;
//            [typeBtn setTitleColor:APPColor forState:(UIControlStateNormal)];
//        }else{
//            typeBtn.layer.borderColor = APPThreeColor.CGColor;
//            [typeBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
//        }
        [self addSubview:typeBtn];
        allWidth = allWidth + width+30;
    
    }
    self.index = index;
    /*
    for (int i = 0; i<goodsTypesArr.count; i++) {
        NSDictionary *dic = goodsTypesArr[i];
        UIButton *button = [UIButton new];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitle:dic[@"name"] forState:UIControlStateNormal];
        [button setTitleColor:APPFourColor forState:UIControlStateNormal];
        [button setTitleColor:APPColor forState:UIControlStateSelected];
        button.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
        button.layer.borderColor = APPColor.CGColor;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = 3;
        [button setTitle:dic[@"name"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake([NSString sizeWithText:dic[@"name"] font:[UIFont systemFontOfSize:14] maxSize:Max_Size].width+10, 60/2));
            make.top.equalTo(self).offset(5);
            if(i == 0){
                make.left.equalTo(self).offset(10);
            }else{
                make.left.equalTo(lastView.mas_right).offset(10);
            }
        }];
        lastView = button;
    }*/
}

- (void)setGoodsid:(NSInteger)goodsid{
//    for (NSInteger i = 0; i<self.goodsTypesArr.count; i++) {
//        NSDictionary *dic = self.goodsTypesArr[i];
//        UIButton * btn = [self viewWithTag:[dic[@"ref_id"] integerValue]];
//        btn.layer.borderColor = APPThreeColor.CGColor;
//        [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
//    }
    UIButton *button = [self viewWithTag:goodsid];
    button.selected = YES;
    button.layer.borderColor = APPColor.CGColor;
    [button setTitleColor:APPColor forState:(UIControlStateNormal)];
//    [self buttonClick:button];
}

-(void)buttonClick:(UIButton *)button{
    NSInteger buttonTag = button.tag;
    for (NSInteger i = 0; i<self.goodsTypesArr.count; i++) {
        NSDictionary *dic = self.goodsTypesArr[i];
        UIButton * btn = [self viewWithTag:[dic[@"ref_id"] integerValue]];
        btn.layer.borderColor = APPThreeColor.CGColor;
        [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    }
    button.layer.borderColor = APPColor.CGColor;
    [button setTitleColor:APPColor forState:(UIControlStateNormal)];
//    BLog(@"当前规格%d",buttonTag);
    if (self.clickBlock) {
        self.clickBlock([NSString stringWithFormat:@"%d",buttonTag]);
    }
    
}
@end
