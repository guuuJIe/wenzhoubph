//
//  StoreAddressTC.h
//  519
//
//  Created by Macmini on 16/12/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "StoreAddressModel.h"

@protocol LocationNavigatorDelegate <NSObject>

-(void)targetStoreLocation:(NSString *)xpoint Ypoint:(NSString *)ypoint withPhonenum:(NSString *)phoneNum withStorename:(NSString *)storeName;;

@end

@interface StoreAddressTC : BaseTC
@property (nonatomic,weak)StoreAddressModel * model;
@property (nonatomic,assign)id<LocationNavigatorDelegate>delegate;
+(NSString *)getId;

@end
