//
//  OrderConfirmGoodTC.m
//  519
//
//  Created by 陈 on 16/9/22.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderConfirmGoodTC.h"

@interface OrderConfirmGoodTC()
@property(nonatomic,strong)UIImageView *goodImg;
@property(nonatomic,strong)UILabel *goodNameLabel;
@property(nonatomic,strong)UILabel *goodInfoLabel;
@property(nonatomic,strong)UILabel *goodPriceLabel;
@property(nonatomic,strong)UILabel *goodCountLabel;
@property(nonatomic,strong)UIView *line;
@end

@implementation OrderConfirmGoodTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.goodImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 60, 60)];
    self.goodNameLabel=[[UILabel alloc]init];
    self.goodNameLabel.textColor=APPFourColor;
    self.goodNameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodNameLabel.numberOfLines=0;
    self.goodPriceLabel=[[UILabel alloc]init];
    self.goodPriceLabel.textColor=APPColor;
    self.goodPriceLabel.textAlignment = NSTextAlignmentRight;
    self.goodPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodInfoLabel=[[UILabel alloc]init];
    self.goodInfoLabel.textColor=APPThreeColor;
    self.goodInfoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodInfoLabel.numberOfLines=0;
    self.goodCountLabel=[[UILabel alloc]init];
    self.goodCountLabel.textAlignment = NSTextAlignmentRight;
    self.goodCountLabel.textColor=APPThreeColor;
    self.goodCountLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.line=[[UIView alloc] initWithFrame:CGRectMake(0, [OrderConfirmGoodTC getCellHeight]-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    [self addSubview:self.goodImg];
    [self addSubview:self.goodNameLabel];
    [self addSubview:self.goodPriceLabel];
    [self addSubview:self.goodInfoLabel];
    [self addSubview:self.goodCountLabel];
    [self addSubview:self.line];
}

-(void)setModel:(OrderGoodsModel *)model{
    _model = model;
    
    NSString *priceStr=[NSString stringWithFormat:@"￥%.2lf",[model.unit_price doubleValue]];
    NSString *countStr=@"x1055";
    
    
    
    self.goodNameLabel.frame=CGRectMake(self.goodImg.frame.size.width+Default_Space*2, self.goodImg.frame.origin.y, Screen_Width-self.goodImg.frame.size.width-Default_Space*4-[NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 40);
    
    self.goodPriceLabel.frame=CGRectMake(Screen_Width-[NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width-10, self.goodNameLabel.frame.origin.y, [NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    
    self.goodInfoLabel.frame=CGRectMake(self.goodNameLabel.frame.origin.x,     [OrderConfirmGoodTC getCellHeight]-Default_Space-20,Screen_Width-self.goodImg.frame.size.width-Default_Space*4-[NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    
    self.goodCountLabel.frame=CGRectMake(Screen_Width-Default_Space-[NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, self.goodInfoLabel.frame.origin.y, [NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    
    [_goodImg sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    
    self.goodNameLabel.text=model.name;
    self.goodPriceLabel.text=priceStr;
    self.goodInfoLabel.text=  model.attr_str;
    self.goodCountLabel.text=[NSString stringWithFormat:@"x%@",model.number];
}


-(void)showDataName:(NSString *)name icon:(NSString *)icon price:(NSString *)price number:(NSString *)number speci:(NSString *)speci{
    NSString *priceStr=@"￥1220.8";
    
    NSString *countStr=@"x1055";
    
    [self.goodImg sd_setImageWithURL:[[NSURL alloc] initWithString:icon]];
    
    self.goodNameLabel.frame=CGRectMake(self.goodImg.frame.size.width+Default_Space*2, self.goodImg.frame.origin.y, Screen_Width-self.goodImg.frame.size.width-Default_Space*4-[NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 40);
    
    self.goodPriceLabel.frame=CGRectMake(Screen_Width-Default_Space-[NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, self.goodNameLabel.frame.origin.y, [NSString sizeWithText:price font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    
    self.goodInfoLabel.frame=CGRectMake(self.goodNameLabel.frame.origin.x,     [OrderConfirmGoodTC getCellHeight]-Default_Space-20,Screen_Width-self.goodImg.frame.size.width-Default_Space*4-[NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
  
    self.goodCountLabel.frame=CGRectMake(Screen_Width-Default_Space-[NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, self.goodInfoLabel.frame.origin.y, [NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    
    self.goodNameLabel.text=name;
    self.goodPriceLabel.text=price;
    self.goodInfoLabel.text=speci;
    self.goodCountLabel.text=number;
}

+(CGFloat)getCellHeight{
    return 80;
}

+(NSString *)getID{
    return @"OrderConfirmTC";
}
@end
