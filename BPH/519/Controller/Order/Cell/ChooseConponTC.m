//
//  ChooseConponTC.m
//  519
//
//  Created by Macmini on 2018/12/17.
//  Copyright © 2018年 519. All rights reserved.
//

#import "ChooseConponTC.h"
#import "UIView+Animated.h"
@interface ChooseConponTC()
@property (nonatomic, strong)UIView  *bgview;
@property (nonatomic, strong)UIView  *bgview2;
@property (nonatomic, strong)UILabel *moneylbl;
@property (nonatomic, strong)UILabel *uselbl;
@property (nonatomic, strong)UILabel *zhuanyonglbl;
@property (nonatomic, strong)UILabel *datelbl;
@property (nonatomic, strong)UILabel *limitlbl;
@property (nonatomic, strong)UILabel *limitgoodstypelbl;
@property (nonatomic, strong)UIView *verticalView;
@property (nonatomic, strong)UIView *horizontalView;
@property (nonatomic, strong)UIImageView *lineimg;
@property (nonatomic, strong)UILabel *rejectLbl;
@end

@implementation ChooseConponTC

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = RGB(240, 240, 240);
        [self setuplayout];
    }
    return self;
}

- (void)setuplayout{
    UIView *bgview = [[UIView alloc] init];
    bgview.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bgview];
    [bgview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.top.equalTo(20);
        make.right.equalTo(-15);
//        make.bottom.equalTo(-15);
        //        make.size.mas_equalTo(CGSizeMake(280, 170));
    }];
    bgview.layer.cornerRadius = 20;
    bgview.layer.masksToBounds = true;
    
    self.bgview2 = bgview;
    
    
    [self.moneylbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgview).offset(25);
        make.left.equalTo(bgview).offset(40);
    }];
    
    [self.uselbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.moneylbl);
        make.top.equalTo(self.moneylbl.mas_bottom).offset(10);
    }];
    
    [self.zhuanyonglbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.moneylbl).offset(95);
        make.top.equalTo(bgview).offset(20);
        make.right.equalTo(bgview).offset(-5);
    }];
    
    [self.datelbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.zhuanyonglbl);
        make.top.equalTo(self.zhuanyonglbl.mas_bottom).offset(15);
    }];
    
    [self.limitlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.zhuanyonglbl);
        make.top.equalTo(self.datelbl.mas_bottom).offset(15);
        make.right.equalTo(bgview).offset(-10);
        
    }];
    
    
    //    UIView *Lineimage = [UIView new];
    //    [self.contentView addSubview:Lineimage];
    //    [Lineimage mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.equalTo(bgview);
    //        make.right.equalTo(bgview);
    //        make.height.equalTo(1);
    //        make.top.equalTo(self.limitlbl.mas_bottom).offset(15);
    //        make.width.mas_equalTo(bgview);
    //    }];
    [self.contentView layoutIfNeeded];
    [bgview layoutIfNeeded];
    UIView *Lineimage = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.limitlbl.frame)+20, Screen_Width -15*2, 1)];
    //    Lineimage.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:Lineimage];
    [self drawLineOfDashByCAShapeLayer:Lineimage lineLength:5 lineSpacing:2 lineColor:RGB(237, 237, 237)];
    //
    //    VerticalView *lineview = [[VerticalView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.uselbl.frame)+15, CGRectGetMinY(bgview.frame), 1, CGRectGetMaxY(self.limitlbl.frame)+20) withLineLength:3 withLineSpacing:2 withLineColor:RGB(209, 209, 209)];
    //    VerticalView *lineview = [[VerticalView alloc]init];
    //    lineview.backgroundColor = [UIColor clearColor];
    //    [self.contentView addSubview:lineview];
    UIImageView *img = [UIImageView new];
    img.image = [UIImage imageNamed:@"dash2"];
    [self.contentView addSubview:img];
    self.lineimg = img;
    
    //    self.verticalView = lineview;
    self.horizontalView = Lineimage;
    
    [self.limitgoodstypelbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgview).offset(15);
        make.top.equalTo(self.limitlbl.mas_bottom).offset(35);
        make.right.equalTo(bgview).offset(-15);
        make.bottom.equalTo(bgview).offset(-20);
    }];
    
    self.bgview = [UIView new];
    self.bgview.backgroundColor = [UIColor whiteColor];
    self.bgview.alpha = 0.6;
    [self.contentView addSubview:self.bgview];
    
    [self.bgview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bgview);
    }];
    self.bgview.layer.cornerRadius = 20;
    self.bgview.layer.masksToBounds = true;
    self.bgview.hidden = true;
    
    [self.rejectLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgview).offset(5);
        make.right.equalTo(bgview);
        make.top.equalTo(bgview.mas_bottom).offset(8);
        make.bottom.equalTo(-10);
    }];
}

- (void)setupdata:(NSDictionary *)dataDic{
    self.moneylbl.text = [NSString stringWithFormat:@"¥%@",dataDic[@"price"]];
    self.moneylbl.attributedText = [ConfigHelper getSumString:_moneylbl.text withRange:[_moneylbl.text rangeOfString:@"¥"] withRangeFont:[UIFont systemFontOfSize:20] withOtherFont:[UIFont systemFontOfSize:30] WithOtherFontColor:RGB(255, 0, 1) withRangeColor:RGB(255, 0, 1)];
    self.uselbl.text = [NSString stringWithFormat:@"%@",dataDic[@"man"]];
    self.zhuanyonglbl.text = [NSString stringWithFormat:@"%@",dataDic[@"name"]];
    self.datelbl.text = [NSString stringWithFormat:@"%@",dataDic[@"end_time"]];
    self.limitlbl.text = [NSString stringWithFormat:@"%@",dataDic[@"shop_ids"]];
    self.limitgoodstypelbl.text = [NSString stringWithFormat:@"%@",dataDic[@"note"]];
    
    [self.bgview2 layoutIfNeeded];
    //    [NSString sizeWithText:<#(NSString *)#> font:<#(UIFont *)#> maxSize:<#(CGSize)#>]
    BLog(@"limitlbl---%f",CGRectGetMaxY(self.limitlbl.frame)+20);
    //    self.verticalView.frame = CGRectMake(CGRectGetMaxX(self.uselbl.frame)+15, CGRectGetMinY(self.bgview2.frame), 1, CGRectGetMaxY(self.limitlbl.frame));
    //    self.horizontalView.frame = CGRectMake(15, CGRectGetMaxY(self.limitlbl.frame)+20, Screen_Width -15*2, 1);
    
    [self.horizontalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.limitlbl.mas_bottom).offset(20);
        make.left.equalTo(15);
        make.right.equalTo(-15);
    }];
    [self.lineimg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.uselbl.mas_right).offset(15);
        make.top.equalTo(20);
        make.bottom.equalTo(self.horizontalView.mas_top);
        make.width.equalTo(@12);
        //        make.height.mas_equalTo(CGRectGetMaxY(self.limitlbl.frame)+20);
    }];
    
    //    [self.verticalView drawRect:self.verticalView.frame];
    //    self.verticalView = [[VerticalView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.uselbl.frame)+15, CGRectGetMinY(self.bgview2.frame), 1, CGRectGetMaxY(self.limitlbl.frame)) withLineLength:3 withLineSpacing:2 withLineColor:RGB(209, 209, 209)];
//    if ([dataDic[@"used"] isEqualToString:@"已使用"] || [dataDic[@"used"] isEqualToString:@"已过期"]) {
//        self.bgview.hidden = false;
//    }else{
//        self.bgview.hidden = true;
//    }
    NSString *str = [NSString stringWithFormat:@"%@",dataDic[@"info"]];
    //可用
    if (str.length<1) {
        self.bgview.hidden = true;
        self.rejectLbl.text = @"";
    }else{
        self.bgview.hidden = false;
        self.rejectLbl.text = [NSString stringWithFormat:@"不可用原因:%@",dataDic[@"info"]];
    }
  
    
}

- (UILabel *)moneylbl{
    if (!_moneylbl) {
        _moneylbl = [UILabel new];
        _moneylbl.text = @"¥50";
        _moneylbl.font = [UIFont systemFontOfSize:25];
        _moneylbl.textColor = RGB(255, 0, 1);
        _moneylbl.loadStyle = TABViewLoadAnimationWithOnlySkeleton;
        [self.contentView addSubview:_moneylbl];
    }
    return _moneylbl;
}

- (UILabel *)uselbl{
    if (!_uselbl) {
        _uselbl = [UILabel new];
        _uselbl.text = @"满100可用";
        _uselbl.font = [UIFont systemFontOfSize:15];
        _uselbl.textColor = RGB(45, 45, 45);
        _uselbl.loadStyle = TABViewLoadAnimationWithOnlySkeleton;
        [self.contentView addSubview:_uselbl];
    }
    return _uselbl;
}

- (UILabel *)zhuanyonglbl{
    if (!_zhuanyonglbl) {
        _zhuanyonglbl = [UILabel new];
        _zhuanyonglbl.text = @"专用券";
        _zhuanyonglbl.font = [UIFont boldSystemFontOfSize:20];
        _zhuanyonglbl.textColor = RGB(45, 45, 45);
        _zhuanyonglbl.numberOfLines = 0;
        [self.contentView addSubview:_zhuanyonglbl];
    }
    return _zhuanyonglbl;
}

- (UILabel *)datelbl{
    if (!_datelbl) {
        _datelbl = [UILabel new];
        _datelbl.text = @"有效期至";
        _datelbl.font = [UIFont systemFontOfSize:15];
        _datelbl.textColor = RGB(140, 140, 140);
        _datelbl.loadStyle = TABViewLoadAnimationLong;
        [self.contentView addSubview:_datelbl];
    }
    return _datelbl;
}

- (UILabel *)limitlbl{
    if (!_limitlbl) {
        _limitlbl = [UILabel new];
        _limitlbl.text = @"仅限牛山店";
        _limitlbl.font = [UIFont systemFontOfSize:15];
        _limitlbl.textColor = RGB(45, 45, 45);
        _limitlbl.numberOfLines = 0;
        [self.contentView addSubview:_limitlbl];
    }
    return _limitlbl;
}

- (UILabel *)rejectLbl
{
    if (!_rejectLbl) {
        _rejectLbl = [UILabel new];
        _rejectLbl.text = @"";
        _rejectLbl.numberOfLines = 0;
        _rejectLbl.textColor = RGB(255, 0, 1);
        _rejectLbl.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_rejectLbl];
    }
    return _rejectLbl;
}

- (UILabel *)limitgoodstypelbl{
    if (!_limitgoodstypelbl) {
        _limitgoodstypelbl = [UILabel new];
        _limitgoodstypelbl.text = @"仅限aaa500ml罐，aaa500ml罐，aaa500ml罐，aaa500ml罐，aaa500ml罐，aaa500ml罐，aaa500ml罐，aaa500ml罐，aaa500ml罐";
        _limitgoodstypelbl.font = [UIFont systemFontOfSize:13];
        _limitgoodstypelbl.textColor = RGB(180, 180, 180);
        _limitgoodstypelbl.numberOfLines = 0;
        [self.contentView addSubview:_limitgoodstypelbl];
    }
    return _limitgoodstypelbl;
}

- (void)drawLineOfDashByCAShapeLayer:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor {
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    
    [shapeLayer setBounds:lineView.bounds];
    
    [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame))];
    
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    
    //  设置虚线颜色为blackColor
    
    [shapeLayer setStrokeColor:lineColor.CGColor];
    
    //  设置虚线宽度
    
    [shapeLayer setLineWidth:CGRectGetHeight(lineView.frame)];
    
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    //  设置线宽，线间距
    
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing],nil]];
    
    //  设置路径
    
    CGMutablePathRef path =CGPathCreateMutable();
    
    CGPathMoveToPoint(path,NULL, 0,0);
    
    CGPathAddLineToPoint(path,NULL,CGRectGetWidth(lineView.frame),0);
    
    [shapeLayer setPath:path];
    
    CGPathRelease(path);
    
    //  把绘制好的虚线添加上来
    
    [lineView.layer addSublayer:shapeLayer];
    
}

@end
