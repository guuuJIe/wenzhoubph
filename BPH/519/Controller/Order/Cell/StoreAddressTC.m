//
//  StoreAddressTC.m
//  519
//
//  Created by Macmini on 16/12/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "StoreAddressTC.h"

@interface StoreAddressTC()
@property(nonatomic,strong)UIImageView * iconImageV;
@property(nonatomic,strong)UIImageView * phoneImageV;
@property(nonatomic,strong)UILabel * businessLabel;
@property(nonatomic,strong)UILabel * nameLabel;
@property(nonatomic,strong)UILabel * addressLabel;
@property(nonatomic,strong)UILabel * phoneLabel;
@property(nonatomic,strong)UILabel * distanceLabel;
@property(nonatomic,strong)UILabel * referenceLabel;
@property(nonatomic,strong)UIButton * locationBtn;
@end

@implementation StoreAddressTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initView];
    }
    return self;
}

-(void)initView{
    
    
    self.iconImageV = [[UIImageView alloc]initWithFrame:CGRectMake(10, Default_Space, 80, 80)];
    self.iconImageV.contentMode = UIViewContentModeScaleAspectFit;
    
    CGFloat origin_x = self.iconImageV.frame.size.width+self.iconImageV.frame.origin.x+10;
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(origin_x, Default_Space, Screen_Width/3+20, 20)];
    self.nameLabel.textColor = APPFourColor;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.font = [UIFont systemFontOfSize:16];
    
    self.phoneImageV = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width/2+Default_Space, Default_Space, 15, 15)];
    self.phoneImageV.image = [UIImage imageNamed:@"电话 (2)"];
    
    
    self.phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.phoneImageV.frame.size.width+self.phoneImageV.frame.origin.x, Default_Space, Screen_Width/2-Default_Space*2, 20)];
    self.phoneLabel.textColor = APPTwoColor;
    self.phoneLabel.textAlignment = NSTextAlignmentRight;
    self.phoneLabel.userInteractionEnabled = YES;
    self.phoneLabel.font = [UIFont systemFontOfSize:13];
    UITapGestureRecognizer * phoneTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(takePhone)];
    [self.phoneLabel addGestureRecognizer:phoneTap];
    
    
    
    
    self.addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(origin_x,CGRectGetMaxY(self.nameLabel.frame) , Screen_Width/4*3-30, 35)];
    self.addressLabel.textColor = APPThreeColor;
    self.addressLabel.numberOfLines = 0;
    self.addressLabel.textAlignment = NSTextAlignmentLeft;
    self.addressLabel.font = [UIFont systemFontOfSize:13];
    
    
    UIView * lineView1 = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImageV.frame)+10, CGRectGetMaxY(self.iconImageV.frame)-1, Screen_Width-120, 1)];
    lineView1.backgroundColor = APPBGColor;
    [self addSubview:lineView1];
    
    UIImageView * businssImg = [[UIImageView alloc]initWithFrame:CGRectMake(origin_x, lineView1.frame.origin.y+5, 15, 15)];
    businssImg.image = [UIImage imageNamed:@"绿-1"];
    
    self.businessLabel = [[UILabel alloc]initWithFrame:CGRectMake(origin_x+22, lineView1.frame.origin.y +lineView1.frame.size.height+3, Screen_Width/2, 20)];
    self.businessLabel.textColor = APPThreeColor;
    self.businessLabel.textAlignment = NSTextAlignmentLeft;
    self.businessLabel.font = [UIFont systemFontOfSize:12];
    
    UIImageView * reference = [[UIImageView alloc]initWithFrame:CGRectMake(origin_x, self.businessLabel.frame.origin.y +self.businessLabel.frame.size.height+2, 15, 15)];
    reference.image = [UIImage imageNamed:@"黄-1"];
    self.referenceLabel = [[UILabel alloc]initWithFrame:CGRectMake(origin_x+22, self.businessLabel.frame.origin.y +self.businessLabel.frame.size.height, Screen_Width/4*3-30, 20)];
    self.referenceLabel.textColor = APPThreeColor;
    self.referenceLabel.textAlignment = NSTextAlignmentLeft;
    self.referenceLabel.font = [UIFont systemFontOfSize:12];
    
    self.locationBtn = [[UIButton alloc] init];
    [self.locationBtn setImage:[UIImage imageNamed:@"Shape"] forState:(UIControlStateNormal)];
    [self.locationBtn setTitleColor:[UIColor orangeColor] forState:(UIControlStateNormal)];
    [self.locationBtn addTarget:self action:@selector(locationBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    self.locationBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    self.locationBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    
    CGFloat lineView_y = _referenceLabel.frame.size.height+_referenceLabel.frame.origin.y+5;
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, lineView_y, Screen_Width, 5)];
    lineView.backgroundColor = APPBGColor;
    
    
    [self addSubview:self.iconImageV];
    [self addSubview:self.phoneImageV];
    [self addSubview:self.businessLabel];
    [self addSubview:self.locationBtn];
    [self addSubview:self.nameLabel];
    [self addSubview:self.phoneLabel];
    [self addSubview:self.addressLabel];
    [self addSubview:self.referenceLabel];
    [self addSubview:businssImg];
    [self addSubview:reference];
    [self addSubview:lineView];
    
}

-(void)locationBtnClick:(UIButton *)button{
    [self.delegate targetStoreLocation:_model.xpoint Ypoint:_model.ypoint withPhonenum:_model.tel withStorename:_model.name];
    
}

-(void)setModel:(StoreAddressModel *)model{
    _model = model;
    [_iconImageV sd_setImageWithURL:[NSURL URLWithString:_model.preview]];
    _nameLabel.text = model.name;
    _phoneLabel.text = model.tel;
    _addressLabel.text = model.address;
    _businessLabel.text = [NSString stringWithFormat:@"营业时间:%@",model.open_time];
    _referenceLabel.text = [NSString stringWithFormat:@"配送范围:%@",model.peisong_fanwei];
    
    CGFloat telWidth=[NSString sizeWithText:model.tel font:[UIFont boldSystemFontOfSize:14] maxSize:CGSizeMake(Screen_Width, Screen_Height)].width;
    _phoneLabel.frame = CGRectMake(Screen_Width-telWidth-10, Default_Space, telWidth, 20);
    _phoneImageV.frame = CGRectMake(Screen_Width-telWidth-20, 12, 15, 15);
    _nameLabel.frame = CGRectMake(CGRectGetMaxX(_iconImageV.frame)+10, Default_Space, Screen_Width-Screen_Width/4-telWidth-50, 20);
    
    NSString * distanceStr = [NSString stringWithFormat:@"%@",model.juli];
    CGFloat selectWidth=[NSString sizeWithText:distanceStr font:[UIFont boldSystemFontOfSize:11] maxSize:CGSizeMake(Screen_Width, Screen_Height)].width;
    
    NSMutableAttributedString* tncString = [[NSMutableAttributedString alloc] initWithString:distanceStr];
    [tncString addAttribute:NSUnderlineStyleAttributeName
                      value:@(NSUnderlineStyleSingle)
                      range:(NSRange){0,[tncString length]}];
    //此时如果设置字体颜色要这样
    [tncString addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor]  range:NSMakeRange(0,[tncString length])];
    //设置下划线颜色...
    [tncString addAttribute:NSUnderlineColorAttributeName value:[UIColor orangeColor] range:(NSRange){0,[tncString length]}];
    [_locationBtn setAttributedTitle:tncString forState:UIControlStateNormal];
    _locationBtn.frame = CGRectMake(Screen_Width-selectWidth-30, _referenceLabel.frame.origin.y, selectWidth+20, 20);
    
}

-(void)takePhone{
    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",_model.tel];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
}
+(NSString *)getId{
    return @"StoreAddress";
}


@end
