//
//  AddGoodTC.h
//  519
//
//  Created by 陈 on 16/9/21.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@interface AddGoodTC : BaseTC

-(void)showData:(NSString *)title btnTitle:(NSArray *)btnTitle btnPrice:(NSArray *)btnPrice btnids:(NSArray *)btnId witnIndex:(NSInteger)index;

+(CGFloat)getCellHeight:(NSArray *)array;

+(NSString *)getID;

@end
