//
//  GoodstypeView.h
//  519
//
//  Created by Macmini on 2018/12/10.
//  Copyright © 2018年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodstypeView : UIView
@property (nonatomic, strong) NSArray * goodsTypesArr;
@property (nonatomic,copy) void(^clickBlock)(NSString *title);
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,assign)NSInteger goodsid;
@end
