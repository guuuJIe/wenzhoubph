//
//  OrderGoodsModel.h
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderGoodsModel : NSObject

@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * unit_price;
@property(nonatomic,copy)NSString * total_price;
@property(nonatomic,copy)NSString * number;
@property(nonatomic,copy)NSString * deal_id;
@property(nonatomic,copy)NSString * attr_str;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * supplier_id;
@property(nonatomic,copy)NSString * icon;


@end
