//
//  AddGoodVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddGoodVC.h"
#import "AddGoodTC.h"
#import "OrderConfirmVC.h"
#import "LoginVC.h"
@interface AddGoodVC()<UITableViewDataSource,UITableViewDelegate>
{
    NSString * _typeId;
}
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIView *goodView;
@property (nonatomic,strong)UIImageView * goodImg;
@property (nonatomic,strong)UILabel*goodNameLabel;
@property (nonatomic,strong)UILabel*goodInfoLabel;
@property (nonatomic,strong)UILabel*goodPriceLabel;
@property (nonatomic,strong)UILabel*goodCountLabel;


@property (nonatomic,strong)UIView * typeView;
@property(nonatomic,strong)NSMutableArray *tagArray;
@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)UIView *infoView;
@property(nonatomic,strong)UILabel *infoPriceTitleLabel;
@property(nonatomic,strong)UILabel *infoPriceLabel;
@property(nonatomic,strong)UILabel *infoCountTitleLabel;
@property(nonatomic,strong)UIButton *infoAddBtn;
@property(nonatomic,strong)UILabel *infoCountLabel;
@property(nonatomic,strong)UIButton *infoRemoveBtn;
@property(nonatomic,strong)UILabel *infoAllPriceTitleLabel;
@property(nonatomic,strong)UILabel *infoAllPriceLabel;

@property(nonatomic,strong)UIView *payView;
@property(nonatomic,strong)UIView *payLine;
@property(nonatomic,strong)UILabel *payAllPriceLabel;
@property(nonatomic,strong)UIButton *payBtn;
@property(nonatomic,assign)double oldPrice;
@property (nonatomic,assign)double beforPrice;
@property (nonatomic,assign)double number;
@property (nonatomic,assign)double  onePrice;
@property (nonatomic,strong)NSMutableArray * allIdMutableArray;

@end

@implementation AddGoodVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.titleStr;
    _onePrice = [self.goodspriceStr  doubleValue];
    _number = 1;
    self.allIdMutableArray = [NSMutableArray new];
    [self initView];
    [self initBar];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(calculatePrice:) name:@"addPriceOn" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(subPrice:) name:@"subPriceOn" object:nil];
}

-(void)calculatePrice:(NSNotification *)noti{
    NSLog(@"%@",noti.object);
    NSArray * leixArray = noti.object;
    NSString * goodsId = leixArray[0];
    NSString * oldGoodid = leixArray[3];
    
    double nowGoodPrice = [leixArray[1] doubleValue];
    
    double oldGoodPrce = [leixArray[2] doubleValue];

    NSLog(@"%@,%f,%f",goodsId,nowGoodPrice,oldGoodPrce);
    
    if (![oldGoodid isEqualToString:@""]) {
        [self.allIdMutableArray removeObject:oldGoodid];
    }
    [self.allIdMutableArray addObject:goodsId];
    NSLog(@"%@",self.allIdMutableArray);

    _onePrice = _onePrice + nowGoodPrice-oldGoodPrce;
    
    _beforPrice = _onePrice * _number;
    [self isBuy_type];
    
}

-(void)subPrice:(NSNotification *)noti{
    NSArray * subArray = noti.object;
    NSString * goodsId = subArray[1];
    NSInteger goodprice = [subArray[0] integerValue];
    
    _onePrice = _onePrice-goodprice;
    [_allIdMutableArray removeObject:goodsId];
    
    _beforPrice = _onePrice*_number;
     [self isBuy_type];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"addPriceOn" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"subPriceOn" object:nil];
}


-(void)initView{
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-50)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=APPBGColor;
    
    self.goodView=[[UIView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 100)];
    self.goodView.backgroundColor=[UIColor whiteColor];
    
    
    self.goodImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 80, 80)];
    self.goodImg.contentMode = UIViewContentModeScaleAspectFit;
    self.goodNameLabel=[[UILabel alloc]init];
    self.goodNameLabel.textColor=APPFourColor;
    self.goodNameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodNameLabel.numberOfLines=0;
    self.goodPriceLabel=[[UILabel alloc]init];
    self.goodPriceLabel.textColor=APPColor;
    self.goodPriceLabel.textAlignment = NSTextAlignmentRight;
    self.goodPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodInfoLabel=[[UILabel alloc]init];
    self.goodInfoLabel.textColor=APPThreeColor;
    self.goodInfoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodInfoLabel.numberOfLines=2;
    self.goodCountLabel=[[UILabel alloc]init];
    self.goodCountLabel.textColor=APPThreeColor;
    self.goodCountLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    [self.goodView addSubview:self.goodImg];
    [self.goodView addSubview:self.goodNameLabel];
    [self.goodView addSubview:self.goodPriceLabel];
    [self.goodView addSubview:self.goodInfoLabel];
    [self.goodView addSubview:self.goodCountLabel];
    [self.scrollView addSubview:self.goodView];
    
    if (_goodtypeArray.count>0) {
        self.typeView = [[UIView alloc]init];
        self.typeView.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:self.typeView];
        
        UILabel * typeLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 100, 20)];
        typeLabel.textColor=APPFourColor;
        typeLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
        typeLabel.text = @"商品规格";
        typeLabel.textAlignment = NSTextAlignmentLeft;
        [self.typeView addSubview:typeLabel];
        
        CGFloat allWidth = 10;
        NSInteger index = 0;
        for (NSInteger i = 0; i<_goodtypeArray.count; i++) {
            NSDictionary * dic = _goodtypeArray[i];
            CGFloat width = [NSString sizeWithText:dic[@"name"] font:[UIFont systemFontOfSize:14] maxSize:Max_Size].width;
            
            if (allWidth+width >= Screen_Width) {
                allWidth = 10;
                index++;
            }
            
            UIButton * typeBtn = [[UIButton alloc]initWithFrame:CGRectMake(allWidth, CGRectGetMaxY(typeLabel.frame)+15+index*45, width+10, 30)];
            [typeBtn setTitle:dic[@"name"] forState:(UIControlStateNormal)];
            
            [typeBtn addTarget:self action:@selector(typeBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
            typeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            typeBtn.tag = 450+i;
            typeBtn.layer.cornerRadius = 3;
            typeBtn.layer.masksToBounds = YES;
            typeBtn.layer.borderWidth = 1;
            if ([self.goodsId isEqualToString:[NSString stringWithFormat:@"%@",dic[@"ref_id"]]]) {
                typeBtn.layer.borderColor = APPColor.CGColor;
                [typeBtn setTitleColor:APPColor forState:(UIControlStateNormal)];
            }else{
                typeBtn.layer.borderColor = APPThreeColor.CGColor;
                [typeBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
            }
            [self.typeView addSubview:typeBtn];
            allWidth = allWidth + width+30;
        }
        UIView * typelineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.typeView.frame)-1, Screen_Width, 1)];
        typelineView.backgroundColor = APPBGColor;
        [self.typeView addSubview:typelineView];
        self.typeView.frame = CGRectMake(0, CGRectGetMaxY(self.goodView.frame)+Default_Space, Screen_Width, 40+(index+1)*50);
    }
    
    
    self.tableView=[[UITableView alloc]init];
    [self.tableView registerClass:[AddGoodTC class] forCellReuseIdentifier:[AddGoodTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.scrollEnabled=NO;
    
    self.infoView=[[UIView alloc]init];
    self.infoView.backgroundColor=[UIColor whiteColor];
    self.infoPriceLabel=[[UILabel alloc]init];
    self.infoPriceLabel.textColor=APPFourColor;
    self.infoPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.infoPriceLabel.textAlignment=NSTextAlignmentRight;
    self.infoAddBtn=[[UIButton alloc]init];
    [self.infoAddBtn setBackgroundImage:[UIImage imageNamed:@"number_add_icon"] forState:UIControlStateNormal];
    self.infoCountLabel=[[UILabel alloc]init];
    self.infoCountLabel.textColor=APPFourColor;
    self.infoCountLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.infoCountLabel.textAlignment=NSTextAlignmentCenter;
    self.infoRemoveBtn=[[UIButton alloc]init];
    [self.infoRemoveBtn setBackgroundImage:[UIImage imageNamed:@"number_remove_icon"] forState:UIControlStateNormal];
    self.infoAllPriceLabel=[[UILabel alloc]init];
    self.infoAllPriceLabel.textColor=APPColor;
    self.infoAllPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.infoAllPriceLabel.textAlignment=NSTextAlignmentRight;

    
    NSString *priceTitleStr=@"单价";
    NSString *countTitleStr=@"数量";
    NSString *allPriceTitleStr=@"总价";
    
    self.infoPriceTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space,[NSString sizeWithText:priceTitleStr font:self.infoPriceLabel.font maxSize:Max_Size].width , 20)];
    self.infoPriceTitleLabel.font=self.infoPriceLabel.font;
    self.infoPriceTitleLabel.text=priceTitleStr;
    self.infoPriceTitleLabel.textColor=self.infoPriceLabel.textColor;
    
    self.infoCountTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.infoPriceTitleLabel.frame.size.height+self.infoPriceTitleLabel.frame.origin.y+Default_Space*2,[NSString sizeWithText:priceTitleStr font:self.infoPriceLabel.font maxSize:Max_Size].width , 20)];
    self.infoCountTitleLabel.font=self.infoPriceLabel.font;
    self.infoCountTitleLabel.text=countTitleStr;
    self.infoCountTitleLabel.textColor=self.infoPriceLabel.textColor;
    
    self.infoAllPriceTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.infoCountTitleLabel.frame.size.height+self.infoCountTitleLabel.frame.origin.y+Default_Space*2,[NSString sizeWithText:priceTitleStr font:self.infoPriceLabel.font maxSize:Max_Size].width , 20)];
    self.infoAllPriceTitleLabel.font=self.infoPriceLabel.font;
    self.infoAllPriceTitleLabel.text=allPriceTitleStr;
    self.infoAllPriceTitleLabel.textColor=self.infoPriceLabel.textColor;
    
    UIView *infoPriceTitleLine=[[UIView alloc]initWithFrame:CGRectMake(0, self.infoCountTitleLabel.frame.origin.y-Default_Space, Screen_Width, Default_Line)];
    infoPriceTitleLine.backgroundColor=APPBGColor;
    UIView *infoCountTitleLine=[[UIView alloc]initWithFrame:CGRectMake(0,  self.infoAllPriceTitleLabel.frame.origin.y-Default_Space, Screen_Width, Default_Line)];
    infoCountTitleLine.backgroundColor=APPBGColor;
    
    [self.infoView addSubview:self.infoPriceLabel];
    [self.infoView addSubview:self.infoAddBtn];
    [self.infoView addSubview:self.infoCountLabel];
    [self.infoView addSubview:self.infoRemoveBtn];
    [self.infoView addSubview:self.infoAllPriceLabel];
    [self.infoView addSubview:self.infoPriceTitleLabel];
    [self.infoView addSubview:self.infoCountTitleLabel];
    [self.infoView addSubview:self.infoAllPriceTitleLabel];
    [self.infoView addSubview:infoPriceTitleLine];
    [self.infoView addSubview:infoCountTitleLine];
    
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        self.payView=[[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-64-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 60)];
    }else{
        self.payView=[[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-44-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 40)];
    }
    self.payView.backgroundColor=[UIColor whiteColor];
    
    self.payAllPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space+5, (Screen_Width/3)*2-Default_Space*2, 20)];
    self.payAllPriceLabel.textColor=APPFourColor;
    self.payAllPriceLabel.textAlignment=NSTextAlignmentCenter;
    self.payAllPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    
    self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-Screen_Width/3, 0, Screen_Width/3, 44)];
    [self.payBtn setBackgroundColor:APPColor];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"确定" forState:UIControlStateNormal];
    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.payLine=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Line)];
    self.payLine.backgroundColor=APPBGColor;
    
    [self.payView addSubview:self.payAllPriceLabel];
    [self.payView addSubview:self.payBtn];
    [self.payView addSubview:self.payLine];
    
    
    [self.scrollView addSubview:self.tableView];
    [self.scrollView addSubview:self.infoView];
    
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.payView];
    [self loadData];
}

-(void)loadData{

    [self showData];
}

-(void)showData{
    if (_goodtypeArray.count>0) {
        self.tableView.frame=CGRectMake(0, self.typeView.frame.origin.y+self.typeView.frame.size.height+1, Screen_Width, [self getTableViewAllHeight]);
    }else{
        self.tableView.frame=CGRectMake(0, self.goodView.frame.origin.y+self.goodView.frame.size.height+Default_Space, Screen_Width, [self getTableViewAllHeight]);
    }
    
    [self.tableView reloadData];
    
    [self countChangeFrame];
    
    self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width, self.infoView.frame.origin.y+self.infoView.frame.size.height+Default_Space);
    
    self.payAllPriceLabel.text= [_buy_type isEqualToString:@"1"] ? [NSString stringWithFormat:@"总计 %.0f积分",[self.goodspriceStr doubleValue]] : [NSString stringWithFormat:@"总计 ￥ %.2lf",[self.goodspriceStr doubleValue]];
    [self.payBtn addTarget:self action:@selector(payBtnOnClick) forControlEvents:UIControlEventTouchUpInside];

}

-(void)countChangeFrame{
    [self.goodImg sd_setImageWithURL:[[NSURL alloc] initWithString:self.goodsImageUrl?self.goodsImageUrl:@""]];
    NSString *priceStr=@"￥2220.89";
    
    NSString *countStr=@"x99";
    
    self.goodNameLabel.frame=CGRectMake(self.goodImg.frame.size.width+Default_Space*2, Default_Space, self.goodView.frame.size.width-self.goodImg.frame.size.width-Default_Space*4-[NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 60);
    self.goodPriceLabel.frame=CGRectMake(self.goodView.frame.size.width-Default_Space-[NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, self.goodNameLabel.frame.origin.y, [NSString sizeWithText:priceStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    self.goodPriceLabel.center = CGPointMake(self.goodPriceLabel.frame.size.width/2+self.goodPriceLabel.frame.origin.x, self.goodNameLabel.frame.size.height/2+Default_Space);
    self.goodInfoLabel.frame=CGRectMake(self.goodNameLabel.frame.origin.x, self.goodView.frame.size.height-Default_Space-20,self.goodView.frame.size.width-self.goodImg.frame.size.width-Default_Space*4-[NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    self.goodCountLabel.frame=CGRectMake(self.goodView.frame.size.width-Default_Space-[NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, self.goodInfoLabel.frame.origin.y, [NSString sizeWithText:countStr font:self.goodPriceLabel.font maxSize:Max_Size].width, 20);
    self.goodNameLabel.text=self.goodsNameStr;
//    self.goodPriceLabel.text=[NSString stringWithFormat:@"￥%.2lf",[self.goodspriceStr doubleValue]];
    self.goodInfoLabel.text=@"";
//    self.goodCountLabel.text=countStr;
    
    self.infoView.frame=CGRectMake(0, self.tableView.frame.size.height + self.tableView.frame.origin.y, Screen_Width, 120);
    
    self.infoPriceLabel.frame = CGRectMake(self.infoPriceTitleLabel.frame.size.width + Default_Space*2, self.infoPriceTitleLabel.frame.origin.y, self.infoView.frame.size.width-self.infoPriceTitleLabel.frame.size.width - Default_Space*3, self.infoPriceTitleLabel.frame.size.height);
    
    //积分非积分商品单价
    self.goodPriceLabel.text = [self.buy_type isEqualToString:@"1"] ?
    [NSString stringWithFormat:@"%.0lf积分",[self.goodspriceStr doubleValue]] :
    [NSString stringWithFormat:@"￥%.2lf",[self.goodspriceStr doubleValue]];
    //单价
    self.infoPriceLabel.text = [self.buy_type isEqualToString:@"1"] ?
    [NSString stringWithFormat:@"%.0lf积分",[self.goodspriceStr doubleValue]] :
    [NSString stringWithFormat:@"￥%.2lf",[self.goodspriceStr doubleValue]];
    
    self.infoAddBtn.frame=CGRectMake(self.infoView.frame.size.width-20-20-[NSString sizeWithText:countStr font:self.infoCountLabel.font maxSize:Max_Size].width-Default_Space*3,self.infoCountTitleLabel.frame.origin.y, 20, 20);
    [self.infoAddBtn addTarget:self action:@selector(infoAddBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.infoCountLabel.frame=CGRectMake(self.infoAddBtn.frame.origin.x+self.infoAddBtn.frame.size.width+Default_Space,self.infoCountTitleLabel.frame.origin.y, [NSString sizeWithText:countStr font:self.infoCountLabel.font maxSize:Max_Size].width, 20);
    //数量
    self.infoCountLabel.text=@"1";
    
    self.infoRemoveBtn.frame = CGRectMake (self.infoCountLabel.frame.origin.x + self.infoCountLabel.frame.size.width+Default_Space,self.infoCountLabel.frame.origin.y, 20, 20);
        [self.infoRemoveBtn addTarget:self action:@selector(infoRemoveBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.infoAllPriceLabel.frame=CGRectMake(self.infoAllPriceTitleLabel.frame.size.width+Default_Space*2, self.infoAllPriceTitleLabel.frame.origin.y,self.infoView.frame.size.width-self.infoAllPriceTitleLabel.frame.size.width-Default_Space*3, self.infoAllPriceTitleLabel.frame.size.height);
    //总价
    self.infoAllPriceLabel.text = [self.buy_type isEqualToString:@"1"] ?
    [NSString stringWithFormat:@"%.0lf积分",[self.goodspriceStr doubleValue]] :
    [NSString stringWithFormat:@"￥%.2lf",[self.goodspriceStr doubleValue]];
}


-(void)typeBtnClick:(UIButton *)clickbtn{
    for (NSInteger i = 0; i<self.goodtypeArray.count; i++) {
        UIButton * btn = [self.view viewWithTag:450+i];
        btn.layer.borderColor = APPThreeColor.CGColor;
        [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    }
    clickbtn.layer.borderColor = APPColor.CGColor;
    [clickbtn setTitleColor:APPColor forState:(UIControlStateNormal)];
    NSDictionary * dic = self.goodtypeArray[clickbtn.tag-450];
    _typeId = [NSString stringWithFormat:@"%@",dic[@"ref_id"]];
    [self refreshData:_typeId];
}


#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.goodtitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *array=[self.goodnameArray objectAtIndex:indexPath.row];
    AddGoodTC *cell=[tableView dequeueReusableCellWithIdentifier:[AddGoodTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[AddGoodTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[AddGoodTC getID]];
    }
    _oldPrice = 0;
    [cell showData:self.goodtitleArray[indexPath.row] btnTitle:array btnPrice:self.goodpriceArray[indexPath.row] btnids:self.goodidArray[indexPath.row] witnIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *array=[self.goodnameArray objectAtIndex:indexPath.row];
    return [AddGoodTC getCellHeight:array];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(CGFloat)getTableViewAllHeight{
    CGFloat height=0;
    for (NSArray *array in self.goodnameArray) {
        height+=[AddGoodTC getCellHeight:array];
    }
    return height;
}



#pragma mark onclick delegate
-(void)payBtnOnClick{
    if (self.allIdMutableArray.count == self.goodtitleArray.count) {
        NSLog(@"allid -- %@",self.allIdMutableArray);
        [self addShoppingCar];
    }else{
        [MBProgressHUD showError:@"请选择商品规格" toView:self.view];
    }
}


-(void)addShoppingCar{
    
    NSString * username= [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * userPwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    if(username){
        [self.allIdMutableArray removeObject:@""];
        NSString * dealAttrId = @"";
        if (self.allIdMutableArray.count>0) {
            for (NSInteger i = 0; i<self.allIdMutableArray.count; i++) {
                if ([dealAttrId isEqualToString:@""]) {
                    dealAttrId = [NSString stringWithFormat:@"%@",self.allIdMutableArray[i]];
                }else{
                    dealAttrId = [NSString stringWithFormat:@"%@,%@",dealAttrId,self.allIdMutableArray[i]];

                }
            }
        }
        
        if ([self.titleStr isEqualToString:@"加入购物车"]) {
            NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=addcart&id=%@&deal_attr=%@&number=%.0lf&email=%@&pwd=%@",FONPort,self.goodsId,dealAttrId,_number,username,userPwd];
            [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {

                if([dic[@"status"] isEqual:@1]){
                    [MBProgressHUD showSuccess:@"加入购物车成功" toView:self.view];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
                }else{
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        
        }else{
            
            [MBProgressHUD showHUD];
            
            NSString * buyNowUrl = [NSString stringWithFormat:@"%@ctl=cart&act=buynow&buynow_id=%@&buynow_attr=%@&buynow_number=%.0lf&email=%@&pwd=%@&buy_type=%@",FONPort,self.goodsId,dealAttrId,_number,username,userPwd,self.buy_type];
            [RequestData requestDataOfUrl:buyNowUrl success:^(NSDictionary *dic) {
                NSLog(@"%@",dic);
            
                NSString * str = [NSString stringWithFormat:@"%@",dic[@"status"]];
                if ([str isEqualToString:@"0"]) {
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }else{
                    OrderConfirmVC *vc=[[OrderConfirmVC alloc] init];
                    vc.typeStr = @"立即购买";
                    vc.goodsDic = dic;
                    vc.str = @"1";
                    vc.icon = self.goodsImageUrl;
                    vc.buynowID = self.goodsId;
                    vc.attrId = dealAttrId;
                    vc.ordernumber = [NSString stringWithFormat:@"%.0lf",self.number];
                    [self tabHidePushVC:vc];
                }
                [MBProgressHUD dissmiss];
            } failure:^(NSError *error) {
//                NSLog(@"%@",error);
                [MBProgressHUD dissmiss];
            }];
        }

    }else{
//        [self tabHidePushVC:[[LoginVC alloc]init]];
        [self tabHidePushVC:[NewLoginVC new]];
    }
}


-(void)refreshData:(NSString *)ids{
     NSString * urlString = [NSString stringWithFormat:@"%@ctl=deal&id=%@",FONPort,ids];
    self.goodsId = ids;
    [RequestData requestDataOfUrl:urlString success:^(NSDictionary *dic) {
        if ([dic[@"status"]  isEqual:@1]) {
            [self.goodImg sd_setImageWithURL:[NSURL URLWithString:dic[@"icon"]]];
            self.goodNameLabel.text = dic[@"name"];
            self.goodPriceLabel.text = [NSString stringWithFormat:@"￥%.2lf",[dic[@"current_price"]floatValue]];
            self.infoPriceLabel.text = [NSString stringWithFormat:@"￥%.2lf",[dic[@"current_price"]floatValue]];
            self.infoAllPriceLabel.text = [NSString stringWithFormat:@"￥%.2lf",[dic[@"current_price"]floatValue]];
            self.payAllPriceLabel.text=[NSString stringWithFormat:@"总计 ￥ %.2lf",[dic[@"current_price"]floatValue]];
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)infoAddBtnOnClick{
    NSLog(@"+");
    if (_number == 99) {
        return;
    }
    _number++;
    _beforPrice = _onePrice* _number;
    self.infoCountLabel.text=[NSString stringWithFormat:@"%.0lf",_number];
     [self isBuy_type];
}

-(void)infoRemoveBtnOnClick{
    NSLog(@"-");
    if (_number == 1) {
        return;
    }
    _number--;
   _beforPrice = _onePrice* _number;
    self.infoCountLabel.text=[NSString stringWithFormat:@"%.0lf",_number];
    [self isBuy_type];
}

-(void)isBuy_type{
    if ([_buy_type isEqualToString:@"1"]) {
        self.payAllPriceLabel.text=[NSString stringWithFormat:@"总计 %.0lf积分",_beforPrice];
        self.infoAllPriceLabel.text=[NSString stringWithFormat:@"%.0lf积分",_beforPrice];
    }else{
        self.payAllPriceLabel.text=[NSString stringWithFormat:@"总计 ￥ %.2lf",_beforPrice];
        self.infoAllPriceLabel.text=[NSString stringWithFormat:@"￥ %.2lf",_beforPrice];
    }
}

@end
