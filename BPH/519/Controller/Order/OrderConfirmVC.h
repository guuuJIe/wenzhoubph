//
//  OrderConfirmVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface OrderConfirmVC : BaseVC
@property(nonatomic,copy)NSString *typeStr;

@property(nonatomic,copy)NSString * icon;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * price;
@property(nonatomic,copy)NSString * number;
@property(nonatomic,copy)NSString * attr;

@property(nonatomic,strong)UILabel *psfwTextLabel;
@property(nonatomic,strong)NSDictionary * goodsDic;
@property(nonatomic,copy)NSString * str;          //0:购物车结算   1:立即购买
@property(nonatomic,copy)NSString * buynowID;
@property(nonatomic,copy)NSString * attrId;
@property(nonatomic,copy)NSString * ordernumber;
@end
