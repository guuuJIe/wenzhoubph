//
//  ChooseConponVC.m
//  519
//
//  Created by Macmini on 2018/12/17.
//  Copyright © 2018年 519. All rights reserved.
//

#import "ChooseConponVC.h"
#import "ChooseConponTC.h"
@interface ChooseConponVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic , strong) UITableView *listTableView;
@property (nonatomic , strong) NSMutableArray *dataArr;
@property (nonatomic,assign)NSInteger pageIndex;
@end

@implementation ChooseConponVC

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _pageIndex = 1;
    [self setupLayout];
    [self.listTableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupLayout{
    self.title = @"选择优惠券";
    self.view.backgroundColor = RGB(240, 240, 240);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"不使用" style:0 target:self action:@selector(choose)];
//    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.equalTo(self.view).offset(0);
    }];
    AdjustsScrollViewInsetNever(self, self.listTableView);
}

- (void)choose{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectConpons" object:nil];
    [self.navigationController popViewControllerAnimated:true];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *rid = @"ChooseConponTC";
    ChooseConponTC *cell = [tableView dequeueReusableCellWithIdentifier:rid];
    if (!cell) {
        cell = [[ChooseConponTC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rid];
    }
    [cell setupdata:self.dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.dataArr[indexPath.row];
    NSString *rejectStr = [NSString stringWithFormat:@"%@",dic[@"info"]];
    if (rejectStr.length>1) {
        [MBProgressHUD showError:rejectStr toView:self.view];
        return;
    }
    if (self.clickblock) {
        self.clickblock(dic);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (UITableView *)listTableView{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] init];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 60;
        [_listTableView registerClass:[ChooseConponTC class] forCellReuseIdentifier:@"ChooseConponTC"];
        _listTableView.backgroundColor = [UIColor clearColor];
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        header.lastUpdatedTimeLabel.hidden = YES;
        _listTableView.mj_header = header;
        WeakSelf(self);
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            weakself.pageIndex = weakself.pageIndex + 1;
            [weakself ChooseConponData];
        }];
        
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (void)loadData{
    _pageIndex = 1;
    [_listTableView.mj_footer endRefreshing];
    [self ChooseConponData];
}

- (void)ChooseConponData{
    NSString * submitUrl;
//    if ([self.fromStr isEqualToString:@"0"]) {
        //购物车购买
//        submitUrl = [NSString stringWithFormat:@"%@ctl=cart&act=select_coupon&delivery_id=%@content=%@&all_account_money=%@&email=%@&pwd=%@&buy_type=%@",FONPort,self.delivery_id,self.content,self.all_account_money,self.email,self.pwd,self.buy_type];
//    }else{
        //非购物车购买
    submitUrl = [NSString stringWithFormat:@"%@ctl=cart&act=select_coupon&buynow_id=%@&buynow_attr=%@&buynow_number=%@&delivery_id=%@&content=%@&all_account_money=%@&email=%@&pwd=%@&buy_type=%@&page=%ld",FONPort,_buynow_id,_buynow_attr,_buynow_number,_delivery_id,_content,_all_account_money,_email,_pwd,_buy_type,(long)_pageIndex];
//    }
    
    WeakSelf(self);
    [RequestData requestDataOfUrl:submitUrl success:^(NSDictionary *dic) {
        if ([dic[@"status"] isEqual:@0]) {
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }else{
            NSArray *conponData = dic[@"item"];
            if (weakself.pageIndex == 1) {
                [weakself.dataArr removeAllObjects];
                weakself.dataArr = [NSMutableArray arrayWithArray:conponData];
            }else{
                if (conponData.count == 0) {
                    
                     [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                }else{
                    [weakself.dataArr addObjectsFromArray:conponData];
                }
            }
           
            [self.listTableView reloadData];
        }
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
