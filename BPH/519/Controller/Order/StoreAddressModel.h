//
//  StoreAddressModel.h
//  519
//
//  Created by Macmini on 16/12/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreAddressModel : NSObject

@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * address;
@property (nonatomic,copy)NSString * peisong_fanwei;
@property (nonatomic,copy)NSString * tel;
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * juli;
@property (nonatomic,copy)NSString * xpoint;
@property (nonatomic,copy)NSString * ypoint;
@property (nonatomic,copy)NSString * preview;
@property (nonatomic,copy)NSString * open_time;
@end
