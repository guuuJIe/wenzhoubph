//
//  OrderPayVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderPayVC.h"
#import "OrderConfirmPayTC.h"
#import "PayResultVC.h"
#import "base64.h"
#import <AlipaySDK/AlipaySDK.h>
#import "PayObject.h"
#import "WXApiManager.h"
#import "MKPAlertView.h"
#import "GoodInfoVC.h"
#import "MainWebView.h"
#import "UIViewController+BackButtonHandler.h"
@interface OrderPayVC()<UITableViewDataSource,UITableViewDelegate,OrderConfirmPayDetegate,WXApiManagerDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UIScrollView *scrollView;

@property(nonatomic,strong)UIView *orderInfoView;
@property(nonatomic,strong)UILabel *orderNumberTitleLabel;
@property(nonatomic,strong)UILabel *orderNumberLabel;
@property(nonatomic,strong)UIView *orderInfoLine;
@property(nonatomic,strong)UIView *orderInfoLine2;
@property(nonatomic,strong)UILabel *orderPriceTitleLabel;
@property(nonatomic,strong)UILabel *orderPriceLabel;
@property(nonatomic,strong)UILabel *wayPriceLabel;
@property(nonatomic,strong)UILabel *wayMoneyLabel;
//优惠券
@property(nonatomic,strong)UILabel *youhuilabel;
@property(nonatomic,strong)UILabel *xuzhifulbl;
@property(nonatomic,strong)UILabel *youhuiValuelabel;
@property(nonatomic,strong)UILabel *xuzhifuValuelbl;
@property(nonatomic,strong)UIView *orderInfoLine3;
@property(nonatomic,strong)UIView *orderInfoLine4;

@property(nonatomic,strong)UIView *payTitleView;
@property(nonatomic,strong)UILabel *payTitleLabel;

@property(nonatomic,strong)UITableView *payTableView;
@property(nonatomic,strong)UIButton *payBtn;

@property(nonatomic,copy)NSString * userName;
@property(nonatomic,copy)NSString * userpwd;

@property(nonatomic,copy)NSString * order_id;
@property(nonatomic,copy)NSString * order_sn;
@property(nonatomic,copy)NSString * payMoney;
@property(nonatomic,copy)NSString * payId;

@property(nonatomic,strong)NSMutableArray * payNameMuarray;
@property(nonatomic,strong)NSMutableArray * payIdMuArray;
@property(nonatomic,strong)NSMutableArray * payLogoMuArray;

@property (nonatomic,assign)BOOL payState;


@end

@implementation OrderPayVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"收银台";
    self.payIdMuArray = [NSMutableArray new];
    self.payNameMuarray = [NSMutableArray new];
    self.payLogoMuArray = [NSMutableArray new];
    
    
    self.userName = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    self.userpwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    [self initView];
    [self payInitBar];
    [self requestDataForOrder];
}
-(void)payInitBar{
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton * btn = [[UIButton alloc]initWithFrame:contentView.bounds];
    [btn setImage:[UIImage imageNamed:@"back_white_icon"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    [contentView addSubview:btn];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
}
-(void)popVC{
    MKPAlertView *alertView = [[MKPAlertView alloc]initWithTitle:@"订单已生成,确定取消支付" message:nil sureBtn:@"确认" cancleBtn:@"取消"];
    alertView.resultIndex = ^(NSInteger index)
    {
        if ([self.paytype isEqualToString:@"nowPay"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[GoodInfoVC class]]) {
                    GoodInfoVC *revise =(GoodInfoVC *)controller;
                    [self.navigationController popToViewController:revise animated:YES];
                }else{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
        
    };
    [alertView showMKPAlertView];
}


-(void)requestDataForOrder{
    
    NSString * orderUrl = [NSString stringWithFormat:@"%@ctl=payment&act=done&id=%@&email=%@&pwd=%@",FONPort,self.orderId,self.userName,self.userpwd];
    
   [RequestData requestDataOfUrl:orderUrl success:^(NSDictionary *dic) {
      
       self.order_sn = dic[@"order_sn"];
       self.payMoney = dic[@"total_price"];
       self.order_id = dic[@"order_id"];
       
       self.orderNumberLabel.text=self.order_sn;//
       self.orderPriceLabel.text=[NSString stringWithFormat:@"￥ %@",dic[@"youhuiquan_fee"]];
       self.wayMoneyLabel.text = [NSString stringWithFormat:@"￥ %.2f",[dic[@"pay_money"] floatValue] +fabs([dic[@"youhuiquan_fee"] floatValue])];
       self.youhuiValuelabel.text = [NSString stringWithFormat:@"¥ %@",dic[@"delivery_fee"]];
       self.xuzhifuValuelbl.text = [NSString stringWithFormat:@"¥ %@",dic[@"pay_money"]];
       for ( NSDictionary * subdic in dic[@"payment_list"]) {
           [self.payNameMuarray addObject: subdic[@"name"]];
           [self.payIdMuArray addObject:subdic[@"id"]];
           [self.payLogoMuArray addObject:subdic[@"logo"]];
       }
       
       [self showData];
   } failure:^(NSError *error) {
       NSLog(@"%@",error);
   }];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)viewDidAppear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}


-(void)initView{
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-40)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=APPBGColor;
    
    self.orderInfoView=[[UIView alloc] initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 200)];
    self.orderInfoView.backgroundColor=[UIColor whiteColor];
    
    self.orderNumberTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space,Default_Space, Screen_Width-Default_Space*2, 20)];
    self.orderNumberTitleLabel.textColor=APPFourColor;
    self.orderNumberTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderNumberTitleLabel.text=@"订单号：";
    self.orderNumberTitleLabel.textAlignment=NSTextAlignmentLeft;
    self.wayPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.orderNumberTitleLabel.frame.origin.y+self.orderNumberTitleLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.wayPriceLabel.textColor=APPFourColor;
    self.wayPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.wayPriceLabel.textAlignment=NSTextAlignmentLeft;
    self.wayPriceLabel.text=@"订单金额：";
    self.orderPriceTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.wayPriceLabel.frame.origin.y+self.wayPriceLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.orderPriceTitleLabel.textColor=APPFourColor;
    self.orderPriceTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderPriceTitleLabel.textAlignment=NSTextAlignmentLeft;
    self.orderPriceTitleLabel.text=@"优惠券：";
    
    self.youhuilabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.orderPriceTitleLabel.frame.origin.y+self.orderPriceTitleLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.youhuilabel.textColor=APPFourColor;
    self.youhuilabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.youhuilabel.textAlignment=NSTextAlignmentLeft;
    self.youhuilabel.text=@"运费：";
    
    self.xuzhifulbl=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.youhuilabel.frame.origin.y+self.youhuilabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.xuzhifulbl.textColor=APPFourColor;
    self.xuzhifulbl.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.xuzhifulbl.textAlignment=NSTextAlignmentLeft;
    self.xuzhifulbl.text=@"需支付：";
    
    self.orderInfoLine=[[UIView alloc] initWithFrame:CGRectMake(0, 40, Screen_Width, Default_Line)];
    self.orderInfoLine.backgroundColor=APPBGColor;
    self.orderInfoLine2=[[UIView alloc] initWithFrame:CGRectMake(0, 80, Screen_Width, Default_Line)];
    self.orderInfoLine2.backgroundColor=APPBGColor;
    self.orderInfoLine3=[[UIView alloc] initWithFrame:CGRectMake(0, 120, Screen_Width, Default_Line)];
    self.orderInfoLine3.backgroundColor=APPBGColor;
    self.orderInfoLine4=[[UIView alloc] initWithFrame:CGRectMake(0, 160, Screen_Width, Default_Line)];
    self.orderInfoLine4.backgroundColor=APPBGColor;
    
    self.orderNumberLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space,Default_Space, Screen_Width-Default_Space*2, 20)];
    self.orderNumberLabel.textColor=APPFourColor;
    self.orderNumberLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderNumberLabel.textAlignment=NSTextAlignmentRight;
    self.wayMoneyLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.orderNumberLabel.frame.origin.y+self.orderNumberLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.wayMoneyLabel.textColor=APPColor;
    self.wayMoneyLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.wayMoneyLabel.textAlignment=NSTextAlignmentRight;
    self.orderPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.wayMoneyLabel.frame.origin.y+self.wayMoneyLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.orderPriceLabel.textColor=APPColor;
    self.orderPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderPriceLabel.textAlignment=NSTextAlignmentRight;
    
    self.youhuiValuelabel = [[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.orderPriceLabel.frame.origin.y+self.orderPriceLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.youhuiValuelabel.textColor=APPColor;
    self.youhuiValuelabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.youhuiValuelabel.textAlignment=NSTextAlignmentRight;
    
    self.xuzhifuValuelbl = [[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.youhuiValuelabel.frame.origin.y+self.youhuiValuelabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.xuzhifuValuelbl.textColor=APPColor;
    self.xuzhifuValuelbl.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.xuzhifuValuelbl.textAlignment=NSTextAlignmentRight;
    
    [self.orderInfoView addSubview:self.orderNumberLabel];
    [self.orderInfoView addSubview:self.orderPriceLabel];
    [self.orderInfoView addSubview:self.orderNumberTitleLabel];
    [self.orderInfoView addSubview:self.orderPriceTitleLabel];
    [self.orderInfoView addSubview:self.youhuilabel];
    [self.orderInfoView addSubview:self.xuzhifulbl];
    [self.orderInfoView addSubview:self.orderInfoLine];
    [self.orderInfoView addSubview:self.orderInfoLine2];
    [self.orderInfoView addSubview:self.orderInfoLine3];
    [self.orderInfoView addSubview:self.orderInfoLine4];
    [self.orderInfoView addSubview:self.youhuiValuelabel];
    [self.orderInfoView addSubview:self.xuzhifuValuelbl];
    [self.orderInfoView addSubview:self.wayPriceLabel];
    [self.orderInfoView addSubview:self.wayMoneyLabel];
    
    self.payTitleView=[[UIView alloc] initWithFrame:CGRectMake(0, self.orderInfoView.frame.origin.y+self.orderInfoView.frame.size.height+Default_Space, Screen_Width, 40)];
    self.payTitleView.backgroundColor=[UIColor whiteColor];
    self.payTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.payTitleLabel.textColor=APPFourColor;
    self.payTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.payTitleLabel.text=@"支付方式";
    [self.payTitleView addSubview:self.payTitleLabel];
    
    self.payTableView=[[UITableView alloc]init];
    [self.payTableView registerClass:[OrderConfirmPayTC class] forCellReuseIdentifier:[OrderConfirmPayTC getID]];
    self.payTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.payTableView.delegate=self;
    self.payTableView.dataSource=self;
    self.payTableView.scrollEnabled=NO;
    self.payTableView.showsVerticalScrollIndicator=NO;
    
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, Screen_Height-64-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 44)];
    }else{
        self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, Screen_Height-44-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 44)];
    }
    [self.payBtn setBackgroundColor:APPColor];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"确认" forState:UIControlStateNormal];
    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.payBtn addTarget:self action:@selector(payBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.orderInfoView];
    [self.scrollView addSubview:self.payTitleView];
    [self.scrollView addSubview:self.payTableView];
    
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.payBtn];
    
//    [self loadData];
}


-(void)showData{
    self.payTableView.frame=CGRectMake(0, self.payTitleView.frame.origin.y+self.payTitleView.frame.size.height+Default_Line, Screen_Width, self.payNameMuarray.count*[OrderConfirmPayTC getCellHeight]);
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.payTableView.frame.origin.y+self.payTableView.frame.size.height);
    
    [self.payTableView reloadData];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.payIdMuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderConfirmPayTC *cell=[tableView dequeueReusableCellWithIdentifier:[OrderConfirmPayTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[OrderConfirmPayTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[OrderConfirmPayTC getID]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell showDataName:self.payNameMuarray[indexPath.row] logo:self.payLogoMuArray[indexPath.row] withids:self.payIdMuArray[indexPath.row]];
    cell.delegate = self;

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [OrderConfirmPayTC getCellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self changeBtnImage:[_payIdMuArray[indexPath.row] integerValue]+6000];
    NSLog(@"%@",self.payIdMuArray[indexPath.row]);
    
}

-(void)changeBtnImage:(NSInteger)tag{
    for (NSInteger i = 0; i<self.payIdMuArray.count; i++) {
        UIButton * btn = [self.view viewWithTag:[self.payIdMuArray[i] integerValue]+6000];
        [btn setImage:[UIImage imageNamed:@"check_0_icon"] forState:(UIControlStateNormal)];
    }
     UIButton * btn = [self.view viewWithTag:tag];
    [btn setImage:[UIImage imageNamed:@"check_1_icon"] forState:(UIControlStateNormal)];
    self.payId = [NSString stringWithFormat:@"%ld",(long)btn.tag-6000];
}

#pragma mark onclick delegate
-(void)payBtnOnClick{
    [MBProgressHUD showHUD];
    __weak typeof (self)weself = self;
    NSString * payUrl = [NSString stringWithFormat:@"%@ctl=payment&act=get_payment_code&order_id=%@&payment_id=%@&email=%@&pwd=%@",FONPort,self.order_id,self.payId,self.userName,self.userpwd];
    [RequestData requestDataOfUrl:payUrl success:^(NSDictionary *dic) {
        
        //余额支付
        if ([_payId isEqualToString:@"17"]) {
            if ([dic[@"status"] isEqual:@1]) {
                [MBProgressHUD dissmiss];
                PayResultVC * payresult = [PayResultVC new];
                payresult.number = @"1";
                payresult.price = weself.payMoney;
                [self tabHidePushVC:payresult];
            }else{
                [MBProgressHUD dissmiss];
                [MBProgressHUD showError:dic[@"info"] toView:weself.view];
            }
        }else{
            //第三方支付
            NSString * payname = dic[@"payment_code"][@"class_name"];
            PayObject * obj = [PayObject new];
            //支付宝
            if ([payname isEqualToString:@"Malipay"]) {
                [obj getParInfoWithURL:dic callback:^(NSDictionary *result) {
                    NSLog(@"pay -- %@",result);
                    NSString * resultStatus = result[@"resultStatus"];
                    if ([resultStatus isEqualToString:@"9000"]) {
                        [MBProgressHUD dissmiss];
                        PayResultVC * payresult = [PayResultVC new];
                        payresult.number = @"1";
                        payresult.price = [NSString stringWithFormat:@"￥%@",weself.payMoney];
                        [self tabHidePushVC:payresult];
                    }else if ([resultStatus integerValue]>=6000 && [resultStatus integerValue] < 7000){
                        [MBProgressHUD dissmiss];
                        [MBProgressHUD showError:@"订单已取消" toView:weself.view];
                    }
                    NSLog(@"result %@",result);
                }];
            }else if([payname isEqualToString:@"WxApp"]){
                //微信
                [obj WXPayinfoWithDic:dic callback:^(Boolean result) {
                    NSLog(@"%hhu",result);
                    if ([WXApi isWXAppInstalled]) {
                        [WXApiManager sharedManager].delegate = self;
                        
                    }else{
                        [MBProgressHUD dissmiss];
                        [MBProgressHUD showError:@"未安装微信" toView:self.view];
                    }
                }];
            }else if([payname isEqualToString:@"Zhaoshang"]){
                //招商银行
                NSString * pay_action = dic[@"payment_code"][@"pay_action"];
                MainWebView * webview = [[MainWebView alloc]init];
                webview.webUrl = pay_action;
                webview.runjump = @"zhaoshangJump";
                [self tabHidePushVC:webview];
                
            }else{
                [MBProgressHUD dissmiss];
                PayResultVC * payresult = [PayResultVC new];
                payresult.number = @"1";
                payresult.price = weself.payMoney;
            }
        }
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}

-(void)managerJunpPay:(PayResp *)respose{
    NSLog(@"resp %@",respose);
    PayResp*response=(PayResp*)respose;
    switch(response.errCode){
        case WXSuccess:{
            PayResultVC * payresult = [PayResultVC new];
            payresult.number = @"1";
            payresult.price = [NSString stringWithFormat:@"￥%@",self.payMoney];
            [self tabHidePushVC:payresult];
        }
            break;
        default:
            [MBProgressHUD showError:@"支付失败" toView:self.view];
            NSLog(@"支付失败，retcode=%d",respose.errCode);
            break;
    }
}

@end
