//
//  PayResultVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "PayResultVC.h"
#import "OrderListVC.h"
#import "WMPageController.h"
@interface PayResultVC()
@property(nonatomic,strong)UIScrollView *scrollView;

@property(nonatomic,strong)UIView *payResultView;
@property(nonatomic,strong)UIImageView *payResultImg;
@property(nonatomic,strong)UILabel *payResultStateLabel;
@property(nonatomic,strong)UILabel *payResultPriceLabel;

@property(nonatomic,strong)UIButton *okBtn;
@property(nonatomic,strong)UIView *shareView;
@property(nonatomic,strong)UIImageView *shareIcon;
@property(nonatomic,strong)UILabel *shareLabel;
@end

@implementation PayResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"支付结果";
    [self initView];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:(UIBarButtonItemStyleDone) target:nil action:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)viewDidAppear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
-(void)initView{
    self.scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    self.scrollView.backgroundColor=APPBGColor;
    
    self.payResultView=[[UIView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 100+Default_Space*2+30*2+100)];
    self.payResultView.backgroundColor=[UIColor whiteColor];
    
    self.payResultImg=[[UIImageView alloc] initWithFrame:CGRectMake((Screen_Width-100)/2, (self.payResultView.frame.size.height-100-Default_Space-30-Default_Space-30)/2, 100, 100)];
    
    self.payResultStateLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.payResultImg.frame.size.height+self.payResultImg.frame.origin.y+Default_Space, self.payResultView.frame.size.width-Default_Space*2, 30)];
    self.payResultStateLabel.textColor=APPFourColor;
    self.payResultStateLabel.font=[UIFont systemFontOfSize:FONT_SIZE_XL];
    self.payResultStateLabel.textAlignment=NSTextAlignmentCenter;
    
    self.payResultPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.payResultStateLabel.frame.size.height+self.payResultStateLabel.frame.origin.y+Default_Space, self.payResultView.frame.size.width-Default_Space*2, 30)];
    self.payResultPriceLabel.textColor=APPFourColor;
    self.payResultPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_XL];
    self.payResultPriceLabel.textAlignment=NSTextAlignmentCenter;
    
    [self.payResultView addSubview:self.payResultImg];
    [self.payResultView addSubview:self.payResultPriceLabel];
    [self.payResultView addSubview:self.payResultStateLabel];
    
    self.okBtn=[[UIButton alloc] initWithFrame:CGRectMake(Default_Space, self.payResultView.frame.origin.y+self.payResultView.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 40)];
    [self.okBtn setBackgroundColor:APPColor];
    [self.okBtn setTitle:@"完成" forState:UIControlStateNormal];
    self.okBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.okBtn addTarget:self action:@selector(okBtnOnClick) forControlEvents:UIControlEventTouchUpInside];

    
    self.shareView=[[UIView alloc] initWithFrame:CGRectMake(Default_Space, self.okBtn.frame.origin.y+self.okBtn.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 40)];
    self.shareView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer *shareViewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareViewOnClick)];
    [self.shareView addGestureRecognizer:shareViewTap];
    
    NSString *shareTitleStr=@"分享给好友，TA也能赚钱你也能赚钱";
    UIFont *shareTitleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.shareIcon=[[UIImageView alloc] initWithFrame:CGRectMake((self.shareView.frame.size.width-[NSString sizeWithText:shareTitleStr font:shareTitleFont maxSize:Max_Size].width-20)/2, Default_Space, 20, 20)];
    self.shareIcon.image=[UIImage imageNamed:@"share_black_icon"];
    
    self.shareLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.shareIcon.frame.origin.x+self.shareIcon.frame.size.width, 0,[NSString sizeWithText:shareTitleStr font:shareTitleFont maxSize:Max_Size].width, 40)];
    self.shareLabel.text=shareTitleStr;
    self.shareLabel.textColor=APPFourColor;
    self.shareLabel.font=shareTitleFont;
    
//    [self.shareView addSubview:self.shareIcon];
//    [self.shareView addSubview:self.shareLabel];
    
    [self.scrollView addSubview:self.payResultView];
    [self.scrollView addSubview:self.okBtn];
//    [self.scrollView addSubview:self.shareView];
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.shareView.frame.origin.y+self.shareView.frame.size.height);
    
    [self.view addSubview:self.scrollView];

    [self loadData];
}

-(void)loadData{
    
    [self showData];
}

-(void)showData{
    NSLog(@"%@",self.price);
    if ([self.number isEqualToString:@"1"]) {
        self.payResultImg.image=[UIImage imageNamed:@"order_pay_result_ok_icon"];
        self.payResultStateLabel.text=@"支付成功";
        self.payResultPriceLabel.text=[NSString stringWithFormat:@"%@",self.price];
    }
    
    
}


#pragma mark onclick
-(void)okBtnOnClick{
//    [self.navigationController popToRootViewControllerAnimated:YES];
    [self allOrderVIewOnClick:0];
    
    
}
-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        Class class=[OrderListVC class];
        [vcArray addObject:class];
        
        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
    }
    
    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
    //    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    pageController.isscore = 0;
    pageController.payJumpPage = @"payJumpPage";
    [pageController initBar];
    
    [self tabHidePushVC: pageController];

    
}
-(void)shareViewOnClick{
    //    分享
    [UMUtil shareUM:self delegate:self title:@"我是标题啊" body:@"我是正文啊" url:@"http://git.oschina.net/" img:nil urlImg:@"http://mlskt.cn/mlskt/public/attachment/201606/28/18/57724bf7f2942_640x480.jpg"];
}


@end
