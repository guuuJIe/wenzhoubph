//
//  PayObject.m
//  519
//
//  Created by Macmini on 16/12/7.
//  Copyright © 2016年 519. All rights reserved.
//

//微信－－－－－－－－－－－－－－－－－－－－－－－－－
#define APP_ID          @"wxd8b6de3105de6063"               //APPID
#define APP_SECRET      @"3dc4b57f3cb2433ba9afbc1bcdfd378c" //appsecret
//商户号，填写商户对应参数
#define MCH_ID          @"1370694902"
//商户API密钥，填写相应参数
#define PARTNER_ID      @"b9qrqUo3UMrqj23K6Zzt1RfAWu23uguH"
//支付结果回调页面
//#define WX_NOTIFY_URL      @"http://519wz.cn/shop.php?ctl=payment&act=notify&class_name=WxApp&is_wap=1"
//获取服务器端支付数据地址（商户自定义）
#define SP_URL          @"http://wxpay.weixin.qq.com/pub_v2/app/app_pay.php"

#import "PayObject.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import "payRequsestHandler.h"
@interface PayObject ()
{
    callBack _back;
}
@end
@implementation PayObject

-(void)getParInfoWithURL:(NSDictionary *)dic callback:(callBack)block{
    
    NSString * order = dic[@"payment_code"][@"config"][@"order_spec"];
    NSString * sign = dic[@"payment_code"][@"config"][@"sign"];
    NSString * type = dic[@"payment_code"][@"config"][@"sign_type"];
    order = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",order,sign,type];
    NSLog(@"order %@",order);
    
    NSString * scheme = @"wzbph";
    
    [[AlipaySDK defaultService]payOrder:order fromScheme:scheme callback:^(NSDictionary *resultDic) {
        NSLog(@"res %@",resultDic);
        
        block(resultDic);
    }];
    
}


-(void)WXPayinfoWithDic:(NSDictionary *)dic callback:(wxpayblock)block{
    NSLog(@"wx %@",dic);
    
    payRequsestHandler *req = [payRequsestHandler alloc] ;
    [req init:APP_ID mch_id:MCH_ID];
    [req setKey:PARTNER_ID];
    //    double priceD=price.doubleValue *100;
    NSMutableDictionary *dict = [req sendPay_demo:@"payment_code"];
    if(dict == nil){
        //错误提示
        NSString *debug = [req getDebugifo];
        NSLog(@"%@\n\n",debug);
    }else{
        NSLog(@"%@\n\n",[req getDebugifo]);
        [dict removeObjectForKey:@"total_fee"];
        [dict setValue:@"0.01" forKey:@"total_fee"];
        
        //调起微信支付
        PayReq* req             = [[PayReq alloc] init];
        req.openID              = dic[@"payment_code"][@"config"][@"ios"][@"appid"];
        req.partnerId           = dic[@"payment_code"][@"config"][@"ios"][@"partnerid"];
        req.prepayId            = dic[@"payment_code"][@"config"][@"ios"][@"prepayid"];
        req.nonceStr            = dic[@"payment_code"][@"config"][@"ios"][@"noncestr"];
        req.timeStamp           = [dic[@"payment_code"][@"config"][@"ios"][@"timestamp"] intValue];
        req.package             = dic[@"payment_code"][@"config"][@"ios"][@"package"];
        req.sign                = dic[@"payment_code"][@"config"][@"ios"][@"sign"];
        
        Boolean result = [WXApi sendReq:req];
        block(result);
        NSLog(@"r-- %hhu\n\n",result);
        
        
    }
    
}

@end
