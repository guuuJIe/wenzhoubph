//
//  StoreAddressModel.m
//  519
//
//  Created by Macmini on 16/12/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "StoreAddressModel.h"

@implementation StoreAddressModel

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.ids = value;
    }
}

@end
