//
//  ChooseConponVC.h
//  519
//
//  Created by Macmini on 2018/12/17.
//  Copyright © 2018年 519. All rights reserved.
//

#import "BaseVC.h"

@interface ChooseConponVC : BaseVC
@property(nonatomic , copy)void(^clickblock)(NSDictionary *dic);
@property(nonatomic,strong)NSDictionary *dic;
@property(nonatomic,strong)NSString *buynow_id;
@property(nonatomic,strong)NSString *buynow_attr;
@property(nonatomic,strong)NSString *buynow_number;
@property(nonatomic,strong)NSString *delivery_id;
@property(nonatomic,strong)NSString *content;
@property(nonatomic,strong)NSString *email;
@property(nonatomic,strong)NSString *pwd;
@property(nonatomic,strong)NSString *buy_type;
@property(nonatomic,strong)NSString *all_account_money;

@property(nonatomic,copy)NSString *fromStr;  //0:购物车结算   1:立即购买
@end
