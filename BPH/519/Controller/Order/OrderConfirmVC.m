//
//  OrderConfirmVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderConfirmVC.h"
#import "OrderConfirmPayTC.h"
#import "OrderConfirmGoodTC.h"
#import "StoreAddressVC.h"
#import "TimerView.h"
#import "SendWayView.h"
#import "PayResultVC.h"
#import "OrderPayVC.h"
#import "AddressVC.h"
#import "AddressEditVC.h"
#import "OrderGoodsModel.h"
#import "OrderListVC.h"
#import "MinecouponVC.h"
#import "ChooseConponVC.h"
@interface OrderConfirmVC()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UITextViewDelegate,SelectTimerDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;

@property(nonatomic,strong)UIView *addressView;
@property(nonatomic,strong)UIImageView *addressIcon;
@property(nonatomic,strong)UILabel *addressNameLabel;
@property(nonatomic,strong)UILabel *addressInfoLabel;
@property(nonatomic,strong)UIImageView *addressRightImg;
@property(nonatomic,strong)UIImageView *addressLine;
@property(nonatomic,strong)UILabel * phoneLabel;
@property(nonatomic,strong)UILabel * addAddressLabel;

@property(nonatomic,strong)UIView *payTitleView;
@property(nonatomic,strong)UILabel *payTitleLabel;
@property(nonatomic,strong)UITableView *payTableView;

@property(nonatomic,strong)UITableView *goodTableView;
@property(nonatomic,strong)UIView *goodInfoView;
@property(nonatomic,strong)UILabel *goodAllPriceLabel;
@property(nonatomic,strong)UILabel *goodExpressPriceLabel;

@property(nonatomic,strong)UIView *psfsView;
@property(nonatomic,strong)UILabel *psfsTitleLabel;
@property(nonatomic,strong)UIButton *psfsBtnLabel;
@property(nonatomic,strong)UIImageView *psfsRightImg;

@property(nonatomic,strong)UIView *pssjView;
@property(nonatomic,strong)UILabel *pssjTitleLabel;
@property(nonatomic,strong)UIButton *pssjBtnLabel;
@property(nonatomic,strong)UIImageView *pssjRightImg;

@property(nonatomic,strong)UIView *psfwView;
@property(nonatomic,strong)UILabel *psfwTitleLabel;
@property(nonatomic,strong)UILabel *explainLabel;
@property(nonatomic,strong)UIImageView * sendGoodAddImg;

@property(nonatomic,strong)UIView *bzView;
@property(nonatomic,strong)UILabel *bzTitleLabel;
@property(nonatomic,strong)UIView *bzLine;
@property(nonatomic,strong)UITextView *bzText;
@property(nonatomic,strong)UILabel * bzContentLabel;

@property(nonatomic,strong)UIView *payView;
@property(nonatomic,strong)UIView *payLine;
@property(nonatomic,strong)UILabel *allPriceLabel;
@property(nonatomic,strong)UIButton *payBtn;

@property(nonatomic,strong)UIView * yuePayView;
@property(nonatomic,strong)UILabel * yuePayLable;
@property(nonatomic,strong)UILabel * priceLabel;
@property(nonatomic,strong)UISwitch *mySwitch;
@property(nonatomic,strong)UIButton * yueBtn;


@property(nonatomic,strong)UIActionSheet *psSheet;
@property(nonatomic,strong)NSArray *psSheetArray;
@property(nonatomic,strong)UIActionSheet *timeSheet;
@property(nonatomic,strong)NSArray *timeSheetArray;

@property(nonatomic,strong)NSArray *timeArray;
@property (nonatomic,copy)NSString * storeAddressId;

@property(nonatomic,strong)NSMutableArray * wayMuArray;
@property (nonatomic,strong)NSMutableArray * idMuArray;
@property (nonatomic,strong)NSMutableArray * wayPriceArray;
@property(nonatomic,copy)NSString * wayIdStr;

//收货人信息
@property (nonatomic,copy)NSString * address;
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * mobile;
@property (nonatomic,copy)NSString * consignee;

//余额
@property (nonatomic,copy)NSString * accountMoney;
@property (nonatomic,copy)NSString * isAccountMoney;
@property (nonatomic,copy)NSString * sendway;
@property (nonatomic,copy)NSString * timer;
@property (nonatomic,copy)NSString * shopid;
@property (nonatomic,copy)NSString * totalPrice;

@property(nonatomic,copy)NSString * shop_name;

@property (nonatomic,copy)NSString * goodsMoney;

@property(nonatomic,strong)NSMutableArray * cartListMuArr;

@property (nonatomic,copy)NSString * email;
@property (nonatomic,copy)NSString * pwd;
//积分
@property (nonatomic,copy)NSString * oneJifen;
@property (nonatomic,copy)NSString * allJifen;
@property (nonatomic,copy)NSString * good_buy_type;
//优惠券
@property (nonatomic,strong)UIView *conponView;
@property (nonatomic,strong)UILabel *leftLbl;
@property (nonatomic,strong)UILabel *rightLbl;
@property (nonatomic,strong)UIImageView *arrowImage;
@property (nonatomic,copy)NSString *conponid;
@end

@implementation OrderConfirmVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.typeStr;
    _wayPriceArray = [NSMutableArray new];
    
    self.cartListMuArr = [NSMutableArray new];
    [self initView];
    [self initBar];
    _email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    _pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    
    if (!_email) {
//        [self tabHidePushVC:[LoginVC new]];
        [self tabHidePushVC:[NewLoginVC new]];
        return ;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UnchooseConpons) name:@"SelectConpons" object:nil];
    
    [self addDataForSelfView];

}

- (void)UnchooseConpons{
    _conponid = NULL;
    self.rightLbl.text = [NSString stringWithFormat:@""];
    [self requestFreight];
}

-(void)addDataForSelfView{
    self.isAccountMoney = @"0";
    self.accountMoney = self.goodsDic[@"account_money"];
    BLog(@"%@",self.goodsDic[@"cart_coupon"]);
    self.priceLabel.text = [NSString stringWithFormat:@"￥ %.2lf",[self.accountMoney doubleValue]];
    //个人信息
    if (![self.goodsDic[@"consignee_info"]isEqual:[NSNull null]]) {
        self.addAddressLabel.hidden = YES;
        self.consignee = [NSString stringWithFormat:@"联系人: %@",self.goodsDic[@"consignee_info"][@"consignee"]];
        self.mobile = self.goodsDic[@"consignee_info"][@"mobile"];
        self.address = [NSString stringWithFormat:@"%@ %@ %@ %@",self.goodsDic[@"consignee_info"][@"region_lv2_name"],self.goodsDic[@"consignee_info"][@"region_lv3_name"],self.goodsDic[@"consignee_info"][@"region_lv4_name"],self.goodsDic[@"consignee_info"][@"address"]];
        self.ids = self.goodsDic[@"consignee_info"][@"id"];

        self.addressNameLabel.text=self.consignee;
        self.addressInfoLabel.text=self.address;
        self.phoneLabel.text = self.mobile;
    }else{
        self.addAddressLabel.hidden = NO;
        self.addressNameLabel.hidden=YES;
        self.addressInfoLabel.hidden=YES;
        self.phoneLabel.hidden=YES;
    }
    
    //商品信息
    self.good_buy_type = [NSString stringWithFormat:@"%@",self.goodsDic[@"buy_type"]];
    if (![self.goodsDic isEqual:[NSNull null]]) {
        
        for (NSDictionary * goodsInfoDic in self.goodsDic[@"cart_list"]) {
            
            if ([self.str isEqualToString:@"1"]) {
                self.name = goodsInfoDic[@"name"];
                self.icon = goodsInfoDic[@"icon"];
                self.number =[NSString stringWithFormat:@"x%@" ,goodsInfoDic[@"number"]];
                if ([_good_buy_type isEqualToString:@"1"]) {
                    self.price = [NSString stringWithFormat:@"%.0lf积分",[goodsInfoDic[@"return_score"] doubleValue]];
                    _goodsMoney = [NSString stringWithFormat:@"%.0lf积分" ,[goodsInfoDic[@"return_total_score"] doubleValue]];
                }else{
                    self.price = [NSString stringWithFormat:@"￥ %.2lf",[goodsInfoDic[@"total_price"] doubleValue]];
                    self.goodsMoney = [NSString stringWithFormat:@"%.2lf" ,[goodsInfoDic[@"unit_price"] doubleValue]];
                }
                
            }else{
                OrderGoodsModel * model = [OrderGoodsModel new];
                [model  setValuesForKeysWithDictionary:goodsInfoDic];
                [self.cartListMuArr addObject:model];
                if ([_good_buy_type isEqualToString:@"1"]) {
                    self.price = [NSString stringWithFormat:@"%.0lf积分",[goodsInfoDic[@"return_score"] doubleValue]];
                }else{
                    self.price = [NSString stringWithFormat:@"￥ %.2lf",[goodsInfoDic[@"total_price"] doubleValue]];
                }
            }
        }
    }
    
    //配送方式
    _wayMuArray = [NSMutableArray new];
    _idMuArray = [NSMutableArray new];
    for (NSDictionary * deleveryDic in self.goodsDic[@"delivery_list"]) {
        NSLog(@"%@,%@",deleveryDic[@"id"],deleveryDic[@"name"]);
        [_wayMuArray addObject:deleveryDic[@"name"]];
        [_idMuArray addObject:deleveryDic[@"id"]];
        
    }
    
    _shop_name = self.goodsDic[@"shop_name"];
    
    //配送时间
    _timeArray = self.goodsDic[@"delivery_time"];
    
    if ([self.good_buy_type isEqualToString:@"1"]) {
        _totalPrice = [NSString stringWithFormat:@"%.2lf",[self.goodsDic[@"total_data"][@"return_total_score"] doubleValue]];
    }else{
        _totalPrice = [NSString stringWithFormat:@"%.2lf",[self.goodsDic[@"total_data"][@"total_price"] doubleValue]];
    }
    
    
    [self chooseDefaultConpon];
    
    [self showData];
    
}
//选择默认的优惠券
- (void)chooseDefaultConpon{
     _conponid = [NSString stringWithFormat:@"%@",self.goodsDic[@"cart_coupon"][@"coupon_id"]];
    if ([self.goodsDic[@"cart_coupon"][@"coupon_id"] integerValue] != 0 ) {
       
        self.rightLbl.text = [NSString stringWithFormat:@"-%@元",self.goodsDic[@"cart_coupon"][@"price"]];
//        [self firstimeCalulateFreight];
    }else{
        self.rightLbl.text = [NSString stringWithFormat:@""];
    }
   
}


-(void)initView{
    
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc] init];
    self.scrollView.backgroundColor=APPBGColor;
    
    self.scrollView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_NavBarHeight-20);
    self.addressView=[[UIView alloc] initWithFrame:CGRectMake(0, Default_Space, self.scrollView.frame.size.width, 72)];
    self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.addressView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addAddress)];
    [self.addressView addGestureRecognizer:tap];
    self.addressIcon=[[UIImageView alloc] initWithFrame:CGRectMake(Default_Space, (self.addressView.frame.size.height-20)/2, 20, 20)];
    self.addressIcon.image=[UIImage imageNamed:@"location_black_icon"];
    self.addressNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.addressIcon.frame.size.width+self.addressIcon.frame.origin.x+Default_Space, Default_Space, 150, 20)];
    self.addressNameLabel.textColor=APPFourColor;
    self.addressNameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.addressNameLabel.frame.size.width+self.addressNameLabel.frame.origin.x+Default_Space, Default_Space, 100, 20)];
    self.phoneLabel.textAlignment = NSTextAlignmentRight;
    self.phoneLabel.textColor = APPFourColor;
    self.phoneLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    self.addressInfoLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.addressIcon.frame.size.width+self.addressIcon.frame.origin.x+Default_Space, self.addressNameLabel.frame.size.height+self.addressNameLabel.frame.origin.y+Default_Space, self.addressView.frame.size.width-20-20-Default_Space*4, 20)];
    self.addressInfoLabel.textColor=APPThreeColor;
    self.addressInfoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.addressRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.addressView.frame.size.width-Default_Space-20, (self.addressView.frame.size.height-20)/2, 20, 20)];
    self.addressRightImg.image=[UIImage imageNamed:@"right_black_icon"];
    self.addressLine=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.addressView.frame.size.height-2,self.addressView.frame.size.width , 2)];
    self.addressLine.backgroundColor=[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"address_line"]];
    self.addAddressLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,(self.addressView.frame.size.height-20)/2,self.addressView.frame.size.width-20-20-Default_Space*4,20)];
    
    self.addAddressLabel.text = @"请添加地址";
    self.addAddressLabel.textColor = APPFourColor;
    self.addAddressLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    [self.addressView addSubview:self.addAddressLabel];
    [self.addressView addSubview:self.phoneLabel];
    [self.addressView addSubview:self.addressIcon];
    [self.addressView addSubview:self.addressNameLabel];
    [self.addressView addSubview:self.addressInfoLabel];
    [self.addressView addSubview:self.addressRightImg];
    [self.addressView addSubview:self.addressLine];
    
    self.goodTableView=[[UITableView alloc]init];
    [self.goodTableView registerClass:[OrderConfirmGoodTC class] forCellReuseIdentifier:[OrderConfirmGoodTC getID]];
    self.goodTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.goodTableView.delegate=self;
    self.goodTableView.dataSource=self;
    self.goodTableView.scrollEnabled=NO;
    self.goodTableView.showsVerticalScrollIndicator=NO;
    
    self.goodInfoView=[[UIView alloc]init];
    self.goodInfoView.backgroundColor=[UIColor whiteColor];
    self.goodAllPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.goodAllPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodAllPriceLabel.textAlignment=NSTextAlignmentRight;
    self.goodExpressPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space,self.goodAllPriceLabel.frame.size.height+self.goodAllPriceLabel.frame.origin.y+Default_Space*2,Screen_Width-Default_Space*2, 20)];
    self.goodExpressPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.goodExpressPriceLabel.textAlignment=NSTextAlignmentRight;
    self.goodExpressPriceLabel.textColor=APPFourColor;
    [self.goodInfoView addSubview:self.goodAllPriceLabel];
    [self.goodInfoView addSubview:self.goodExpressPriceLabel];
    
    self.psfsView=[[UIView alloc]init];
    self.psfsView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer *psfsViewTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(psfsViewOnClick)];
    [self.psfsView addGestureRecognizer:psfsViewTap];
    NSString *psfsTitleStr=@"配送方式";
    UIFont *psfsTitleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.psfsTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, [NSString sizeWithText:psfsTitleStr font:psfsTitleFont maxSize:Max_Size].width, 20)];
    self.psfsTitleLabel.text=psfsTitleStr;
    self.psfsTitleLabel.font=psfsTitleFont;
    self.psfsTitleLabel.textColor=APPFourColor;
    self.psfsBtnLabel=[[UIButton alloc] initWithFrame:CGRectMake(self.psfsTitleLabel.frame.size.width+self.psfsTitleLabel.frame.origin.x+Default_Space, Default_Space, Screen_Width-self.psfsTitleLabel.frame.size.width-Default_Space*4-20, 20)];
    [self.psfsBtnLabel setTitleColor:APPThreeColor forState:UIControlStateNormal];
    self.psfsBtnLabel.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
        self.psfsBtnLabel.enabled=NO;
    [self.psfsBtnLabel setTitle:@"选择配送方式" forState:UIControlStateNormal];
    self.psfsBtnLabel.titleLabel.font=psfsTitleFont;
    self.psfsRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.psfsBtnLabel.frame.size.width+self.psfsBtnLabel.frame.origin.x+Default_Space, Default_Space, 20, 20)];
    self.psfsRightImg.image=[UIImage imageNamed:@"right_black_icon"];
    [self.psfsView addSubview:self.psfsTitleLabel];
    [self.psfsView addSubview:self.psfsBtnLabel];
    [self.psfsView addSubview:self.psfsRightImg];

    self.psfwView=[[UIView alloc]init];
    self.psfwView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer * psfwTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(psfwOnClick)];
    [self.psfwView addGestureRecognizer:psfwTap];
    
    self.conponView = [[UIView alloc] init];
    self.conponView.backgroundColor = [UIColor whiteColor];
    
    NSString *psfsTitleStr2=@"优惠券";
    self.leftLbl=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, [NSString sizeWithText:psfsTitleStr2 font:psfsTitleFont maxSize:Max_Size].width, 20)];
    self.leftLbl.text=psfsTitleStr2;
    self.leftLbl.font=psfsTitleFont;
    self.leftLbl.textColor=APPFourColor;
    self.rightLbl = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width-70, Default_Space, Screen_Width-self.leftLbl.frame.size.width-Default_Space*4-20, 20)];
    self.rightLbl.text = @"";
    self.rightLbl.font=psfsTitleFont;
    self.rightLbl.textColor=RGB(237, 73, 71);
    self.arrowImage=[[UIImageView alloc]initWithFrame:CGRectMake(self.psfsBtnLabel.frame.size.width+self.psfsBtnLabel.frame.origin.x+Default_Space, Default_Space, 20, 20)];
    self.arrowImage.image=[UIImage imageNamed:@"right_black_icon"];
     [self.conponView addSubview:self.leftLbl];
    [self.conponView addSubview:self.rightLbl];
    [self.conponView addSubview:self.arrowImage];
    UITapGestureRecognizer *conponViewTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(conponViewTapOnClick)];
    [self.conponView addGestureRecognizer:conponViewTap];
    
    
    self.psfwTextLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 5, Screen_Width, 35)];
    self.psfwTextLabel.textAlignment = NSTextAlignmentCenter;
    self.psfwTextLabel.numberOfLines=2;
    self.psfwTextLabel.font=[UIFont boldSystemFontOfSize:FONT_SIZE_M];
    self.psfwTextLabel.textColor=APPFourColor;
    [self.psfwView addSubview:self.psfwTextLabel];
    
    self.bzView=[[UIView alloc] init];
    self.bzView.backgroundColor=[UIColor whiteColor];
    self.bzTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.bzTitleLabel.textColor=APPFourColor;
    self.bzTitleLabel.text=@"备注";
    self.bzTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.bzLine=[[UIView alloc]initWithFrame:CGRectMake(0, 40, Screen_Width, Default_Line)];
    self.bzLine.backgroundColor=APPBGColor;
    self.bzText=[[UITextView alloc]initWithFrame:CGRectMake(Default_Space, self.bzLine.frame.size.height+self.bzLine.frame.origin.y, Screen_Width-Default_Space*2, 100)];
    self.bzText.delegate=self;
    self.bzText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.bzText.textColor=APPFourColor;
    self.bzContentLabel = [[UILabel alloc]initWithFrame:CGRectMake(2, Default_Space,50, 20)];
    self.bzContentLabel.text = @"备注";
    self.bzContentLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    self.bzContentLabel.textColor = APPThreeColor;
    self.bzContentLabel.textAlignment = NSTextAlignmentLeft;
    [self.bzText addSubview:self.bzContentLabel];
    [self.bzView addSubview:self.bzTitleLabel];
    [self.bzView addSubview:self.bzLine];
    [self.bzView addSubview:self.bzText];
    
    
    self.yuePayView = [[UIView alloc]init];
    self.yuePayView.backgroundColor = [UIColor whiteColor];
    self.yuePayLable = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width/3, 20)];
    self.yuePayLable.text = @"余额支付";
    self.yuePayLable.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    self.yuePayLable.textColor = APPFourColor;
    self.yuePayLable.textAlignment = NSTextAlignmentLeft;
    self.priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.yuePayLable.frame.size.width+_yuePayLable.frame.origin.x+Default_Space, Default_Space, Screen_Width/2-50, 20)];
    self.priceLabel.text = @"￥100";
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    self.priceLabel.textColor = APPColor;
    self.priceLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    
    self.yueBtn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-50, 5, 30, 30)];
    [self.yueBtn setImage:[UIImage imageNamed:@"close_icon"] forState:(UIControlStateNormal)];
    [self.yueBtn setImage:[UIImage imageNamed:@"open_icon"] forState:(UIControlStateSelected)];
    [self.yueBtn addTarget:self action:@selector(yuebtnonClick) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.yuePayView addSubview:_yuePayLable];
    [self.yuePayView addSubview:_priceLabel];
    [self.yuePayView addSubview:_yueBtn];
    
    [self.scrollView addSubview:self.addressView];
    [self.scrollView addSubview:self.goodTableView];
    [self.scrollView addSubview:self.goodInfoView];
    [self.scrollView addSubview:self.psfsView];
    [self.scrollView addSubview:self.conponView];
    [self.scrollView addSubview:self.pssjView];
    [self.scrollView addSubview:self.psfwView];
    [self.scrollView addSubview:self.bzView];
    [self.scrollView addSubview:self.yuePayView];
    
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        self.payView=[[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-64-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 64)];
    }else{
        self.payView=[[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-44-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 44)];
    }
    self.payView.backgroundColor=[UIColor whiteColor];
    self.allPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space+5, (Screen_Width/3)*2-Default_Space*2, 20)];
    self.allPriceLabel.textColor=APPFourColor;
    self.allPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.allPriceLabel.textAlignment=NSTextAlignmentCenter;
    self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-Screen_Width/3, 0, Screen_Width/3, 44)];
    [self.payBtn setBackgroundColor:APPColor];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"确定" forState:UIControlStateNormal];
    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.payBtn addTarget:self action:@selector(nextBtn) forControlEvents:(UIControlEventTouchUpInside)];
    self.payLine=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Line)];
    self.payLine.backgroundColor=APPBGColor;
    [self.payView addSubview:self.allPriceLabel];
    [self.payView addSubview:self.payBtn];
    [self.payView addSubview:self.payLine];
    
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.payView];
    
    [self showData];
}

-(void)loadData{

    [self showData];
}

-(void)showData{

    if ([self.str isEqualToString:@"1"]) {
        self.goodTableView.frame=CGRectMake(0, self.addressView.frame.size.height+self.addressView.frame.origin.y+Default_Space, Screen_Width, [OrderConfirmGoodTC getCellHeight]);
    }else{
        self.goodTableView.frame=CGRectMake(0, self.addressView.frame.size.height+self.addressView.frame.origin.y+Default_Space, Screen_Width, [OrderConfirmGoodTC getCellHeight]*self.cartListMuArr.count);
    }
    
    self.goodInfoView.frame=CGRectMake(0, self.goodTableView.frame.size.height+self.goodTableView.frame.origin.y+Default_Line, Screen_Width, 80);
    self.psfsView.frame=CGRectMake(0, self.goodInfoView.frame.size.height+self.goodInfoView.frame.origin.y+Default_Space, Screen_Width, 45);
     self.conponView.frame = CGRectMake(0, CGRectGetMaxY(self.psfsView.frame)+Default_Space, Screen_Width, 45);
    self.psfwView.frame=CGRectMake(0, self.conponView.frame.size.height+self.conponView.frame.origin.y+Default_Space, Screen_Width, 45);
   
    self.bzView.frame=CGRectMake(0, self.psfwView.frame.size.height+self.psfwView.frame.origin.y+Default_Space, Screen_Width, 140);
    self.yuePayView.frame = CGRectMake(0, self.bzView.frame.size.height+self.bzView.frame.origin.y+Default_Space, Screen_Width, 40);
    
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.yuePayView.frame.origin.y+self.yuePayView.frame.size.height+Default_Space*3);
    
    
    
    
    self.goodExpressPriceLabel.text=@"运费：￥0";
    self.psfwTextLabel.text=[NSString stringWithFormat:@"此商品只能在[%@]提货或配送",_shop_name];
//    self.explainLabel.text = @"商家配送17点完成下单前当日配送，\n17点后完成下单次日配送";
    
    if ([self.good_buy_type isEqualToString:@"1"]) {
        _totalPrice = [NSString stringWithFormat:@"%.0lf积分",[_totalPrice doubleValue]];
        NSMutableAttributedString *allPrice=[[NSMutableAttributedString alloc] initWithString:_totalPrice];
        [allPrice addAttribute:NSForegroundColorAttributeName value:APPColor range:NSMakeRange(0,allPrice.string.length)];
        self.goodAllPriceLabel.attributedText=allPrice;
        self.allPriceLabel.text=[NSString stringWithFormat:@"总计:%@",_totalPrice ];
    }else{
        _totalPrice = [NSString stringWithFormat:@"￥ %@",_totalPrice ];
        NSMutableAttributedString *allPrice=[[NSMutableAttributedString alloc] initWithString:_totalPrice];
        [allPrice addAttribute:NSForegroundColorAttributeName value:APPFourColor range:NSMakeRange(0,2)];
        [allPrice addAttribute:NSForegroundColorAttributeName value:APPColor range:NSMakeRange(2,allPrice.string.length-2)];
        self.goodAllPriceLabel.attributedText=allPrice;
        self.allPriceLabel.text=[NSString stringWithFormat:@"总计:%@",_totalPrice ];
    }
    [self.payTableView reloadData];
    [self.goodTableView reloadData];
    
    double remainMoney = [self.accountMoney doubleValue];
    double nowMoney = [self.goodsMoney doubleValue];
    if (remainMoney < nowMoney || remainMoney == 0) {
        _yuePayView.hidden = YES;
    }else{
        _yuePayView.hidden = NO;
    }
}

-(void)addAddress{
    
    if (self.addAddressLabel.hidden == NO) {
        AddressEditVC * vc = [AddressEditVC new];
        vc.isedit = @"0"; //0:添加 1:修改
        vc.isBuy = @"1";    
        vc.typeStr=@"添加地址";
        __weak typeof (self)weself = self;
        vc.pushBlock = ^(NSString * name,NSString * phone,NSString * address){
            weself.addAddressLabel.hidden = YES;
            weself.addressNameLabel.hidden=NO;
            weself.addressInfoLabel.hidden=NO;
            weself.phoneLabel.hidden=NO;
            weself.addressNameLabel.text=name;
            weself.addressInfoLabel.text=address;
            weself.phoneLabel.text = phone;
            if (weself.wayIdStr) {
                [weself requestFreight];
            }
        };
        
        [self tabHidePushVC:vc];
    }else{
        AddressVC * vc = [AddressVC new];
        vc.pushStr = @"0";
        __weak typeof (self)weself = self;
        vc.pushBlock = ^(NSString * name,NSString * phone,NSString * address,NSString * ids){
            weself.addAddressLabel.hidden = YES;
            weself.addressNameLabel.hidden=NO;
            weself.addressInfoLabel.hidden=NO;
            weself.phoneLabel.hidden=NO;
            weself.ids = ids;
            weself.addressNameLabel.text=name;
            weself.addressInfoLabel.text=address;
            weself.phoneLabel.text = phone;
            if (weself.wayIdStr) {
                [weself requestFreight];
            }
        };
        [self tabHidePushVC:vc];
    }
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([self.str isEqualToString:@"1"]) {
        return 1;
    }
    return _cartListMuArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderConfirmGoodTC *cell=[tableView dequeueReusableCellWithIdentifier:[OrderConfirmGoodTC getID] forIndexPath:indexPath];
    
    if(cell==nil){
        cell=[[OrderConfirmGoodTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[OrderConfirmGoodTC getID]];
    }
    if ([self.str isEqualToString:@"1"]) {
        [cell showDataName:self.name icon:self.icon price:self.price number:self.number speci:self.attr];
    }else{
        OrderGoodsModel * model = _cartListMuArr[indexPath.row];
        cell.model = model;
    }
        
        return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return [OrderConfirmGoodTC getCellHeight];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark onClick
//配送方式
-(void)psfsViewOnClick{
    
    SendWayView * sendway = [[SendWayView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)nameArray:[self.wayMuArray copy] withIdArray:[self.idMuArray copy] withPriceArr:[_wayPriceArray copy]];
    __weak typeof (self)weself = self;
    sendway.sendWayBlock = ^(NSString * wayids,NSString * wayName){
        weself.wayIdStr = wayids;
        [weself.psfsBtnLabel setTitle:wayName forState:(UIControlStateNormal)];
        [weself.psfsBtnLabel setTitleColor:APPFourColor forState:(UIControlStateNormal)];
        [weself requestFreight];
    };
    [self.view addSubview:sendway];
}

//选取优惠券
- (void)conponViewTapOnClick{
//    NSString * name = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
//    if (name) {
//        [self tabHidePushVC:[MinecouponVC new]];
//    }else{
//        [self tabHidePushVC:[NewLoginVC new]];
//    }
    ChooseConponVC *vc = [ChooseConponVC new];
//    if (self.str == 0) {
//        //购物车购买
//        vc.delivery_id = self.wayIdStr;
//        vc.content = self.bzText.text;
//        vc.all_account_money = self.isAccountMoney;
//        vc.buy_type = self.good_buy_type;
//    }else{
        //非购物车购买
        vc.buynow_id = self.buynowID;
        vc.buynow_attr = self.attrId;
        vc.buynow_number = self.ordernumber;
        vc.delivery_id = self.wayIdStr;
        vc.content = self.bzText.text;
        vc.all_account_money = self.isAccountMoney;
        
        vc.buy_type = self.good_buy_type;
//    }
    vc.email = self.email;
    vc.pwd = self.pwd;
    vc.fromStr = self.str;
    WeakSelf(self);
    vc.clickblock = ^(NSDictionary *dic) {
        BLog(@"%@",dic);
        weakself.conponid = [NSString stringWithFormat:@"%@",dic[@"coupon_id"]];
        self.rightLbl.text = [NSString stringWithFormat:@"-%@元",dic[@"price"]];
        [weakself requestFreight];
    };
    [self tabHidePushVC:vc];
    
}

//时间
-(void)pssjViewOnClick{
    TimerView * timerView=  [[TimerView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    timerView.alltimerArr = self.timeArray;
    timerView.timerdelegate = self;
    [self.view addSubview:timerView];
}
//门店
-(void)psfwOnClick{
}


-(void)yuebtnonClick{
    double remainMoney = [self.accountMoney doubleValue];
    double nowMoney = [self.goodsMoney doubleValue];
    
    if (remainMoney < nowMoney) {
        self.yueBtn.selected = NO;
        self.isAccountMoney = @"0";
        [MBProgressHUD showError:@"" toView:self.view];
    }else{
        if (self.yueBtn.selected == YES) {
            self.yueBtn.selected = NO;
            self.isAccountMoney = @"0";
        }else{
            self.yueBtn.selected = YES;
            self.isAccountMoney = @"1";
        }
    }
}


-(void)nextBtn{   
    
    NSString * submitUrl;
    [MBProgressHUD showHUD];
    if ([self.str isEqualToString:@"0"]) {
        //购物车购买
        submitUrl = [NSString stringWithFormat:@"%@ctl=cart&act=done&&delivery_id=%@content=%@&all_account_money=%@&email=%@&pwd=%@&buy_type=%@&coupon_id=%@",FONPort,self.wayIdStr,self.bzText.text,self.isAccountMoney,_email,_pwd,_good_buy_type,_conponid];
    }else{
        //非购物车购买
        submitUrl = [NSString stringWithFormat:@"%@ctl=cart&act=done&buynow_id=%@&buynow_attr=%@&buynow_number=%@&delivery_id=%@&content=%@&all_account_money=%@&email=%@&pwd=%@&buy_type=%@&coupon_id=%@",FONPort,self.buynowID,self.attrId,self.ordernumber,self.wayIdStr,self.bzText.text,self.isAccountMoney,_email,_pwd,_good_buy_type,_conponid];
    }
    
    
    [RequestData requestDataOfUrl:submitUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:@"payover"];
        [MBProgressHUD dissmiss];
        if ([dic[@"status"] isEqual:@1]) {
            
            NSString * value = [self.allPriceLabel.text stringByReplacingOccurrencesOfString:@"总计:" withString:@""];
            value = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
            value = [value stringByReplacingOccurrencesOfString:@"￥" withString:@""];
            if([self.isAccountMoney isEqualToString:@"1"] || [self.good_buy_type isEqualToString:@"1"] || [value floatValue] == 0.0){
                PayResultVC * payresult = [PayResultVC new];
                payresult.number = @"1";
                payresult.price = self.goodAllPriceLabel.text;
                [self tabHidePushVC:payresult];
            }else{
                OrderPayVC * orderPay = [OrderPayVC new];
                orderPay.orderId = dic[@"order_id"];
                [self tabHidePushVC:orderPay];
            }
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

//第一次进来的时候的计算总价
- (void)firstimeCalulateFreight{
    NSString * freightUrl;
    if ([self.str isEqualToString:@"0"]) {
        
        /**
         *  购物车购买
         */
        freightUrl = [NSString stringWithFormat:@"%@ctl=cart&act=count_buy_total&delivery_id=%@&email=%@&pwd=%@&coupon_id=%@",FONPort,_wayIdStr,_email,_pwd,_conponid];
    }else{
        
        /**
         *  非购物车购买
         */
        freightUrl = [NSString stringWithFormat:@"%@ctl=cart&act=count_buy_total&delivery_id=%@&buynow_id=%@&buynow_attr=%@&buynow_number=%@&email=%@&pwd=%@&buy_type=%@&coupon_id=%@",FONPort,_wayIdStr,_buynowID,_attrId,_ordernumber,_email,_pwd,_good_buy_type,_conponid];
    }
    
    [RequestData requestDataOfUrl:freightUrl success:^(NSDictionary *dic) {
        NSString * freight = [NSString stringWithFormat:@"%@",dic[@"delivery_fee"]];
        NSString * isSupport = [NSString stringWithFormat:@"%@",dic[@"is_support"]];
        self.goodExpressPriceLabel.text=[NSString stringWithFormat:@"运费: ￥%.2lf",[freight doubleValue]];
        NSString * price = [NSString stringWithFormat:@"%@",dic[@"pay_price"]];
        if ([self.good_buy_type isEqualToString:@"1"]) {
            //                    self.allPriceLabel.text=[NSString stringWithFormat:@"总计: %.2lf积分",[price doubleValue]];
        }else{
            self.allPriceLabel.text=[NSString stringWithFormat:@"总计: ￥%.2lf",[price doubleValue]];
        }
    } failure:^(NSError *error) {
         [MBProgressHUD showError:error.domain toView:self.view];
    }];
    
 
}

//运费计算
-(void)requestFreight{
    
    NSString * freightUrl;
    if ([self.str isEqualToString:@"0"]) {
        
        /**
         *  购物车购买
         */
        freightUrl = [NSString stringWithFormat:@"%@ctl=cart&act=count_buy_total&delivery_id=%@&email=%@&pwd=%@&coupon_id=%@",FONPort,_wayIdStr,_email,_pwd,_conponid];
    }else{
        
        /**
         *  非购物车购买
         */
        freightUrl = [NSString stringWithFormat:@"%@ctl=cart&act=count_buy_total&delivery_id=%@&buynow_id=%@&buynow_attr=%@&buynow_number=%@&email=%@&pwd=%@&buy_type=%@&coupon_id=%@",FONPort,_wayIdStr,_buynowID,_attrId,_ordernumber,_email,_pwd,_good_buy_type,_conponid];
    }
    
    
    [RequestData requestDataOfUrl:freightUrl success:^(NSDictionary *dic) {
        
        NSString * freight = [NSString stringWithFormat:@"%@",dic[@"delivery_fee"]];
        NSString * isSupport = [NSString stringWithFormat:@"%@",dic[@"is_support"]];
        if([isSupport doubleValue] < 0){
            [MBProgressHUD showError:@"部分商品暂不支持配送" toView:self.view];
        }else{
            if([dic[@"status"] isEqual:@1]){
                self.goodExpressPriceLabel.text=[NSString stringWithFormat:@"运费: ￥%.2lf",[freight doubleValue]];
                NSString * price = [NSString stringWithFormat:@"%@",dic[@"pay_price"]];
                if ([self.good_buy_type isEqualToString:@"1"]) {
//                    self.allPriceLabel.text=[NSString stringWithFormat:@"总计: %.2lf积分",[price doubleValue]];
                }else{
                    self.allPriceLabel.text=[NSString stringWithFormat:@"总计: ￥%.2lf",[price doubleValue]];
                }
                
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            
        }
    } failure:^(NSError *error) {
        
    }];
}


#pragma mark selecttimerdelegate
-(void)selectTimerDay:(NSString *)day hour:(NSString *)hour withnumber:(NSString *)number{
    NSString * btnTitle;
    self.timer = number;
    if ([day isEqualToString:@"0"]) {
         btnTitle = [NSString stringWithFormat:@"今天 %@",hour];
    }else{
        btnTitle = [NSString stringWithFormat:@"明天 %@",hour];
    }
    
    [_pssjBtnLabel setTitle:btnTitle forState:(UIControlStateNormal)];
}


#pragma mark UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
        [self.pssjBtnLabel setTitle:[self.timeSheetArray objectAtIndex:buttonIndex] forState:UIControlStateNormal];
        [self.pssjBtnLabel setTitleColor:APPFourColor forState:UIControlStateNormal];

}

#pragma mark textview delegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.bzContentLabel.hidden = YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if(self.bzText.text.length==0){
        self.bzContentLabel.hidden = NO;
    }
}




@end
