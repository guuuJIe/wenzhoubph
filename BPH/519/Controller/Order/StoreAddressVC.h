//
//  StoreAddressVC.h
//  519
//
//  Created by Macmini on 16/12/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"


@interface StoreAddressVC : BaseVC

@property(nonatomic,copy)NSString * storeCityId;
@property (nonatomic,copy)void(^storeAddressBlock)(NSString * ids,NSString * storeName);

@end
