//
//  AddGoodVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface AddGoodVC : BaseVC
@property(nonatomic,copy)NSString *titleStr;
@property (nonatomic,strong)NSArray * goodtitleArray;
@property (nonatomic,strong)NSArray * goodnameArray;
@property (nonatomic,strong)NSArray * goodpriceArray;
@property (nonatomic,strong)NSArray * goodidArray;

@property (nonatomic,strong)NSArray * goodtypeArray;

@property (nonatomic,copy)NSString * goodsImageUrl;
@property (nonatomic,copy)NSString * goodsNameStr;
@property (nonatomic,copy)NSString * goodspriceStr;
@property (nonatomic,copy)NSString * goodsId;
@property (nonatomic,copy)NSString * buy_type;
@property (nonatomic,copy)NSString * buyScore;
@end
