//
//  User.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import "UserVC.h"
#import "LoginVC.h"
#import "UserMenuTC.h"
#import "OrderListVC.h"
#import "CollectVC.h"
#import "AddressVC.h"
#import "ShareVC.h"
#import "BackMoneyVC.h"
#import "AboutVC.h"
#import "UserInfoVC.h"
#import "AboutUsVC.h"

#import "GoodListVC.h"
#import "MainModel.h"
#import "MainWebView.h"
#import "GoodInfoVC.h"
#import "ScoreStoreVC.h"
#import "MinecouponVC.h"
@interface UserVC ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,NSURLSessionDownloadDelegate>
@property (nonatomic,strong)UIButton *barRightBtn;

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;

@property(nonatomic,strong)UIView *userView;
@property(nonatomic,strong)UIImageView *userImgView;
@property(nonatomic,strong)UILabel *userNameLabel;
@property(nonatomic,strong)UILabel *userBalanceLabel;
@property(nonatomic,strong)UIImageView *userInfoImgView;
@property(nonatomic,strong)UILabel * noLoginLabel;
@property(nonatomic,strong)UILabel * interLabel;
@property(nonatomic,strong)UIView *orderView;
@property(nonatomic,strong)UIView *allOrderView;
@property(nonatomic,strong)NSMutableArray *itemOrderArray;

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *userMenuInfoEntitys;
@property(nonatomic,strong)NSArray *menuTitleArray;
@property(nonatomic,strong)NSArray *menuIconArray;

@property(nonatomic,strong)UILabel *kfdhLabel;
@property(nonatomic,strong)UILabel *pssjLabel;
@property(nonatomic,strong)UILabel *versionlabel;


@property(nonatomic,copy)NSString * versionStr;
@property (nonatomic,copy)NSString * tel;
@end

@implementation UserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的";
    
    [self initBar];
    [self initView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(requestDataforUser) name:@"refershUserMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshbySelfView:) name:@"refreshHome3" object:nil];
    [self requestDataforUser];
}
-(void)refreshbySelfView:(NSNotification *)noti{
    MainModel * model = noti.object;
    [self viewControllerIsJumpBy:model.data withtype:model.type];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"refershUserMessage" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)requestDataforUser{
    self.userEmail = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    self.userPwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
//    if (self.userEmail) {
        NSString * userUrl = [NSString stringWithFormat:@"%@ctl=user_center&email=%@&pwd=%@",FONPort,self.userEmail,self.userPwd];
        [RequestData requestDataOfUrl:userUrl success:^(NSDictionary *dic) {
            
            if([dic[@"status"] isEqual:@1]){
                
                if (dic[@"user_avatar"]) {
                    self.userImgView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dic[@"user_avatar"]]]];
                }else{
                    NSString * imgstr = [[NSUserDefaults standardUserDefaults]objectForKey:@"headimgurl"];
                    self.userImgView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgstr]]];
                }
                
                self.userNameLabel.text = dic[@"user_name"];
                self.userBalanceLabel.text = [NSString stringWithFormat:@"余额:%@",dic[@"user_money_format"]];
                self.interLabel.text = [NSString stringWithFormat:@"积分:%@",dic[@"user_score"]];
            }else{
                
            }
            self.tel = dic[@"kf_phone"];
            self.kfdhLabel.text = [NSString stringWithFormat:@"客服电话：%@",dic[@"kf_phone"]];
            [self showData];
            
        } failure:^(NSError *error) {
            [MBProgressHUD showError:@"网络错误" toView:self.view];
            NSLog(@"%@",error);
        }];
        
//    }else{
//
//        [self showData];
//    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self requestDataforUser];
}

-(void)initBar{
    
    self.title = @"我的";
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    self.barRightBtn=[[UIButton alloc]initWithFrame:contentView.bounds];
    [self.barRightBtn setTitle:@"退出" forState:UIControlStateNormal];
    self.barRightBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.barRightBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    [self.barRightBtn addTarget:self action:@selector(barItemBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:self.barRightBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
    if (self.userName == nil) {
        self.barRightBtn.hidden = YES;
    }
}

-(void)initView{
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, Screen_Height-self.barView.frame.size.height-self.statusBarView.frame.size.height-self.tabBarController.tabBar.frame.size.height)];
    self.scrollView.backgroundColor=APPBGColor;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.bounces=NO;
    
    self.userView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 120)];
    self.userView.backgroundColor=APPColor;
    UITapGestureRecognizer *userViewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userViewOnClick)];
    [self.userView addGestureRecognizer:userViewTap];
    
    self.userImgView=[[UIImageView alloc]initWithFrame:CGRectMake(20, (self.userView.frame.size.height-60)/2, 60, 60)];
    self.userImgView.layer.cornerRadius=30;
    self.userImgView.layer.masksToBounds=YES;
    
    //已登录
    self.userNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.userImgView.frame.size.width+self.userImgView.frame.origin.x+20, self.userImgView.frame.origin.y+10, self.userView.frame.size.width-self.userImgView.frame.size.width-self.userImgView.frame.origin.x-20-20-20, 20)];
    self.userNameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.userNameLabel.textColor=[UIColor whiteColor];
    self.userBalanceLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.userNameLabel.frame.origin.x,self.userNameLabel.frame.origin.y+self.userNameLabel.frame.size.height, self.userNameLabel.frame.size.width, self.userNameLabel.frame.size.height)];
    self.userBalanceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.userBalanceLabel.textColor=[UIColor whiteColor];
    self.interLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.userNameLabel.frame.origin.x, self.userBalanceLabel.frame.origin.y+self.userBalanceLabel.frame.size.height, self.userNameLabel.frame.size.width, 20)];
    self.interLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.interLabel.textColor=[UIColor whiteColor];
    self.userInfoImgView=[[UIImageView alloc]initWithFrame:CGRectMake(self.userView.frame.size.width-40, (self.userView.frame.size.height-20)/2, 20, 20)];
    self.userInfoImgView.image=[UIImage imageNamed:@"right_white_icon"];
    //未登录
    self.noLoginLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.userView.frame.size.width-self.userImgView.frame.size.width-self.userImgView.frame.origin.x-20-20-20, 20)];
    self.noLoginLabel.center = CGPointMake(self.userView.frame.size.width/2+20, self.userView.frame.size.height/2);
    self.noLoginLabel.hidden = YES;
    self.noLoginLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.noLoginLabel.textColor=[UIColor whiteColor];
    
    
    
    
    //40 +  w / 4  mar＝10
    self.orderView=[[UIView alloc]initWithFrame:CGRectMake(0, self.userView.frame.origin.y+self.userView.frame.size.height+Default_Space, self.userView.frame.size.width, 40+Default_Line+(Screen_Width-(5*Default_Space))/4+2*Default_Space-Default_Space)];
    self.orderView.backgroundColor=[UIColor whiteColor];
    self.allOrderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.orderView.frame.size.width, 40)];
    UITapGestureRecognizer *allOrderViewTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(allOrderVIewOnClickToFirst)];
    [self.allOrderView addGestureRecognizer:allOrderViewTap];
    
    UIImageView *myOrderImgView=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 20, 20)];
    myOrderImgView.image=[UIImage imageNamed:@"wodedingdan_icon"];
    
    NSString *myOrderStr=@"我的订单";
    UIFont *myOrderFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    UILabel *myOrderLabel=[[UILabel alloc]initWithFrame:CGRectMake(myOrderImgView.frame.origin.x+myOrderImgView.frame.size.width+Default_Space, Default_Space, [NSString sizeWithText:myOrderStr font:myOrderFont maxSize:Max_Size].width, 20)];
    myOrderLabel.text=myOrderStr;
    myOrderLabel.textColor=APPFourColor;
    myOrderLabel.textAlignment=NSTextAlignmentCenter;
    myOrderLabel.font=myOrderFont;
    
    NSString *allOrderStr=@"查看全部订单";
    UIFont *allOrderFont=[UIFont systemFontOfSize:FONT_SIZE_XX];
    UILabel *allOrderLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.allOrderView.frame.size.width-Default_Space-20-Default_Space-[NSString sizeWithText:allOrderStr font:allOrderFont maxSize:Max_Size].width, Default_Space, [NSString sizeWithText:allOrderStr font:allOrderFont maxSize:Max_Size].width, 20)];
    allOrderLabel.text=allOrderStr;
    allOrderLabel.textColor=APPFourColor;
    allOrderLabel.textAlignment=NSTextAlignmentCenter;
    allOrderLabel.font=allOrderFont;
    
    UIImageView *allOrderImgView=[[UIImageView alloc]initWithFrame:CGRectMake(self.allOrderView.frame.size.width-Default_Space-20,Default_Space, 20, 20)];
    allOrderImgView.image=[UIImage imageNamed:@"right_black_icon"];
    
    UIView *allOrderLine=[[UIView alloc]initWithFrame:CGRectMake(0, self.allOrderView.frame.size.height, self.allOrderView.frame.size.width, Default_Line)];
    allOrderLine.backgroundColor=APPBGColor;
    
    NSArray *orderTypeTitleArray=[[NSArray alloc] initWithObjects:@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSArray *orderTypeImgArray=[[NSArray alloc] initWithObjects:@"daifukuan_icon",@"daipeisong_icon",@"daipingjia_icon",@"shouhou_icon", nil];
    self.itemOrderArray =[[NSMutableArray alloc]init];
    for (int i=0; i<orderTypeTitleArray.count; i++) {
        //间距 10 相互之间
        CGFloat width=(self.orderView.frame.size.width-Default_Space*5)/4;
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake((width*i)+(i+1)*Default_Space, allOrderLine.frame.origin.y+allOrderLine.frame.size.height+Default_Space,width, width-Default_Space*2)];
        view.tag = 4000+i;
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTap:)];
        [view addGestureRecognizer:tap];
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(30, 10, view.frame.size.width-55, view.frame.size.width-55)];
        img.image=[UIImage imageNamed:[orderTypeImgArray objectAtIndex:i]];
        
        UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(3, CGRectGetMaxY(img.frame)+10, view.frame.size.width, 20)];
        title.textColor=APPFourColor;
        title.text=[orderTypeTitleArray objectAtIndex:i];
        title.font=[UIFont systemFontOfSize:FONT_SIZE_M];
        title.textAlignment=NSTextAlignmentCenter;
        
        [view addSubview:img];
        [view addSubview:title];
     
        [self.itemOrderArray addObject:view];
        
        [self.orderView addSubview:view];
    }
    
    self.tableView=[[UITableView alloc]init];
    [self.tableView registerClass:[UserMenuTC class] forCellReuseIdentifier:[UserMenuTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.scrollEnabled=NO;
    
    self.kfdhLabel=[[UILabel alloc]init];
    self.kfdhLabel.textColor=APPColor;
    self.kfdhLabel.text=@"客服电话：";
    self.kfdhLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.kfdhLabel.backgroundColor=[UIColor whiteColor];
    self.kfdhLabel.textAlignment=NSTextAlignmentCenter;
    self.kfdhLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(taptel)];
    [self.kfdhLabel addGestureRecognizer:tap];
    
    NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
    self.versionlabel=[[UILabel alloc]init];
    self.versionlabel.textColor=APPThreeColor;
    //self.versionlabel.text=[NSString stringWithFormat:@"v%@",nowSysVer];
    self.versionlabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.versionlabel.textAlignment=NSTextAlignmentCenter;

    
    [self.userView addSubview:self.userImgView];
    [self.userView addSubview:self.userNameLabel];
    [self.userView addSubview:self.userBalanceLabel];
    [self.userView addSubview:self.userInfoImgView];
    [self.userView addSubview: self.noLoginLabel];
    
    [self.allOrderView addSubview:myOrderImgView];
    [self.allOrderView addSubview:myOrderLabel];
    [self.allOrderView addSubview:allOrderLabel];
    [self.allOrderView addSubview:allOrderImgView];
    [self.orderView addSubview:self.allOrderView];
    [self.orderView addSubview:allOrderLine];
    
    [self.scrollView addSubview:self.userView];
    [self.scrollView addSubview:self.orderView];
    [self.scrollView addSubview:self.tableView];
    [self.scrollView addSubview:self.kfdhLabel];
    [self.scrollView addSubview:self.versionlabel];
    
    [self.view addSubview:self.scrollView];
    
//    [self LoadData];
}
-(void)taptel{
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}
-(void)viewTap:(UITapGestureRecognizer *)tap{
    NSInteger index = tap.view.tag-4000;
    [self allOrderVIewOnClick:index+1];
}
-(void)allOrderVIewOnClickToFirst{
    [self allOrderVIewOnClick:0];
}
-(void)LoadData{
    
    [self showData];
}

-(void)showData{
    self.userEmail = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    self.userName = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_name"];
    
    if (self.userName) {
        self.userNameLabel.text = self.userName;
        self.userNameLabel.hidden = NO;
        self.userBalanceLabel.hidden = NO;
        self.barRightBtn.hidden = NO;
        self.interLabel.hidden = NO;
        self.noLoginLabel.hidden = YES;

    }else{
        self.userNameLabel.hidden = YES;
        self.userBalanceLabel.hidden = YES;
        self.barRightBtn.hidden = YES;
        self.interLabel.hidden = YES;
        self.noLoginLabel.hidden = NO;
        
        _userImgView.image = [UIImage imageNamed:@"user_white"];
    }
    
    self.noLoginLabel.text = @"登录/注册";
    
    //,@"分享有礼",@"我的返利",@""   ,@"fenxiangyouli_icon",@"wodefanli_icon",@""
    self.menuTitleArray=[[NSArray alloc]initWithObjects:@"我的收藏",@"我的评论",@"我的优惠券",@"地址管理",@"",@"分享有礼",@"门店招商",@"",@"清空缓存",@"在线客服",@"当前版本",@"推荐给朋友",@"关于泊啤汇",nil];
    self.menuIconArray=[[NSArray alloc]initWithObjects:@"wodeshoucang_icon",@"daipingjia_icon",@"conponimage",@"dizhiguanli_icon",@"",@"礼物",@"招商",@"",@"qingchuhuancun_icon",@"1的副本",@"guanyuwomen_icon",@"礼物",@"guanyuwomen_icon",nil];
    NSArray *menuInfoArray=[[NSArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",[NSString stringWithFormat:@"%.1fMB",[[SDImageCache sharedImageCache] checkTmpSize]],@"",[NSString stringWithFormat:@"v%@",versio],@"",@"", nil];
    
    self.userMenuInfoEntitys=[[NSMutableArray alloc]init];
    CGFloat tableviewHeight=0;
    for (int i=0; i<self.menuTitleArray.count; i++) {
        NSString *title=[self.menuTitleArray objectAtIndex:i];
        if(title.length==0){
            tableviewHeight=tableviewHeight+Default_Space;
        }else{
            tableviewHeight=tableviewHeight+40;
        }
        UserMenuEntity *entity=[[UserMenuEntity alloc]init];
        entity.title=[self.menuTitleArray objectAtIndex:i];
        entity.icon=[self.menuIconArray objectAtIndex:i];
        entity.info=[menuInfoArray objectAtIndex:i];
        [self.userMenuInfoEntitys addObject:entity];
    }
    [self.tableView reloadData];
    self.tableView.frame=CGRectMake(0, self.orderView.frame.origin.y+self.orderView.frame.size.height+Default_Space, Screen_Width,tableviewHeight);
    
    self.kfdhLabel.frame=CGRectMake(0, self.tableView.frame.origin.y+self.tableView.frame.size.height+Default_Space, Screen_Width, 30);

    self.versionlabel.frame = CGRectMake(0, self.kfdhLabel.frame.size.height+self.kfdhLabel.frame.origin.y, Screen_Width, 30);
    
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.versionlabel.frame.origin.y+self.versionlabel.frame.size.height+Default_Space);
}


#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userMenuInfoEntitys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserMenuEntity *entity=[self.userMenuInfoEntitys objectAtIndex:indexPath.row];
    
    if(entity.title.length==0){
        UITableViewCell *cell=[[UITableViewCell alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, Default_Space)];
        cell.backgroundColor=APPBGColor;
        return cell;
    }else{
        UserMenuTC *cell=[tableView dequeueReusableCellWithIdentifier:[UserMenuTC getID] forIndexPath:indexPath];
        if(cell==nil){
            cell=[[UserMenuTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[UserMenuTC getID]];
        }
        [cell showData:entity];
        
        return cell;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserMenuEntity *entity=[self.userMenuInfoEntitys objectAtIndex:indexPath.row];
    
    if(entity.title.length==0){
        return Default_Space;
    }else{
        return 40;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSString *title=[self.menuTitleArray objectAtIndex:indexPath.row];
    
    if([title equals:@"清空缓存"]){
        [[SDImageCache sharedImageCache]clearDisk];
        [self showData];
    }else if([title equals:@"在线客服"]){
        MainWebView * vc = [[MainWebView alloc]init];
         NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
        vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=%@&order_sn=%@",EMAIL,nowSysVer,[[UIDevice currentDevice] model],@"",@""];
        [self tabHidePushVC:vc];
    }else if([title equals:@"当前版本"]){
        [self requestDataOfServerVersion];
    }else if([title equals:@"关于泊啤汇"]){
        [self tabHidePushVC:[[AboutUsVC alloc] init]];
    }else if ([title equals:@"门店招商"]){
        MainWebView * web = [[MainWebView alloc]init];
        web.webUrl = @"http://www.bphapp.com/wap/index.php?ctl=notice&useragent=IOS_bOpIhUi90_&data_id=9";
        [self tabHidePushVC:web];
    }else if ([title equals:@"推荐给朋友"]){
//        MainWebView * web = [[MainWebView alloc]init];
//        web.webUrl = @"https://www.bphapp.com/download";
//        [self tabHidePushVC:web];
        
        [UMUtil shareUM:self delegate:self title:@"泊啤汇" body:@"喝进口酒,上泊啤汇!" url:@"https://www.bphapp.com/download" img:nil urlImg:nil];
        
    }
    else
    if(_userEmail){
        if([title equals:@"我的收藏"]){
            [self tabHidePushVC:[[CollectVC alloc] init]];
        }else if([title equals:@"地址管理"]){
            [self tabHidePushVC:[[AddressVC alloc] init]];
        }else if([title equals:@"分享有礼"]){
            MainWebView * web = [[MainWebView alloc]init];
            web.webUrl = @"http://www.bphapp.com/wap/index.php?ctl=invite_friend&useragent=IOS_bOpIhUi90_&act=i2";
            [self tabHidePushVC:web];
        }else if([title equals:@"门店招商"]){
           
        }else if([title equals:@"我的返利"]){
            [self tabHidePushVC:[[BackMoneyVC alloc] init]];
        }else if ([title equals:@"我的优惠券"]){
            [self tabHidePushVC:[MinecouponVC new]];
        }else if([title equals:@"我的评论"]){
            MainWebView * web = [[MainWebView alloc]init];
            web.webUrl = @"http://www.bphapp.com/wap/index.php?ctl=dp_list&act=mydp";
            [self tabHidePushVC:web];
        }
    }else{
//        [self tabHidePushVC:[LoginVC new]];
        [self tabHidePushVC:[NewLoginVC new]];
    }
   
}
//版本信息数据请求
-(void)requestDataOfServerVersion{
    
    NSString * versionUrl  =[NSString stringWithFormat:@"%@ctl=version&dev_type=ios",FONPort];
    [RequestData requestDataOfUrl:versionUrl success:^(NSDictionary *dic) {
        if (dic && [dic[@"status"] isEqual: @1]) {
            _versionStr = dic[@"ios_down_url"];
            NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
            if ([ nowSysVer compare:dic[@"serverVersion"]] == NSOrderedAscending) {
                if ([dic[@"forced_upgrade"] isEqual:@1]) {
                    
                    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"版本升级" message:dic[@"ios_upgrade"]  preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        _versionStr = [_versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_versionStr]];
                    }]];
                    [self presentViewController:alertVC animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"版本升级" message:dic[@"ios_upgrade"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    [alertview show];
                }
            }else{
                UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"已是最新版本" message:nil  preferredStyle:(UIAlertControllerStyleAlert)];
                [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                [self presentViewController:alertVC animated:YES completion:nil];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        _versionStr = [_versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
        NSLog(@"%@",_versionStr);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_versionStr]];
    }
}

-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        Class class=[OrderListVC class];
        [vcArray addObject:class];
        
        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
    }
    
    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
//    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    pageController.isscore = 0;
    [pageController initBar];
    
    NSLog(@"%@",_userEmail);
    if (_userEmail) {
        [self tabHidePushVC: pageController];
    }else{
//        [self tabHidePushVC:[LoginVC new]];
        [self tabHidePushVC:[NewLoginVC new]];
    }
    
}

-(void)userViewOnClick{
    
    if (self.barRightBtn.hidden == NO) {
        UserInfoVC * userinfo = [UserInfoVC new];
        userinfo.infouserPwd = self.userPwd;
        userinfo.infousername = self.userName;
        userinfo.infoImage = self.userImgView.image;
        [self tabHidePushVC:userinfo];
    }else{
//      [self tabHidePushVC:[[LoginVC alloc]init]];
        [self tabHidePushVC:[NewLoginVC new]];
    }
    
}

//登出
-(void)barItemBtnOnClick{
    [MBProgressHUD showHUD];
    NSString * loginOutUrl = [NSString stringWithFormat:@"%@&ctl=user&act=loginout",FONPort];
    
    [RequestData requestDataOfUrl:loginOutUrl success:^(NSDictionary *dic) {
        NSString * appDomain =[ [NSBundle mainBundle]bundleIdentifier];
        [[NSUserDefaults standardUserDefaults]removePersistentDomainForName: appDomain];
        self.barRightBtn.hidden = YES;
        self.userNameLabel.hidden = YES;
        self.userBalanceLabel.hidden = YES;
        self.interLabel.hidden = YES;
        self.noLoginLabel.hidden = NO;
        _userImgView.image = [UIImage imageNamed:@"user_white"];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"loginOut" object:nil];
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"]);
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];

}



- (void)saveVideoFile:(NSURL *)location WithFileName:(NSString *)filename{
    
    //1.拿到cache文件夹的路径
    NSString *cache=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject];
    //2,拿到cache文件夹和文件名
    NSString *file=[cache stringByAppendingPathComponent:filename];
    
    
    [[NSFileManager defaultManager] moveItemAtURL:location toURL:[NSURL fileURLWithPath:file] error:nil];
    //3，保存视频到相册
    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(file)) {
        //保存相册核心代码
        UISaveVideoAtPathToSavedPhotosAlbum(file, self, nil, nil);
    }
    
}


#pragma mark 数据跳转处理
-(void)viewControllerIsJumpBy:(NSDictionary *)dataArr withtype:(NSString *)type{
    NSLog(@"%@,%@",dataArr,type);
    
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    NSString * item;
    if ([type isEqualToString:@"0"]) {
        item = dataArr[@"url"];
        webView.webUrl = item;
        if(item.length>2){
            [self tabHidePushVC:webView];
        }
    }else
        if ([type isEqualToString:@"11"]) {
            item = dataArr[@"tid"];
            goods.idStr = [NSString stringWithFormat:@"%@",item];
            goods.selectstr = @"mainJumpGoods";
            [self tabHidePushVC:goods];
        }else
            if ([type isEqualToString:@"12"]) {
                item = dataArr[@"cate_id"];
                goods.idStr = [NSString stringWithFormat:@"%@",item];
                goods.selectstr = @"mainJumpGoods";
                [self tabHidePushVC:goods];
            }else
                if ([type isEqualToString:@"21"]) {
                    item = dataArr[@"item_id"];
                    GoodInfoVC * vc = [GoodInfoVC new];
                    vc.goodsId = item;
                    [self tabHidePushVC:vc];
                }else
                    if ([type isEqualToString:@"13"]) {
                        item = dataArr[@"data"];
                        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
                    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
