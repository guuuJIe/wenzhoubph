//
//  BPHScanVC.m
//  519
//
//  Created by Macmini on 2018/12/13.
//  Copyright © 2018年 519. All rights reserved.
//
#define JSCGlobalTheme0f70044 [UIColor colorWithHexString:@"f70044"]
#import "BPHScanVC.h"
#import "LBXScanResult.h"
#import "LBXScanWrapper.h"
#import "LBXScanVideoZoomView.h"
#import "LBXAlertAction.h"
#import "GoodInfoVC.h"
#import "MainWebView.h"
#import "GoodListVC.h"
@interface BPHScanVC ()
@property (nonatomic, strong) UILabel *promptLabel;
@end

@implementation BPHScanVC
-(void)initViewController{
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 3;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
//    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_part_net"];;
    
    //码框周围4个角的颜色
    style.colorAngle = JSCGlobalTheme0f70044;
    //矩形框颜色
    style.colorRetangleLine = JSCGlobalTheme0f70044;
    
    self.style = style;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initViewController];
    [self setupNavigationBar];
    [self.view addSubview:self.promptLabel];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
//    self.view.backgroundColor = [UIColor blackColor];
    
    //设置扫码后需要扫码图像
    self.isNeedScanImage = YES;
    
    
}

- (void)dealloc{
    BLog(@"销毁了");
}

- (void)setupNavigationBar {
    self.navigationItem.title = @"扫一扫";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"相册" style:(UIBarButtonItemStyleDone) target:self action:@selector(openPhoto)];
}

- (void)showError:(NSString*)str
{
    
    [LBXAlertAction showAlertWithTitle:@"提示" msg:str buttonsStatement:@[@"知道了"] chooseBlock:nil];
    
}


- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
    if (array.count < 1)
    {
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
    for (LBXScanResult *result in array) {
        NSLog(@"scanResult:%@",result.strScanned);
    }
    
    LBXScanResult *scanResult = array[0];
    
    NSString*strResult = scanResult.strScanned;
    
    self.scanImage = scanResult.imgScanned;
    
    if (!strResult) {
        
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    //震动提醒
    // [LBXScanWrapper systemVibrate];
    //声音提醒
    //[LBXScanWrapper systemSound];
    
    [self showNextVCWithScanResult:scanResult];
    
}

- (void)showNextVCWithScanResult:(LBXScanResult*)strResult{
    NSString *scanRes = strResult.strScanned;
    [self reStartDevice];
    if ([scanRes hasPrefix:@"http"]) {
        
        BLog(@"我是链接");
        if ([scanRes rangeOfString:@"ctl=list"].location != NSNotFound) {
            NSArray * arr = [scanRes componentsSeparatedByString:@"?"];
            NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
            NSString * las;
            for (NSString * substr in lastArr) {
                NSString * str;
                if ([substr isEqualToString:lastArr.firstObject]) {
                    str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@"\""];
                }else{
                    str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@""];
                }
                
                str = [str stringByReplacingOccurrencesOfString:@"]=" withString:@"\":\""];
                if (!las) {
                    las = str;
                }else{
                    las = [NSString stringWithFormat:@"%@\",\"%@",las,str];
                }
            }
            NSString * url = [NSString stringWithFormat:@"%@f={%@",FONPort,las];
            url = [url stringByReplacingOccurrencesOfString:@"ctl=list\",\"" withString:@"}&ctl=goods&"];
            GoodListVC * vc = [GoodListVC new];
            vc.isFromWeb = YES;
            vc.url = url;
            [self.navigationController pushViewController:vc animated:true];
        }else if([scanRes rangeOfString:@"ctl=item"].location != NSNotFound){
            NSArray * urlArr = [scanRes componentsSeparatedByString:@"="];
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = urlArr.lastObject;
             [self.navigationController pushViewController:vc animated:true];
        }else{
//            if ([scanRes rangeOfString:@"\0"].location != NSNotFound) {
//                scanRes = [scanRes substringWithRange:[scanRes rangeOfString:@"\0"]];
//                NSRange range = [scanRes rangeOfString:@"scanRes"];//匹配得到的下标
//                NSLog(@"rang:%@",NSStringFromRange(range));
//            }
            
            NSLog(@"%@",[NSString stringWithFormat:@"%@", scanRes]);
            
            MainWebView *jumpVC = [[MainWebView alloc] init];
            jumpVC.webUrl = [NSString stringWithFormat:@"%@", scanRes];
            [self.navigationController pushViewController:jumpVC animated:true];
        }
    }else{
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = scanRes;
        [self.navigationController pushViewController:vc animated:true];
        BLog(@"我是数字");
    }
}

- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
    if (!strResult) {
        
        strResult = @"识别失败";
    }
    
    __weak __typeof(self) weakSelf = self;
    [LBXAlertAction showAlertWithTitle:@"扫码内容" msg:strResult buttonsStatement:@[@"知道了"] chooseBlock:^(NSInteger buttonIdx) {
        [weakSelf reStartDevice];
    }];
    
    
    
}


- (UILabel *)promptLabel {
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc] init];
        _promptLabel.backgroundColor = [UIColor clearColor];
        CGFloat promptLabelX = 0;
        CGFloat promptLabelY = 0.73 * self.view.frame.size.height;
        CGFloat promptLabelW = self.view.frame.size.width;
        CGFloat promptLabelH = 25;
        _promptLabel.frame = CGRectMake(promptLabelX, promptLabelY, promptLabelW, promptLabelH);
        _promptLabel.textAlignment = NSTextAlignmentCenter;
        _promptLabel.font = [UIFont boldSystemFontOfSize:13.0];
        _promptLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _promptLabel.text = @"将二维码/条码放入框内, 即可自动扫描";
    }
    return _promptLabel;
}

#pragma mark -底部功能项
//打开相册
- (void)openPhoto
{
    if ([LBXScanWrapper isGetPhotoPermission])
        [self openLocalPhoto];
    else
    {
        [self showError:@"      请到设置->隐私中开启本程序相册权限     "];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
