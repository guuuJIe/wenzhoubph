//
//  Car.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import "CarVC.h"
#import "CarTC.h"
#import "OrderPayVC.h"
#import "OrderConfirmVC.h"
#import "CarModel.h"
#import "GoodInfoVC.h"
#import "LoginVC.h"
#import "GoodListVC.h"
#import "MainModel.h"
#import "MainWebView.h"
#import "GoodInfoVC.h"
#import "ScoreStoreVC.h"
#import "NewCarTC.h"
@interface CarVC ()<UITableViewDataSource,UITableViewDelegate,RemoveGoodsDelegate>
@property(nonatomic,strong)UIButton *backBtn;
@property (nonatomic,strong)UIButton *barRightBtn;

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)UIView *payView;
@property(nonatomic,strong)UILabel *allPriceLabel;
@property(nonatomic,strong)UIButton *payBtn;
@property(nonatomic,copy)NSString * username;
@property (nonatomic,copy)NSString * userpwd;
@property (nonatomic,copy)NSString * attr;
@property (nonatomic,copy)NSString * attr_str;
@property (nonatomic,copy)NSString * deal_id;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * max;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * number;
@property (nonatomic,copy)NSString * return_score;
@property (nonatomic,copy)NSString * return_total_score;
@property (nonatomic,copy)NSString * sub_name;
@property (nonatomic,copy)NSString * total_price;
@property (nonatomic,copy)NSString * unit_price;

@property (nonatomic,strong)NSMutableArray * iconMuarray;
@property (nonatomic,strong)NSMutableArray * nameMuarray;
@property (nonatomic,strong)NSMutableArray * subnameMuarray;
@property (nonatomic,strong)NSMutableArray * numberMuarray;
@property (nonatomic,strong)NSMutableArray * priceMuarray;
@property (nonatomic,strong)NSMutableArray * totalMuArray;
@property (nonatomic,strong)NSMutableArray *idMuarray;
@property (nonatomic,strong)NSMutableArray * dealidArray;
@property (nonatomic,strong)NSMutableArray * attrIdmuArray;
@property (nonatomic,strong)NSArray * dataArray;


@property (nonatomic,assign)NSInteger index;
//@property (nonatomic,assign)NSInteger goodsNumber;

@property (nonatomic,strong)NSMutableArray * changeGoodNumberMuArr;
@property (nonatomic,assign)double totalPrice;

@property(nonatomic,strong)UIView * showNoDataView;



@property (nonatomic,strong)NSMutableArray * cardataArray;

@end

@implementation CarVC
-(NSMutableArray *)cardataArray{
    if (!_cardataArray) {
        _cardataArray = [[NSMutableArray alloc]init];
    }
    return _cardataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"我是购物车");
    self.navigationItem.title = @"购物车";
    self.totalPrice = 0;
    self.nameMuarray = [NSMutableArray new];
    self.iconMuarray = [NSMutableArray new];
    self.subnameMuarray = [NSMutableArray new];
    self.numberMuarray = [NSMutableArray new];
    self.priceMuarray = [NSMutableArray new];
    self.idMuarray = [NSMutableArray new];
    self.dealidArray = [NSMutableArray new];
    self.attrIdmuArray = [NSMutableArray new];
    self.totalMuArray = [NSMutableArray new];
    
    [self initView];
    [self initBar];
    [self requestDataForCar];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshCart:) name:@"refreshCart" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addGoodsNum:) name:@"addGoodsNum" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginOut) name:@"loginOut" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshbySelfView:) name:@"refreshHome2" object:nil];
}

-(void)refreshbySelfView:(NSNotification *)noti{
    MainModel * model = noti.object;
    [self viewControllerIsJumpBy:model.data withtype:model.type];
}

-(void)viewWillDisappear:(BOOL)animated{
    if (_barRightBtn.selected == YES) {
        [self barItemBtnOnClick];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(self.totalPrice  > 0.00){
        [self.showNoDataView removeFromSuperview];
    }else{
        if (!self.showNoDataView) {
             self.showNoDataView.hidden = NO;
        }
    }
}

-(void)loginOut{
    [self refreshDataOfCart];
    [self showNoData];
    [_tableView reloadData];
}


//删除goods
-(void)removeGoodsArray:(NSArray *)arr{
    self.dataArray = arr;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"是否删除" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self delGoodsid:1];
    }
}

-(void)delGoodsid:(NSInteger)index{
    if([self.username isEqualToString:@""]){
//        [self tabHidePushVC:[[LoginVC alloc]init]];
        [self tabHidePushVC:[NewLoginVC new]];
    }
    NSInteger number = [self.dataArray[3] integerValue];
    double price = [self.dataArray[2] doubleValue];
    self.totalPrice = self.totalPrice - number * price;
    [self changeTotalPrice:self.totalPrice];
    
    NSInteger cellIndex = [self.dataArray[index] integerValue];
    
    [self.nameMuarray removeObjectAtIndex:cellIndex];
    [self.iconMuarray removeObjectAtIndex:cellIndex];
    [self.subnameMuarray removeObjectAtIndex:cellIndex];
    [self.numberMuarray removeObjectAtIndex:cellIndex];
    [self.priceMuarray removeObjectAtIndex:cellIndex];
    [self.idMuarray removeObjectAtIndex:cellIndex];
    [self.dealidArray removeObjectAtIndex:cellIndex];
    [self.attrIdmuArray removeObjectAtIndex:cellIndex];
    [self.totalMuArray removeObjectAtIndex:cellIndex];
    [self.tableView reloadData];
    if(!self.nameMuarray.count){
        self.showNoDataView.hidden = NO;
        self.tableView.hidden = YES;
        [self showNoData];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString * removeCartUrl = [NSString stringWithFormat:@"%@ctl=cart&act=del&id=%@&email=%@&pwd=%@",FONPort,self.dataArray[0],self.username,self.userpwd];
        [RequestData requestDataOfUrl:removeCartUrl success:^(NSDictionary *dic) {
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    });
}

//刷新cart
-(void)refreshCart:(NSNotification *)obj{
    [self refreshDataOfCart];
    [self requestDataForCar];
}

-(void)refreshDataOfCart{
    [self.nameMuarray removeAllObjects];
    [self.iconMuarray removeAllObjects];
    [self.subnameMuarray removeAllObjects];
    [self.numberMuarray removeAllObjects];
    [self.priceMuarray removeAllObjects];
    [self.idMuarray removeAllObjects];
    [self.attrIdmuArray removeAllObjects];
    [self.dealidArray removeAllObjects];
    [self.totalMuArray removeAllObjects];
    
}
-(void)addGoodsNum:(NSNotification *)noti{
    
    NSInteger index = [noti.object[1] integerValue];
    NSInteger oldNumber = [self.numberMuarray[index]integerValue];
    NSInteger newNumber = [noti.object[0] integerValue];
    double  onePrice = [noti.object[2] doubleValue];
    
    self.totalPrice = self.totalPrice+(newNumber-oldNumber)*onePrice;
    [self changeTotalPrice:self.totalPrice];
    self.numberMuarray[index] = noti.object[0];
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"refreshCart" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"addGoodsNum" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"loginOut" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)requestDataForCar{
    
    _index =1;
    self.username = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    self.userpwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    if(self.username){
        NSString * cardataUrl = [NSString stringWithFormat:@"%@ctl=cart&act=index&email=%@&pwd=%@",FONPort,self.username,self.userpwd];
        [RequestData requestDataOfUrl:cardataUrl success:^(NSDictionary *dic) {
            
            [MBProgressHUD dissmiss];
            if (![dic[@"cart_list"] isEqual:[NSNull null]]) {
                for (NSDictionary * subdic in dic[@"cart_list"]) {
                    [self.nameMuarray addObject: subdic[@"name"]];
                    [self.subnameMuarray addObject:subdic[@"attr_str"]];
                    [self.priceMuarray addObject:subdic[@"unit_price"]];
                    [self.numberMuarray addObject:subdic[@"number"]];
                    [self.iconMuarray addObject:subdic[@"icon"]];
                    [self.idMuarray addObject:subdic[@"id"]];
                    [self.dealidArray addObject:subdic[@"deal_id"]];
                    [self.attrIdmuArray addObject:subdic[@"attr"]];
                    [self.totalMuArray addObject:subdic[@"total_price"]];
                }
                _total_price = [NSString stringWithFormat:@"%.2lf", [dic[@"total_data"][@"total_price"] doubleValue]];
                [self showData];
            }else{
                if (!self.showNoDataView) {
                    [self showNoData];
                }
            }
        } failure:^(NSError *error) {
            [MBProgressHUD dissmiss];
            NSLog(@"%@",error);
        }];
    }else{
        [self showNoData];
    }
}


-(void)initView{
    //滑动view
    self.tableView=[[UITableView alloc]init];
//    [self.tableView registerClass:[CarTC class] forCellReuseIdentifier:[CarTC getID]];
    [self.tableView registerNib:[UINib nibWithNibName:@"NewCarTC" bundle:nil] forCellReuseIdentifier:@""];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.backgroundColor=APPBGColor;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    
    
    //刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshDataOfCart)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    

    self.payView=[[UIView alloc]init];
    self.payView.backgroundColor=APPFourColor;
    
    //获取手机型号
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        if(self.haveBackBtn){
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-64);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-64, Screen_Width, 44);
        }else{
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-64);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-64-64, Screen_Width, 44);
        }
    }else{
        if(self.haveBackBtn){
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-40);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-44, Screen_Width, 44);
        }else{
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-40);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-44-50, Screen_Width, 44);
        }
    }
    
    
   
    self.allPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, (Screen_SelfWidth/3)*2-Default_Space*2, 20)];
    
    self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_SelfWidth-Screen_SelfWidth/3, 0, Screen_SelfWidth/3, self.payView.frame.size.height)];
    [self.payBtn setBackgroundColor:APPColor];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"结算" forState:UIControlStateNormal];
    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.payBtn addTarget:self action:@selector(orderpayBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.payView addSubview:self.allPriceLabel];
    [self.payView addSubview:self.payBtn];

    [self.view addSubview:self.tableView];
    [self.view addSubview:self.payView];
}

-(void)loadData{
    _index ++ ;
    if(self.username){
        NSString * cardataUrl = [NSString stringWithFormat:@"%@ctl=cart&act=index&page=%ld&email=%@&pwd=%@",FONPort,(long)_index,self.username,self.userpwd];
        [RequestData requestDataOfUrl:cardataUrl success:^(NSDictionary *dic) {
            if (![dic[@"cart_list"] isEqual:[NSNull null]]) {
                for (NSDictionary * subdic in dic[@"cart_list"]) {
                    [self.nameMuarray addObject: subdic[@"name"]];
                    [self.subnameMuarray addObject:subdic[@"attr_str"]];
                    [self.priceMuarray addObject:subdic[@"unit_price"]];
                    [self.numberMuarray addObject:subdic[@"number"]];
                    [self.iconMuarray addObject:subdic[@"icon"]];
                    [self.idMuarray addObject:subdic[@"id"]];
                    [self.dealidArray addObject:subdic[@"deal_id"]];
                    [self.attrIdmuArray addObject:subdic[@"attr"]];
                    [self.totalMuArray addObject:subdic[@"total_price"]];
                }
                _total_price = [NSString stringWithFormat:@"%.2lf", [dic[@"total_data"][@"total_price"] doubleValue]];
                [self.tableView reloadData];
                
                [self showData];
            }else{
                
                _total_price = @"0.00";
                if (!self.showNoDataView) {
                    [self showNoData];
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

-(void)showData{
    self.showNoDataView.hidden = YES;
    self.tableView.hidden = NO;
    self.payBtn.backgroundColor = APPColor;
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    
    [self.tableView reloadData];
    self.totalPrice = [_total_price doubleValue];
    [self changeTotalPrice:self.totalPrice];
}

-(void)showNoData{
    self.showNoDataView.hidden = NO;
    self.tableView.hidden = YES;
    self.payBtn.backgroundColor = APPThreeColor;
    _showNoDataView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    _showNoDataView.backgroundColor = APPBGColor;
    [self.view addSubview:_showNoDataView];
    
    UIImageView * carImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width/3, Screen_Width/3)];
    carImageView.center = CGPointMake(Screen_SelfWidth/2, Screen_SelfWidth/3);
    carImageView.image  = [UIImage imageNamed:@"goodscar2"];
    [_showNoDataView addSubview:carImageView];
    
    UILabel * carLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_SelfWidth, 40)];
    carLabel.center = CGPointMake(Screen_SelfWidth/2, carImageView.frame.size.height+carImageView.frame.origin.y+Default_Space+20);
    carLabel.textColor = APPThreeColor;
    carLabel.text = @"购物车还是空的，先去逛逛吧";
    carLabel.textAlignment = NSTextAlignmentCenter;
    carLabel.font = [UIFont systemFontOfSize:FONT_SIZE_L];
    [_showNoDataView addSubview:carLabel];
    
    UIButton * carBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    carBtn.center = CGPointMake(Screen_SelfWidth/2, carLabel.frame.size.height+carLabel.frame.origin.y+Default_Space+35) ;
    [carBtn setTitle:@"去逛逛" forState:(UIControlStateNormal)];
    [carBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    carBtn.backgroundColor = APPColor;
    [carBtn addTarget:self action:@selector(goGoodsList) forControlEvents:(UIControlEventTouchUpInside)];
    [_showNoDataView addSubview:carBtn];
    [self changeTotalPrice:0.00];
}

-(void)goGoodsList{
    GoodListVC * vc = [GoodListVC new];
    vc.selectstr = @"catJpmpGoodsList";
    [self tabHidePushVC:vc];
}

-(void)changeTotalPrice:(double)totalPrice{
    NSLog(@"%f",totalPrice);
    
    NSMutableAttributedString *priceStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计￥ %.2lf",totalPrice]];
    [priceStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,3)];
    [priceStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FONT_SIZE_X] range:NSMakeRange(0, 3)];
    [priceStr addAttribute:NSForegroundColorAttributeName value:APPColor range:NSMakeRange(3,priceStr.length-3)];
    [priceStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FONT_SIZE_M] range:NSMakeRange(3, priceStr.length-3)];
    
    self.allPriceLabel.attributedText = priceStr;
}



#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.nameMuarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CarTC *cell=[tableView dequeueReusableCellWithIdentifier:[CarTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[CarTC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[CarTC getID]];
    }
    cell.delegate = self;
    if (self.idMuarray) {
        [cell showDataCellTitle:self.nameMuarray[indexPath.row] subname:self.subnameMuarray[indexPath.row] icon:self.iconMuarray[indexPath.row] price:self.priceMuarray[indexPath.row] number:self.numberMuarray[indexPath.row] cartId:self.idMuarray[indexPath.row] cellIdex:indexPath.row isEdit:self.barRightBtn.selected] ;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CarTC getCellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.barRightBtn.selected == NO) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = self.dealidArray[indexPath.row];
        [self tabHidePushVC:vc];
    }
    
    
}

#pragma mark bar delegate
//购物车右上角编辑btn
-(void)barItemBtnOnClick{
    if(self.barRightBtn.selected){
        [self.barRightBtn setSelected:NO];
        for (NSInteger i = 0; i<_idMuarray.count; i++) {
            NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=update&id=%@&num=%@&email=%@&pwd=%@",FONPort,self.idMuarray[i],self.numberMuarray[i],_username,_userpwd];
            [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
                if([dic[@"status"] isEqual:@1]){
//                    [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
                }else{
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }
    }else{
        [self.barRightBtn setSelected:YES];
    }
    [self.tableView reloadData];
}

//购物车结算btn
-(void)orderpayBtnOnClick{
    if (_username) {
        if (_nameMuarray.count>0) {
            NSString * checkUrl = [NSString stringWithFormat:@"%@ctl=cart&act=check&email=%@&pwd=%@",FONPort,_username,_userpwd];
            [RequestData requestDataOfUrl:checkUrl success:^(NSDictionary *dic) {
                if ([dic[@"status"] isEqual:@0]) {
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }else{
                    OrderConfirmVC * vc =  [[OrderConfirmVC alloc] init];
                    vc.str = @"0";
                    vc.typeStr = @"结算";
                    vc.goodsDic = dic;
                    [self tabHidePushVC:vc];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
            }];
        }
    }else{
//        [self tabHidePushVC:[LoginVC new]];
        [self tabHidePushVC:[NewLoginVC new]];
    }
}


#pragma mark 数据跳转处理
-(void)viewControllerIsJumpBy:(NSDictionary *)dataArr withtype:(NSString *)type{
    NSLog(@"%@,%@",dataArr,type);
    
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    NSString * item;
    if ([type isEqualToString:@"0"]) {
        item = dataArr[@"url"];
        webView.webUrl = item;
        if(item.length>2){
            [self tabHidePushVC:webView];
        }
    }else
        if ([type isEqualToString:@"11"]) {
            item = dataArr[@"tid"];
            goods.idStr = [NSString stringWithFormat:@"%@",item];
            goods.selectstr = @"mainJumpGoods";
            [self tabHidePushVC:goods];
        }else
            if ([type isEqualToString:@"12"]) {
                item = dataArr[@"cate_id"];
                goods.idStr = [NSString stringWithFormat:@"%@",item];
                goods.selectstr = @"mainJumpGoods";
                [self tabHidePushVC:goods];
            }else
                if ([type isEqualToString:@"21"]) {
                    item = dataArr[@"item_id"];
                    GoodInfoVC * vc = [GoodInfoVC new];
                    vc.goodsId = item;
                    [self tabHidePushVC:vc];
                }else
                    if ([type isEqualToString:@"13"]) {
                        item = dataArr[@"data"];
                        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
                    }
}



@end
