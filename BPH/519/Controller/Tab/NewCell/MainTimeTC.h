//
//  MainTimeTC.h
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

typedef void(^mainTimeBlock)(int num, NSString * ids);
#import <UIKit/UIKit.h>

@interface MainTimeTC : BaseTC
@property(nonatomic,copy)mainTimeBlock block;
@property (nonatomic,strong)UICollectionView * collectionView;
-(void)showTimeData:(NSArray *)dataArr andneedReload:(BOOL)isReload;
+(NSString*)getID;
@property (nonatomic,assign)BOOL isNeedRefresh;
@end
