//
//  MainElseHotTC.h
//  519
//
//  Created by Macmini on 17/3/23.
//  Copyright © 2017年 519. All rights reserved.
//
typedef void(^MainElseHotBlock)(NSString * ids);
#import "BaseTC.h"

@interface MainElseHotTC : BaseTC
@property (nonatomic,copy)MainElseHotBlock block;

-(void)showMainElseHotData:(NSArray *)dataArray;

+(NSString *)getelseId;
@property(nonatomic,strong)UICollectionView *collectionView;

@property(nonatomic,strong)NSIndexPath *index;

@end
