//
//  MainSubjectTC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/3.
//  Copyright © 2018年 519. All rights reserved.
//

#import "MainSubjectTC.h"
#import "MainModel.h"

@interface MainSubjectTC()<UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,strong)NSMutableArray * dataArray;
@property (nonatomic,assign)CGFloat  height;
@end

@implementation MainSubjectTC
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
      
        [self craeteUI];
    }
    return self;
}

-(void)craeteUI{
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/750*76)];
    imageView.image = [UIImage imageNamed:@"主题精选"];
    [self addSubview:imageView];
    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake(Screen_Width/2-.5, _height);
    layout.minimumInteritemSpacing = 1;
    layout.minimumLineSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"subjectCellIde"];
    [self addSubview:self.collectionView];
}

-(void)showSubjectData:(NSArray *)dataArray withSubHeight:(CGFloat)subHeight{
    if (self.dataArray) {
        [self.dataArray removeAllObjects];
    }
    _height = subHeight;
    for (NSDictionary * subdic in dataArray) {
        MainModel * model = [[MainModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.dataArray addObject:model];
    }
    self.collectionView.frame = CGRectMake(0, 40, Screen_Width, (self.dataArray.count+1)/2*_height);
//    if (_isNeedRefresh) {
//        _isNeedRefresh = !_isNeedRefresh;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
    
//    }
    
}



#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * subjectCell = @"subjectCellIde";
    MainModel * model = self.dataArray[indexPath.row];
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:subjectCell forIndexPath:indexPath];
    if(cell==nil){
        cell=[[UICollectionViewCell alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    UIImageView * imgView = [[UIImageView alloc]initWithFrame:cell.bounds];
    [imgView sd_setImageWithURL:[NSURL URLWithString:model.img]];
    [cell addSubview:imgView];
    
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(Screen_Width/2-.5, _height);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MainModel * model = self.dataArray[indexPath.row];
    if (self.block) {
        self.block(model.data, model.type);
    }
}


//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(CGFloat )collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
+(NSString *)getID{
    return @"MainSubjectTC";
}
@end
