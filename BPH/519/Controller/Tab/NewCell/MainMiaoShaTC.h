//
//  MainMiaoShaTC.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/3.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^MainMiaoShaBlock)(int num, NSString *ids);
#import <UIKit/UIKit.h>

@interface MainMiaoShaTC : UITableViewCell



@property (nonatomic,assign)BOOL isNeedRefresh;
@property (nonatomic,copy)MainMiaoShaBlock block;
-(void)showMiaoshaDataByTimer:(NSDictionary *)dics withData:(NSArray *)dataArr andneedReload:(BOOL)isReload;
+(NSString*)getID;
@end
