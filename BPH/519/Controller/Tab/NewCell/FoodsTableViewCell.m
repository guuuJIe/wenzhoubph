//
//  FoodsTableViewCell.m
//  519
//
//  Created by Macmini on 2019/1/10.
//  Copyright © 2019 519. All rights reserved.
//

#import "FoodsTableViewCell.h"

@interface FoodsTableViewCell()
@property (nonatomic, strong)UIImageView *leftImage;
@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *currentPrice;
@property (nonatomic, strong)UILabel *originPrice;
@property (nonatomic, strong)UILabel *sold;
@end

@implementation FoodsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    [self.leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.equalTo(15);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImage.mas_right).offset(12);
        make.top.equalTo(self.leftImage);
        make.right.equalTo(-10);
    }];
    
    [self.currentPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.title);
        make.bottom.equalTo(self.leftImage);
    }];
    
    [self.originPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.currentPrice.mas_right).offset(8);
        make.bottom.equalTo(self.currentPrice);
    }];
    
    [self.sold mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.originPrice);
        make.right.equalTo(-15);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = RGB(240, 240, 240);
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftImage.mas_bottom).offset(15);
        make.left.right.equalTo(self.contentView);
        make.height.equalTo(@1);
        make.bottom.equalTo(self.contentView);
    }];
}

- (void)setTuanModel:(TuanListModel *)tuanModel{
    [self.leftImage sd_setImageWithURL:[NSURL URLWithString:tuanModel.img]];
    self.title.text = tuanModel.name;
    self.currentPrice.text = [NSString stringWithFormat:@"%@元",tuanModel.current_price];
    self.originPrice.text = [NSString stringWithFormat:@"门市价%@元",tuanModel.origin_price];
    self.sold.text = [NSString stringWithFormat:@"已售%@",tuanModel.buy_count];
}


- (UIImageView *)leftImage{
    if (!_leftImage) {
        _leftImage = [UIImageView new];
        [self.contentView addSubview:_leftImage];
    }
    return _leftImage;
}

- (UILabel *)title{
    if (!_title) {
        _title = [UILabel new];
        _title.text = @"10-12合家套餐";
        _title.textColor = RGB(65, 65, 65);
        _title.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)currentPrice{
    if (!_currentPrice) {
        _currentPrice = [UILabel new];
        _currentPrice.text = @"998元";
        _currentPrice.textColor = RGB(214, 65, 66);
        _currentPrice.font = [UIFont boldSystemFontOfSize:18];
        [self.contentView addSubview:_currentPrice];
    }
    return _currentPrice;
}

- (UILabel *)originPrice{
    if (!_originPrice) {
        _originPrice = [UILabel new];
        _originPrice.text = @"门市价1388元";
        _originPrice.textColor = RGB(166, 165, 166);
        _originPrice.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:_originPrice];
    }
    return _originPrice;
}

- (UILabel *)sold{
    if (!_sold) {
        _sold = [UILabel new];
        _sold.text = @"已售999";
        _sold.textColor = RGB(166, 165, 166);
        _sold.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:_sold];
    }
    return _sold;
}



@end
