//
//  MainNavTC.h
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

typedef void(^MainNavBlock)(NSDictionary * dataArr,NSString *type);
#import <UIKit/UIKit.h>
#import "HRAdView.h"
#import "MainModel.h"


@interface MainNavTC : BaseTC
@property (nonatomic,copy)MainNavBlock block;
-(void)showData:(NSArray *)data withBgImg:(NSString *)bgImgUrl andneedReload:(BOOL)isReload; 
@property(nonatomic,strong)UICollectionView *collectionView;
+(NSString *)getID;
@property (nonatomic,assign)BOOL isNeedRefresh;
@end
