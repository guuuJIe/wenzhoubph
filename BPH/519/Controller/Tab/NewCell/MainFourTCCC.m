//
//  MainFourTCCC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/4.
//  Copyright © 2018年 519. All rights reserved.
//

#import "MainFourTCCC.h"

@interface MainFourTCCC()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@end

@implementation MainFourTCCC

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
