//
//  AdverView.h
//  519
//
//  Created by Macmini on 2017/12/13.
//  Copyright © 2017年 519. All rights reserved.
//
typedef void(^advImageBlock)(void);
#import <UIKit/UIKit.h>

@interface AdverView : UIView
@property (nonatomic,copy)advImageBlock block;
@property(nonatomic,strong)UIImageView * advImageV;
-(instancetype)initWithFrame:(CGRect)frame withAdvUrl:(NSString*)advUrl;
@property (nonatomic,strong)UIButton *cancelBtn;
@end
