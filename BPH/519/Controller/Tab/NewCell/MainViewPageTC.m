//
//  MainViewPageTC.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainViewPageTC.h"
#import "MainModel.h"
@interface MainViewPageTC()<SDCycleScrollViewDelegate>
@property (nonatomic,strong)SDCycleScrollView *cycleScrollView;
@property (nonatomic,strong)NSMutableArray * firstArray;
@property(nonatomic,strong)UIView *cityView;
@property(nonatomic,strong)UILabel *cityLabel;
@property(nonatomic,strong)UIImageView *bottomImg;

@property(nonatomic,strong)UIImageView *selectImg;
@end

@implementation MainViewPageTC

-(NSMutableArray *)firstArray{
    if (!_firstArray) {
        _firstArray = [[NSMutableArray alloc]init];
    }
    return _firstArray;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self initView];
    }
    return  self;
}

-(void)initView{
    NSLog(@"scre %lf",Screen_StatusBarHeight);
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.cycleScrollView = [[SDCycleScrollView alloc]init];
    [self.cycleScrollView setAutoScrollTimeInterval:5];
    [self.cycleScrollView setAutoScroll:YES];
    [self.cycleScrollView setInfiniteLoop:YES];
    self.cycleScrollView.delegate=self;
    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@"pageControlCurrentDot"];
    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
    [self addSubview:self.cycleScrollView];
}

-(void)showImg:(NSArray *)imgs withTag:(NSInteger)tag{
    if (self.firstArray) {
        [self.firstArray removeAllObjects];
    }
    NSMutableArray * imgArr = [[NSMutableArray alloc]init];
    for (NSDictionary * subdic in imgs) {
        MainModel * model = [[MainModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.firstArray addObject:model];
        [imgArr addObject:subdic[@"img"]];
    }
    
    self.cycleScrollView.tag = tag;
    [self.cycleScrollView setFrame:CGRectMake(0, 0, Screen_Width, self.frame.size.height)];
    [self.cycleScrollView setImageURLStringsGroup:imgArr];
}

-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    MainModel * model = self.firstArray[index];
    if (self.block) {
        self.block(model.data, model.type);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+(NSString *)getID{
    return @"MainViewPageTC";
}

@end
