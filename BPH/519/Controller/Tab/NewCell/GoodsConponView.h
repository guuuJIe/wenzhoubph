//
//  GoodsConponView.h
//  519
//
//  Created by Macmini on 2019/1/16.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsConponView : UIView

- (void)setupData:(NSDictionary *)data;

@property (nonatomic,copy) void(^getClick)(void);

- (void)setUIwithImageName:(NSString *)name andColor:(NSString *)textcolor;
@end
