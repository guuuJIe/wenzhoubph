//
//  MainNavTC.m
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#define ActionHeight self.frame.size.height-40
#define OneCollectionLine Screen_Width/750*320/2
#define navNum 5
#import "MainNavTC.h"
#import "MainNavTCCC.h"
#import "MainModel.h"
@interface MainNavTC()<UICollectionViewDataSource,UICollectionViewDelegate>

@property(nonatomic,strong)NSMutableArray * thirdMuArr;

@property (nonatomic,strong)UICollectionViewFlowLayout *layout;
@property (nonatomic,strong)UIButton * newsbtn;
@property (nonatomic,strong)NSTimer * timer;
@property (nonatomic,strong)NSArray * color;
//action
@property (nonatomic,strong)UIImageView * imgView;

@property (nonatomic,strong)UIImageView * threeImageView;
@end

@implementation MainNavTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.thirdMuArr = [[NSMutableArray alloc]init];
        [self initView];
     
    }
    return  self;
}

-(void)initView{
    
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    _imgView = [[UIImageView alloc]init];
    [self addSubview:_imgView];
    
    
    _layout=[[ UICollectionViewFlowLayout alloc ] init ];
    _layout.itemSize = CGSizeMake(Screen_Width/navNum, ActionHeight);
    _layout.minimumInteritemSpacing = 0;
    _layout.minimumLineSpacing = 0;
    _layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, OneCollectionLine) collectionViewLayout:_layout];
    self.collectionView.backgroundColor=[UIColor colorWithWhite:0 alpha:0];
    [self.collectionView registerClass:[MainNavTCCC class] forCellWithReuseIdentifier:[MainNavTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self addSubview:self.collectionView];
    
}

-(void)showData:(NSArray *)data withBgImg:(NSString *)bgImgUrl andneedReload:(BOOL)isReload{
    if (self.thirdMuArr) {
        [self.thirdMuArr removeAllObjects];
    }
    NSMutableArray * imgArr = [[NSMutableArray alloc]init];
    for (NSDictionary * subdic in data) {
        MainModel * model = [[MainModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.thirdMuArr addObject:model];
        [imgArr addObject:subdic[@"img"]];
    }
    //按钮列表
    if (imgArr.count>0) {
        self.collectionView.frame = CGRectMake(0, 0, Screen_Width, OneCollectionLine*((imgArr.count+4)/5));
    }else{
        self.collectionView.frame = CGRectMake(0, 0, Screen_Width, ActionHeight);
    }
    _isNeedRefresh = isReload;
    if (_isNeedRefresh && [kUserDefaults boolForKey:@"isreload"]) {
//        isReload = !isReload;
        _isNeedRefresh = !_isNeedRefresh;
//        [kUserDefaults setBool:false forKey:@"isreload"];
       
    }
    
    BLog(@"%@",[NSThread currentThread]);
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
         [self.collectionView reloadData];
    });

//    [UIView animateWithDuration:0 animations:^{
//        [self.collectionView reloadData];
//    }];
    
    //活动背景图
    _imgView.frame = CGRectMake(0, 0, Screen_Width, OneCollectionLine*((imgArr.count+4)/5));
    [_imgView sd_setImageWithURL:[NSURL URLWithString:bgImgUrl]];
}


#pragma mark collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.thirdMuArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
//
//    MainNavTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainNavTCCC getID] forIndexPath:indexPath];
//    if(cell==nil){
//        cell=[[MainNavTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.width)];
//    }
    MainNavTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainNavTCCC getID] forIndexPath:indexPath];
    [cell collectionViewShowData:self.thirdMuArr[indexPath.row]];

    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/navNum, OneCollectionLine);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MainModel * model = self.thirdMuArr[indexPath.row];
    if (self.block) {
        self.block(model.data, model.type);
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(NSString *)getID{
    return @"MainNavTC";
}



@end
