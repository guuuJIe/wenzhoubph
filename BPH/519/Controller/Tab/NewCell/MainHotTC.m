//
//  MainHotTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainHotTC.h"
#import "MainHotTCCC.h"
#import "MainModel.h"

@interface MainHotTC()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic,strong)NSMutableArray * dataMuarray;


@end

@implementation MainHotTC
-(NSMutableArray *)dataMuarray{
    if (!_dataMuarray) {
        _dataMuarray = [[NSMutableArray alloc]init];
    }
    return _dataMuarray;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/750*76)];
    imageView.image = [UIImage imageNamed:@"爆款推荐"];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:imageView];

    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake((Screen_Width-5*3)/2, (Screen_Width-5*3)/2+60);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, Screen_Width/750*76, Screen_Width,0) collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[MainHotTCCC class] forCellWithReuseIdentifier:[MainHotTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self addSubview: self.collectionView];
}


-(void)showMainHotData:(NSArray *)dataArray{
    if (_dataMuarray) {
        [_dataMuarray removeAllObjects];
    }
    for (NSDictionary *subdic in dataArray) {
        GoodsModel * model = [[GoodsModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.dataMuarray addObject:model];
    }
    self.collectionView.frame=CGRectMake(0, Screen_Width/750*76, Screen_Width, ((Screen_Width-5*3)/2+65));
    [self.collectionView reloadData];
}


#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataMuarray.count ;
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
//
//    MainHotTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainHotTCCC getID] forIndexPath:indexPath];
//    if(cell==nil){
//        cell=[[MainHotTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
//    }
    MainHotTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainHotTCCC getID] forIndexPath:indexPath];
    GoodsModel * model = self.dataMuarray[indexPath.row];
    cell.model = model;
    
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((Screen_Width-5*3)/2, (Screen_Width-5*3)/2+60);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsModel * model = self.dataMuarray[indexPath.row];
    if(self.block){
        self.block(model.ids);
    }
    
    self.indexpath = indexPath;
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

+(NSString *)getID{
    return @"MainHotTC";
}

@end
