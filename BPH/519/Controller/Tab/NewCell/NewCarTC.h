//
//  NewCarTC.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/5.
//  Copyright © 2018年 519. All rights reserved.
//

typedef void(^deleteGoodsByCarBlock)(NSString * ids);
typedef void(^refreshData)(void);
#import <UIKit/UIKit.h>
#import "CarModel.h"
@interface NewCarTC : UITableViewCell
@property (nonatomic,strong)CarModel * model;
@property (nonatomic,copy)deleteGoodsByCarBlock block;
@property (nonatomic,copy)refreshData refreshBlock;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextField *countTextField;

@end
