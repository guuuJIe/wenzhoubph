//
//  FoodsTableViewCell.h
//  519
//
//  Created by Macmini on 2019/1/10.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopListModel.h"
@class TuanListModel;
@interface FoodsTableViewCell : UITableViewCell

@property (nonatomic, strong)TuanListModel *tuanModel;


@end
