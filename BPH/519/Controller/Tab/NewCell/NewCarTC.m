//
//  NewCarTC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/5.
//  Copyright © 2018年 519. All rights reserved.
//

#import "NewCarTC.h"

@implementation NewCarTC

- (IBAction)subBtnClick:(UIButton *)sender {
    self.countTextField.text = [NSString stringWithFormat:@"%ld",[self.countTextField.text integerValue] - 1];
    [self addGoods];
}

- (IBAction)addBtnClick:(UIButton *)sender {
    self.countTextField.text = [NSString stringWithFormat:@"%ld",[self.countTextField.text integerValue] + 1];
    [self addGoods];
}

- (IBAction)deleBtnClick:(UIButton *)sender {
    if (self.block) {
        self.block(_model.ids);
    }
}

-(void)setModel:(CarModel *)model{
    _model = model;
    [_icon sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    _nameLabel.text = model.name;
    _priceLabel.text = [NSString stringWithFormat:@"￥%.2f",model.total_price];
    _countTextField.text = [NSString stringWithFormat:@"%@",model.number];
}


-(void)addGoods{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=update&id=%@&num=%@&email=%@&pwd=%@",FONPort,_model.ids,_countTextField.text,EMAIL,PWD];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        if([dic[@"status"] isEqual:@1]){
            if (self.refreshBlock) {
                self.refreshBlock();
            }
        }else{
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}



- (void)awakeFromNib {
    [super awakeFromNib];
   self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
