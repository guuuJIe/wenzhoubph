//
//  MainDayTC.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/3.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^MainDayBlock)(int num, NSString * ids);
#import <UIKit/UIKit.h>

@interface MainDayTC : UITableViewCell
@property(nonatomic,copy)MainDayBlock block;
-(void)showMainDayData:(NSArray *)dataArray andneedReload:(BOOL)isReload;
+(NSString *)getID;
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,assign)BOOL isNeedRefresh;
@end
