//
//  MainNavTCCC.h
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"
@interface MainNavTCCC : BaseCC

-(void)collectionViewShowData:(MainModel *)model;
+(NSString*)getID;
@end
