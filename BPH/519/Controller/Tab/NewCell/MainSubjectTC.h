//
//  MainSubjectTC.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/3.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^MainSubjectBlock)(NSDictionary * dataArr,NSString *type);
#import <UIKit/UIKit.h>

@interface MainSubjectTC : UITableViewCell
@property (nonatomic,copy)MainSubjectBlock block;
-(void)showSubjectData:(NSArray *)dataArray withSubHeight:(CGFloat)subHeight;
+(NSString *)getID;
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,assign)BOOL isNeedRefresh;
@end
