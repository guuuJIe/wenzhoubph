//
//  MainHotTCCC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainHotTCCC.h"

@interface MainHotTCCC()

@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UIImageView *car;
@property(nonatomic,strong)UILabel * oldPrice;
@property(nonatomic,strong)UIView * bgview;
@property(nonatomic,strong)UILabel * buyNowLabel;
@end

@implementation MainHotTCCC

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor whiteColor];
    NSString * priceText = @"￥11.1";
    CGFloat priceWidth = [NSString sizeWithText:priceText font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:self.price.frame.size].width;
    CGFloat lineWidth =[NSString sizeWithText:@"￥110" font:[UIFont systemFontOfSize:FONT_SIZE_XX] maxSize:self.price.frame.size].width;
   
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.img.frame.origin.y+self.img.frame.size.height+Default_Space+Default_Space, self.frame.size.width-20, 30)];
    self.title.font=[UIFont systemFontOfSize:12];
    self.title.textColor=APPFourColor;
    self.title.lineBreakMode=NSLineBreakByTruncatingTail;
    self.title.numberOfLines=0;
    self.price=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20)];
    self.price.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.price.lineBreakMode = NSLineBreakByWordWrapping;
    self.price.textColor=APPOneColor;
    self.oldPrice = [[UILabel alloc]initWithFrame:CGRectMake(self.price.frame.size.width+self.price.frame.origin.x,self.price.frame.origin.y+1,  priceWidth, 20)];
    self.oldPrice.textColor = APPThreeColor;
    self.oldPrice.textAlignment = NSTextAlignmentLeft;
    self.oldPrice.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    _bgview = [[UIView alloc]initWithFrame:CGRectMake(0,0,lineWidth, 1)];
    _bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
    _bgview.backgroundColor = APPThreeColor;
    [self.oldPrice addSubview:_bgview];
    
    self.buyNowLabel = [[UILabel alloc]initWithFrame:CGRectMake(priceWidth*2+Default_Space+5, self.price.frame.origin.y+1, self.frame.size.width-priceWidth*2-Default_Space*2-5, 20)];
    self.buyNowLabel.textColor = APPFourColor;
    self.buyNowLabel.textAlignment = NSTextAlignmentRight;
    self.buyNowLabel.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    self.buyNowLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:_buyNowLabel];
    
    [self addSubview:self.img];
    [self addSubview:self.title];
    [self addSubview:self.price];
    [self addSubview:self.oldPrice];
    
}

-(void)setModel:(GoodsModel *)model{
    _model = model;
    if (model.ids.length<1) {
        self.hidden = YES;
        return;
    }
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.title.text = model.name;
    
    NSString * moneystr = [NSString stringWithFormat:@"￥%@",model.current_price];
    if (moneystr.length>5) {
        moneystr = [NSString stringWithFormat:@"￥%.2lf",[model.current_price doubleValue]];
    }
    self.price.text = moneystr;
    self.oldPrice.text = [NSString stringWithFormat:@"￥%@",model.origin_price];
    self.buyNowLabel.text = [NSString stringWithFormat:@"%@人已购买",model.buy_count];
    
    CGFloat priceWidth = [NSString sizeWithText:self.price.text font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    if (priceWidth>self.frame.size.width/3) {
        priceWidth = self.frame.size.width/3;
    }
    self.price.frame = CGRectMake(Default_Space,self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20);
    
    CGFloat moneyWidth = [NSString sizeWithText:self.oldPrice.text font:[UIFont systemFontOfSize:FONT_SIZE_X] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    self.oldPrice.frame = CGRectMake(priceWidth+Default_Space+5, self.price.frame.origin.y, moneyWidth, 20);
    
    self.buyNowLabel.frame = CGRectMake(priceWidth+moneyWidth+Default_Space, self.price.frame.origin.y, self.frame.size.width-priceWidth-moneyWidth-Default_Space-10, 20);
    self.bgview.frame = CGRectMake(0, 0, moneyWidth, 1);
    self.bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
}
-(void)showDataOfImage:(NSString *)imageUrl name:(NSString *)name withMoney:(NSString *)money withcurrentPrice:(NSString *)currentPrice withBuyNowNumber:(NSString *)number withGoodId:(NSString *)goodId{
    
    if (goodId.length<1) {
        self.hidden = YES;
        return;
    }
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.title.text = name;
    self.price.text = money;
    self.oldPrice.text = currentPrice;
    self.buyNowLabel.text = number;
    
    CGFloat priceWidth = [NSString sizeWithText:money font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    if (priceWidth>self.frame.size.width/3) {
        priceWidth = self.frame.size.width/3;
    }
    self.price.frame = CGRectMake(Default_Space,self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20);
    
    CGFloat moneyWidth = [NSString sizeWithText:currentPrice font:[UIFont systemFontOfSize:FONT_SIZE_X] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    self.oldPrice.frame = CGRectMake(priceWidth+Default_Space+5, self.price.frame.origin.y, moneyWidth, 20);
    
    self.buyNowLabel.frame = CGRectMake(priceWidth+moneyWidth+Default_Space, self.price.frame.origin.y, self.frame.size.width-priceWidth-moneyWidth-Default_Space-10, 20);
    self.bgview.frame = CGRectMake(0, 0, moneyWidth, 1);
    self.bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
    
}

- (UIImageView *)img{
    if (!_img) {
        self.img=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space,Default_Space, self.frame.size.width-20, self.frame.size.width-20)];
        self.img.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _img;
}

+(NSString *)getID{
    return @"MainHotTCCC";
}
@end
