//
//  MainFourTC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/2.
//  Copyright © 2018年 519. All rights reserved.
//

#import "MainFourTC.h"

@interface MainFourTC()
@property (nonatomic,strong)UIView *actionView;
@property (nonatomic,strong)UIView *downActionView;

@property (nonatomic,strong)NSMutableArray *actionMuArr;
@property (nonatomic,strong)NSMutableArray *newsMuArr;
@property (nonatomic,strong)NSMutableArray *noticeArr;

@property (nonatomic,strong)UIView * runView;
@property (nonatomic,strong)UILabel * firstLabel;
@property (nonatomic,strong)UILabel * secondLabel;
@property (nonatomic,assign)NSInteger timeInteger;
@property (nonatomic,strong)NSTimer * timer;

@property (nonatomic,strong)UIImageView *getHeightImage;
@end
@implementation MainFourTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.actionMuArr = [[NSMutableArray alloc]init];
        self.newsMuArr = [[NSMutableArray alloc]init];
        self.noticeArr = [[NSMutableArray alloc]init];
        self.backgroundColor = [UIColor whiteColor];

        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.actionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    [self addSubview:self.actionView];
    
    self.downActionView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.actionView.frame), Screen_Width, 100)];
    self.downActionView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.downActionView];
    
    
    HRAdView * view = [[HRAdView alloc]init];
    view.textAlignment = NSTextAlignmentLeft;//默认
    view.isHaveTouchEvent = YES;
    view.labelFont = [UIFont systemFontOfSize:14];
    view.color = APPFourColor;
    view.time = 4.0f;
    view.defaultMargin = 10;
    view.numberOfTextLines = 2;
    view.edgeInsets = UIEdgeInsetsMake(10, 10 , 8, 10);
    view.headImg = [UIImage imageNamed:@"泊啤汇头条"];
    [self.downActionView addSubview:view];
    self.adView = view;

    __weak typeof(self) weakself = self;
    self.adView.clickAdBlock = ^(NSUInteger index){
        MainModel * model = weakself.newsMuArr[index];
        if (weakself.block) {
            weakself.block(model.data, model.type);
        }
    };
}

-(void)showActionImgArr:(NSArray *)actionArr withactionTitleArr:(NSArray *)titleArr andPicSize:(CGSize)size{
    if (self.actionMuArr) {
        [self.actionMuArr removeAllObjects ];
    }
    NSMutableArray * imgArr = [[NSMutableArray alloc]init];
    for (NSDictionary * subdic in actionArr) {
        MainModel * model = [[MainModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.actionMuArr addObject:model];
        [imgArr addObject:subdic[@"img"]];
    }

    if(self.noticeArr){
        [self.noticeArr removeAllObjects];
    }
    if (self.newsMuArr) {
        [self.newsMuArr removeAllObjects];
    }
    for (NSDictionary * subdic in titleArr) {
        MainModel * model = [[MainModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.newsMuArr addObject:model];
        [self.noticeArr addObject:subdic[@"name"]];
    }
    
    if (actionArr.count>0) {
        //获取image宽高
//        SDWebImageManager
//        __block UIImage  *goodsImage;
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgArr[0]]];
//            UIImage *image = [UIImage imageWithData:data];
//            goodsImage = image;
//        });
        
        
        
        self.actionView.frame = CGRectMake(0, 0, Screen_Width, (Screen_Width/(size.width*imgArr.count)*size.height));
        if (self.actionView) {
            for (UIView * subView in self.actionView.subviews) {
                [subView removeFromSuperview];
            }
        }
        for (NSInteger i = 0; i<imgArr.count; i++) {
            UIImageView * acimage = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width/imgArr.count*i, 0, Screen_Width/imgArr.count, self.actionView.frame.size.height)];
            acimage.tag = 1000+i;
            [acimage sd_setImageWithURL:[NSURL URLWithString:imgArr[i]]];
            [self.actionView addSubview:acimage];
            
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionOnclick:)];
            [acimage addGestureRecognizer:tap];
        }
    }else{
        for (UIView * subview in self.actionView.subviews) {
            [subview removeFromSuperview];
        }
    }
    
    if (actionArr.count>0) {
        self.downActionView.frame = CGRectMake(0, CGRectGetMaxY(self.actionView.frame), Screen_Width, 70);
    }else{
        self.downActionView.frame = CGRectMake(0, 0, Screen_Width, 70);
    }
    
    
    
    if (self.adView.oneLabel) {
        [self.adView.oneLabel removeFromSuperview];
    }
    [self.adView addLabelTitle:_noticeArr];
    [self.adView beginScroll];
    self.adView.frame = CGRectMake(0, 15, Screen_Width, 50);
}
-(void)actionOnclick:(UITapGestureRecognizer *)tap{
    NSInteger i = tap.view.tag-1000;
    MainModel * model = self.actionMuArr[i];
    if (self.block) {
        self.block(model.data, model.type);
    }
}

- (UIImageView *)getHeightImage{
    if (!_getHeightImage) {
        _getHeightImage = [UIImageView new];
    }
    return _getHeightImage;
}

+(NSString*)getID{
    return @"MainFourTC";
}
@end
