//
//  MainViewPageTC.h
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

typedef void(^pageBlock)(NSDictionary * dataArr,NSString *type);
#import <UIKit/UIKit.h>

@interface MainViewPageTC : BaseTC
@property (nonatomic,copy)pageBlock block;

-(void)showImg:(NSArray *)imgs withTag:(NSInteger)tag;
+(NSString *)getID;
@end
