//
//  MainADVFourTC.m
//  519
//
//  Created by Macmini on 16/11/24.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainADVFourTC.h"
#import "MainModel.h"
@interface MainADVFourTC()<SDCycleScrollViewDelegate>
@property (nonatomic,strong)SDCycleScrollView *cycleScrollView;
@property (nonatomic,strong)NSMutableArray * secondMuArray;
@end

@implementation MainADVFourTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.secondMuArray = [[NSMutableArray alloc]init];
        [self initView];
    }
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    self.cycleScrollView = [[SDCycleScrollView alloc]init];
    [self.cycleScrollView setAutoScrollTimeInterval:5];
    [self.cycleScrollView setAutoScroll:YES];
    [self.cycleScrollView setInfiniteLoop:YES];
    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@"pageControlCurrentDot"];
    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
    
    [self addSubview:self.cycleScrollView];
}

-(void)showImgOfAdvFour:(NSArray *)imgs withTag:(NSInteger)tag{
    if(self.secondMuArray){
        [self.secondMuArray removeAllObjects];
    }
    if (imgs.count>0) {
        NSMutableArray * imgArr = [[NSMutableArray alloc]init];
        for (NSDictionary * subdic in imgs) {
            MainModel * model = [[MainModel alloc]init];
            [model setValuesForKeysWithDictionary:subdic];
            [self.secondMuArray addObject:model];
            [imgArr addObject:subdic[@"img"]];
        }
        self.cycleScrollView.hidden = NO;
         self.cycleScrollView.delegate=self;
        [self.cycleScrollView setFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/640*180)];
        [self.cycleScrollView setImageURLStringsGroup:imgArr];
    }else{
        self.cycleScrollView.hidden = YES;
    }
}

-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    MainModel * model = self.secondMuArray[index];
    if (self.block) {
        self.block(model.data, model.type);
    }
}


-(void)selectViewOnClickOfFour{
//    [self.delegate selectViewOnClickOfFour];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


+(NSString *)getID{
    return @"MainADVFourTC";
}
@end
