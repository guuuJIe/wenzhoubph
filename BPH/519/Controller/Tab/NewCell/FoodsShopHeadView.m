//
//  FoodsShopHeadView.m
//  519
//
//  Created by Macmini on 2019/1/10.
//  Copyright © 2019 519. All rights reserved.
//

#import "FoodsShopHeadView.h"

@interface FoodsShopHeadView()
@property (nonatomic,strong)UILabel *title;
@property (nonatomic,strong)UILabel *address;
@property (nonatomic,strong)UIButton *tel;
@property (nonatomic,strong)UIImageView *distanceImage;
@property (nonatomic,strong)UIButton *distance;
@end

@implementation FoodsShopHeadView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(15);
        make.left.equalTo(10);
    }];
    
    [self.address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.title);
        make.top.equalTo(50);
        make.right.mas_equalTo(-100);
    }];
    
    [self.tel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.centerY.equalTo(self.title);
    }];
    
//    [self.telImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.tel.mas_left);
//        make.centerY.equalTo(self.tel);
//    }];
    
    [self.distance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.centerY.equalTo(self.address);
    }];
    
    [self.distanceImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.distance.mas_left).offset(0);
        make.centerY.equalTo(self.distance);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = RGB(240, 240, 240);
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.address.mas_bottom).offset(15);
        make.left.right.equalTo(self.contentView);
        make.height.equalTo(@1);
        make.bottom.equalTo(self.contentView);
    }];
}

- (void)setShopModel:(ShopListModel *)shopModel{
    _shopModel = shopModel;
    self.title.text = [NSString stringWithFormat:@"%@",shopModel.name];
    self.address.text = [NSString stringWithFormat:@"%@",shopModel.address];
    [self.tel setTitle:shopModel.tel forState:0];
    
    NSString * distanceStr = [NSString stringWithFormat:@"%@",shopModel.juli];
    NSMutableAttributedString* tncString = [[NSMutableAttributedString alloc] initWithString:distanceStr];
    [tncString addAttribute:NSUnderlineStyleAttributeName
                      value:@(NSUnderlineStyleSingle)
                      range:(NSRange){0,[tncString length]}];
    //此时如果设置字体颜色要这样
    [tncString addAttribute:NSForegroundColorAttributeName value:RGB(190, 81, 94) range:NSMakeRange(0,[tncString length])];
    //设置下划线颜色...
    [tncString addAttribute:NSUnderlineColorAttributeName value:RGB(190, 81, 94) range:(NSRange){0,[tncString length]}];
    [self.distance setAttributedTitle:tncString forState:UIControlStateNormal];
}


- (void)callSb{
//    NSMutableString * string = [[NSMutableString alloc] initWithFormat:@"tel:%@",self.shopModel.tel];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.shopModel.tel]];
//    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.shopModel.tel]]];
    [ConfigHelper dailNumber:self.shopModel.tel];
}

- (void)locationClick{
    [self.delegate targetStoreLocation:[NSString stringWithFormat:@"%f",_shopModel.xpoint] Ypoint:[NSString stringWithFormat:@"%f",_shopModel.ypoint] withPhonenum:_shopModel.tel withStorename:_shopModel.name];
}

- (UILabel *)title{
    if (!_title) {
        _title = [UILabel new];
        _title.text = @"王朝大酒店";
        _title.textColor = RGB(65, 65, 65);
        _title.font = [UIFont boldSystemFontOfSize:18];
        [_title sizeToFit];
        [self.contentView addSubview:_title];
    }
    return _title;
}

- (UILabel *)address{
    if (!_address) {
        _address = [UILabel new];
        _address.text = @"温州市鹿城区";
        _address.textColor = RGB(166, 165, 166);
        _address.numberOfLines = 0;
        _address.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_address];
    }
    return _address;
}

- (UIImageView *)distanceImage{
    if (!_distanceImage) {
        _distanceImage = [UIImageView new];
        _distanceImage.image = [UIImage imageNamed:@"location"];
        [self.contentView addSubview:_distanceImage];
    }
    return _distanceImage;
}

- (UIButton *)tel{
    if (!_tel) {
        _tel = [[UIButton alloc] init];
        [_tel setImage:[UIImage imageNamed:@"telphone"] forState:0];
        [_tel setTitle:@"025632" forState:0];
        [_tel setTitleColor:RGB(166, 165, 166) forState:0];
        [_tel setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 50)];
        [_tel addTarget:self action:@selector(callSb) forControlEvents:UIControlEventTouchUpInside];
        [_tel.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self.contentView addSubview:_tel];
    }
    return _tel;
}

- (UIButton *)distance
{
    if (!_distance) {
        _distance = [[UIButton alloc] init];
//        [_distance setImage:[UIImage imageNamed:@"location"] forState:0];
        
        [_distance setTitle:@"28m" forState:0];
        [_distance setTitleColor:RGB(190, 81, 94) forState:0];
        [_distance setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [_distance addTarget:self action:@selector(locationClick) forControlEvents:UIControlEventTouchUpInside];
        [_distance.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self.contentView addSubview:_distance];
    }
    return _distance;
}


@end
