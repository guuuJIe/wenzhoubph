//
//  MainHotTC.h
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//
typedef void(^MainHotBlock)(NSString * ids);
#import "BaseTC.h"

@interface MainHotTC : BaseTC
@property (nonatomic,copy)MainHotBlock block;


-(void)showMainHotData:(NSArray *)dataArray;

+(NSString *)getID;
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,strong)NSIndexPath *indexpath;
@end
