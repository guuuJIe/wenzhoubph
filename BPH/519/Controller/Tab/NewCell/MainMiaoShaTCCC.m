//
//  MainFourTCCC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/4.
//  Copyright © 2018年 519. All rights reserved.
//

#import "MainMiaoShaTCCC.h"

@interface MainMiaoShaTCCC()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@end

@implementation MainMiaoShaTCCC

-(void)setModel:(GoodsModel *)model{
    _model = model;
    [_goodsImage sd_setImageWithURL: [NSURL URLWithString:model.icon]];
    _titleLable.text = model.name;
    NSString * price = [NSString stringWithFormat:@"￥%@",model.current_price];
    if (price.length>5) {
        price = [NSString stringWithFormat:@"￥%.2lf",[model.current_price floatValue]];
    }
    _priceLabel.text =price;
    _oldPriceLabel.text = [NSString stringWithFormat:@"原价:%@",model.origin_price];
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+(NSString *)getID{
    return @"MainMiaoshaTCCC";
}
@end
