//
//  FoodsShopHeadView.h
//  519
//
//  Created by Macmini on 2019/1/10.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopListModel.h"

@protocol LocationNavigatorDelegate <NSObject>

-(void)targetStoreLocation:(NSString *)xpoint Ypoint:(NSString *)ypoint withPhonenum:(NSString *)phoneNum withStorename:(NSString *)storeName;;

@end

@interface FoodsShopHeadView : UITableViewHeaderFooterView
@property (nonatomic,strong)ShopListModel *shopModel;
@property (nonatomic,weak)id<LocationNavigatorDelegate>delegate;
@end
