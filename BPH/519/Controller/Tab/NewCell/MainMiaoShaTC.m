//
//  MainMiaoShaTC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/3.
//  Copyright © 2018年 519. All rights reserved.
//

#import "MainMiaoShaTC.h"
#import "MainMiaoShaTCCC.h"
#import "MainModel.h"

@interface MainMiaoShaTC()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
{
    NSInteger _index;
    NSInteger _miaoshaHour;
    NSInteger _beginTime;
    NSInteger _endTime ;
    NSInteger _status;
    NSInteger _nowTime;
}
@property(nonatomic,strong)CountDown *countDown;
@property (nonatomic,assign)NSInteger endSecond;
@property (nonatomic,strong)NSTimer * timer;
@property (nonatomic,strong)NSTimer * scrollTimer;
@property (nonatomic,strong)UIScrollView * scrollView;
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,strong)NSMutableArray * dataArray;

@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)UILabel * firstLabel;
@property (nonatomic,strong)UILabel * secondLabel;
@property (nonatomic,strong)UILabel * thirdLabel;
@property (nonatomic,strong)UILabel * maoLabel1;
@property (nonatomic,strong)UILabel * maoLabel2;
@end
@implementation MainMiaoShaTC
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
        return _dataArray;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _index = 0;
        self.backgroundColor = [UIColor whiteColor];
        [self createUI];
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(addActionTime) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop]addTimer:_timer forMode:NSRunLoopCommonModes];
//        [_timer fire];
        _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollViewTimer) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop]addTimer:_scrollTimer forMode:NSRunLoopCommonModes];
//        [_scrollTimer fire];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStautes) name:@"needRefresh" object:nil];
    }
    return self;
}
- (void)changeStautes{
    _isNeedRefresh = true;
}
-(void)addActionTime{
    if (_endSecond > 0) {
        _endSecond--;
        [self formatTimeWithTimeInterVal:_endSecond];
    }else{
        switch (_status) {
            case 0:{
                _status = 1;
                NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
                NSTimeInterval time=[date timeIntervalSince1970];
                _endSecond = _endTime - time;
                _titleLabel.text = @"距结束";
            }
                break;
            case 1:
                _status = 2;
                _titleLabel.text = @"秒杀已结束";
                CGRect rect = _titleLabel.frame;
                CGFloat width = [NSString sizeWithText:_titleLabel.text font:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(Screen_Width, 20)].width;
                rect.size.width = width;
                _titleLabel.frame = rect;
                _firstLabel.hidden = YES;
                _secondLabel.hidden = YES;
                _thirdLabel.hidden = YES;
                _maoLabel1.hidden = YES;
                _maoLabel2.hidden = YES;
                break;
            case 2:
                _firstLabel.text =@"00";
                _secondLabel.text =@"00";
                _thirdLabel.text =@"00";
                [_timer setFireDate:[NSDate distantFuture]];
                break;
            default:
                break;
        }
    }
}
-(void)scrollViewTimer{
    
    [self.scrollView setContentOffset:CGPointMake(_index*300, 0) animated:YES];
    _index++;
}
-(void)createUI{
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    
    UIImageView * imgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lineView.frame)+10, 60, 20)];
    imgView.image = [UIImage imageNamed:@"超级秒杀1"];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imgView];
    UIImageView * subIcon = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame)+10, CGRectGetMaxY(lineView.frame)+10, 40, 20)];
    subIcon.image = [UIImage imageNamed:@"Group 31"];
    subIcon.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:subIcon];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(subIcon.frame)+10, CGRectGetMaxY(lineView.frame)+10, 40, 20)];
    _titleLabel.textColor = six9;
    _titleLabel.text = @"距结束";
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_titleLabel];
    
    
    _firstLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_titleLabel.frame)+10, CGRectGetMaxY(lineView.frame)+10, 40, 20)];
    _firstLabel.textColor = [UIColor whiteColor];
    _firstLabel.backgroundColor = APPFFColor;
    _firstLabel.layer.cornerRadius = 3;
    _firstLabel.layer.masksToBounds = YES;
    _firstLabel.textAlignment = NSTextAlignmentCenter;
    _firstLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_firstLabel];
    
    
    _maoLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_firstLabel.frame), CGRectGetMaxY(lineView.frame)+10, 10, 20)];
    _maoLabel1.text = @":";
    _maoLabel1.textAlignment = NSTextAlignmentCenter;
    _maoLabel1.textColor = APPFFColor;
    _maoLabel1.font = [UIFont systemFontOfSize:13];
    [self addSubview:_maoLabel1];
    
    _secondLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_maoLabel1.frame), CGRectGetMaxY(lineView.frame)+10, 20, 20)];
    _secondLabel.textColor = [UIColor whiteColor];
    _secondLabel.backgroundColor = APPFFColor;
    _secondLabel.textAlignment = NSTextAlignmentCenter;
    _secondLabel.font = [UIFont systemFontOfSize:15];
    _secondLabel.layer.cornerRadius = 3;
    _secondLabel.layer.masksToBounds = YES;
    [self addSubview:_secondLabel];
    
    _maoLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_secondLabel.frame), CGRectGetMaxY(lineView.frame)+10, 10, 20)];
    _maoLabel2.text = @":";
    _maoLabel2.textAlignment = NSTextAlignmentCenter;
    _maoLabel2.textColor = APPFFColor;
    _maoLabel2.font = [UIFont systemFontOfSize:13];
    [self addSubview:_maoLabel2];
    
    _thirdLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_maoLabel2.frame), CGRectGetMaxY(lineView.frame)+10, 20, 20)];
    _thirdLabel.textColor = [UIColor whiteColor];
    _thirdLabel.backgroundColor = APPFFColor;
    _thirdLabel.textAlignment = NSTextAlignmentCenter;
    _thirdLabel.font = [UIFont systemFontOfSize:13];
    _thirdLabel.layer.cornerRadius = 3;
    _thirdLabel.layer.masksToBounds = YES;
    [self addSubview:_thirdLabel];
    

    UIImageView * rightImg = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-20, 24, 13, 13)];
    rightImg.image = [UIImage imageNamed:@"箭头1"];
    [self addSubview:rightImg];
    
    UIButton * moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-60, 20, 40, 20)];
    [moreBtn setTitle:@"更多" forState:(UIControlStateNormal)];
    moreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [moreBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [moreBtn addTarget:self action:@selector(moreBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:moreBtn];
    
    
    self.scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, Screen_Width, 100)];
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.backgroundColor=[UIColor whiteColor];
    self.scrollView.bounces=NO;
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    [self addSubview:self.scrollView];
    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 0;
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MainMiaoShaTCCC" bundle:nil] forCellWithReuseIdentifier:[MainMiaoShaTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self.scrollView addSubview:self.collectionView];

    
}

-(void)addLabelInView:(UIView*)view color:(UIColor *)color bgColor:(UIColor *)bgcolor font:(CGFloat )font alignment:(NSTextAlignment *)alignment label:(UILabel *)label{
    label.textColor = color;
    label.backgroundColor = bgcolor;
    label.font = [UIFont systemFontOfSize:font];
    
    [view addSubview:label];
}
-(void)showMiaoshaDataByTimer:(NSDictionary *)dics withData:(NSArray *)dataArr andneedReload:(BOOL)isReload{
//    BLog(@"%@",dics);
    
    if (dataArr.count<1) {
        for (UIView * subView in self.subviews) {
            subView.hidden = YES;
        }
        return;
    }
   
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dics options:NSJSONWritingPrettyPrinted error:nil];
//    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
 
//    BLog(@"%@",str);
    
    if (!dics) {
        for (UIView * subView in self.subviews) {
            subView.hidden = YES;
            
        }
        return;
    }else{
        for (UIView * subView in self.subviews) {
            subView.hidden = NO;
        }
    }
 
    _beginTime = [[NSString stringWithFormat:@"%@",dics[@"begin_time"]] integerValue]+8*3600;
    _endTime = [[NSString stringWithFormat:@"%@",dics[@"end_time"]] integerValue]+8*3600;
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time=[date timeIntervalSince1970];
    
    if (_beginTime - time > 0) {
        _status = 0;
        _endSecond = _beginTime - time ;
        self.titleLabel.text = @"距开始";
        _firstLabel.hidden = NO;
        _secondLabel.hidden = NO;
        _thirdLabel.hidden = NO;
        _maoLabel1.hidden = NO;
        _maoLabel2.hidden = NO;
    }else if(time - _beginTime > 0 && time-_endTime<0){
        _status = 1;
        _endSecond = _endTime - time;
        _firstLabel.hidden = NO;
        _secondLabel.hidden = NO;
        _thirdLabel.hidden = NO;
        _maoLabel1.hidden = NO;
        _maoLabel2.hidden = NO;
    }else{
        _status = 2;
        self.titleLabel.text = @"秒杀已结束";
        CGRect rect = self.titleLabel.frame;
        rect.size.width = 150;
        self.titleLabel.frame = rect;
        _firstLabel.hidden = YES;
        _secondLabel.hidden = YES;
        _thirdLabel.hidden = YES;
        _maoLabel1.hidden = YES;
        _maoLabel2.hidden = YES;
    }
    
    
    if (_endSecond > 0) {
        [_timer setFireDate:[NSDate distantPast]];
        [self formatTimeWithTimeInterVal:_endSecond];   
    }
    
    if (self.dataArray) {
        [self.dataArray removeAllObjects];
    }
    for (NSDictionary * subdic in dataArr) {
        GoodsModel * model = [[GoodsModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.dataArray addObject:model];
    }
    if (self.dataArray.count > 1) {
        for(NSInteger i = 0; i<2; i++){
            GoodsModel * model = self.dataArray[i];
            [self.dataArray addObject:model];
        }
    }else{
        [_scrollTimer setFireDate:[NSDate distantFuture]];
    }
   
    self.scrollView.contentSize = CGSizeMake(300*self.dataArray.count, 100);
    self.collectionView.frame = CGRectMake(0, 0, 300*self.dataArray.count, 100);
    _isNeedRefresh = isReload;
    if (_isNeedRefresh && [kUserDefaults boolForKey:@"isreload"]) {
//        isReload = !isReload;
//        [kUserDefaults setBool:false forKey:@"isreload"];
        [self.collectionView reloadData];
    }
}




-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger i = scrollView.contentOffset.x / 300;
    if (i >= _dataArray.count-2 && _dataArray.count>1) {
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x - 300*i, 0);
        _index = scrollView.contentOffset.x/300;
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
     [_scrollTimer setFireDate:[NSDate distantFuture]];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _index = scrollView.contentOffset.x / 300;
    [_scrollTimer setFireDate:[NSDate distantPast]];
}
#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
//
//    MainMiaoShaTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainMiaoShaTCCC getID] forIndexPath:indexPath];
//    if(cell==nil){
//        cell=[[MainMiaoShaTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
//    }
    MainMiaoShaTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainMiaoShaTCCC getID] forIndexPath:indexPath];
    GoodsModel * model = self.dataArray[indexPath.row];
    cell.model = model;
    
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(290, 100);
}

////每个item之间的间距
//- (CGFloat)collectionView:(UICollectionView *)collectionView
//                   layout:(UICollectionViewLayout*)collectionViewLayout
//minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 10;
//}


//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsModel * model = self.dataArray[indexPath.row];
    if (self.block) {
        self.block(1, model.ids);
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


//转换时间格式
- (void)formatTimeWithTimeInterVal:(NSTimeInterval)timeInterVal{
    
    int days = (int)(timeInterVal/(3600*24));
    int hours = (int)((timeInterVal-days*24*3600)/3600);
    int minute = (int)(timeInterVal-days*24*3600-hours*3600)/60;
    int second = timeInterVal-days*24*3600-hours*3600-minute*60;
    hours = hours+days*24;
    _firstLabel.text = [NSString stringWithFormat:@"%02d",hours];
    _secondLabel.text = [NSString stringWithFormat:@"%02d",minute];
    _thirdLabel.text = [NSString stringWithFormat:@"%02d",second];
    
    if (_miaoshaHour != hours) {
        _miaoshaHour = hours;
        CGFloat width = [NSString sizeWithText:_firstLabel.text font:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(Screen_Width, 20)].width+8;
        CGRect rect = _firstLabel.frame;
        rect.size.width = width;
        _firstLabel.frame = rect;
        _maoLabel1.frame = CGRectMake(CGRectGetMaxX(_firstLabel.frame), CGRectGetMinY(_firstLabel.frame), 10, 20);
        _secondLabel.frame =CGRectMake(CGRectGetMaxX(_maoLabel1.frame), CGRectGetMinY(_firstLabel.frame), 20, 20);
        _maoLabel2.frame =CGRectMake(CGRectGetMaxX(_secondLabel.frame), CGRectGetMinY(_firstLabel.frame), 10, 20);
        _thirdLabel.frame =CGRectMake(CGRectGetMaxX(_maoLabel2.frame), CGRectGetMinY(_firstLabel.frame), 20, 20);
    }
}
-(void)moreBtn{
    if (self.block) {
        self.block(0, [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?useragent=IOS_bOpIhUi90_&ctl=yingxiao&act=miaosha&shop_id=%@",shop_id]);
    }
}

-(void)dealloc{
    [_timer invalidate];
    [_scrollTimer invalidate];
    _scrollTimer = nil;
    _timer = nil;
}


+(NSString*)getID{
    return @"MainMiaoshaTC";
}
@end
