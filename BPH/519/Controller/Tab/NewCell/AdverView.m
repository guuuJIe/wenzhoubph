//
//  AdverView.m
//  519
//
//  Created by Macmini on 2017/12/13.
//  Copyright © 2017年 519. All rights reserved.
//

#import "AdverView.h"

@interface AdverView()
@property (nonatomic,strong)UIImageView * advImage;
@property (nonatomic,copy)NSString * advStr;

@end

@implementation AdverView

-(instancetype)initWithFrame:(CGRect)frame withAdvUrl:(NSString*)advUrl{
    if (self = [super initWithFrame:frame]) {
        _advStr = advUrl;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.55] ;
        [self createADVUI];
    }
    return self;
}

-(void)createADVUI{
    _advImageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width)];
    _advImageV.center = CGPointMake(Screen_Width/2, Screen_Height*0.45);
    _advImageV.layer.cornerRadius = 5;
    _advImageV.layer.masksToBounds = YES;
    _advImageV.userInteractionEnabled = YES;
    [_advImageV sd_setImageWithURL:[NSURL URLWithString:_advStr]];
    [self addSubview:_advImageV];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [_advImageV addGestureRecognizer:tap];
    
    
    UIButton * canBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_advImageV.frame)-37, CGRectGetMinY(_advImageV.frame)-3, 40, 40)];
    canBtn.center=CGPointMake(_advImageV.frame.size.width/2, _advImageV.superview.frame.size.height*0.65+15);
    [canBtn setImage:[UIImage imageNamed:@"button_退出"] forState:(UIControlStateNormal)];
    [canBtn addTarget:self action:@selector(adcCancle) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:canBtn];
    _cancelBtn = canBtn;
}

-(void)tap{
    if (self.block) {
        self.block();
    }
}


-(void)adcCancle{
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
//        if (finished) {
            BLog(@"消失");
//            [self removeFromSuperview];
//        }
    }];
    
//    [UIView animateWithDuration:4 animations:^{
//        self.advImageV.alpha = 0;
//    } completion:^(BOOL finished) {
//        [self removeFromSuperview];
//    }];
    
}

@end
