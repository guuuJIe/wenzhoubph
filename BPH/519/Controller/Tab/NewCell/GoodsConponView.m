//
//  GoodsConponView.m
//  519
//
//  Created by Macmini on 2019/1/16.
//  Copyright © 2019 519. All rights reserved.
//

#import "GoodsConponView.h"

@interface GoodsConponView()
@property (nonatomic,strong)UIImageView *conponImg;
@property (nonatomic,strong)UILabel *conponLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *getButton;
@end

@implementation GoodsConponView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = true;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)]];
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    [self.conponImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(10*AdapterScal);
        make.right.equalTo(self).offset(-10*AdapterScal);
        make.bottom.equalTo(self).offset(-10);
    }];
    
    [self.conponLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(17);
        make.left.equalTo(95*AdapterScal);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.conponLabel);
        make.top.equalTo(self.conponLabel.mas_bottom).offset(10);
    }];
    
    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.conponImg).offset(3);
        make.right.equalTo(self.conponImg).offset(-21*AdapterScal);
    }];
}

- (void)setupData:(NSDictionary *)data{
    self.conponLabel.text = [NSString stringWithFormat:@"%@元优惠券",data[@"price"]];
    self.timeLabel.text = [NSString stringWithFormat:@"%@",data[@"end_time"]];
}

- (void)tap{
    if (self.getClick) {
        self.getClick();
    }
}

- (void)setUIwithImageName:(NSString *)name andColor:(NSString *)textcolor{
    self.conponImg.image = [UIImage imageNamed:name];
    
    self.conponLabel.textColor = RGB(160, 160, 160);
    self.timeLabel.textColor = RGB(148, 148, 148);
    
    self.getButton.text = @"已领取";
    self.getButton.textColor = RGB(148, 148, 148);
}


- (UIImageView *)conponImg{
    if (!_conponImg) {
        _conponImg = [UIImageView new];
        _conponImg.image = [UIImage imageNamed:@"goodsConpon"];
        [self addSubview:_conponImg];
    }
    return _conponImg;
}

- (UILabel *)conponLabel{
    if (!_conponLabel) {
        _conponLabel = [UILabel new];
        _conponLabel.text = @"";
        _conponLabel.font = [UIFont systemFontOfSize:18];
        _conponLabel.textColor = RGB(104, 26, 0);
        [self addSubview:_conponLabel];
    }
    return _conponLabel;
}//

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.text = @"";
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = RGB(141, 78, 0);
        [self addSubview:_timeLabel];
    }
    return _timeLabel;
}

- (UILabel *)getButton{
    if (!_getButton) {
        _getButton = [[UILabel alloc] init];
        _getButton.text = @"立即领取";
        _getButton.font = [UIFont systemFontOfSize:13];
        _getButton.textColor = RGB(141, 78, 0);
        [self addSubview:_getButton];
    }
    return _getButton;
}

@end
