//
//  MainDayTC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/3.
//  Copyright © 2018年 519. All rights reserved.
//
#define cellHeight Screen_Width/3.3+50
#import "MainDayTC.h"
#import "MainTimeTCCC.h"
#import "MainModel.h"
@interface MainDayTC()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
{
    NSInteger _index;
}
@property (nonatomic,strong)NSTimer * scrollTimer;
@property (nonatomic,strong)UIScrollView * scrollView;

@property (nonatomic,strong)NSMutableArray * dataArray;
@end

@implementation MainDayTC
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        _index = 0;
        [self createUI];
        _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollViewTimer) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop]addTimer:_scrollTimer forMode:NSRunLoopCommonModes];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStautes) name:@"needRefresh" object:nil];
//        [_scrollTimer fire];
    }
    return self;
}
-(void)scrollViewTimer{
    if (self.dataArray.count - 4 == _index) {
        [self.scrollView setContentOffset:CGPointMake((_index+1)*(Screen_Width/3.3), 0) animated:YES];
        return;
    }
    [self.scrollView setContentOffset:CGPointMake(_index*(Screen_Width/3.3), 0) animated:YES];
    _index++;
}

- (void)changeStautes{
    _isNeedRefresh = true;
}
-(void)createUI{
    
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/750*76)];
    imageView.image = [UIImage imageNamed:@"天天特价"];
    [self addSubview:imageView];
    
    UIImageView * rightImg = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-20, 14, 13, 13)];
    rightImg.image = [UIImage imageNamed:@"箭头1"];
    [self addSubview:rightImg];
    
    UIButton * moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-60, 10, 40, 20)];
    [moreBtn setTitle:@"更多" forState:(UIControlStateNormal)];
    moreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [moreBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [moreBtn addTarget:self action:@selector(moreBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:moreBtn];
    
    self.scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame), Screen_Width, cellHeight)];
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.backgroundColor=[UIColor whiteColor];
    self.scrollView.bounces=NO;
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    [self addSubview:self.scrollView];
    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self.collectionView registerClass:[MainTimeTCCC class] forCellWithReuseIdentifier:[MainTimeTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self.scrollView addSubview:self.collectionView];
    
}

-(void)showMainDayData:(NSArray *)dataArray andneedReload:(BOOL)isReload{
    if (self.dataArray) {
        [self.dataArray removeAllObjects];
    }
    if (dataArray.count<1) {
        for (UIView * subView in self.subviews) {
            subView.hidden = YES;
        }
        return ;
    }else{
        for (UIView * subView in self.subviews) {
            subView.hidden = NO;
        }
    }
    for (NSDictionary * subdic in dataArray) {
        GoodsModel * model = [[GoodsModel alloc]init];
        [model setValuesForKeysWithDictionary:subdic];
        [self.dataArray addObject:model];
    }
    if (self.dataArray.count<4) {
        [_scrollTimer invalidate];
        _scrollTimer = nil;
    }else{
//        for(NSInteger i = 0; i<4; i++){
//            GoodsModel * model = self.dataArray[i];
//            [self.dataArray addObject:model];
//        }
//        _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollViewTimer) userInfo:nil repeats:YES];
//        [[NSRunLoop mainRunLoop]addTimer:_scrollTimer forMode:NSRunLoopCommonModes];
    }
    
    self.scrollView.contentSize = CGSizeMake(Screen_Width/3.3*self.dataArray.count, cellHeight);
    self.collectionView.frame = CGRectMake(0, 0, Screen_Width/3.3*self.dataArray.count, cellHeight);
    _isNeedRefresh = isReload;
    if (_isNeedRefresh && [kUserDefaults boolForKey:@"isreload"] ) {
//        isReload = !isReload;
        _isNeedRefresh = !_isNeedRefresh;
        [self.collectionView reloadData];
        [kUserDefaults setBool:false forKey:@"isreload"];
    }

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger i = self.scrollView.contentOffset.x / (Screen_Width/3.3);
    if (_dataArray.count > 4) {
        if (i >= _dataArray.count-4) {
//            scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x - (Screen_Width/3.3)*i, 0);
            _index = 0;
        }
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    BLog(@"scrollView.contentOffset ======= %f",scrollView.contentOffset.x);
    [_scrollTimer setFireDate:[NSDate distantFuture]];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _index =scrollView.contentOffset.x / (Screen_Width/3.3);
//    BLog(@"拖动的索引值 ======= %d -----%f --------%d",_index,(self.dataArray.count-4)*(Screen_Width/3.3),self.dataArray.count);
//    [scrollView setContentOffset:CGPointMake(_index*(Screen_Width/3.3), 0) animated:YES];
    [_scrollTimer setFireDate:[NSDate distantPast]];
}

#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
//
//    MainTimeTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainTimeTCCC getID] forIndexPath:indexPath];
//    if(cell==nil){
//        cell=[[MainTimeTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
//    }
  
    
    MainTimeTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainTimeTCCC getID] forIndexPath:indexPath];
    GoodsModel * model = self.dataArray[indexPath.row];
    cell.model = model;
    
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/3.3, cellHeight);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsModel * model = self.dataArray[indexPath.row];
    if (self.block) {
        self.block(1, model.ids);
    }
}
-(void)moreBtn{
    if (self.block) {
        self.block(0, @"");
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(CGFloat )collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}


+(NSString *)getID{
    return @"MainDayTC";
}
-(void)dealloc{
    if (_scrollTimer) {
        [_scrollTimer invalidate];
        _scrollTimer = nil;
    }
    
}
@end
