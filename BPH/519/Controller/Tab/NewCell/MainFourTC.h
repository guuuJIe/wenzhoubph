//
//  MainFourTC.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/2.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^MainFourBlock)(NSDictionary * dataArr,NSString *type);
#import <UIKit/UIKit.h>
#import "MainModel.h"
#import "HRAdView.h"
@interface MainFourTC : UITableViewCell
@property (nonatomic,copy)MainFourBlock block;
@property (nonatomic, strong) HRAdView *adView;
-(void)showActionImgArr:(NSArray *)actionArr withactionTitleArr:(NSArray *)titleArr andPicSize:(CGSize)size;


+(NSString*)getID;
@end
