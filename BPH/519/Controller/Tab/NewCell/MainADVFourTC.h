//
//  MainADVFourTC.h
//  519
//
//  Created by Macmini on 16/11/24.
//  Copyright © 2016年 519. All rights reserved.
//

typedef void(^advBlock)(NSDictionary * dataArr,NSString *type);
#import "BaseTC.h"


@interface MainADVFourTC : BaseTC

@property (nonatomic,copy)advBlock block;
-(void)showImgOfAdvFour:(NSArray *)imgs withTag:(NSInteger)tag;

+(NSString *)getID;
@end
