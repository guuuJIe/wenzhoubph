//
//  NewCarVC.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/5.
//  Copyright © 2018年 519. All rights reserved.
//

#import "NewCarVC.h"
#import "CarModel.h"
#import "NewCarTC.h"
#import "GoodListVC.h"
#import "GoodInfoVC.h"
#import "OrderConfirmVC.h"
#import "MainModel.h"
#import "MainWebView.h"
#import "ScoreStoreVC.h"
@interface NewCarVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataArray;
@property (nonatomic,strong)UITableView * tableView;

@property(nonatomic,strong)UIButton *backBtn;
@property (nonatomic,strong)UIButton *barRightBtn;
@property(nonatomic,strong)UIView *payView;
@property(nonatomic,strong)UIView *showNoDataView;
@property(nonatomic,strong)UILabel *allPriceLabel;
@property(nonatomic,strong)UIButton *payBtn;

@property(nonatomic,copy)NSString * username;
@property (nonatomic,copy)NSString * userpwd;
@property (nonatomic,copy)NSString * allPrice;
@end

@implementation NewCarVC

-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshDataOfCart];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"购物车";
    self.view.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshbySelfView:) name:@"refreshHome2" object:nil];
    [self showUI];
}

- (void)dealloc{
    BLog(@"销毁了");
}

-(void)refreshbySelfView:(NSNotification *)noti{
    MainModel * model = noti.object;
    [self viewControllerIsJumpBy:model.data withtype:model.type];
}

-(void)showUI{
    
    //滑动view
    self.tableView=[[UITableView alloc]init];
    [self.tableView registerNib:[UINib nibWithNibName:@"NewCarTC" bundle:nil] forCellReuseIdentifier:@"newcartcIde"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.backgroundColor=APPBGColor;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:self.tableView];
    //刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshDataOfCart)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    self.payView=[[UIView alloc]init];
    self.payView.backgroundColor=APPFourColor;
    
    //获取手机型号
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        if(self.haveBackBtn){
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-64);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-64, Screen_Width, 44);
        }else{
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-64);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-64-64, Screen_Width, 44);
        }
    }else{
        if(self.haveBackBtn){
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-40);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-44, Screen_Width, 44);
        }else{
            self.tableView.frame=CGRectMake(0, 0, Screen_Width, Screen_Height-40);
            self.payView.frame=CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-44-50, Screen_Width, 44);
        }
    }
    [self.view addSubview:self.payView];
    
    self.allPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, (Screen_SelfWidth/3)*2-Default_Space*2, 20)];
    self.allPriceLabel.textColor = [UIColor whiteColor];
    self.allPriceLabel.textAlignment = NSTextAlignmentLeft;
    
    
    
    self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_SelfWidth-Screen_SelfWidth/3, 0, Screen_SelfWidth/3, self.payView.frame.size.height)];
    [self.payBtn setBackgroundColor:APPColor];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"结算" forState:UIControlStateNormal];
    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.payBtn addTarget:self action:@selector(orderpayBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.payView addSubview:self.allPriceLabel];
    [self.payView addSubview:self.payBtn];
}
//购物车结算btn
-(void)orderpayBtnOnClick{
    if (_username) {
        if (_dataArray.count>0) {
            NSString * checkUrl = [NSString stringWithFormat:@"%@ctl=cart&act=check&email=%@&pwd=%@",FONPort,_username,_userpwd];
            WeakSelf(self);
            [RequestData requestDataOfUrl:checkUrl success:^(NSDictionary *dic) {
                StrongSelf(self);
                if ([dic[@"status"] isEqual:@0]) {
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }else{
                    OrderConfirmVC * vc =  [[OrderConfirmVC alloc] init];
                    vc.str = @"0";
                    vc.typeStr = @"结算";
                    vc.goodsDic = dic;
                    [self tabHidePushVC:vc];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
            }];
        }
    }else{
        [self tabHidePushVC:[NewLoginVC new]];
    }
}
#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewCarTC *cell=(NewCarTC *)[tableView dequeueReusableCellWithIdentifier:@"newcartcIde" forIndexPath:indexPath];
//    NewCarTC *cell = [tableView dequeueReusableCellWithIdentifier:@"newcartcIde"];
//    if(cell==nil){
//        cell=[[NewCarTC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"newcartcIde"];
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"NewCarTC" owner:nil options:nil] firstObject];
//    }
    CarModel * model = self.dataArray[indexPath.row];
    cell.model = model;
    WeakSelf(self);
    cell.block = ^(NSString *ids) {
        StrongSelf(self);
        [self deleteData:ids];
    };
    cell.refreshBlock = ^{
        StrongSelf(self);
        [self refreshDataOfCart];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CarModel * model = self.dataArray[indexPath.row];
    GoodInfoVC * vc = [GoodInfoVC new];
    vc.goodsId = model.deal_id;
    [self tabHidePushVC:vc];
}






-(void)refreshDataOfCart{
    
    self.username = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    self.userpwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    if(self.username){
        
        NSString * cardataUrl = [NSString stringWithFormat:@"%@ctl=cart&act=index&email=%@&pwd=%@",FONPort,self.username,self.userpwd];
        WeakSelf(self);
        [RequestData requestDataOfUrl:cardataUrl success:^(NSDictionary *dic) {
            StrongSelf(self);
            if (dic[@"cart_list"]) {
//                NSLog(@"购物车请求到数据 ===== %zd 条",[dic[@"cart_list"] count]);
            } else {
//                NSLog(@"购物车数据请求错误");
            }
            
            if (self.dataArray) {
                [self.dataArray removeAllObjects];
            }
            
            [MBProgressHUD dissmiss];
            if (![dic[@"cart_list"] isEqual:[NSNull null]]) {
                
                
                
                for (NSDictionary * subdic in dic[@"cart_list"]) {
                    CarModel * model = [[CarModel alloc]init];
                    [model setValuesForKeysWithDictionary:subdic];
                    [self.dataArray addObject:model];
                }
                self.allPriceLabel.text = [NSString stringWithFormat:@"￥%@", dic[@"total_data"][@"total_price"]];
                if (self.showNoDataView) {
                    [self.showNoDataView removeFromSuperview];
                    self.showNoDataView = nil;
                }
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }else{
                if (!self.showNoDataView) {
                    [self showNoData];
                }
            }
        } failure:^(NSError *error) {
            [MBProgressHUD dissmiss];
            NSLog(@"%@",error);
        }];
    }else{
        [self showNoData];
    }
}

-(void)deleteData:(NSString *)ids{
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:@"是否删除" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [self delGoodsid:ids];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)delGoodsid:(NSString *)ids{
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString * removeCartUrl = [NSString stringWithFormat:@"%@ctl=cart&act=del&id=%@&email=%@&pwd=%@",FONPort,ids,self.username,self.userpwd];
        WeakSelf(self);
        [RequestData requestDataOfUrl:removeCartUrl success:^(NSDictionary *dic) {
            StrongSelf(self);
            [self refreshDataOfCart];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    });
}



-(void)showData{
    
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    
    if (self.showNoDataView) {
        [self.showNoDataView removeFromSuperview];
        self.showNoDataView = nil;
    }
}
-(void)showNoData{
    self.allPriceLabel.text = @"";
    _showNoDataView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, self.view.frame.size.height)];
    _showNoDataView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_showNoDataView];
    
    UIImageView * carImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width/3, Screen_Width/3)];
    carImageView.center = CGPointMake(Screen_SelfWidth/2, Screen_SelfWidth/3);
    carImageView.image  = [UIImage imageNamed:@"goodscar2"];
    [_showNoDataView addSubview:carImageView];
    
    UILabel * carLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_SelfWidth, 40)];
    carLabel.center = CGPointMake(Screen_SelfWidth/2, carImageView.frame.size.height+carImageView.frame.origin.y+Default_Space+20);
    carLabel.textColor = APPThreeColor;
    carLabel.text = @"购物车还是空的，先去逛逛吧";
    carLabel.textAlignment = NSTextAlignmentCenter;
    carLabel.font = [UIFont systemFontOfSize:FONT_SIZE_L];
    [_showNoDataView addSubview:carLabel];
    
    UIButton * carBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    carBtn.center = CGPointMake(Screen_SelfWidth/2, carLabel.frame.size.height+carLabel.frame.origin.y+Default_Space+35) ;
    [carBtn setTitle:@"去逛逛" forState:(UIControlStateNormal)];
    [carBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    carBtn.backgroundColor = APPColor;
    [carBtn addTarget:self action:@selector(goGoodsList) forControlEvents:(UIControlEventTouchUpInside)];
    [_showNoDataView addSubview:carBtn];
}


-(void)goGoodsList{
    GoodListVC * vc = [GoodListVC new];
    vc.selectstr = @"catJpmpGoodsList";
    [self tabHidePushVC:vc];
}



#pragma mark 数据跳转处理
-(void)viewControllerIsJumpBy:(NSDictionary *)dataArr withtype:(NSString *)type{
    NSLog(@"%@,%@",dataArr,type);
    
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    NSString * item;
    if ([type isEqualToString:@"0"]) {
        item = dataArr[@"url"];
        webView.webUrl = item;
        if(item.length>2){
            [self tabHidePushVC:webView];
        }
    }else
        if ([type isEqualToString:@"11"]) {
            item = dataArr[@"tid"];
            goods.idStr = [NSString stringWithFormat:@"%@",item];
            goods.selectstr = @"mainJumpGoods";
            [self tabHidePushVC:goods];
        }else
            if ([type isEqualToString:@"12"]) {
                item = dataArr[@"cate_id"];
                goods.idStr = [NSString stringWithFormat:@"%@",item];
                goods.selectstr = @"mainJumpGoods";
                [self tabHidePushVC:goods];
            }else
                if ([type isEqualToString:@"21"]) {
                    item = dataArr[@"item_id"];
                    GoodInfoVC * vc = [GoodInfoVC new];
                    vc.goodsId = item;
                    [self tabHidePushVC:vc];
                }else
                    if ([type isEqualToString:@"13"]) {
                        item = dataArr[@"data"];
                        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
                    }
}












- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
