//
//  NewMainViewController.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/2.
//  Copyright © 2018年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMainViewController : BaseVC
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic, strong) NSIndexPath * currentIndexPath;
@property (nonatomic, strong) NSString * selectStr;
@end
