//
//  TableVC.h
//  519
//
//  Created by Mac on 2019/11/1.
//  Copyright © 2019 519. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableVC : BaseVC
@property (nonatomic) UITableView *tableView;
@end

NS_ASSUME_NONNULL_END
