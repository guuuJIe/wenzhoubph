//
//  ShopListModel.h
//  519
//
//  Created by Macmini on 2019/1/11.
//  Copyright © 2019 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopListModel : NSObject
@property (nonatomic,strong)NSString *ids;

@property (nonatomic,strong)NSString *preview;

@property (nonatomic,assign)CGFloat xpoint;

@property (nonatomic,strong)NSString *open_time;

@property (nonatomic,strong)NSString *juli;

@property (nonatomic,strong)NSString *peisong_fanwei;

@property (nonatomic,strong)NSArray *tuan_deal_list;

@property (nonatomic,strong)NSString *address;

@property (nonatomic,strong)NSString *tel;

@property (nonatomic,strong)NSString *is_verify;

@property (nonatomic,strong)NSString *avg_point;

@property (nonatomic,assign)NSInteger distance;

@property (nonatomic,strong)NSString *name;

@property (nonatomic,assign)CGFloat ypoint;

@end

@interface TuanListModel : NSObject

@property (nonatomic,strong)NSString *ids;

@property (nonatomic,strong)NSString *name;

@property (nonatomic,strong)NSString *brief;

@property (nonatomic,strong)NSString *img;

@property (nonatomic,strong)NSString *origin_price;

@property (nonatomic,strong)NSString *current_price;

@property (nonatomic,strong)NSString *buy_count;

@property (nonatomic,strong)NSString *stock;


@end
