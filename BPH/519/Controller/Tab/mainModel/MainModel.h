//
//  MainModel.h
//  519
//
//  Created by 梵蒂冈 on 2018/8/2.
//  Copyright © 2018年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainModel : NSObject
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * img;
@property (nonatomic,copy)NSString * type;
@property (nonatomic,copy)NSDictionary * data;
@property (nonatomic,copy)NSString * ctl;
@property (nonatomic,copy)NSString * color;
@end

@interface GoodsModel : NSObject
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * sub_name;
@property (nonatomic,copy)NSString * brief;
@property (nonatomic,copy)NSString * buy_count;
@property (nonatomic,copy)NSString * current_price;
@property (nonatomic,copy)NSString * origin_price;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * begin_time;
@property (nonatomic,copy)NSString * end_time;
@property (nonatomic,copy)NSString * time_status;
@property (nonatomic,copy)NSString * is_refund;
@property (nonatomic,copy)NSString * deal_score;
@end

@interface CateModel : NSObject
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * cate_img;
@property (nonatomic,copy)NSString * cate_id;
@property (nonatomic,copy)NSArray * bcate_type;
@end

