//
//  CarModel.h
//  519
//
//  Created by Macmini on 16/12/2.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarModel : NSObject

@property (nonatomic,copy)NSString * attr;
@property (nonatomic,copy)NSString * attr_str;
@property (nonatomic,copy)NSString * deal_id;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * max;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * number;
@property (nonatomic,copy)NSString * return_score;
@property (nonatomic,copy)NSString * return_total_score;
@property (nonatomic,copy)NSString * sub_name;
@property (nonatomic,assign)float total_price;
@property (nonatomic,copy)NSString * unit_price;
@end
