//
//  ShopListModel.m
//  519
//
//  Created by Macmini on 2019/1/11.
//  Copyright © 2019 519. All rights reserved.
//

#import "ShopListModel.h"

@implementation ShopListModel

- (void)setValue:(id)value forKey:(NSString *)key{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ids = value;
    }else if ([key isEqualToString:@"tuan_deal_list"]){
        NSMutableArray *dataArr = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dic in value) {
            TuanListModel *model = [[TuanListModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [dataArr addObject:model];
        }
        self.tuan_deal_list = dataArr;
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

@end

@implementation TuanListModel

- (void)setValue:(id)value forKey:(NSString *)key{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ids = value;
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

@end
