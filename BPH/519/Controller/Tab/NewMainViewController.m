//
//  NewMainViewController.m
//  519
//
//  Created by 梵蒂冈 on 2018/8/2.
//  Copyright © 2018年 519. All rights reserved.
//


#import "NewMainViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "MainViewPageTC.h"
#import "MainADVFourTC.h"
#import "MainNavTC.h"
#import "MainFourTC.h"
#import "MainMiaoShaTC.h"
#import "MainTimeTC.h"
#import "MainDayTC.h"
#import "MainSubjectTC.h"
#import "MainHotTC.h"
#import "MainElseHotTC.h"

#import "GoodListVC.h"
#import "MainWebView.h"
#import "GoodInfoVC.h"
#import "ScoreStoreVC.h"
#import "StoreAddressVC.h"
#import "SelectVC.h"
#import "TimeGoodVC.h"

#import "AdverView.h"
#import "WCQRCodeVC.h"
#import "BPHScanVC.h"
#import "FoodSectorVC.h"
#import "MapNavigatorVC.h"
#import "UIScrollView+EmptyDataSet.h"
#import "NetWorkBadView.h"
@interface NewMainViewController ()<UITableViewDelegate,UITableViewDataSource,BMKLocationManagerDelegate,UIScrollViewDelegate,DZNEmptyDataSetSource>

@property (nonatomic,strong)MainViewPageTC * firstCell;
@property (nonatomic,strong)MainADVFourTC * secondCell;
@property (nonatomic,strong)MainNavTC * thirdCell;
@property (nonatomic,strong)MainFourTC * fourCell;
@property (nonatomic,strong)MainMiaoShaTC * fiveCell;
@property (nonatomic,strong)MainTimeTC * sixCell;
@property (nonatomic,strong)MainDayTC * eightCell;
@property (nonatomic,strong)MainSubjectTC * tenCell;
@property (nonatomic,strong)MainHotTC * twelveCell;
@property (nonatomic,strong)MainElseHotTC * elseCell;
@property (nonatomic,strong)NSArray * thirdMuArray;
@property (nonatomic,strong)NSArray * tenArray;
@property (nonatomic,strong)NSArray * elseArray;
@property (nonatomic,strong)NSMutableArray * popArr;
@property (nonatomic,strong)NSDictionary * dic;
@property (nonatomic,copy)NSString * storeName;

@property (nonatomic,strong)UIButton * storeBtn;
//定位
@property(nonatomic,strong)BMKLocationManager * locService;
@property(nonatomic,assign)float x;
@property(nonatomic,assign)float y;
@property(nonatomic,copy)NSString * cityId;
//门店坐标
@property(nonatomic,copy)NSString * xpoint;
@property(nonatomic,copy)NSString * ypoint;
@property(nonatomic,copy)NSString * storeImg;

//版本升级url
@property(nonatomic,copy)NSString * versionStr;

@property(nonatomic,strong)NSMutableArray * shopMuArr;
@property(nonatomic,strong)NSMutableArray * shopIdMuArr;

@property (nonatomic,strong)UIView *titleView;
@property (nonatomic,strong)UIImageView * downImg;
@property (nonatomic,copy)NSString * endTime;

@property (nonatomic,assign)CGFloat  subheight;

@property (nonatomic,strong)AdverView * adv;

@property (nonatomic,strong)UIImageView *getHeightImage;

@property (nonatomic,assign)CGSize picSize;

@property (nonatomic, assign) BOOL isReload;

@property (assign, nonatomic) CGFloat sectionThreeHeight;

@property (nonatomic,strong) UIWindow *appWindow;
@end

@implementation NewMainViewController

-(NSMutableArray *)popArr{
    if (!_popArr) {
        _popArr = [[NSMutableArray alloc]init];
    }
    return _popArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.picSize = CGSizeMake(80, 80);
    self.isReload = false;
     _subheight = 75*AdapterScal;
    //self.navigationItem.title = @"home";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshbySelfView:) name:@"refreshHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushToAd) name:@"pushtoad" object:nil];
    //网络状态发生变化的时候的通知方法
    [[NSNotificationCenter defaultCenter]addObserver:self
                                               selector:@selector(NetWorkStatesChange:) name:@"netWorkChangeEventNotification"
                                                 object:nil];
    [self addTabbar];
    [self createUI];
    
//    [self requestDataSourceOfMain];
}

- (void)NetWorkStatesChange:(NSNotification *)sender{
    int networkState = [[sender object] intValue];
    switch (networkState) {
        case -1:
            //未知网络状态
            [self.tableView reloadNetUnworkDataSet];
            break;
            
        case 0:
            //没有网络
            [self.tableView reloadNetUnworkDataSet];
            break;
            
        case 1:
            //3G或者4G，反正用的是流量
            [self.tableView reloadNetworkDataSet];
            [self isOpenLocation];
            break;
            
        case 2:
            //WIFI网络
            [self.tableView reloadNetworkDataSet];
            [self isOpenLocation];
            break;
            
        default:
            break;
    }
}



-(void)refreshbySelfView:(NSNotification *)noti{
    if (noti.object) {
        MainModel * model = noti.object;
        [self viewControllerIsJumpBy:model.data withtype:model.type];
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"shop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.tableView.mj_header beginRefreshing];
    }
    
}
//广告页跳转
-(void)pushToAd{
    NSString * runtype = [kUserDefaults objectForKey:@"advImageType"];
    NSString * runImageId = [kUserDefaults objectForKey:@"advImageId"];
    [self pushWebView:runImageId withType:runtype];
}
-(void)addTabbar{
    _titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 160, 30)];
    _titleView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = _titleView;
    
    UIImageView * titleImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 42, 20)];
    titleImg.image = [UIImage imageNamed:@"图层 0"];
    titleImg.contentMode = UIViewContentModeScaleAspectFit;
    [_titleView addSubview:titleImg];
    
    BLog(@"宽度%f",CGRectGetWidth(self.titleView.frame)-13);
    _downImg = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.titleView.frame)+25, 13, 10, 5)];
    _downImg.contentMode = UIViewContentModeScaleAspectFit;
    _downImg.image = [UIImage imageNamed:@"Triangle1"];
    [_titleView addSubview:_downImg];
    
    self.storeBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(titleImg.frame)+3, 0, 100, 30)];
    self.storeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.storeBtn setTitle:@"牛山店" forState:(UIControlStateNormal)];
    [self.storeBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [self.storeBtn addTarget:self action:@selector(clickStoreBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [_titleView addSubview:self.storeBtn];
    
    
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30,30)];
    UIButton * rightBtn=[[UIButton alloc]initWithFrame:contentView.bounds];
    [rightBtn setImage:[UIImage imageNamed:@"搜索64"] forState:(UIControlStateNormal)];
    [rightBtn setSelected:NO];
    [rightBtn addTarget:self action:@selector(searchByRightItem) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(2, 2, 2, 2);
    [contentView addSubview:rightBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
    
    UIView * leftcontentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25,25)];
    UIButton * leftBtn=[[UIButton alloc]initWithFrame:leftcontentView.bounds];
    [leftBtn setImage:[UIImage imageNamed:@"扫一扫 (2)"] forState:(UIControlStateNormal)];
    [leftBtn setSelected:NO];
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4);
    [leftBtn addTarget:self action:@selector(saobyleftItem) forControlEvents:UIControlEventTouchUpInside];
    [leftcontentView addSubview:leftBtn];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftcontentView];

    
}
-(void)saobyleftItem{
    [self codeScanVC];
}
-(void)searchByRightItem{
    SelectVC *vc=[[SelectVC alloc]init];
    [self tabHidePushVC:vc];
}
-(void)clickStoreBtn{
    StoreAddressVC * store = [StoreAddressVC new];
    store.storeCityId = _cityId;
    __weak typeof (self)weSelf = self;
    store.storeAddressBlock = ^(NSString * ids,NSString * name){
        __strong typeof (weSelf)self = weSelf;
        self.storeName = name;
        CGFloat selectWidth=[NSString sizeWithText:name font:[UIFont boldSystemFontOfSize:14] maxSize:CGSizeMake(Screen_Width, Screen_Height)].width;
        CGRect rect = self.storeBtn.frame;
        rect.size.width = selectWidth+10;
        self.storeBtn.frame = rect;
        self.titleView.frame = CGRectMake(0, 0, 73 + selectWidth, 30) ;
        [self.storeBtn setTitle:name forState:normal];
        [self.tableView.mj_header beginRefreshing];
        
        [[NSUserDefaults standardUserDefaults]setObject:ids forKey:@"shop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    } ;
    
    [self tabHidePushVC:store];
}

-(void)createUI{
    UITabBarController *tabBarVC = [[UITabBarController alloc] init];
    CGFloat tabBarHeight = tabBarVC.tabBar.frame.size.height;
    
    self.tableView = [[UITableView alloc] init];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
//    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-tabBarHeight-30*AdapterHeightScal) style:(UITableViewStylePlain)];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = false;
    [self.tableView registerClass:[MainViewPageTC class] forCellReuseIdentifier:[MainViewPageTC getID]];
    [self.tableView registerClass:[MainADVFourTC class] forCellReuseIdentifier:[MainADVFourTC getID]];
    [self.tableView registerClass:[MainNavTC class] forCellReuseIdentifier:[MainNavTC getID]];
    [self.tableView registerClass:[MainFourTC class] forCellReuseIdentifier:[MainFourTC getID]];
    [self.tableView registerClass:[MainMiaoShaTC class] forCellReuseIdentifier:[MainMiaoShaTC getID]];
    [self.tableView registerClass:[MainTimeTC class] forCellReuseIdentifier:[MainTimeTC getID]];
    [self.tableView registerClass:[MainDayTC class] forCellReuseIdentifier:[MainDayTC getID]];
    [self.tableView registerClass:[MainSubjectTC class] forCellReuseIdentifier:[MainSubjectTC getID]];
    [self.tableView registerClass:[MainHotTC class] forCellReuseIdentifier:[MainHotTC getID]];
    [self.tableView registerClass:[MainElseHotTC class] forCellReuseIdentifier:[MainElseHotTC getelseId]];
   
    WeakSelf(self);
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        StrongSelf(self);
        
        [self loadData];
        self.isReload = true;
        [kUserDefaults setBool:true forKey:@"isreload"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"needRefresh" object:nil];
    }];
    header.lastUpdatedTimeLabel.hidden = YES;
    header.arrowView.hidden = YES;
    self.tableView.mj_header = header;
    
}
-(void)loadData{
    
    [self requestDataSourceOfMain];
}
#pragma mark tableViewdelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 11+(_elseArray.count-1)/2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return Screen_Width/640*360;
        //return Screen_Width/375*189;
    }else if (indexPath.row == 1){
        NSArray * arr =self.dic[@"adv_6"];
        if (arr.count>0) {
            return Screen_Width / 640 * 180;
        }
        return 0;
    }else if (indexPath.row == 2){
        
        if (_thirdMuArray.count > 0) {
            CGFloat oneLine = Screen_Width/750*320/2;
            return oneLine * ((_thirdMuArray.count+4)/5);
        }
        return 0;
       
    }
    else if (indexPath.row == 3){
        NSArray * actionArr = self.dic[@"adv_3_pic"];
        if (actionArr.count>0) {
            WeakSelf(self);
//            __block CGFloat height = 70;
//            __block CGFloat width = 70;
//            [self.getHeightImage sd_setImageWithURL:[NSURL URLWithString:actionArr[0][@"img"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                  height = image.size.height;
//                  width = image.size.width;
//
//                StrongSelf(self);
//                self.picSize = CGSizeMake(width, height);
//                [self.tableView beginUpdates];
//                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//                [self.tableView endUpdates];
////
//            }];
            __block UIImage *secondImage;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:actionArr[0][@"img"]]];
                UIImage *image = [UIImage imageWithData:data];
                secondImage = image;
                self.picSize = CGSizeMake(secondImage.size.width, secondImage.size.height);
//                dispatch_sync(dispatch_get_main_queue(), ^{
//                    if(secondImage){
                        weakself.sectionThreeHeight = Screen_Width/(secondImage.size.width*3)*secondImage.size.height + 70;
////                        return Screen_Width/(secondImage.size.width*3)*secondImage.size.height + 70;
//                    }
//                });
//                dispatch_sync(dispatch_get_main_queue(), ^{
//                    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//                });
                
               
            });
            
            
            return weakself.sectionThreeHeight == 0?200:weakself.sectionThreeHeight;
        }
        else{
            return 70;
        }
    }else if(indexPath.row == 4){
        NSArray * arr = _dic[@"miaosha_deal_list"][@"items"];
        if (arr.count>0) {
            return 160;
        }else{
            return 0;
        }
        
    }else if(indexPath.row ==5){
        NSArray * arr = _dic[@"qianggou_deal_list"];
        if (arr.count>0) {
            return 100+Screen_Width/3.3;
        }else{
            return 0;
        }
        
    }else if (indexPath.row == 6){
        NSArray * arr =self.dic[@"adv_4"];
        if (arr.count>0) {
            return Screen_Width / 640 * 180;
        }
        return 0;
    }else if (indexPath.row == 7){
        NSArray * arr = self.dic[@"best_deal_list"];
        if (arr.count>0) {
            return 60+Screen_Width/3.3 + Screen_Width/750*76;
        }else{
            return 0;
        }
        
    }else if (indexPath.row == 8){
        NSArray * arr =self.dic[@"adv_7"];
        if (arr.count>0) {
            return Screen_Width / 640 * 180;
        }
        return 0;
    }else if (indexPath.row == 9){
        WeakSelf(self);
        if(self.tenArray.count>0){
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.tenArray[0][@"img"]]];
                UIImage *image = [UIImage imageWithData:data];
                weakself.subheight = (Screen_Width/2)/image.size.width*image.size.height;
            });
            
            return (self.tenArray.count+1)/2*self.subheight+Screen_Width/750*76;
        }
        
        return 0;
       
    }else if (indexPath.row == 10){
        NSArray * arr =self.dic[@"adv_5"];
        if (arr.count>0) {
            return Screen_Width / 640 * 180;
        }
        return 0;
    }else if(indexPath.row==11){
        if (_elseArray.count) {
            return 40+((Screen_Width-5*3)/2+25+Screen_Width/750*76);
        }else{
            return 0;
        }
    }else{
        if (_elseArray.count) {
            return (Screen_Width-5*3)/2+65;
        }else{
            return 0;
        }
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak typeof(self)weself = self;
    if (indexPath.row == 0) {
//        static NSString *MainPageTC = @"MainViewPageTC";
        _firstCell = [tableView dequeueReusableCellWithIdentifier:[MainViewPageTC getID] forIndexPath:indexPath];
        if(_firstCell==nil){
            _firstCell=[[MainViewPageTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainViewPageTC getID]];
        }
        [_firstCell showImg:self.dic[@"advs"] withTag:100];
        _firstCell.block = ^(NSDictionary *dataArr, NSString *type) {
//            __strong typeof(weself) strongSelf = weself;
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        } ;
        return _firstCell;
    }else
    if (indexPath.row == 1) {
        static NSString *MainFourTC = @"MainADVFourTC";
        _secondCell = [tableView dequeueReusableCellWithIdentifier:MainFourTC];;
        if (_secondCell == nil) {
            _secondCell = [[MainADVFourTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:MainFourTC];
        }
        NSArray * arr =self.dic[@"adv_6"];
        [_secondCell showImgOfAdvFour:arr withTag:200] ;
        _secondCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        
        return _secondCell;
    }else
    if (indexPath.row == 2) {
        static NSString *NavTC = @"MainNavTC";
        _thirdCell =[tableView dequeueReusableCellWithIdentifier:NavTC];
        if(_thirdCell==nil){
            _thirdCell=[[MainNavTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NavTC];
        }
        NSString * actionBgUrl;
        for (NSDictionary * advBtnDic in self.dic[@"adv_btn_img"]) {
            if (!actionBgUrl) {
                actionBgUrl = advBtnDic[@"img"];
            }
        }
        [_thirdCell showData:self.thirdMuArray withBgImg:actionBgUrl andneedReload:self.isReload];
        _thirdCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        return _thirdCell;
    }else
    if (indexPath.row == 3) {
        static NSString *FourTC = @"MainFourTC";
        _fourCell =[tableView dequeueReusableCellWithIdentifier:FourTC];
        if(_fourCell==nil){
            _fourCell=[[MainFourTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FourTC];
        }
        [_fourCell showActionImgArr:self.dic[@"adv_3_pic"] withactionTitleArr:self.dic[@"index_notice"] andPicSize:self.picSize];
        _fourCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        return _fourCell;
    }else
    if (indexPath.row == 4) {
        static NSString *MiaoshaTC = @"MainMiaoshaTC";
        _fiveCell =[tableView dequeueReusableCellWithIdentifier:MiaoshaTC];
        if(_fiveCell==nil){
            _fiveCell=[[MainMiaoShaTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MiaoshaTC];
        }
        [_fiveCell showMiaoshaDataByTimer:_dic[@"miaosha_deal_list"][@"miaosha"] withData:_dic[@"miaosha_deal_list"][@"items"] andneedReload:self.isReload];
        _fiveCell.block = ^(int num, NSString *ids) {
            [weself viewControllerIsJumpBynNum:num withGoodsId:ids andNSIndexPath:indexPath];
        };
        
        return _fiveCell;
    }else
    if (indexPath.row == 5) {//限时抢购 轮播
        static NSString *TimeTC = @"MainTimeTC";
        _sixCell =[tableView dequeueReusableCellWithIdentifier:TimeTC];
        if(_sixCell==nil){
            _sixCell=[[MainTimeTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TimeTC];
        }
        
        [_sixCell showTimeData:_dic[@"qianggou_deal_list"] andneedReload:self.isReload];
        
        _sixCell.block = ^(int num, NSString *ids) {
            [weself viewControllerIsJumpBynNum:num withGoodsId:ids andNSIndexPath:indexPath];
        };
        return _sixCell;
    }else
    if (indexPath.row == 6) {//科腾啤酒
        static NSString *MainFourTC = @"MainADVFourTC";
        _secondCell =[tableView dequeueReusableCellWithIdentifier:MainFourTC];
        if(_secondCell==nil){
            _secondCell=[[MainADVFourTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MainFourTC];
        }
        NSArray * arr =self.dic[@"adv_4"];
        [_secondCell showImgOfAdvFour:arr withTag:201] ;
        _secondCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        return _secondCell;
    }else
    if (indexPath.row == 7) {//天天特价 轮播
        _eightCell =[tableView dequeueReusableCellWithIdentifier:[MainDayTC getID] forIndexPath:indexPath];
        if(_eightCell==nil){
            _eightCell=[[MainDayTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainDayTC getID]];
        }
        
        [_eightCell showMainDayData:self.dic[@"best_deal_list"] andneedReload:self.isReload];
        _eightCell.block = ^(int num, NSString *ids) {
            if (num == 1) {
                [weself viewControllerIsJumpBynNum:num withGoodsId:ids andNSIndexPath:indexPath];
            }else{
                GoodListVC * vc = [[GoodListVC alloc]init];
                vc.selectstr = @"&type_mz=1";
                [weself tabHidePushVC:vc];
            }
        };
        
        return _eightCell;
    }else
    if (indexPath.row == 8) {
        static NSString *MainFourTC = @"MainADVFourTC";
        _secondCell =[tableView dequeueReusableCellWithIdentifier:MainFourTC];
        if(_secondCell==nil){
            _secondCell=[[MainADVFourTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MainFourTC];
        }
        [_secondCell showImgOfAdvFour:self.dic[@"adv_7"] withTag:202] ;
        _secondCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        return _secondCell;
    }
    else
    if (indexPath.row == 9) {
        _tenCell =[tableView dequeueReusableCellWithIdentifier:[MainSubjectTC getID]];
        if(_tenCell==nil){
            _tenCell=[[MainSubjectTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainSubjectTC getID]];
        }
        if (!_subheight) {
            WeakSelf(self);
//            [self.getHeightImage sd_setImageWithURL:[NSURL URLWithString:self.tenArray[0][@"img"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                StrongSelf(self);
//                self.subheight = (Screen_Width/2)/image.size.width*image.size.height;
//                [self.tableView beginUpdates];
//                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//                [self.tableView endUpdates];
//            }];
            
//            _subheight = 70;
           
        }
        [_tenCell showSubjectData:_tenArray withSubHeight:self.subheight] ;
        _tenCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        return _tenCell;
    }else
    if (indexPath.row == 10) {
        _secondCell =[tableView dequeueReusableCellWithIdentifier:[MainADVFourTC getID]];
        if(_secondCell==nil){
            _secondCell=[[MainADVFourTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainADVFourTC getID]];
        }
        [_secondCell showImgOfAdvFour:self.dic[@"adv_5"] withTag:203] ;
        _secondCell.block = ^(NSDictionary *dataArr, NSString *type) {
            [weself viewControllerIsJumpBy:dataArr withtype:type];
        };
        return _secondCell;
    }else if(indexPath.row == 11){
        if(_elseArray.count){
            _twelveCell=[tableView cellForRowAtIndexPath:indexPath];
            if(_twelveCell==nil){
                _twelveCell=[[MainHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainHotTC getID]];
            }
            [_twelveCell showMainHotData:@[_elseArray[0],_elseArray[1]]];
            
            _twelveCell.block = ^(NSString *ids) {
                weself.selectStr = @"1";
                [weself hotGoodsIsJumpBynNum:1 withGoodsId:ids andNSIndexPath:indexPath];
            };
        }else{
            _twelveCell.hidden = YES;
        }
        return _twelveCell;
    }else{
        NSInteger index = indexPath.row-11;
        static NSString *elseHotTC = @"ElseMainHotTC";
        if(_elseArray.count){
            _elseCell=[tableView dequeueReusableCellWithIdentifier:elseHotTC];
            if(_elseCell==nil){
                _elseCell=[[MainElseHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainElseHotTC getelseId]];
            }
            if (index*2+1 == _elseArray.count) {
                [_elseCell showMainElseHotData:@[_elseArray[index*2]]];
            }else{
                [_elseCell showMainElseHotData:@[_elseArray[index*2],_elseArray[index*2+1]]];
            }
            _elseCell.block = ^(NSString *ids) {
                weself.selectStr = @"2";
                [weself hotGoodsIsJumpBynNum:1 withGoodsId:ids andNSIndexPath:indexPath];
            };
        }else{
            _elseCell.hidden = YES;
        }
        return _elseCell;
    }
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NetWorkBadView *view = [NetWorkBadView ViewClass];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 50, CGRectGetHeight(self.view.frame)/2-50, 100, 100);

    return view;
}
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
//    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
//    [manager stopMonitoring];
//    [manager startMonitoring];
    [self.tableView reloadNetworkDataSet];
    [self loadData];
}

#pragma mark 数据跳转处理
-(void)viewControllerIsJumpBy:(NSDictionary *)dataArr withtype:(NSString *)type{
    NSLog(@"%@,%@",dataArr,type);
    
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    FoodSectorVC *foodsVC = [[FoodSectorVC alloc] init];
    NSString * item;
    if ([type isEqualToString:@"0"]) {
        item = dataArr[@"url"];
        if ([item rangeOfString:@"ctl=list"].location != NSNotFound) {
            NSArray * arr = [item componentsSeparatedByString:@"?"];
            NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
            NSString * las;
            for (NSString * substr in lastArr) {
                
                if ([substr containsString:@"f["]) {
                    
                }else if([substr containsString:@"id"]){
                    
                }
                
                NSString * str;
                if ([substr isEqualToString:lastArr.firstObject]) {
                    str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@"\""];
                }else{
                    str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@""];
                }
                
                str = [str stringByReplacingOccurrencesOfString:@"]=" withString:@"\":\""];
                if (!las) {
                    las = str;
                }else{
                    las = [NSString stringWithFormat:@"%@\",\"%@",las,str];
                }
            }
            NSString * url = [NSString stringWithFormat:@"%@f={%@",FONPort,las];
            url = [url stringByReplacingOccurrencesOfString:@",\"ctl=list\",\"" withString:@"}&ctl=goods&"];
            
            GoodListVC * vc = [GoodListVC new];
            vc.isFromWeb = YES;
            vc.url = url;
            vc.selectstr = @"isfromWeb";
            [self tabHidePushVC:vc];
        }else{
            webView.webUrl = item;
            if(item.length>2){
                [self tabHidePushVC:webView];
            }
        }
    }else
    if ([type isEqualToString:@"11"]) {
//        item = dataArr[@"tid"];
//        goods.idStr = [NSString stringWithFormat:@"%@",item];
//        goods.selectstr = @"mainJumpGoods";
//        [self tabHidePushVC:goods];
        foodsVC.isFirst = true;
       
        [self tabHidePushVC:foodsVC];
    }else
    if ([type isEqualToString:@"12"]) {
        item = dataArr[@"cate_id"];
        goods.idStr = [NSString stringWithFormat:@"%@",item];
        goods.selectstr = @"mainJumpGoods";
        [self tabHidePushVC:goods];
    }else
    if ([type isEqualToString:@"21"]) {
        item = dataArr[@"item_id"];
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = item;
        [self tabHidePushVC:vc];
    }else
    if ([type isEqualToString:@"13"]) {
        item = dataArr[@"data"];
        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
    }else if ([type isEqualToString:@"17"]){//领取优惠券
        NSString *reqUrl = [NSString stringWithFormat:@"%@ctl=yingxiao&act=youhuiquan&quan_id=%@",FONPort,dataArr[@"quan_id"]];
        [RequestData requestDataOfUrl:reqUrl success:^(NSDictionary *dic) {
             [MBProgressHUD showError:dic[@"info"] toView:self.view];
        } failure:^(NSError *error) {
             [MBProgressHUD showError:@"网络错误" toView:self.view];
        }];
    }else if([type isEqualToString:@"51"]){
        MapNavigatorVC *vc = [[MapNavigatorVC alloc] init];
        vc.xpoint = [[NSUserDefaults standardUserDefaults] objectForKey:@"xPoint"];
        vc.ypoint = [[NSUserDefaults standardUserDefaults] objectForKey:@"yPoint"];
        vc.phoneNum = [[NSUserDefaults standardUserDefaults] objectForKey:@"tel"];
        vc.storeName = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
        BaseNC *nav = [[BaseNC alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

-(void)viewControllerIsJumpBynNum:(int)num withGoodsId:(NSString *)ids andNSIndexPath:(NSIndexPath *)index{
    
    if (num == 1) {
       
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = ids;
       
        [self tabHidePushVC:vc];
    }
    if (num == 0) {
        MainWebView * webView = [[MainWebView alloc]init];
        webView.webUrl = ids;
        if(ids.length>2){
            [self tabHidePushVC:webView];
        }
    }
}

-(void)hotGoodsIsJumpBynNum:(int)num withGoodsId:(NSString *)ids andNSIndexPath:(NSIndexPath *)index{
    
    if (num == 1) {
        self.currentIndexPath = index;
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = ids;
        self.navigationController.delegate = vc;
        [self tabHidePushVC:vc];
    }
    if (num == 0) {
        MainWebView * webView = [[MainWebView alloc]init];
        webView.webUrl = ids;
        if(ids.length>2){
            [self tabHidePushVC:webView];
        }
    }
}

-(void)pushWebView:(NSString *)weburl withType:(NSString *)typeId{
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    //网页
    if ([typeId isEqualToString:@"0"]) {
        webView.webUrl = weburl;
        if(weburl.length>2){
            [self tabHidePushVC:webView];
        }
    }
    //团购
    if ([typeId isEqualToString:@"11"]) {
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        
        [self tabHidePushVC:goods];
    }
    //商品列表
    if ([typeId isEqualToString:@"12"]) {
        
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        [self tabHidePushVC:goods];
    }
    //商品详情
    if ([typeId isEqualToString:@"21"]) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = weburl;
        [self tabHidePushVC:vc];
    }
    //积分
    if ([typeId isEqualToString:@"13"]) {
        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
    }
}

#pragma mark 数据请求
//首页数据请求
- (void)requestDataSourceOfMain{
    [self.popArr removeAllObjects];
    NSString * name = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    NSString * mainUrl = [NSString stringWithFormat:@"%@ctl=index&email=%@&pwd=%@",FONPort,name,pwd];
    [RequestData requestDataOfUrl:mainUrl success:^(NSDictionary *dic) {
        NSString * returnStr = [NSString stringWithFormat:@"%@",dic[@"return"]];
        if ([returnStr isEqualToString:@"1"]) {
            self.dic = dic;
            self.storeName = dic[@"shop_name"];
            CGFloat selectWidth=[NSString sizeWithText:self.storeName font:[UIFont boldSystemFontOfSize:14] maxSize:CGSizeMake(Screen_Width, Screen_Height)].width;
            CGRect rect = self.storeBtn.frame;
            rect.size.width = selectWidth+10;
            self.storeBtn.frame = rect;
            self.titleView.frame = CGRectMake(0, 0, 73 + selectWidth, 30) ;
            self.downImg.frame = CGRectMake(CGRectGetWidth(self.titleView.frame), 13, 10, 5);
            
            [self.storeBtn setTitle:dic[@"shop_name"] forState:normal];
            self.thirdMuArray = dic[@"indexs"];
            self.tenArray = dic[@"adv_zhuti"];
            self.elseArray = dic[@"supplier_deal_list"];
            NSArray * array = self.dic[@"qianggou_deal_list"];
            if(array.count>0){
                self.endTime = self.dic[@"qianggou_deal_list"][0][@"end_time"];
            }else{
                self.endTime = 0;
            }
            for ( NSDictionary * subdic in dic[@"adv_pop"]) {
                MainModel * model = [[MainModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [self.popArr addObject:model];
            }
            
            
            NSArray * advBtn =dic[@"adv_index_btn"];
            //if (advBtn.count>0) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"addBarBtnImage" object:advBtn];
            //}
            [self showData];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
//        self.isReload = false;
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        [self.tableView.mj_header endRefreshing];
//        self.isReload = false;
        NSLog(@"-- %@",error);
    }];
}

-(void)showData{
    self.appWindow = [UIApplication sharedApplication].keyWindow;
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    [self setStatusBarBackgroundColor:[UIColor clearColor]];
    if (self.popArr.count > 0) {
        MainModel * model = self.popArr[0];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.img]];
        UIImage *image = [UIImage imageWithData:data];
        CGFloat heigth = (Screen_Width-10)*image.size.height/image.size.width;
        
        if (self.adv) {
            [self.adv removeFromSuperview];
            self.adv = nil;
        }
        self.adv = [[AdverView alloc]initWithFrame:self.appWindow.bounds withAdvUrl:model.img];
        WS(weakSelf);
        self.adv.block = ^{
            [weakSelf viewControllerIsJumpBy:model.data withtype:model.type];
        };
        CGRect frame = self.adv.advImageV.frame;
        
        [self.adv.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(-tabBarHeight-(Screen_Width/6/2));
            make.centerX.equalTo(self.adv);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        [self.adv.advImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.adv.cancelBtn.mas_top).offset(-39);
            make.centerX.equalTo(self.adv);
            make.size.mas_equalTo(CGSizeMake(frame.size.width, heigth));
        }];
        
//        self.adv.advImageV.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, heigth);
//        self.adv.advImageV.center = CGPointMake(Screen_Width/2, (Screen_Height/2-heigth/2));
//        BLog(@"%f",(frame.origin.y+heigth/2))
//        self.adv.cancelBtn.center = CGPointMake(Screen_Width/2, CGRectGetMaxY(self.adv.advImageV.frame));
//
//        self.adv.cancelBtn.frame = CGRectMake(Screen_Width/2-20, self.adv.advImageV.origin.y+CGRectGetHeight(self.adv.advImageV.frame), 40, 40);
        [self.navigationController.view addSubview:self.adv];
    }
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
   
}


//viewWillAppear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPBGColor]];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
//    [self setStatusBarBackgroundColor:[UIColor whiteColor]];
//    [ConfigHelper showTabBar:[MainTabBarController sharedController]];
    [self requestDataOfServerVersion];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPColor]];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//    [self setStatusBarBackgroundColor:APPColor];
    if (self.adv) {
        [self.adv removeFromSuperview];
        self.adv = nil;
    }
//    [ConfigHelper hideTabBar:[MainTabBarController sharedController]];
}


-(void)isOpenLocation{
    //判断定位是否可用
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
        //定位功能可用
         
        _locService = [[BMKLocationManager alloc]init];
        _locService.delegate = self;
        //设置返回位置的坐标系类型
        _locService.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        //设置距离过滤参数
        _locService.distanceFilter = kCLDistanceFilterNone;
        //设置预期精度参数
        _locService.desiredAccuracy = kCLLocationAccuracyBest;
        //设置应用位置类型
        _locService.activityType = CLActivityTypeAutomotiveNavigation;
        WS(ws);
        [_locService requestLocationWithReGeocode:0 withNetworkState:0 completionBlock:^(BMKLocation * _Nullable location, BMKLocationNetworkState state, NSError * _Nullable error) {
            [ws didUpDataLocation:location];
        }];
    }else if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        
        //定位不能用
        if (!_cityId) {
            _cityId = @"9999";
        }
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"stop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self requestDataForLocation:_cityId];
    }
}

-(void)refreshLocation{
    _x = 0;
    _y = 0;
    [self isOpenLocation];
}
/*
 *  定位
 *  cityid:城市ID
 */
-(void)requestDataForLocation:(NSString *)cityid{
    if (_shopMuArr) {
        [_shopIdMuArr removeAllObjects];
        [_shopMuArr removeAllObjects];
    }else{
        _shopIdMuArr = [NSMutableArray new];
        _shopMuArr = [NSMutableArray new];
    }
    
    if (!_x && !_y) {
        _x = 0;
        _y = 0;
    }
    NSString * locationUrl = [NSString stringWithFormat:@"%@ctl=shop&act=index&city_shop_id=%@&xpoint=%f&ypoint=%f",FONPort,cityid,_x,_y];
    
    [RequestData requestDataOfUrl:locationUrl success:^(NSDictionary *dic) {
        if (dic && dic[@"shop_list"]) {
            NSString * tel;
            for (NSDictionary * subdic in dic[@"shop_list"]) {
                [self.shopIdMuArr addObject:subdic[@"id"]];
                [self.shopMuArr addObject:subdic[@"name"]];
                if (!self.ypoint || !self.xpoint || !tel || !self.storeImg) {
                    self.xpoint = subdic[@"xpoint"];
                    self.ypoint = subdic[@"ypoint"];
                    tel = subdic[@"tel"];
                    self.storeImg = subdic[@"img"];
                }
                break;
            }
            NSDictionary * storeDic = @{@"xpoint":self.xpoint,@"ypoint":self.ypoint,@"shopName":self.shopMuArr[0],@"tel":tel};
            [[NSUserDefaults standardUserDefaults]setObject:self.shopIdMuArr[0] forKey:@"shop_id"];
            [[NSUserDefaults standardUserDefaults]setObject:storeDic forKey:@"storeInfo"];
            [[NSUserDefaults standardUserDefaults] setObject:self.xpoint forKey:@"xPoint"];
            [[NSUserDefaults standardUserDefaults] setObject:self.ypoint forKey:@"yPoint"];
            [[NSUserDefaults standardUserDefaults] setObject:self.shopMuArr.firstObject forKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:tel forKey:@"tel"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self.tableView.mj_header beginRefreshing];
            
            //webview添加.userAgent 方法
            UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectZero];
            NSString * oldAgent = [webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
            
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

//版本信息数据请求
-(void)requestDataOfServerVersion{
    
    NSString * versionUrl  =[NSString stringWithFormat:@"%@ctl=version&dev_type=ios",FONPort];
    [RequestData requestDataOfUrl:versionUrl success:^(NSDictionary *dic) {
        if (dic && [dic[@"status"] isEqual: @1]) {
            self.versionStr = dic[@"ios_down_url"];
            NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
            //如果手机当前版本小于服务器的版本号 提示更新
            if ([ nowSysVer compare:dic[@"serverVersion"]] == NSOrderedAscending) {
                if ([dic[@"forced_upgrade"] isEqual:@1]) {
                    
                    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"版本升级" message:dic[@"ios_upgrade"]  preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        self.versionStr = [self.versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.versionStr]];
                    }]];
                    [self presentViewController:alertVC animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"版本升级" message:dic[@"ios_upgrade"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    [alertview show];
                }
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

//跳转APPstore
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        _versionStr = [_versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_versionStr]];
    }
}

#pragma MAKLocationDelegate
-(void)didUpDataLocation:(BMKLocation*)location{
    if (!_x || !_y || _x == 0 || _y == 0) {
        _y = location.location.coordinate.latitude;
        _x = location.location.coordinate.longitude;
        CLGeocoder * geocoder = [CLGeocoder new];
        [geocoder reverseGeocodeLocation:location.location completionHandler:^(NSArray * array, NSError * _Nullable error) {
            if (array.count > 0){
                CLPlacemark *placemark = [array objectAtIndex:0];
                //将获得的所有信息显示到label上
                
                //获取城市
                NSString *city = placemark.locality;
                if (!city) {
                    //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                    city = placemark.administrativeArea;
                    if (!city) {
                        city = @"温州";
                    }
                }
                [self requestDataForLocationCity:city];
            }else{
                [self requestDataForLocationCity:@"温州"];
            }
        }];
        _locService.delegate = nil;
    }
}
-(void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    
}

-(void)requestDataForLocationCity:(NSString *)cityStr{
    NSString * cityUrl = [NSString stringWithFormat:@"%@ctl=city&act=get_city&city_name=%@",FONPort,cityStr];
    [RequestData requestDataOfUrl:cityUrl success:^(NSDictionary *dic) {
        
        self.cityId = dic[@"current_city"][@"id"];
        [self requestDataForLocation:self.cityId];
    } failure:^(NSError *error) {
        
        if (error.code == -1009) {
            
            [MBProgressHUD showError:@"网络错误" toView:self.view];
        }else{
            [MBProgressHUD showError:@"定位出错" toView:self.view];
        }
        
    }];
}


//设置状态栏颜色
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    UIView *statusBar;
    
    if (@available(iOS 13.0, *)) {
       UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
         statusBar = [[UIView alloc] initWithFrame:keyWindow.windowScene.statusBarManager.statusBarFrame];
    } else {
        // Fallback on earlier versions
        statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    }
        
   
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

-(void)codeScanVC{
//    WCQRCodeVC * vc = [[WCQRCodeVC alloc]init];
    BPHScanVC *vc = [[BPHScanVC alloc] init];
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        switch (status) {
            case AVAuthorizationStatusNotDetermined: {
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [self.navigationController pushViewController:vc animated:YES];
                        });
                        NSLog(@"用户第一次同意了访问相机权限 - - %@", [NSThread currentThread]);
                    } else {
                        NSLog(@"用户第一次拒绝了访问相机权限 - - %@", [NSThread currentThread]);
                    }
                }];
                break;
            }
            case AVAuthorizationStatusAuthorized: {
//                [self tabHidePushVC:vc];
                [self.navigationController pushViewController:vc animated:true];
                break;
            }
            case AVAuthorizationStatusDenied: {
                UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"请去-> [设置 - 隐私 - 相机 - SGQRCodeExample] 打开访问开关" preferredStyle:(UIAlertControllerStyleAlert)];
                UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {

                }];

                [alertC addAction:alertA];
                [self presentViewController:alertC animated:YES completion:nil];
                break;
            }
            case AVAuthorizationStatusRestricted: {
                NSLog(@"因为系统原因, 无法访问相册");
                break;
            }

            default:
                break;
        }
        return;
    }

    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"未检测到您的摄像头" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {

    }];

    [alertC addAction:alertA];
    [self presentViewController:alertC animated:YES completion:nil];
    
//    FoodSectorVC *vc = [[FoodSectorVC alloc] init];
   
}

#pragma mark ----UIScrollViewDelgate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"%f",scrollView.contentOffset.y);
//    if (scrollView.contentOffset.y <= 0) {
//        [UIView animateWithDuration:2.0 animations:^{
//            [self setNeedsStatusBarAppearanceUpdate];
//        }];
//    }
//    CGRect rect = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:10 inSection:0]];
//    CGFloat sectionHeaderHeight = rect.origin.y;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        
//        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//        
//    }

  
  
}


- (UIImageView *)getHeightImage{
    if (!_getHeightImage) {
        _getHeightImage = [UIImageView new];
    }
    return _getHeightImage;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation{
    return UIStatusBarAnimationFade;
}




@end
