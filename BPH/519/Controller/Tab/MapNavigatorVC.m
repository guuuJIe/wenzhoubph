//
//  MapNavigatorVC.m
//  519
//
//  Created by Macmini on 17/6/9.
//  Copyright © 2017年 519. All rights reserved.
//

#import "MapNavigatorVC.h"
#import "BNCoreServices.h"
@interface MapNavigatorVC ()<BMKMapViewDelegate,BMKGeoCodeSearchDelegate,BMKLocationManagerDelegate,BNNaviRoutePlanDelegate,BNNaviUIManagerDelegate,BNNaviRoutePlanDelegate>
{
    BOOL useMyLocation;
}
//定位
@property(nonatomic,strong)BMKLocationManager * locService;
@property(nonatomic,strong)BMKMapView * mapView;
@property(nonatomic,strong)BMKAnnotationView * annotationView;
@property(nonatomic,strong)BMKPointAnnotation * annotation;
@property(nonatomic,strong)UIButton *backBtn;
@end

@implementation MapNavigatorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
//    [self initBar];
    [self initView];
    
    [self isOpenLocation];
    
}

-(void)isOpenLocation{
    
    //判断定位是否可用
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
        useMyLocation = YES;
        //定位功能可用
        _locService = [[BMKLocationManager alloc]init];
        _locService.delegate = self;
        [_locService startUpdatingLocation];
        
    }else if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        useMyLocation = NO;
    }
}
-(void)popVC{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)initView{
    self.mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-50)];
    self.mapView.zoomLevel = 16;
    self.mapView.showsUserLocation = NO;
    self.mapView.mapType=BMKMapTypeStandard;
    self.mapView.userTrackingMode=BMKUserTrackingModeFollow;
    [self.view addSubview:self.mapView];
    
    CLLocationCoordinate2D coor = CLLocationCoordinate2DMake([_ypoint floatValue], [_xpoint floatValue]);
    _mapView.centerCoordinate = coor;
    _annotationView= [[BMKAnnotationView alloc]init];
    _annotation= [[BMKPointAnnotation alloc]init];
    _annotation.coordinate= coor;
    _annotation.title = self.storeName;
    _annotationView.centerOffset = CGPointMake(0, 0);
    [_mapView addAnnotation:_annotation];
    [_mapView selectAnnotation:_annotation animated:YES];
//    [_mapView setCenterCoordinate:coor animated:YES];

    UIButton * backBtn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_SelfWidth-50, Screen_StatusBarHeight+20, 30, 30)];
    [backBtn setImage:[UIImage imageNamed:@"errors"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:backBtn];
    
    
    UIView * endView = [[UIView alloc]initWithFrame:CGRectMake(0, _mapView.frame.size.height+_mapView.frame.origin.y-BottomAreaHeight, Screen_Width, 50+BottomAreaHeight)];
    [self.view addSubview:endView];
    endView.backgroundColor = [UIColor whiteColor];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width/2, 50)];
    label.text = [NSString stringWithFormat:@"电话:%@",_phoneNum];
    label.textColor = APPFourColor;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15];
    label.userInteractionEnabled = YES;
    [endView addSubview:label];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(phoneClick)];
    [label addGestureRecognizer:tap];
    
    UIButton * nextBtn = [[UIButton alloc]initWithFrame:CGRectMake(Screen_Width/2, 0, Screen_Width/2, 50)];
    [nextBtn setTitle:@"导航" forState:(UIControlStateNormal)];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    nextBtn.backgroundColor = APPColor;
    [nextBtn addTarget:self action:@selector(nextBtnclick) forControlEvents:(UIControlEventTouchUpInside)];
    [endView addSubview:nextBtn];
    
    
    
    
}

-(void)phoneClick{
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",_phoneNum];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}


- (BOOL)checkServicesInited
{
    if(![BNCoreServices_Instance isServicesInited])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                            message:@"引擎尚未初始化完成，请稍后再试"
                                                           delegate:nil
                                                  cancelButtonTitle:@"我知道了"
                                                  otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    return YES;
}

-(void)nextBtnclick{
    if (![self checkServicesInited]) return;
    
    //节点数组
    NSMutableArray *nodesArray = [[NSMutableArray alloc]initWithCapacity:2];
    
    //起点 传入的是原始的经纬度坐标，若使用的是百度地图坐标，可以使用BNTools类进行坐标转化
    CLLocation *myLocation = [BNCoreServices_Location getLastLocation];
    BNRoutePlanNode *startNode = [[BNRoutePlanNode alloc] init];
    startNode.pos = [[BNPosition alloc] init];
    if (useMyLocation) {
        startNode.pos.x = myLocation.coordinate.longitude;
        startNode.pos.y = myLocation.coordinate.latitude;
        startNode.pos.eType = BNCoordinate_OriginalGPS;
    }
    else {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"未开启定位" message:@"请在设置中开启等位" preferredStyle:(UIAlertControllerStyleAlert)];
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        return ;
    }
    [nodesArray addObject:startNode];
    
    //终点
    BNRoutePlanNode *endNode = [[BNRoutePlanNode alloc] init];
    endNode.pos = [[BNPosition alloc] init];
    endNode.pos.x = [_xpoint floatValue];
    endNode.pos.y = [_ypoint floatValue];
    endNode.pos.eType = BNCoordinate_BaiduMapSDK;
    [nodesArray addObject:endNode];
    //发起路径规划
    [BNCoreServices_RoutePlan setDisableOpenUrl:YES];
    [BNCoreServices_RoutePlan startNaviRoutePlan:BNRoutePlanMode_Recommend naviNodes:nodesArray time:nil delegete:self userInfo:nil];
}

//算路成功回调
-(void)routePlanDidFinished:(NSDictionary *)userInfo
{
    NSLog(@"算路成功");
    
    //路径规划成功，开始导航
    [BNCoreServices_UI showPage:BNaviUI_NormalNavi delegate:self extParams:nil];
}
//算路失败回调
- (void)routePlanDidFailedWithError:(NSError *)error andUserInfo:(NSDictionary*)userInfo
{
    switch ([error code]%10000)
    {
        case BNAVI_ROUTEPLAN_ERROR_LOCATIONFAILED:
            [MBProgressHUD showError:@"定位失败" toView:self.view];
            NSLog(@"暂时无法获取您的位置,请稍后重试");
            break;
        case BNAVI_ROUTEPLAN_ERROR_ROUTEPLANFAILED:
            [MBProgressHUD showError:@"发起导航失败" toView:self.view];
            NSLog(@"无法发起导航");
            break;
        case BNAVI_ROUTEPLAN_ERROR_LOCATIONSERVICECLOSED:
            [MBProgressHUD showError:@"请打开定位功能" toView:self.view];
            NSLog(@"定位服务未开启,请到系统设置中打开定位服务。");
            break;
        case BNAVI_ROUTEPLAN_ERROR_NODESTOONEAR:
            _mapView.zoomLevel = 20;
            [MBProgressHUD showError:@"已到达终点" toView:self.view];
            NSLog(@"起终点距离起终点太近");
            break;
        default:
            [MBProgressHUD showError:@"网络错误，请稍后再试" toView:self.view];
            NSLog(@"算路失败");
            break;
    }
}

//算路取消回调
-(void)routePlanDidUserCanceled:(NSDictionary*)userInfo {
    NSLog(@"算路取消");
}


#pragma mark - 安静退出导航

//- (void)exitNaviUI
//{
//    [BNCoreServices_UI exitPage:EN_BNavi_ExitTopVC animated:YES extraInfo:nil];
//}

#pragma mark - BNNaviUIManagerDelegate

//退出导航页面回调
//- (void)onExitPage:(BNaviUIType)pageType  extraInfo:(NSDictionary*)extraInfo
//{
//    if (pageType == BNaviUI_NormalNavi)
//    {
//        NSLog(@"退出导航");
//    }
//    else if (pageType == BNaviUI_Declaration)
//    {
//        NSLog(@"退出导航声明页面");
//    }
//}


#pragma MAKLocationDelegate
-(void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
  
    if(userLocation.location!=nil) {

    }
    [_mapView updateLocationData:userLocation];
    
}

-(void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    [_mapView updateLocationData:userLocation];
}


-(void)viewWillAppear:(BOOL)animated
{
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
    _locService.delegate = self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
    _locService.delegate = nil;
    [_locService stopUpdatingLocation];
}


@end
