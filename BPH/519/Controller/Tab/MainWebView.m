//
//  MainWebView.m
//  519
//
//  Created by Macmini on 16/12/20.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainWebView.h"
#import "OrderListVC.h"
#import "GoodInfoVC.h"
#import "GoodListVC.h"
#import <cmbkeyboard/CMBWebKeyboard.h>
#import <cmbkeyboard/NSString+Additions.h>
#import <WebKit/WebKit.h>

// WKWebView 内存不释放的问题解决
@interface wv : NSObject<WKScriptMessageHandler>

//WKScriptMessageHandler 这个协议类专门用来处理JavaScript调用原生OC的方法
@property (nonatomic, weak) id<WKScriptMessageHandler> scriptDelegate;

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;

@end
@implementation wv

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate {
    self = [super init];
    if (self) {
        _scriptDelegate = scriptDelegate;
    }
    return self;
}

#pragma mark - WKScriptMessageHandler
//遵循WKScriptMessageHandler协议，必须实现如下方法，然后把方法向外传递
//通过接收JS传出消息的name进行捕捉的回调方法
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
    if ([self.scriptDelegate respondsToSelector:@selector(userContentController:didReceiveScriptMessage:)]) {
        [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
    }
}

@end

@interface MainWebView ()<UIWebViewDelegate,UIGestureRecognizerDelegate,WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>
@property(nonatomic,strong)UIButton * backBtn;
@property(nonatomic,strong)UIWebView * webView;
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIImageView *imgView;
@property (nonatomic,strong)JSContext *context;
@property (nonatomic,strong)NSString *content;
@property (nonatomic,strong)UIBarButtonItem *closeItem;

@property (nonatomic,strong) UIProgressView *progressView;

@property (nonatomic) WKWebView *wkwebView;

@property (nonatomic) BOOL isFirst;

@property (nonatomic) UIProgressView *progress;
@end

@implementation MainWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [MBProgressHUD showHUD];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    NSLog(@"%@",self.webUrl);
    self.view.backgroundColor = [UIColor whiteColor];
//    [self initview];
    [self initbacBar];
    [self initWkwebView];
    _isFirst = true;
}

-(void)initbacBar{
    UIView * rightcontentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIButton * shareBtn = [[UIButton alloc]initWithFrame:rightcontentView.bounds];
    [shareBtn setImage:[UIImage imageNamed:@"share_white_icon"] forState:(UIControlStateNormal)];
    [shareBtn addTarget:self action:@selector(shareGood) forControlEvents:(UIControlEventTouchUpInside)];
    [rightcontentView addSubview:shareBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightcontentView];
    
    
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIButton * btn = [[UIButton alloc]initWithFrame:contentView.bounds];
    [btn setImage:[UIImage imageNamed:@"返回"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    [contentView addSubview:btn];
    UIBarButtonItem * item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    item.width = -15;
    self.navigationItem.leftBarButtonItems = @[item,[[UIBarButtonItem alloc]initWithCustomView:contentView],self.closeItem];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    if ([_webUrl rangeOfString:@"act=doubleEleven"].location != NSNotFound) {
        [UIApplication  sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];

        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
        [self setStatusBarBackgroundColor:[UIColor whiteColor]];
    }
    

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:(UIBarMetricsDefault)];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self setStatusBarBackgroundColor:APPColor];
}

-(void)popVC{
//    if ([self.runjump isEqualToString:@"runjump"]) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"runjumpmain" object:nil];
//    }else{
//        [self.navigationController popViewControllerAnimated:YES];
//    }
    if ([self.wkwebView canGoBack]) {
        [self.wkwebView goBack];
    }else{
        [self closeNative];
    }
}
-(void)shareGood{
    //    分享
    [UMUtil shareUM:self delegate:self title:self.navigationItem.title body:self.content?self.content:@"" url:self.webUrl img:nil urlImg:nil];
}
//js调用方法
- (void)share{
    BLog(@"点击了分享");
    NSArray *args = [JSContext currentArguments];
    for (JSValue *jsVal in args) {
        BLog(@"%@", jsVal.toString);
    }
    
    if (args.count == 4) {
        JSValue *title = args[0];
//        share.title = title.toString;
        
        JSValue *detail = args[1];
        self.content = detail.toString;
        
        JSValue *url = args[2];
        self.webUrl = url.toString;
        
        JSValue *imageUrl = args[3];
//        self. = imageUrl.toString;
    }
    
    [UMUtil shareUM:self delegate:self title:self.navigationItem.title body:self.content?self.content:@"" url:self.webUrl img:nil urlImg:nil];
}

- (void)sharewebContent:(NSString *)title andBody:(NSString *)content andWeburl:(NSString *)url{
    
    [UMUtil shareUM:self delegate:self title:title body:content url:url img:nil urlImg:nil];
}

-(void)initview{
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0,0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight)];
    NSString *encodedString=[self.webUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:encodedString]]];
    [_webView request];
    _webView.delegate = self;
    _webView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_webView];
    if (_webUrl.length<3) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
        label.text = @"暂无内容";
        label.textColor = APPThreeColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(Screen_Width/2, 150);
        [_webView addSubview:label];
    }
    UILongPressGestureRecognizer * longPressed = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    longPressed.delegate = self;
    [self.webView addGestureRecognizer:longPressed];
    
   
}

- (void)initWkwebView{
    
    [self.view addSubview:self.wkwebView];
    [self.view addSubview:self.progressView];
    
    NSString *encodedString=[self.webUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedString]];
    NSArray *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies;
    //Cookies数组转换为requestHeaderFields
    NSDictionary *requestHeaderFields = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
    //设置请求头
    request.allHTTPHeaderFields = requestHeaderFields;
    [self.wkwebView loadRequest:request];
   
    if (_webUrl.length<3) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
        label.text = @"暂无内容";
        label.textColor = APPThreeColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(Screen_Width/2, 150);
        [_wkwebView addSubview:label];
    }
    
   
    
    [self.wkwebView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    [self.wkwebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    
    if ([keyPath isEqualToString:@"title"] && object == _wkwebView) {
        
        self.navigationItem.title = _wkwebView.title;
    }else if([keyPath isEqualToString:@"estimatedProgress"]){
        NSLog(@"网页加载进度 = %f",_wkwebView.estimatedProgress);
        self.progressView.progress = _wkwebView.estimatedProgress;
        if (_wkwebView.estimatedProgress >= 1.0f) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.progressView.progress = 0;
            });
                           
            
        }
        
        
    }
    
}

- (void)longPressed:(UITapGestureRecognizer*)recognizer{
    //只在长按手势开始的时候才去获取图片的url
    if (recognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    CGPoint touchPoint = [recognizer locationInView:self.webView];
    NSString *js = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).src", touchPoint.x, touchPoint.y];
    NSString *urlToSave = [self.webView stringByEvaluatingJavaScriptFromString:js];
    if (urlToSave.length == 0) {
        return;
    }
    NSLog(@"获取到图片地址：%@",urlToSave);
    [self longPress:urlToSave];
    
}
//可以识别多个手势
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)startLogin{
    [self tabHidePushVC:[NewLoginVC new]];
}

- (void)closeNative{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    self.context = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    self.context[@"wv"] = self;

}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
//    self.context = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
//    self.context[@"wv"] = self;
//    NSString *textJS = @"description_text()";
//    [self.context evaluateScript:textJS];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //    获取当前页面的title
    self.navigationItem.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
   
    self.content =  [self.webView stringByEvaluatingJavaScriptFromString:@"description_text();"];
//    self.content = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerHTML"];
//    __weak typeof (self) weakSelf = self;
//    self.context[@"startLogin"] = ^(){
//        [weakSelf startLogin];
//    };
    
//    [self.context evaluateScript:@"wv.startLogin()"];
    
   
    [MBProgressHUD dissmiss];
}




-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [MBProgressHUD dissmiss];
    [MBProgressHUD showError:@"请求错误" toView:self.view];
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"%@\n%@\n%@",request.URL,request.HTTPMethod,request.URL.host);
    [MBProgressHUD dissmiss];
    if ([request.URL.host isCaseInsensitiveEqualToString:@"cmbls"]) {
        CMBWebKeyboard *secKeyboard = [CMBWebKeyboard shareInstance];
        [secKeyboard showKeyboardWithRequest:request];
        secKeyboard.webView = webView;

        UITapGestureRecognizer* myTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self.view addGestureRecognizer:myTap]; //这个可以加到任何控件上,比如你只想响应WebView，我正好填满整个屏幕
        myTap.delegate = self;
        myTap.cancelsTouchesInView = NO;
        return NO;
    }
    NSString * urlstring = [NSString stringWithFormat:@"%@",request.URL];
    if ([urlstring rangeOfString:@"http://cmbnprm/"].location != NSNotFound) {
        self.backBtn.hidden = YES;
        [self allOrderVIewOnClick:0];
    }
    if ([urlstring rangeOfString:@"bphapp.com"].location != NSNotFound) {
        
        if ([urlstring rangeOfString:@"ctl=item"].location != NSNotFound) {
            NSArray * urlArr = [urlstring componentsSeparatedByString:@"="];
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = urlArr.lastObject;
            [self tabHidePushVC:vc];
            return NO;
        }
        else
            if ([urlstring rangeOfString:@"ctl=list"].location != NSNotFound) {
                NSArray * arr = [urlstring componentsSeparatedByString:@"?"];
                NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
                NSString * las;
                for (NSString * substr in lastArr) {
                    NSString * str;
                    if ([substr isEqualToString:lastArr.firstObject]) {
                        str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@"\""];
                    }else{
                        str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@""];
                    }
                    
                    str = [str stringByReplacingOccurrencesOfString:@"]=" withString:@"\":\""];
                    if (!las) {
                        las = str;
                    }else{
                        las = [NSString stringWithFormat:@"%@\",\"%@",las,str];
                    }
                }
                NSString * url = [NSString stringWithFormat:@"%@f={%@",FONPort,las];
                url = [url stringByReplacingOccurrencesOfString:@",\"ctl=list\",\"" withString:@"}&ctl=goods&"];
                
                GoodListVC * vc = [GoodListVC new];
                vc.isFromWeb = YES;
                vc.url = url;
                [self tabHidePushVC:vc];
                return NO;
            }
        
//          if([urlstring rangeOfString:@"ctl=user"].location != NSNotFound) {
//               [self tabHidePushVC:[NewLoginVC new]];
//          }
    }
    

    return  YES;
}



-(void)longPress:(NSString *)imgurl{
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgurl]];
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:(UIAlertControllerStyleActionSheet)];
    UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"保存" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:data], self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }];
    UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"分享给朋友" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        WXMediaMessage *mediaMsg = [WXMediaMessage message];
        WXImageObject *imgObj = [WXImageObject object];
        imgObj.imageData = data;
        mediaMsg.mediaObject = imgObj;
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.message = mediaMsg;
        req.bText = NO;
        req.scene = WXSceneSession;
        [WXApi sendReq:req];
    }];
    UIAlertAction * action3 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    [MBProgressHUD showSuccess:msg toView:self.view];
}

-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)handleSingleTap:(UITapGestureRecognizer *)sender{
    [[CMBWebKeyboard shareInstance] hideKeyboard];

}

-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        Class class=[OrderListVC class];
        [vcArray addObject:class];

        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
    }

    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
//    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    pageController.payJumpPage = @"payJumpPage";
    [pageController initBar];
    pageController.isscore = 0;
    [self tabHidePushVC: pageController];


}

#pragma mark-----WKWebviewDelegate---
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    
    NSLog(@"方法名:%@", message.name);
    NSLog(@"参数:%@", message.body);
    if ([message.name isEqualToString:@"share"]) {
        NSDictionary *dic = message.body;
        [self sharewebContent:dic[@"title"] andBody:dic[@"sub"] andWeburl:dic[@"url"]];
    }
    
    if ([message.name isEqualToString:@"login"]) {
        [self startLogin];
    }
    
    
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
//     self.content =  [self.webView stringByEvaluatingJavaScriptFromString:@"description_text();"];
    [self.wkwebView evaluateJavaScript:@"description_text();" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        self.content = result;
    }];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    NSString * urlstring = [NSString stringWithFormat:@"%@",navigationAction.request.URL];
    
    if (_isFirst) {
        _isFirst = false;
        decisionHandler(WKNavigationActionPolicyAllow);
    }else{
        if ([urlstring rangeOfString:@"bphapp.com"].location != NSNotFound) {
            if ([urlstring rangeOfString:@"ctl=item"].location != NSNotFound || [urlstring rangeOfString:@"ctl=list"].location != NSNotFound){
                decisionHandler(WKNavigationActionPolicyCancel);
            }else{
                decisionHandler(WKNavigationActionPolicyAllow);
            }
        }else{
            decisionHandler(WKNavigationActionPolicyAllow);
        }
        
    }
    
    if ([urlstring rangeOfString:@"http://cmbnprm/"].location != NSNotFound) {
        self.backBtn.hidden = YES;
        [self allOrderVIewOnClick:0];
    }
    if ([urlstring rangeOfString:@"bphapp.com"].location != NSNotFound) {
//
        if ([urlstring rangeOfString:@"ctl=item"].location != NSNotFound) {
            NSArray * urlArr = [urlstring componentsSeparatedByString:@"="];
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = urlArr.lastObject;
            [self tabHidePushVC:vc];
//             decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
        else if ([urlstring rangeOfString:@"ctl=list"].location != NSNotFound) {
                NSArray * arr = [urlstring componentsSeparatedByString:@"?"];
                NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
                NSString * las;
                for (NSString * substr in lastArr) {
                    NSString * str;
                    if ([substr isEqualToString:lastArr.firstObject]) {
                        str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@"\""];
                    }else{
                        str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@""];
                    }
                    
                    str = [str stringByReplacingOccurrencesOfString:@"]=" withString:@"\":\""];
                    if (!las) {
                        las = str;
                    }else{
                        las = [NSString stringWithFormat:@"%@\",\"%@",las,str];
                    }
                }
                NSString * url = [NSString stringWithFormat:@"%@f={%@",FONPort,las];
                url = [url stringByReplacingOccurrencesOfString:@",\"ctl=list\",\"" withString:@"}&ctl=goods&"];
            
                GoodListVC * vc = [GoodListVC new];
                vc.isFromWeb = YES;
                vc.url = url;
                [self tabHidePushVC:vc];
            
                return;
        }
    }
    
    
   
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
    [MBProgressHUD dissmiss];
    [MBProgressHUD showError:@"请求错误" toView:self.view];
}

- (UIBarButtonItem *)closeItem{
    if (!_closeItem) {
        _closeItem = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStylePlain target:self action:@selector(closeNative)];
        [_closeItem setTintColor:[UIColor whiteColor]];
    }
    return _closeItem;
}


#pragma mark - dealloc
- (void)dealloc
{
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    _webView.delegate = nil;
    [_webView stopLoading];
    [_wkwebView removeObserver:self forKeyPath:NSStringFromSelector(@selector(title))];
    [_wkwebView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];

}

- (void)setStatusBarBackgroundColor:(UIColor *)color {

//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//        statusBar.backgroundColor = color;
//    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (WKWebView *)wkwebView {
    if (!_wkwebView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        config.preferences = [[WKPreferences alloc] init];
        config.preferences.minimumFontSize = 10;
        config.preferences.javaScriptEnabled = YES;
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;

        
        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
//        WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:cookie injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
//        [userContentController addUserScript:cookieScript];
        
        [userContentController addScriptMessageHandler:self name:@"share"];
        [userContentController addScriptMessageHandler:self name:@"login"];
        
        config.userContentController = userContentController;
        config.selectionGranularity = WKSelectionGranularityDynamic;
//        config.allowsInlineMediaPlayback = YES;
//        config.mediaTypesRequiringUserActionForPlayback = false;
        
      
        _wkwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0,0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight) configuration:config];
        
        _wkwebView.navigationDelegate = self;//导航代理
        _wkwebView.UIDelegate = self;//UI代理
        _wkwebView.backgroundColor = [UIColor whiteColor];
        _wkwebView.allowsBackForwardNavigationGestures = YES;
        
    }
    return _wkwebView;
}

- (UIProgressView *)progressView
{
    if (!_progressView){
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0,  0, Screen_Width, 10)];
        _progressView.tintColor = APPColor;
        _progressView.trackTintColor = [UIColor clearColor];
       
    }
    return _progressView;
}

@end
