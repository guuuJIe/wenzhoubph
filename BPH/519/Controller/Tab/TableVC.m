//
//  TableVC.m
//  519
//
//  Created by Mac on 2019/11/1.
//  Copyright © 2019 519. All rights reserved.
//

#import "TableVC.h"

@interface TableVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation TableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height) style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = false;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    AdjustsScrollViewInsetNever(self, self.tableView);
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        static NSString *rid = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rid forIndexPath:indexPath];
        cell.textLabel.text = @"111";
        cell.backgroundColor = [UIColor purpleColor];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *rid = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rid forIndexPath:indexPath];
        cell.backgroundColor = [UIColor greenColor];
        cell.textLabel.text = @"222";
        return cell;
    }
    return nil;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 30;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
        view.backgroundColor = [UIColor redColor];
        return view;
        
    }else if (section == 0){
         UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
         view.backgroundColor = [UIColor redColor];
         return view;
    }
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 80;
    }
    return 60;
}




@end
