//
//  MapNavigatorVC.h
//  519
//
//  Created by Macmini on 17/6/9.
//  Copyright © 2017年 519. All rights reserved.
//

#import "BaseVC.h"

@interface MapNavigatorVC : BaseVC
@property(nonatomic,copy)NSString * xpoint;
@property(nonatomic,copy)NSString * ypoint;
@property(nonatomic,copy)NSString * phoneNum;
@property(nonatomic,copy)NSString * storeName;
@end
