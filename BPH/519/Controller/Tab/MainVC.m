//
//  MainVC.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import "MainVC.h"
#import "MainViewPageTC.h"
#import "MainNavTC.h"
#import "MainTimeTC.h"
#import "MainWebAdvTC.h"
#import "MainRecTC.h"
#import "MainAdvTC.h"
#import "MainHotTC.h"
#import "MainADVFourTC.h"
#import "MainElseHotTC.h"
#import "GoodInfoVC.h"
#import "SelectVC.h"
#import "AFNetworking.h"
#import "base64.h"
#import "GoodListVC.h"
#import "MainWebView.h"
#import "TimeGoodVC.h"
#import "StoreAddressVC.h"
#import <sys/utsname.h>
#import "RedpackView.h"
#import "MainTabBarController.h"
#import "ScoreStoreVC.h"
#import "AdverView.h"
@interface MainVC ()<BMKLocationManagerDelegate,MainWebDelegate,UITableViewDelegate,UITableViewDataSource>

{
    LoadView * _loadView;
    NSString * _agmentStr;
    CGFloat _imageHeight;
    NSInteger  _redPackNum;
    NSString * _advUrl;//弹出广告链接
    NSString * _ctlType;//跳转类型
    NSString * _jumpUrl;//跳转目的地
    int _advnum;
}
@property (nonatomic,strong)UIView * navview;
@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)MainTabBarController * maintab;
//红包
@property(nonatomic,strong)RedpackView * redView;
@property(nonatomic,strong)UITableView *tableView;
@property (strong, nonatomic)CountDown *countDown;
@property (strong,nonatomic)NSArray *countDownArray;

@property (nonatomic,strong)MainViewPageTC * mainRunImagecell;
@property(nonatomic,strong)NSMutableArray *data;
@property (nonatomic,strong)NSMutableArray * imageArray;
@property (nonatomic,strong)NSMutableArray * advIdMuArr;
@property (nonatomic,strong)NSMutableArray * urlMuArr;
@property (nonatomic,strong)NSMutableArray * typeMuArr;


@property (nonatomic,strong)MainNavTC * mainNavCell;
@property (nonatomic,strong)NSMutableArray * imageNavArray;
@property (nonatomic,strong)NSMutableArray * nameNavArray;
@property (nonatomic,strong)NSMutableArray * idNavArray;
@property (nonatomic,strong)NSMutableArray * typeNavArray;
@property (nonatomic,strong)NSMutableArray * titleArray;
@property (nonatomic,strong)NSMutableArray * newsUrlArray;
@property (nonatomic,strong)NSMutableArray * newsTypeArray;
@property (nonatomic,strong)NSMutableArray * actionImageArray;
@property (nonatomic,strong)NSMutableArray * actionTypeArray;
@property (nonatomic,strong)NSMutableArray * actionidArray;
@property (nonatomic,copy)NSString * actionBgUrl;
@property (nonatomic,strong)NSMutableArray * navTextColor;


@property (nonatomic,strong)MainADVFourTC * mainADVFourCell;
@property (nonatomic,strong)NSMutableArray * advFourArray;
@property (nonatomic,strong)NSMutableArray * advFourIDArray;
@property (nonatomic,strong)NSMutableArray * advFourtypeArray;

@property (nonatomic,strong)MainTimeTC * mainTimeTCCell;
@property (nonatomic,strong)NSMutableArray * timerImageArray;
@property (nonatomic,strong)NSMutableArray * timerLabelArray;
@property (nonatomic,strong)NSMutableArray * timerMoneyArray;
@property (nonatomic,strong)NSMutableArray * timeridArray;
@property (nonatomic,strong)NSMutableArray * timerBuyCountArray;
@property (nonatomic,copy)NSString * endTimer;

@property (nonatomic,strong)MainWebAdvTC * mainWebCell;
@property (nonatomic,copy)NSString * webString;


@property (nonatomic,strong)MainRecTC * mainRectCCell;
@property (nonatomic,strong)NSMutableArray * rectImageArray;
@property (nonatomic,strong)NSMutableArray * rectUrlArray;
@property (nonatomic,strong)NSMutableArray * rectIdArray;

@property (nonatomic,strong)MainAdvTC * mainADVFiveCell;
@property (nonatomic,strong)NSMutableArray * advFiveImgMuArray;
@property (nonatomic,strong)NSMutableArray * advFiveUrlMuArray;
@property (nonatomic,strong)NSMutableArray * advFiveIdMuArray;
@property (nonatomic,strong)NSMutableArray * advFivetypeMuArray;

@property (nonatomic,strong)MainHotTC * mainHotCell;
@property (nonatomic,strong)MainElseHotTC * mainElseHotCell;
@property (nonatomic,strong)NSMutableArray * hotImageArray;
@property (nonatomic,strong)NSMutableArray * hotBuyNumberArray;
@property (nonatomic,strong)NSMutableArray * hotMoneyArray;
@property (nonatomic,strong)NSMutableArray * hotNameArray;
@property (nonatomic,strong)NSMutableArray * hotIdArray;
@property (nonatomic,strong)NSMutableArray * hotCurrentArray;
@property (nonatomic,strong)NSArray * muarrArr;

//买赠
@property (nonatomic,strong)NSMutableArray * buyImageArray;
@property (nonatomic,strong)NSMutableArray * buyNumberArray;
@property (nonatomic,strong)NSMutableArray * buyMoneyArray;
@property (nonatomic,strong)NSMutableArray * buyNameArray;
@property (nonatomic,strong)NSMutableArray * buyIdArray;
@property (nonatomic,strong)NSMutableArray * buyCurrentArray;
@property (nonatomic,strong)UIScrollView * scrollView;

@property (nonatomic,strong)NSIndexPath * indexPath;

//定位
@property(nonatomic,strong)BMKLocationManager * locService;
@property(nonatomic,assign)float x;
@property(nonatomic,assign)float y;
@property(nonatomic,copy)NSString * cityId;
//门店坐标
@property(nonatomic,copy)NSString * xpoint;
@property(nonatomic,copy)NSString * ypoint;

//版本升级url
@property(nonatomic,copy)NSString * versionStr;

@property(nonatomic,strong)NSMutableArray * shopMuArr;
@property(nonatomic,strong)NSMutableArray * shopIdMuArr;

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"我是首页");
    _advnum = 0;
    _imageArray = [[NSMutableArray alloc]init];
    _advIdMuArr = [NSMutableArray new];
    _urlMuArr = [NSMutableArray new];
    _typeMuArr = [NSMutableArray new];
    
    _imageNavArray = [[NSMutableArray alloc]init];
    _nameNavArray = [[NSMutableArray alloc]init];
    _idNavArray = [NSMutableArray new];
    _typeNavArray = [NSMutableArray new];
    _titleArray = [NSMutableArray new];
    _newsUrlArray = [NSMutableArray new];
    _newsTypeArray = [NSMutableArray new];
    _actionImageArray = [NSMutableArray new];
    _actionTypeArray = [NSMutableArray new];
    _actionidArray = [NSMutableArray new];
    _navTextColor = [NSMutableArray new];
    
    _timerImageArray = [[NSMutableArray alloc]init];
    _timerLabelArray = [[NSMutableArray alloc]init];
    _timerMoneyArray = [[NSMutableArray alloc]init];
    _timeridArray = [[NSMutableArray alloc]init];
    _timerBuyCountArray = [NSMutableArray new];
    
    _rectImageArray = [[NSMutableArray alloc]init];
    _rectIdArray = [[NSMutableArray alloc]init];
    _rectUrlArray = [[NSMutableArray alloc]init];
    
    _hotImageArray = [[NSMutableArray alloc]init];
    _hotMoneyArray = [[NSMutableArray alloc]init];
    _hotNameArray = [[NSMutableArray alloc]init];
    _hotIdArray = [[NSMutableArray alloc]init];
    _hotCurrentArray = [NSMutableArray new];
    _hotBuyNumberArray = [NSMutableArray new];
    
    _buyImageArray = [[NSMutableArray alloc]init];
    _buyMoneyArray = [[NSMutableArray alloc]init];
    _buyNameArray = [[NSMutableArray alloc]init];
    _buyIdArray = [[NSMutableArray alloc]init];
    _buyCurrentArray = [NSMutableArray new];
    _buyNumberArray = [NSMutableArray new];
    
    _advFourArray = [NSMutableArray new];
    _advFourIDArray = [NSMutableArray new];
    _advFourtypeArray = [NSMutableArray new];
    
    _advFiveImgMuArray = [NSMutableArray new];
    _advFiveUrlMuArray = [NSMutableArray new];
    _advFiveIdMuArray = [NSMutableArray new];
    _advFivetypeMuArray = [NSMutableArray new];
    

//    [self initTabBar];
    [self isOpenLocation];
    [self initView];
    
    
    
    [self requestDataOfServerVersion];
    
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onClickNav:) name:@"navNotification" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onClicktiemr:) name:@"timerNotification" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onClickhot:) name:@"hotNotification" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onRectJump:) name:@"rectJump" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginOut) name:@"loginOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushToAd) name:@"pushtoad" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newsJump:) name:@"bphNewsJump" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:@"refreshLocation" object:nil];
    
}

-(void)willEnterForeground{
    [self refreshLocation];
    [self requestDataOfServerVersion];
}

-(void)isOpenLocation{
    //判断定位是否可用
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
             //定位功能可用
             _locService = [[BMKLocationManager alloc]init];
             _locService.delegate = self;
             [_locService startUpdatingLocation];
        
    }else if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        
        //定位不能用
        if (!_cityId) {
            _cityId = @"9999";
        }
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"stop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self requestDataForLocation:_cityId];
    }
}

-(void)refreshLocation{
    _x = 0;
    _y = 0;
    [self isOpenLocation];
}
-(void)loginOut{
    _locService.delegate = self;
    [_locService startUpdatingLocation];
    [self requestDataForLocation:_cityId];
}

//rect
-(void)onRectJump:(NSNotification *)noti{
    NSInteger index = [noti.object integerValue];
     [self pushWebView:_rectUrlArray[index] withType:_rectIdArray[index]];
}
//nav
-(void)onClickNav:(NSNotification *)noti{
    NSInteger index = [noti.object integerValue];
    [self pushWebView:self.idNavArray[index] withType:_typeNavArray[index]];
}
//限时
-(void)onClicktiemr:(NSNotification *)noti{
    NSInteger index = [noti.object integerValue];
    GoodInfoVC * info = [GoodInfoVC new];
    info.goodsId = _timeridArray[index];
    [self tabHidePushVC:info];
}
//爆款
-(void)onClickhot:(NSNotification *)noti{
    NSString * goodId = noti.object;
    [self  barItemBtnOnClick:goodId];
}
//广告页跳转
-(void)pushToAd{
    NSString * runtype = [kUserDefaults objectForKey:@"advImageType"];
    NSString * runImageId = [kUserDefaults objectForKey:@"advImageId"];
    [self pushWebView:runImageId withType:runtype];
}

//头条跳转
-(void)newsJump:(NSNotification *)noti{
    NSArray * arr = noti.object;
    [self pushWebView:arr[0] withType:arr[1]];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"navNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"timerNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"hotNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"rectJump" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"loginOut" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"bphNewsJump" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"pushtoad" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"refreshLocation" object:nil];
}

/*
 *  定位
 *  cityid:城市ID
 */
-(void)requestDataForLocation:(NSString *)cityid{
    if (_shopMuArr) {
        [_shopIdMuArr removeAllObjects];
        [_shopMuArr removeAllObjects];
    }else{
        _shopIdMuArr = [NSMutableArray new];
        _shopMuArr = [NSMutableArray new];
    }
    
    if (!_x && !_y) {
        _x = 0;
        _y = 0;
    }
    NSString * locationUrl = [NSString stringWithFormat:@"%@ctl=shop&act=index&city_shop_id=%@&xpoint=%f&ypoint=%f",FONPort,cityid,_x,_y];
    
    [RequestData requestDataOfUrl:locationUrl success:^(NSDictionary *dic) {
        if (dic && dic[@"shop_list"]) {
            NSString * tel;
            for (NSDictionary * subdic in dic[@"shop_list"]) {
                [self.shopIdMuArr addObject:subdic[@"id"]];
                [self.shopMuArr addObject:subdic[@"name"]];
                if (!self.ypoint || !self.xpoint || !tel) {
                    self.xpoint = subdic[@"xpoint"];
                    self.ypoint = subdic[@"ypoint"];
                    tel = subdic[@"tel"];
                }
                break;
            }
            NSDictionary * storeDic = @{@"xpoint":self.xpoint,@"ypoint":self.ypoint,@"shopName":_shopMuArr[0],@"tel":tel};
            [[NSUserDefaults standardUserDefaults]setObject:self.shopIdMuArr[0] forKey:@"shop_id"];
            [[NSUserDefaults standardUserDefaults]setObject:storeDic forKey:@"storeInfo"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self.tableView.mj_header beginRefreshing];
            
            //webview添加.userAgent 方法
            UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectZero];
            NSString * oldAgent = [webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
            
        }
        if (self.shopMuArr.count>0) {
           // [_mainRunImagecell showShopName:_shopMuArr[0]];
//            _titleLabel.text = _shopMuArr[0];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}



//版本信息数据请求
-(void)requestDataOfServerVersion{
    
    NSString * versionUrl  =[NSString stringWithFormat:@"%@ctl=version&dev_type=ios",FONPort];
    [RequestData requestDataOfUrl:versionUrl success:^(NSDictionary *dic) {
        if (dic && [dic[@"status"] isEqual: @1]) {
            self.versionStr = dic[@"ios_down_url"];
            NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
            if ([ nowSysVer compare:dic[@"serverVersion"]] == NSOrderedAscending) {
                if ([dic[@"forced_upgrade"] isEqual:@1]) {
                    
                    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"版本升级" message:dic[@"ios_upgrade"]  preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        self.versionStr = [_versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.versionStr]];
                    }]];
                    [self presentViewController:alertVC animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"版本升级" message:dic[@"ios_upgrade"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    [alertview show];
                }
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}
//跳转APPstore
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        _versionStr = [_versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_versionStr]];
    }
}
//首页数据请求
- (void)requestDataSourceOfMain{
    NSString * name = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
     NSString * mainUrl = [NSString stringWithFormat:@"%@cal=index&email=%@&pwd=%@",FONPort,name,pwd];
    [RequestData requestDataOfUrl:mainUrl success:^(NSDictionary *dic) {
        
//        self.redPackNum = [[NSString stringWithFormat:@"%@",dic[@"hongbao_count"]]integerValue];
        
        //广告数据
        [self requestAdvPopData:dic[@"adv_pop"]];
        
        //0
        for (NSDictionary * runImageDic in dic[@"advs"]) {
            [self.imageArray addObject:runImageDic[@"img"]];
            [self.advIdMuArr addObject:runImageDic[@"id"]];
            [self mutableArrAddStringtypeMuArr:self.typeMuArr dataMuArr:self.urlMuArr type:runImageDic[@"type"] withData:runImageDic[@"data"]];
        }
        
        
        
        //1
        for (NSDictionary * navImageDic in dic[@"indexs"]) {
            [self.imageNavArray addObject:navImageDic[@"img"]];
            [self.nameNavArray addObject:navImageDic[@"name"]];
            [self.navTextColor addObject:navImageDic[@"color"]];
            
            [self mutableArrAddStringtypeMuArr:_typeNavArray dataMuArr:_idNavArray type:navImageDic[@"type"] withData:navImageDic[@"data"]];
        }
        for (NSDictionary * notiDic in dic[@"index_notice"]) {
            [_titleArray addObject:notiDic[@"name"]];
             [self mutableArrAddStringtypeMuArr:_newsTypeArray dataMuArr:_newsUrlArray type:notiDic[@"type"] withData:notiDic[@"data"]];
        }
        for (NSDictionary * actionDic in dic[@"adv_3_pic"]) {
            [_actionImageArray addObject:actionDic[@"img"]];
            [self mutableArrAddStringtypeMuArr:_actionTypeArray dataMuArr:_actionidArray type:actionDic[@"type"] withData:actionDic[@"data"]];
        }
        for (NSDictionary * advBtnDic in dic[@"adv_btn_img"]) {
            if (!_actionBgUrl) {
                _actionBgUrl = advBtnDic[@"img"];
            }
        }
        //2
        for (NSDictionary * timerDic in dic[@"qianggou_deal_list"]) {
            if (self.endTimer.length<1) {
                _endTimer = timerDic[@"end_time_format"];
            }
            [_timerImageArray addObject:timerDic[@"icon"]];
            [_timerLabelArray addObject:timerDic[@"name"]];
            [_timerMoneyArray addObject:timerDic[@"current_price"]];
            [_timeridArray addObject:timerDic[@"id"]];
            [_timerBuyCountArray addObject:timerDic[@"buy_count"]];
        }
        
        //3
        for (NSDictionary * advFourDic in dic[@"adv_4"]) {
            [_advFourArray addObject:advFourDic[@"img"]];
            
            [self mutableArrAddStringtypeMuArr:_advFourtypeArray dataMuArr:_advFourIDArray type:advFourDic[@"type"] withData:advFourDic[@"data"]];
        }

        //4
        for (NSDictionary * buyDic in dic[@"best_deal_list"]) {
            [_buyNameArray addObject:buyDic[@"name"]];
            [_buyImageArray addObject:buyDic[@"icon"]];
            [_buyMoneyArray addObject:buyDic[@"origin_price"]];
            [_buyIdArray addObject:buyDic[@"id"]];
            [_buyCurrentArray addObject:buyDic[@"current_price"]];
            [_buyNumberArray addObject:buyDic[@"buy_count"]];
        }
      
        //5
        _webString = dic[@"zt_html"];
       
        
        //6
        for (NSDictionary * gundongDic in dic[@"adv_gundong"]) {
            [_rectImageArray addObject:gundongDic[@"img"]];
            [self mutableArrAddStringtypeMuArr:_rectIdArray dataMuArr:_rectUrlArray type:gundongDic[@"type"] withData:gundongDic[@"data"]];
        }
    
        
        //7
        for (NSDictionary * advFiveDic in dic[@"adv_5"]) {
            [_advFiveImgMuArray addObject:advFiveDic[@"img"]];
             [self mutableArrAddStringtypeMuArr:_advFivetypeMuArray dataMuArr:_advFiveIdMuArray type:advFiveDic[@"type"] withData:advFiveDic[@"data"]];
        }
        
        //8
        for (NSDictionary * hotDic in dic[@"supplier_deal_list"]) {
            [_hotNameArray addObject:hotDic[@"name"]];
            [_hotImageArray addObject:hotDic[@"icon"]];
            [_hotMoneyArray addObject:hotDic[@"origin_price"]];
            [_hotIdArray addObject:hotDic[@"id"]];
            [_hotCurrentArray addObject:hotDic[@"current_price"]];
            [_hotBuyNumberArray addObject:hotDic[@"buy_count"]];
        }
        
        
        [self showData];
        
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        
        [MBProgressHUD showError:@"网络错误" toView:self.view];

        NSLog(@"-- %@",error);
        
    }];
}

-(void)loadTap{
    [self allArrayRemoveObjects];
    [self loadData];
}

-(void)mutableArrAddStringtypeMuArr:(NSMutableArray *)typeArr dataMuArr:(NSMutableArray *)dataArr type:(NSString *)type withData:(NSDictionary *)data{
    if ([type isEqualToString:@"0"]) {
        [dataArr addObject: data[@"url"]];
    }else
    if ([type isEqualToString:@"11"]) {
        [dataArr addObject: data[@"tid"]];
    }else
    if ([type isEqualToString:@"12"]) {
        [dataArr addObject: data[@"cate_id"]];
    }else
    if ([type isEqualToString:@"21"]) {
        [dataArr addObject: data[@"item_id"]];
    }else
    if ([type isEqualToString:@"13"]) {
        [dataArr addObject: data[@"data"]];
    }
    [typeArr addObject:type];
}

-(void)requestAdvPopData:(NSArray *)advPopArr {
    for (NSDictionary * subdic in advPopArr) {
        _advUrl = subdic[@"img"];
        _ctlType = [NSString stringWithFormat:@"%@",subdic[@"type"]];
        if ([_ctlType isEqualToString:@"0"]) {
            _jumpUrl = subdic[@"data"][@"url"];
        }else
            if ([_ctlType isEqualToString:@"11"]) {
                 _jumpUrl = subdic[@"data"][@"tid"];
            }else
                if ([_ctlType isEqualToString:@"12"]) {
                     _jumpUrl = subdic[@"data"][@"cate_id"];
                }else
                    if ([_ctlType isEqualToString:@"21"]) {
                         _jumpUrl = subdic[@"data"][@"item_id"];
                    }else
                        if ([_ctlType isEqualToString:@"13"]) {
                             _jumpUrl = subdic[@"data"][@"data"];
                        }
    }
}

#pragma mark 初始化 view
-(void)initView{
    self.view.backgroundColor=APPBGColor;
    
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_SelfWidth, Screen_SelfHeight)];
    [self.tableView registerClass:[MainViewPageTC class] forCellReuseIdentifier:[MainViewPageTC getID]];
    [self.tableView registerClass:[MainNavTC class] forCellReuseIdentifier:[MainNavTC getID]];
    [self.tableView registerClass:[MainTimeTC class] forCellReuseIdentifier:[MainTimeTC getID]];
    [self.tableView registerClass:[MainADVFourTC class] forCellReuseIdentifier:[MainADVFourTC getID]];
    [self.tableView registerClass:[MainWebAdvTC class] forCellReuseIdentifier:[MainWebAdvTC getID]];
    [self.tableView registerClass:[MainRecTC class] forCellReuseIdentifier:[MainRecTC getID]];
    [self.tableView registerClass:[MainAdvTC class] forCellReuseIdentifier:[MainAdvTC getID]];
    [self.tableView registerClass:[MainHotTC class] forCellReuseIdentifier:[MainHotTC getID]];
    [self.tableView registerClass:[MainElseHotTC class] forCellReuseIdentifier:[MainElseHotTC getelseId]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.contentInset = UIEdgeInsetsMake(-Screen_StatusBarHeight,0 , 0, 0);
    self.countDown = [[CountDown alloc] init];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    [self.view addSubview:self.tableView];
    
    

}

-(void)loadData{
    [self allArrayRemoveObjects];
    [self requestDataSourceOfMain];
}


-(void)allArrayRemoveObjects{
    _endTimer = @"";
    [_imageArray removeAllObjects];
    [_urlMuArr removeAllObjects];
    [_typeMuArr removeAllObjects];
    [_imageNavArray removeAllObjects];
    [_nameNavArray removeAllObjects];
    [_titleArray removeAllObjects];
    [_newsUrlArray removeAllObjects];
    [_newsTypeArray removeAllObjects];
    [_typeNavArray removeAllObjects];
    [_timerImageArray removeAllObjects];
    [_timerLabelArray removeAllObjects];
    [_timerMoneyArray removeAllObjects];
    [_timerBuyCountArray removeAllObjects];
    [_timeridArray removeAllObjects];
    [_rectImageArray removeAllObjects];
    [_rectUrlArray removeAllObjects];
    [_actionImageArray removeAllObjects];
    [_actionTypeArray removeAllObjects];
    [_actionidArray removeAllObjects];
    [_navTextColor removeAllObjects];
    _actionBgUrl = nil;
    
    [_hotImageArray removeAllObjects];
    [_hotMoneyArray removeAllObjects];
    [_hotNameArray removeAllObjects];
    [_hotBuyNumberArray removeAllObjects];
    [_hotIdArray removeAllObjects];
    [_hotCurrentArray removeAllObjects];
    
    [_advFourArray removeAllObjects];
    [_advFiveImgMuArray removeAllObjects];
    [_advFiveUrlMuArray removeAllObjects];
    [_idNavArray removeAllObjects];
    [_advFiveIdMuArray removeAllObjects];
    [_advIdMuArr removeAllObjects];
    [_rectIdArray removeAllObjects];
    [_advFourIDArray removeAllObjects];
    
    [_buyImageArray removeAllObjects];
    [_buyMoneyArray removeAllObjects];
    [_buyNameArray removeAllObjects];
    [_buyNumberArray removeAllObjects];
    [_buyIdArray removeAllObjects];
    [_buyCurrentArray removeAllObjects];
    
    [_shopIdMuArr removeAllObjects];
    [_shopMuArr removeAllObjects];
}


-(void)showData{
    
    [self tableView:self.tableView heightForRowAtIndexPath:_indexPath];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.tableView reloadData];
    ///每秒回调一次
    [self.countDown countDownWithPER_SECBlock:^{
        [self updateTimeInVisibleCells];
    }];
    
    if (_redPackNum > 0) {
        _redView = [[RedpackView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        [self.view addSubview:_redView];
    }
//    if (_advnum == 0) {
        if (_advUrl) {
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_advUrl]];
            UIImage *image = [UIImage imageWithData:data];
            CGFloat heigth = (Screen_Width-10)*image.size.height/image.size.width;
            
            AdverView * adv = [[AdverView alloc]initWithFrame:self.view.bounds withAdvUrl:_advUrl];
            adv.block = ^{
                [self pushWebView:_jumpUrl withType:_ctlType];
            };
            CGRect frame = adv.advImageV.frame;
            adv.advImageV.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, heigth);
            [self.view addSubview:adv];
        }
//        _advnum += 1;
//    }
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8+_hotImageArray.count/2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    _indexPath = indexPath;
    if(indexPath.row==0){
        _mainRunImagecell = [tableView dequeueReusableCellWithIdentifier:[MainViewPageTC getID] forIndexPath:indexPath];
        if(_mainRunImagecell==nil){
            _mainRunImagecell=[[MainViewPageTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainViewPageTC getID]];
        }
        _mainRunImagecell.tag = 100;
        [_mainRunImagecell showImg:_imageArray withTag:100];
        return _mainRunImagecell;
    }else if(indexPath.row==1){
        _mainNavCell =[tableView dequeueReusableCellWithIdentifier:[MainNavTC getID] forIndexPath:indexPath];
        if(_mainNavCell==nil){
            _mainNavCell=[[MainNavTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainNavTC getID]];
        }
        return _mainNavCell;
    }else if(indexPath.row==2){
        _mainTimeTCCell =[tableView dequeueReusableCellWithIdentifier:[MainTimeTC getID] forIndexPath:indexPath];
        if(_mainTimeTCCell==nil){
            _mainTimeTCCell=[[MainTimeTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainTimeTC getID]];
        }
        
        if(_timeridArray.count){

        _mainTimeTCCell.tag=indexPath.row;
        }else{
            _mainTimeTCCell.hidden = YES;
        }
        return _mainTimeTCCell;
    }else if (indexPath.row == 3){
        
        _mainADVFourCell = [tableView dequeueReusableCellWithIdentifier:[MainADVFourTC getID] forIndexPath:indexPath];
        if (_mainADVFourCell == nil) {
            _mainADVFourCell = [[MainADVFourTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[MainADVFourTC getID]];
        }
       // _mainADVFourCell.delegate = self;
        [_mainADVFourCell showImgOfAdvFour:_advFourArray withTag:200] ;
        return _mainADVFourCell;
        
    }else if(indexPath.row==4){
        _mainHotCell=[tableView dequeueReusableCellWithIdentifier:[MainHotTC getID] forIndexPath:indexPath];
        if(_mainHotCell==nil){
            _mainHotCell=[[MainHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainHotTC getID]];
        }


        
        return _mainHotCell;
    }else if(indexPath.row==5){
        _mainWebCell =[tableView dequeueReusableCellWithIdentifier:[MainWebAdvTC getID] forIndexPath:indexPath];
        if(_mainWebCell==nil){
            _mainWebCell=[[MainWebAdvTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainWebAdvTC getID]];
        }
        _mainWebCell.delegate = self;
        [_mainWebCell showData:_webString];

        return _mainWebCell;
    }else if(indexPath.row==6){
        _mainRectCCell =[tableView dequeueReusableCellWithIdentifier:[MainRecTC getID] forIndexPath:indexPath];
        if(_mainRectCCell==nil){
            _mainRectCCell=[[MainRecTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainRecTC getID]];
        }
        [_mainRectCCell showData:_rectImageArray];
        return _mainRectCCell;
    }else if(indexPath.row==7){
        _mainADVFiveCell = [tableView dequeueReusableCellWithIdentifier:[MainAdvTC getID] forIndexPath:indexPath];
        if (_mainADVFiveCell == nil) {
            _mainADVFiveCell = [[MainAdvTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[MainAdvTC getID]];
        }
        _mainADVFiveCell.delegate = self;
        [_mainADVFiveCell showImgOfAdvFour:_advFiveImgMuArray withTag:300];
        return _mainADVFiveCell;
    }else if(indexPath.row==8){
        if(_hotImageArray.count){
                _mainHotCell=[tableView dequeueReusableCellWithIdentifier:[MainHotTC getID] forIndexPath:indexPath];
                if(_mainHotCell==nil){
                    _mainHotCell=[[MainHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainHotTC getID]];
                }

        }else{
            _mainHotCell.hidden = YES;
        }
        return _mainHotCell;
    }else{
        NSInteger index = indexPath.row-8;
        if(_hotImageArray.count){
            _mainElseHotCell=[tableView dequeueReusableCellWithIdentifier:[MainElseHotTC getelseId] forIndexPath:indexPath];
            if(_mainElseHotCell==nil){
                _mainElseHotCell=[[MainElseHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainElseHotTC getelseId]];
            }

        }else{
            _mainElseHotCell.hidden = YES;
        }
        return _mainElseHotCell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if(indexPath.row==0){
        return Screen_Width/640*360;
    }else if(indexPath.row==1){
        CGFloat oneLine = Screen_Width*0.4/2;
        if (_actionImageArray.count>0) {
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_actionImageArray[0]]];
            UIImage *image = [UIImage imageWithData:data];
            //san'mu
            return _nameNavArray.count>0 ? oneLine * ((self.nameNavArray.count+4)/5)+40+10+(Screen_Width/(image.size.width*3)*image.size.height)-1 : 40+10+(Screen_Width/(image.size.width*3)*image.size.height)-1;
        }else{
            return _nameNavArray.count>0 ? oneLine * ((self.nameNavArray.count+4)/5)+40+20-1 : 40+20-1;
        }
    }else if(indexPath.row==2){
        if (_timeridArray.count) {
            return Default_Space+40+0.1+(60+Screen_Width/3.3);
        }else{
            return 0;
        }
    }else if (indexPath.row == 3){
        if(_advFourArray.count>0){
            return Default_Space+Screen_Width / 640 * 180;
        }
        return 0;
        
    }else if(indexPath.row==4){
        if (self.buyImageArray.count) {
            int lineNumber=ceil(self.buyImageArray.count/2.0);
            return Default_Space+40+0.5+(((Screen_Width-5*3)/2+65)*lineNumber);
        }else{
            return 0;
        }
    } else if(indexPath.row==5){
        return Default_Space+Screen_Width/2;
    }else if(indexPath.row==6){
        return Default_Space+Screen_Width/2.2;
    }else if(indexPath.row==7){
        return Default_Space+Screen_Width / 640 * 180;
    }else if(indexPath.row==8){
        if (_hotImageArray.count) {
            return 40+10.5+((Screen_Width-5*3)/2+65);
        }else{
            return 0;
        }
    }else{
        if (_hotImageArray.count) {
            return (Screen_Width-5*3)/2+65;
        }else{
            return 0;
        }
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)mainActiontype:(NSInteger)inter{
    [self pushWebView:self.actionidArray[inter] withType:self.actionTypeArray[inter]];
}


#pragma mark 轮播 delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    NSString * jumpId = @"";
    NSString * typeId = @"";
    

    if (cycleScrollView.tag == 100) {
        jumpId = self.urlMuArr[index];
        typeId = self.typeMuArr[index];
        
    }
    if (cycleScrollView.tag == 200) {
        jumpId = self.advFourIDArray[index];
        typeId = self.advFourtypeArray[index];
    }
    if (cycleScrollView.tag == 300) {
        jumpId = self.advFiveIdMuArray[index];
        typeId = self.advFivetypeMuArray[index];
    }
    
    [self pushWebView:jumpId withType:typeId];
}


#pragma mark 倒计时相关方法
//更新cell 可见 cell 数据
-(void)updateTimeInVisibleCells{
    NSArray  *cells = self.tableView.visibleCells; //取出屏幕可见ceLl
    for (UITableViewCell *cell in cells) {
        if([cell class]==[MainTimeTC class]){
            MainTimeTC *mainTimeTC=(MainTimeTC *)cell;
        }else{
            continue;
        }
        
    }
}

//获取 index cell 数据
-(NSString *)getNowTimeWithString:(NSString *)aTimeString{
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 截止时间date格式
    NSDate  *expireDate = [formater dateFromString:aTimeString];
    NSDate  *nowDate = [NSDate date];
    // 当前时间字符串格式
    NSString *nowDateStr = [formater stringFromDate:nowDate];
    // 当前时间date格式
    nowDate = [formater dateFromString:nowDateStr];
    
    NSTimeInterval timeInterval =[expireDate timeIntervalSinceDate:nowDate];
    
    int days = (int)(timeInterval/(3600*24));
    int hours = (int)((timeInterval-days*24*3600)/3600);
    int minutes = (int)(timeInterval-days*24*3600-hours*3600)/60;
    int seconds = timeInterval-days*24*3600-hours*3600-minutes*60;
    
    NSString *dayStr;NSString *hoursStr;NSString *minutesStr;NSString *secondsStr;
    //天
    dayStr = [NSString stringWithFormat:@"%d",days];
    //小时
    hoursStr = [NSString stringWithFormat:@"%d",hours];
    //分钟
    if(minutes<Default_Space)
        minutesStr = [NSString stringWithFormat:@"0%d",minutes];
    else
        minutesStr = [NSString stringWithFormat:@"%d",minutes];
    //秒
    if(seconds < Default_Space)
        secondsStr = [NSString stringWithFormat:@"0%d", seconds];
    else
        secondsStr = [NSString stringWithFormat:@"%d",seconds];
    if (hours<=0&&minutes<=0&&seconds<=0) {
        return @"活动已经结束！";
    }
    if (days) {
        return [NSString stringWithFormat:@"%@天 %@小时 %@分 %@秒", dayStr,hoursStr, minutesStr,secondsStr];
    }
    return [NSString stringWithFormat:@"%@小时 %@分 %@秒",hoursStr , minutesStr,secondsStr];
}



#pragma  mark cell delegate

-(void)selectViewOnClick{
    SelectVC *vc=[[SelectVC alloc]init];
    [self tabHidePushVC:vc];
}



-(void)allTimeGood{
    TimeGoodVC * vc =[[TimeGoodVC alloc] init];
    vc.endTimer = _endTimer;
    [self tabHidePushVC:vc];
}

-(void)pageHomeOnClick{
    StoreAddressVC * store = [StoreAddressVC new];
    store.storeCityId = _cityId;
    __weak typeof (self)weSelf = self;
    store.storeAddressBlock = ^(NSString * ids,NSString * name){
        __strong typeof (weSelf)self = weSelf;
        
        CGFloat selectWidth=[NSString sizeWithText:name font:[UIFont boldSystemFontOfSize:FONT_SIZE_L] maxSize:CGSizeMake(Screen_Width, Screen_Height)].width;
//        self.mainRunImagecell.homeView.frame = CGRectMake(0, Screen_StatusBarHeight, selectWidth+60, 28);
//        self.mainRunImagecell.homeView.center = CGPointMake(Screen_Width/2, 22+Screen_StatusBarHeight);
//        self.mainRunImagecell.letDownImg.frame = CGRectMake(selectWidth+35, 8, 15, 15);
//
//        self.mainRunImagecell.selectLabel.text = name;
//        self.mainRunImagecell.selectLabel.frame = CGRectMake(25, 0, selectWidth+10, 28);
        
        [self.tableView.mj_header beginRefreshing];
        
        [[NSUserDefaults standardUserDefaults]setObject:ids forKey:@"shop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        //webview添加.userAgent 方法
        _agmentStr =[NSString stringWithFormat:@"IOS_bOpIhUi90_<shop_id>%@</shop_id>",ids];
        UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectZero];
        NSString * oldAgent = [webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
//        oldAgent = [oldAgent stringByReplacingOccurrencesOfString:_agmentStr withString:@""];
//        NSString * newAgent = [oldAgent stringByAppendingString:[NSString stringWithFormat:@"IOS_bOpIhUi90_<shop_id>%@</shop_id>",ids]];
//        [[NSUserDefaults standardUserDefaults] setObject:newAgent forKey:@"UserAgent"];
//        [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent" : newAgent, @"User-Agent" : newAgent}];
        NSLog(@"老gent:%@",oldAgent);
        
    } ;
    
    [self tabHidePushVC:store];
}


-(void)barItemBtnOnClick:(NSString *)goodsId{

    GoodInfoVC *vc=[[GoodInfoVC alloc]init];
    vc.goodsId = goodsId;
    [self tabHidePushVC:vc];
}



-(void)pushWebView:(NSString *)weburl withType:(NSString *)typeId{
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    //网页
    if ([typeId isEqualToString:@"0"]) {
        webView.webUrl = weburl;
        if(weburl.length>2){
            [self tabHidePushVC:webView];
        }
    }
    //团购
    if ([typeId isEqualToString:@"11"]) {
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        
        [self tabHidePushVC:goods];
    }
    //商品列表
    if ([typeId isEqualToString:@"12"]) {
        
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        [self tabHidePushVC:goods];
    }
    //商品详情
    if ([typeId isEqualToString:@"21"]) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = weburl;
        [self tabHidePushVC:vc];
    }
    //积分
    if ([typeId isEqualToString:@"13"]) {
//        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
//        goods.typeId = typeId;
//        goods.selectstr = @"mainJumpGoods";
//        [self tabHidePushVC:goods];
        
        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
    }
}


#pragma moreBuyGoodsDelegate
-(void)moregoodsJump{
    GoodListVC * goods = [[GoodListVC alloc]init];
    goods.idStr = @"nil&type_mz=1";
    goods.selectstr = @"mainJumpGoods";
    [self tabHidePushVC:goods];
}




#pragma MAKLocationDelegate
-(void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    if (!_x || !_y || _x == 0 || _y == 0) {
        _y = userLocation.location.coordinate.latitude;
        _x = userLocation.location.coordinate.longitude;

        CLGeocoder * geocoder = [CLGeocoder new];
        [geocoder reverseGeocodeLocation:userLocation.location completionHandler:^(NSArray * array, NSError * _Nullable error) {
            if (array.count > 0){
                CLPlacemark *placemark = [array objectAtIndex:0];
                //将获得的所有信息显示到label上
                
                //获取城市
                NSString *city = placemark.locality;
                if (!city) {
                    //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                    city = placemark.administrativeArea;
                    if (!city) {
                        city = @"温州";
                    }
                }
                [self requestDataForLocationCity:city];
            }else{
                [self requestDataForLocationCity:@"温州"];
            }
            
        }];
        
        _locService.delegate = nil;
    }
}

-(void)didUpdateUserHeading:(BMKUserLocation *)userLocation{

}

-(void)requestDataForLocationCity:(NSString *)cityStr{
    NSString * cityUrl = [NSString stringWithFormat:@"%@ctl=city&act=get_city&city_name=%@",FONPort,cityStr];
    [RequestData requestDataOfUrl:cityUrl success:^(NSDictionary *dic) {
        
        _cityId = dic[@"current_city"][@"id"];
        [self requestDataForLocation:_cityId];
    } failure:^(NSError *error) {
        
        if (error.code == -1009) {
           
            [MBProgressHUD showError:@"网络错误" toView:self.view];
        }else{
            [MBProgressHUD showError:@"定位出错" toView:self.view];
        }
        
    }];
}
#pragma mark mainwebdeleagete
-(void)webPush:(NSString *)pushUrl withUrlId:(NSString *)urlId{
    if ([pushUrl isEqualToString:@"item"]) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = urlId;
        [self tabHidePushVC:vc];
    }
    if ([pushUrl isEqualToString:@"list"]) {
        GoodListVC * vc = [GoodListVC new];
        vc.idStr = urlId;
        [self tabHidePushVC:vc];
    }
    if ([pushUrl isEqualToString:@"web"]) {
        MainWebView * vc = [MainWebView new];
        vc.webUrl = urlId;
        [self tabHidePushVC:vc];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    [super viewWillAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.y > 40.0){
        [self setStatusBarBackgroundColor:[UIColor colorWithHexString:@"0xEF5350" andAlpha:1]];
    }else{
        [self setStatusBarBackgroundColor:[UIColor colorWithHexString:@"0xEF5350" andAlpha:0]];
    }
}
//设置状态栏颜色
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

-(void)setNavviewt{
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 30)];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.center = CGPointMake(Screen_Width/2, _navview.frame.size.height-20);
    [_navview addSubview:_titleLabel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
