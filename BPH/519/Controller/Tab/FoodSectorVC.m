//
//  FoodSectorVC.m
//  519
//
//  Created by Macmini on 2019/1/10.
//  Copyright © 2019 519. All rights reserved.
//

#import "FoodSectorVC.h"
#import "FoodsTableViewCell.h"
#import "FoodsShopHeadView.h"
#import "ShopListModel.h"
#import "MapNavigatorVC.h"
#import "GoodInfoVC.h"
@interface FoodSectorVC ()<UITableViewDelegate,UITableViewDataSource,BMKLocationManagerDelegate,LocationNavigatorDelegate>

@property (nonatomic,strong)UITableView *listTableView;
@property (nonatomic,strong)NSArray *dataArr;
@property(nonatomic,assign)float x;
@property(nonatomic,assign)float y;
@property (nonatomic,strong)NSMutableArray * dataMuArray;

//定位
@property(nonatomic,strong)BMKLocationManager * locService;
@property (nonatomic,assign)NSInteger pageIndex;

@end


@implementation FoodSectorVC

- (NSMutableArray *)dataMuArray{
    if (!_dataMuArray) {
        _dataMuArray = [NSMutableArray new];
    }
    return _dataMuArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"美食团购";
    
//    self.dataArr = @[@[@"",@""],@[@""],@[@"",@"",@""]];
    
    [self setupLayout];
    
    self.view.backgroundColor = RGB(240, 240, 240);
   
    [self.listTableView.mj_header beginRefreshing];
}

- (void)dealloc{
    BLog(@"销毁了");
}


- (void)setupLayout{
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}
#pragma mark---location
-(void)isOpenLocation{
    //判断定位是否可用
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
        //定位功能可用
        _locService = [[BMKLocationManager alloc]init];
        _locService.delegate = self;
        //设置返回位置的坐标系类型
        _locService.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        //设置距离过滤参数
        _locService.distanceFilter = kCLDistanceFilterNone;
        //设置预期精度参数
        _locService.desiredAccuracy = kCLLocationAccuracyBest;
        //设置应用位置类型
        _locService.activityType = CLActivityTypeAutomotiveNavigation;
        WS(ws);
        [_locService requestLocationWithReGeocode:0 withNetworkState:0 completionBlock:^(BMKLocation * _Nullable location, BMKLocationNetworkState state, NSError * _Nullable error) {
            [ws didUpDataLocation:location];
        }];
    }else if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        
        //定位不能用
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"stop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self requestData];
    }
}


#pragma mark location
-(void)targetStoreLocation:(NSString *)xpoint Ypoint:(NSString *)ypoint withPhonenum:(NSString *)phoneNum withStorename:(NSString *)storeName{
//    NSDictionary*storedic = [[NSUserDefaults standardUserDefaults]objectForKey:@"storeInfo"];
    MapNavigatorVC *vc = [[MapNavigatorVC alloc] init];
    vc.xpoint = xpoint;
    vc.ypoint = ypoint;
    vc.phoneNum = phoneNum;
    vc.storeName = storeName;
    BaseNC *nav = [[BaseNC alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark---uitableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.dataMuArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    ShopListModel *shopModel = self.dataMuArray[section];
    return shopModel.tuan_deal_list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *rid = @"FoodsTableViewCell";
    FoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rid];
    if (cell == nil) {
        cell = [[FoodsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rid];
    }
    
    ShopListModel *shopModel = self.dataMuArray[indexPath.section];
    TuanListModel *model = shopModel.tuan_deal_list[indexPath.row];
    cell.tuanModel = model;
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *headId = @"FoodsShopHeadView";
    FoodsShopHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headId];
    if (!headView) {
        headView = [[FoodsShopHeadView alloc] initWithReuseIdentifier:headId];
    }
    headView.delegate = self;
    headView.shopModel = self.dataMuArray[section];
    return headView;
    //http://www.bphapp.com/wap/index.php?ctl=tuan
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ShopListModel *shopModel = self.dataMuArray[indexPath.section];
    TuanListModel *model = shopModel.tuan_deal_list[indexPath.row];
    GoodInfoVC *vc = [[GoodInfoVC alloc] init];
    vc.goodsId = model.ids;
    [self tabHidePushVC:vc];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView *)listTableView{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_listTableView registerClass:[FoodsTableViewCell class] forCellReuseIdentifier:@"FoodsTableViewCell"];
        _listTableView.estimatedRowHeight = 80;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        [_listTableView registerClass:[FoodsShopHeadView class] forHeaderFooterViewReuseIdentifier:@"FoodsShopHeadView"];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
//            [self requestData];
            self.pageIndex = 1;
            if (self.isFirst) {
                self.isFirst = !self.isFirst;
                [self isOpenLocation];
            }else{
                [self requestData];
            }
             [self.listTableView.mj_footer endRefreshing];
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
             StrongSelf(self);
            self.pageIndex = self.pageIndex + 1;
            [self requestData];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

#pragma mark ---mapDelegate
-(void)didUpDataLocation:(BMKLocation*)location{
    if (!_x || !_y || _x == 0 || _y == 0) {
        _y = location.location.coordinate.latitude;
        _x = location.location.coordinate.longitude;
        [self requestData];
        _locService.delegate = nil;
    }
}

-(void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    
}

#pragma mark -request

- (void)requestData{
//    if (self.dataMuArray) {
//        [self.dataMuArray removeAllObjects];
//    }else{
//        self.dataMuArray = [NSMutableArray new];
//    }
    if(!_x && !_y){
        _x = 0 ;
        _y = 0 ;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@ctl=tuan&page=%d&xpoint=%f&ypoint=%f",FONPort,_pageIndex,_x,_y];
    
    WeakSelf(self);
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        StrongSelf(self);
        if ([dic[@"status"] isEqual:@1]) {
            NSArray *dataArr = dic[@"shop_list"];
            
            if (self.pageIndex == 1) {
                [self.dataMuArray removeAllObjects];
                [self.listTableView.mj_footer endRefreshing];
                NSMutableArray *shopArr = [NSMutableArray new];
                for (NSDictionary *dic in dataArr) {
                    ShopListModel *listModel = [[ShopListModel alloc] init];
                    [listModel setValuesForKeysWithDictionary:dic];
                    [shopArr addObject:listModel];
                }
                self.dataMuArray = [NSMutableArray arrayWithArray:shopArr];
            }else{
                if (dataArr.count == 0) {
                    self.pageIndex -- ;
                    [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                }else{
                    NSMutableArray *shopArr = [NSMutableArray new];
                    for (NSDictionary *dic in dataArr) {
                        ShopListModel *listModel = [[ShopListModel alloc] init];
                        [listModel setValuesForKeysWithDictionary:dic];
                        [shopArr addObject:listModel];
                    }
                    [self.dataMuArray addObjectsFromArray:shopArr];
                }
            }
            
            [self.listTableView reloadData];
            
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
        [self.listTableView.mj_header endRefreshing];
       
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
