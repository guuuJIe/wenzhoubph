//
//  SelectIMGTC.h
//  519
//
//  Created by 陈 on 16/9/9.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@protocol TypeIMGTCDelegate

-(void)allGoodOnClick:(NSInteger)goodsId;

@end

@interface TypeIMGTC : BaseTC

@property(nonatomic,strong)id<TypeIMGTCDelegate> delegate;

-(void)showData:(NSString *)title withImg:(NSString *)img withIndex:(NSInteger)index;

+(CGFloat)getCellHeight;

+(NSString *)getID;
@end
