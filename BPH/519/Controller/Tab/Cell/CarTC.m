//
//  CarTC.m
//  519
//
//  Created by 陈 on 16/9/20.
//  Copyright © 2016年 519. All rights reserved.
//

#import "CarTC.h"

@interface CarTC()
@property(nonatomic,strong)UIView *line;

@property(nonatomic,strong)UIView *garyView;
@property(nonatomic,strong)UILabel *stateLabel;
@property(nonatomic,strong)UIButton *stateBtn;

@property(nonatomic,strong)UIButton * tureBtn;
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,strong)UILabel *priceLabel;
@property(nonatomic,strong)UILabel *countLabel;

@property(nonatomic,strong)UIView *editView;
@property(nonatomic,strong)UIButton *addBtn;
@property(nonatomic,strong)UIButton *removeBtn;
@property(nonatomic,strong)UILabel *nowCountLabel;
@property(nonatomic,strong)UIButton *deleteBtn;

@property (nonatomic,assign)NSInteger interNumber;
@property (nonatomic,copy)NSString * cartId;
@property (nonatomic,copy)NSString * onePrice;
@property(nonatomic,assign)BOOL isEdit;

@end

@implementation CarTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0,[CarTC getCellHeight]-Default_Space , Screen_Width, Default_Space)];
    self.line.backgroundColor=APPBGColor;
    
    
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 80, 80)];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.img.frame.size.width+self.img.frame.origin.x+Default_Space, self.img.frame.origin.y, Screen_Width-Default_Space*3-self.img.frame.size.width, 40)];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.textColor=APPFourColor;
    self.nameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.nameLabel.numberOfLines = 0;
    self.infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.nameLabel.frame.origin.x, self.nameLabel.frame.origin.y+self.nameLabel.frame.size.height, self.nameLabel.frame.size.width, 20)];
    self.infoLabel.numberOfLines=0;
    self.infoLabel.textColor=APPThreeColor;
    self.infoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.priceLabel=[[UILabel alloc]init];
    self.priceLabel.textColor=APPColor;
    self.priceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.countLabel=[[UILabel alloc]init];
    self.countLabel.textColor=APPThreeColor;
    self.countLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.countLabel.textAlignment=NSTextAlignmentCenter;
    
    
    self.editView=[[UIView alloc]initWithFrame:CGRectMake(self.nameLabel.frame.origin.x, self.infoLabel.frame.size.height+self.infoLabel.frame.origin.y, self.nameLabel.frame.size.width, 20)];
    self.editView.backgroundColor=[UIColor whiteColor];
    self.editView.hidden=YES;
    self.addBtn=[[UIButton alloc]init];
    [self.addBtn setBackgroundImage:[UIImage imageNamed:@"number_add_icon"] forState:UIControlStateNormal];
    [self.addBtn addTarget:self action:@selector(addBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    self.nowCountLabel=[[UILabel alloc]init];
    self.nowCountLabel.textColor=APPFourColor;
    self.nowCountLabel.textAlignment=NSTextAlignmentCenter;
    self.nowCountLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.removeBtn=[[UIButton alloc]init];
    [self.removeBtn setBackgroundImage:[UIImage imageNamed:@"number_remove_icon"] forState:UIControlStateNormal];
    [self.removeBtn addTarget:self action:@selector(removeBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    self.deleteBtn=[[UIButton alloc]init];
    self.deleteBtn.layer.cornerRadius=3;
    self.deleteBtn.layer.masksToBounds=YES;
    [self.deleteBtn setBackgroundColor:APPColor];
    self.deleteBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.deleteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteBtn addTarget:self action:@selector(deleteBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.editView addSubview:self.addBtn];
    [self.editView addSubview:self.nowCountLabel];
    [self.editView addSubview:self.removeBtn];
    [self.editView addSubview:self.deleteBtn];
    
    [self addSubview:self.tureBtn];
    [self addSubview:self.nameLabel];
    [self addSubview:self.infoLabel];
    [self addSubview:self.img];
    [self addSubview:self.priceLabel];
    [self addSubview:self.countLabel];
    [self addSubview:self.editView];
    [self addSubview:self.line];
}



-(void)showDataCellTitle:(NSString *)title subname:(NSString *)subname icon:(NSString *)icon price:(NSString *)price number:(NSString *)number  cartId:(NSString *)cartid  cellIdex:(NSInteger)cellIdex isEdit:(BOOL)isEdit{
    self.interNumber = [number integerValue];
    self.cartId = cartid;
    self.cellIdex = cellIdex;
    self.onePrice = price;
    self.isEdit=isEdit;
    
    [self.garyView setHidden:YES];
    if(self.isEdit){
        [self.editView setHidden:NO];
    }else{
        [self.editView setHidden:YES];
    }
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",icon]]];
    self.nameLabel.text = title;
    self.infoLabel.text = subname;
    
    NSString *priceStr=[NSString stringWithFormat:@"￥ %.2lf",[price doubleValue]];
    self.priceLabel.frame=CGRectMake(self.nameLabel.frame.origin.x, self.infoLabel.frame.size.height+self.infoLabel.frame.origin.y,[NSString sizeWithText:priceStr font:self.priceLabel.font maxSize:Max_Size].width , 20);
    self.priceLabel.text = priceStr;
    
    NSString *countStr=@"x100";
    self.countLabel.frame=CGRectMake(Screen_Width-Default_Space-[NSString sizeWithText:countStr font:self.countLabel.font maxSize:Max_Size].width , self.infoLabel.frame.size.height+self.infoLabel.frame.origin.y,[NSString sizeWithText:countStr font:self.countLabel.font maxSize:Max_Size].width , 20);
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",(long)self.interNumber];
    
    self.addBtn.frame=CGRectMake(0,0, self.editView.frame.size.height, self.editView.frame.size.height);
    NSString *nowCountStr=@"100";
    self.nowCountLabel.frame=CGRectMake(self.addBtn.frame.origin.x+self.addBtn.frame.size.width+Default_Space, 0, [NSString sizeWithText:nowCountStr font:self.nowCountLabel.font maxSize:Max_Size].width, self.editView.frame.size.height);
    self.nowCountLabel.text=[NSString stringWithFormat:@"%ld",(long)self.interNumber];
    self.removeBtn.frame=CGRectMake(self.nowCountLabel.frame.size.width+self.nowCountLabel.frame.origin.x+Default_Space,0, self.editView.frame.size.height, self.editView.frame.size.height);
    
    NSString *deleteStr=@"删除";
    self.deleteBtn.frame=CGRectMake(self.editView.frame.size.width-[NSString sizeWithText:deleteStr font:self.deleteBtn.titleLabel.font maxSize:Max_Size].width-Default_Space*4,0, [NSString sizeWithText:deleteStr font:self.deleteBtn.titleLabel.font maxSize:Max_Size].width+Default_Space*2, self.editView.frame.size.height);
    [self.deleteBtn setTitle:deleteStr forState:UIControlStateNormal];
    
}

-(void)addBtnOnClick{
    if (_interNumber>=99) {
        return;
    }
    
    _interNumber++;
    
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",(long)_interNumber];
    self.nowCountLabel.text=[NSString stringWithFormat:@"%ld",(long)_interNumber];
    
    
    NSString * index = [NSString stringWithFormat:@"%ld",(long)self.cellIdex];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"addGoodsNum" object:@[self.nowCountLabel.text,index,_onePrice]];
}

-(void)removeBtnOnClick{
    if (_interNumber<=1) {
        return;
    }
    _interNumber--;
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",(long)_interNumber];
    self.nowCountLabel.text=[NSString stringWithFormat:@"%ld",(long)_interNumber];
    
    NSString * index = [NSString stringWithFormat:@"%ld",(long)self.cellIdex];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"addGoodsNum" object:@[self.nowCountLabel.text,index,_onePrice]];
}

-(void)deleteBtnOnClick{
    NSString * index = [NSString stringWithFormat:@"%ld",(long)self.cellIdex];
    NSString * number = [NSString stringWithFormat:@"%ld",(long)self.interNumber];
    
    NSArray * arr = @[self.cartId,index,self.onePrice,number];
    [self.delegate removeGoodsArray:arr];
}


+(CGFloat)getCellHeight{

    return 100+Default_Space;
}

+(NSString *)getID{
    return @"CarTC";
}
@end
