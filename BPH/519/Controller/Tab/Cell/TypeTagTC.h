//
//  SelectTagTC.h
//  519
//
//  Created by 陈 on 16/9/9.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@protocol TypeTagTcDelegate <NSObject>

-(void)allTypeTagOnClick:(NSInteger)tag withCellIdex:(NSInteger)index;

@end



@interface TypeTagTC : BaseTC

@property (nonatomic,assign)id<TypeTagTcDelegate>delegate;

-(void)showData:(NSArray *)array withCellIndex:(NSInteger)index;

+(CGFloat)getCellHeight:(NSArray *)array;

+(NSString *)getID;
@end
