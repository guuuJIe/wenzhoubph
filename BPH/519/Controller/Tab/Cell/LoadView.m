//
//  LoadView.m
//  519
//
//  Created by Macmini on 17/2/6.
//  Copyright © 2017年 519. All rights reserved.
//

#import "LoadView.h"

@implementation LoadView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

-(void)initView{
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 130)];
    imageView.center = CGPointMake(self.frame.size.width/2, self.frame.size.width*0.55);
    imageView.image = [UIImage imageNamed:@"bottle_icon_200"];
    [self addSubview:imageView];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    _titleLabel.center = CGPointMake(self.frame.size.width/2, imageView.frame.size.height+imageView.frame.origin.y+40);
    _titleLabel.font = [UIFont systemFontOfSize:15];
    _titleLabel.text = @"努力加载中...";
    _titleLabel.textColor = APPThreeColor;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLabel];
    
}

@end
