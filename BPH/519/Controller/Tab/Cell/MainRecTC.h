//
//  MainAdvTC.h
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@interface MainRecTC : BaseTC

-(void)showData:(NSArray *)data;

+(NSString *)getID;
@end
