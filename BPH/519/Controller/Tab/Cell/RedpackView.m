//
//  RedpackView.m
//  519
//
//  Created by Macmini on 2017/8/17.
//  Copyright © 2017年 519. All rights reserved.
//

#import "RedpackView.h"

@interface RedpackView()
{
    NSInteger _pageNum;
    float _redpageMoney;
    
}
@property(nonatomic,strong)UIView * bgView;
@property(nonatomic,strong)UIImageView * redPageImage;
@property(nonatomic,strong)UIImageView * doneImage;
@property(nonatomic,strong)UILabel * moneyLabel;
@end

@implementation RedpackView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setRedpackUI];
        
    }
    return self;
}

-(void)setRedpackUI{
    self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.3];
    self.bgView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:_bgView];
    
    [self getRedpack];
    self.bgView.transform = CGAffineTransformIdentity;

    UIButton * backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    backBtn.center = CGPointMake(self.frame.size.width-50, self.frame.size.height/5);
    [backBtn setImage:[UIImage imageNamed:@"xx-2"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(backOnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:backBtn];
    
    
    [UIView animateWithDuration:0.2 animations:^{
        self.transform = CGAffineTransformMakeScale(2, 2);
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

-(void)getRedpack{
    self.redPageImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.redPageImage.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    [self.bgView addSubview:self.redPageImage];
    self.redPageImage.image = [UIImage imageNamed:@"noopenview"];
    
    UIButton * nowbtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/3, 50)];
    nowbtn.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/1.77);
    [nowbtn addTarget:self action:@selector(nowBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.bgView addSubview:nowbtn];
}

-(void)nowBtnClick:(UIButton *)btn{
    [self requestRedpack];
    self.redPageImage.image = [UIImage imageNamed:@"openpack"];
    [btn removeFromSuperview];
    
    UILabel * moneyLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/3, 60)];
    moneyLabel.center = CGPointMake(self.frame.size.width/1.6, self.frame.size.height/2.05);
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    moneyLabel.textColor = [UIColor redColor];
    moneyLabel.font = [UIFont fontWithName:@"CourierNewPs-BoldItalicMT" size:30.f];
    self.moneyLabel = moneyLabel;
    [self.bgView addSubview:self.moneyLabel];
    
    
    self.doneImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.doneImage.image = [UIImage imageNamed:@"overbtn"];
    [self.bgView addSubview:self.doneImage];
    
    
    UIButton * donebtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/3, 50)];
    donebtn.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/1.57);
    [donebtn addTarget:self action:@selector(doneBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.bgView addSubview:donebtn];
    
    
    
    //波浪动画
    CATransition *anima = [CATransition animation];
    anima.type = @"rippleEffect";//设置动画的类型
    anima.subtype = kCATransitionFromRight; //设置动画的方向
    anima.duration = 1.0f;
    [self.bgView.layer addAnimation:anima forKey:@"rippleEffectAnimation"];
    
}
-(void)doneBtnClick:(UIButton *)btn{
    [self.doneImage removeFromSuperview];
    [self.redPageImage removeFromSuperview];
    [self.moneyLabel removeFromSuperview];
    
    if (_pageNum == 0) {
        [self removeFromSuperview];
    }else{
        [self getRedpack];
    }
    
    //翻转动画
    CATransition *anima = [CATransition animation];
    anima.type = @"oglFlip";//设置动画的类型
    anima.subtype = kCATransitionFromRight; //设置动画的方向
    anima.duration = 1.0f;
    
    [self.bgView.layer addAnimation:anima forKey:@"oglFlipAnimation"];
    
}


-(void)requestRedpack{
    NSString * name = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    NSString * url = [NSString stringWithFormat:@"http://www.bphapp.com/cxb_api/index.php?ctl=yingxiao&act=fan_hongbao&email=%@&pwd=%@",name,pwd];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        NSString * status = [NSString stringWithFormat:@"%@",dic[@"status"]];
        if ([status isEqualToString:@"1"]) {
            float f = [dic[@"hongbao"] floatValue];
            self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",f];
            _pageNum = [[NSString stringWithFormat:@"%@",dic[@"hongbao_count"]] integerValue];
        }else{
            
            [MBProgressHUD showError:dic[@"info"] toView:self];
            [self removeFromSuperview];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"" toView:self];
    }];
}

-(void)backOnClick{
    NSLog(@"我是BACK按钮");
    [self removeFromSuperview];
}

@end
