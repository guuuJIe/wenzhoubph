//
//  MainWebAdvTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainWebAdvTC.h"
#import "MainWebView.h"
@interface MainWebAdvTC()<UIWebViewDelegate>

@property (nonatomic,strong)UILabel * label;
@property(nonatomic,copy)NSString * htmlString;
@property (strong, nonatomic) NSTimer    *timer; // 顶部轮播图片定时器
@property (nonatomic,assign)BOOL isadd;
@end

@implementation MainWebAdvTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _isadd = NO;
        [self initView];
        
    }
    
    return  self;
}

-(void)dealloc{
    self.webView.delegate = nil;
    [self.webView stopLoading];
}

-(void)initView{
    self.backgroundColor=APPBGColor;
        self.selectionStyle=UITableViewCellSelectionStyleNone;

    self.webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, Screen_Width/2)];
    self.webView.backgroundColor=[UIColor whiteColor];
    self.webView.scrollView.scrollEnabled=NO;
    self.webView.delegate = self;
    [self addSubview:self.webView];
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, Screen_Width/2)];
    [self addSubview:_label];
}

-(void)showData:(NSString *)webString{

//    dispatch_async(dispatch_get_main_queue(), ^{
    [self.webView loadHTMLString:webString baseURL:nil];
//    });
    
}


#pragma mark webviewDelegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"%@",request.URL);
    NSString * urlStr = [NSString stringWithFormat:@"%@",request.URL];
    NSArray * urlArr = [urlStr componentsSeparatedByString:@"="];
    if ([urlStr rangeOfString:@"http://"].location != NSNotFound) {
        if ([urlStr rangeOfString:@"ctl=item"].location != NSNotFound) {
            [self.delegate webPush:@"item" withUrlId:urlArr.lastObject];
        }else {
            [self.delegate webPush:@"web" withUrlId:urlStr];
        }
        return NO;
    }
    
    return YES;
}

+(NSString *)getID{
    return @"MainWebAdvTC";
}

@end
