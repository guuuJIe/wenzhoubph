//
//  UserMenuTC.m
//  519
//
//  Created by 陈 on 16/9/3.
//  Copyright © 2016年 519. All rights reserved.
//

#import "UserMenuTC.h"

@interface UserMenuTC()
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *info;
@property(nonatomic,strong)UIImageView *left;
@property(nonatomic,strong)UIView *line;
@end

@implementation UserMenuTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    return  self;
}

-(void)initView{
    UIView *selectView=[[UIView alloc]initWithFrame:self.bounds];
    selectView.backgroundColor=APPBGColor;
    self.selectedBackgroundView=selectView;
    self.backgroundColor=[UIColor whiteColor];
    
    self.icon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 20, 20)];
    
    self.title=[[UILabel alloc]init];
    self.title.textColor=APPFourColor;
    self.title.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.info=[[UILabel alloc]init];
    self.info.textColor=APPFourColor;
    self.info.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.left=[[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-Default_Space-20, Default_Space, 20, 20)];
    self.left.image=[UIImage imageNamed:@"right_black_icon"];
    
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0, 40-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    [self addSubview:self.icon];
    [self addSubview:self.title];
    [self addSubview:self.info];
    [self addSubview:self.left];
    [self addSubview:self.line];
}

-(void)showData:(UserMenuEntity *)entity{
    self.icon.image=[UIImage imageNamed:entity.icon];
    self.title.frame=CGRectMake(self.icon.frame.size.width+self.icon.frame.origin.x+Default_Space, Default_Space, [NSString sizeWithText:entity.title font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width, 20);
    self.info.frame=CGRectMake(self.left.frame.origin.x-Default_Space-[NSString sizeWithText:entity.info font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width, Default_Space,[NSString sizeWithText:entity.info font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width , 20);
    
    self.title.text = entity.title;
    self.info.text = entity.info;
    
    
}

+(NSString *)getID{
    return @"UserMenuTC";
}

@end
