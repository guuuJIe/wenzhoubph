//
//  TypeTagTC.m
//  519
//
//  Created by 陈 on 16/9/9.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TypeTagTC.h"

@interface TypeTagTC()
@property (nonatomic,assign)NSInteger cellIndex;
@end

@implementation TypeTagTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
}
-(void)showData:(NSArray *)array withCellIndex:(NSInteger)index{
    
    _cellIndex = index;
    
    NSArray *subViews=[self subviews];
    
    for (UIView *itemView in subViews){
        [itemView removeFromSuperview];
    }
    
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=30;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=[UIFont systemFontOfSize:FONT_SIZE_L];
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(tagNowX, tagNowLine*tagItemHeight+Default_Space, itemWidth, tagItemHeight)];
        view.backgroundColor=[UIColor clearColor];
        
        UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, 5, itemWidth-Default_Space*2, tagItemHeight-Default_Space)];
        title.textColor=APPFourColor;
        title.textAlignment=NSTextAlignmentCenter;
        title.text=titleStr;
        title.font=titleFont;
        
        view.tag=i;
        UITapGestureRecognizer *itemTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClick:)];
        [view addGestureRecognizer:itemTap];
        [view addSubview:title];
        
        [self addSubview:view];
        tagNowX+=itemWidth;
    }
}

-(void)itemClick:(id)sender{
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
//    [MBProgressHUD showError:[NSString stringWithFormat:@"%ld",_cellIndex] toView:self];
    [self.delegate allTypeTagOnClick:[tap view].tag withCellIdex:_cellIndex];
}

+(CGFloat)getCellHeight:(NSArray *)array{

    CGFloat tagNowX=0;
    CGFloat tagItemHeight=30;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=[UIFont systemFontOfSize:FONT_SIZE_L];
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        tagNowX+=itemWidth;
    }
    
    return (tagNowLine+1)*tagItemHeight+Default_Space*2;
}

+(NSString *)getID{
    return @"TypeTagTC";
}
@end
