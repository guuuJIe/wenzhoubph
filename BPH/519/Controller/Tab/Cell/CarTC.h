//
//  CarTC.h
//  519
//
//  Created by 陈 on 16/9/20.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "CarModel.h"

@protocol RemoveGoodsDelegate <NSObject>

-(void)removeGoodsArray:(NSArray *)arr;
@end

@interface CarTC : BaseTC

@property (nonatomic,strong)CarModel * model;
@property(nonatomic,assign)id<RemoveGoodsDelegate>delegate;
@property (nonatomic,assign)NSInteger goodsNumber;
@property (nonatomic,assign)NSInteger cellIdex;

-(void)showDataCellTitle:(NSString*)title subname:(NSString *)subname icon:(NSString *)icon price:(NSString *)price number:(NSString *)number cartId:(NSString *)cartid cellIdex:(NSInteger)cellIdex isEdit:(BOOL)isEdit;

+(CGFloat)getCellHeight;

+(NSString *)getID;
@end
