//
//  UserMenuTC.h
//  519
//
//  Created by 陈 on 16/9/3.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@interface UserMenuTC : BaseTC
-(void)showData:(UserMenuEntity *)entity;
+(NSString *)getID;
@end
