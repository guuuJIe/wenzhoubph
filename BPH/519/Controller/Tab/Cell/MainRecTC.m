//
//  MainAdvTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainRecTC.h"
#import "MainRecTCCC.h"

@interface MainRecTC() <UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSArray *datas;
@property(nonatomic,strong)UIImageView * rectimageView;
@property(nonatomic,assign)BOOL isadd;
@end

@implementation MainRecTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
        _isadd = NO;
    }
    
    return  self;
}

-(void)initView{
    
    
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, Screen_Width/2.2)];
    self.scrollView.contentSize = CGSizeMake( Screen_Width/2.2*4, Screen_Width/2.2);
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.bounces=NO;
    [self addSubview:self.scrollView];
}



-(void)showData:(NSArray *)data {

        for (NSInteger i = 0; i<data.count; i++) {
            _rectimageView = [[UIImageView alloc]initWithFrame:CGRectMake(i * Screen_Width/2.2, 0, Screen_Width/2.2, self.scrollView.frame.size.height)];
            _rectimageView.contentMode = UIViewContentModeScaleAspectFit;
            [_rectimageView sd_setImageWithURL:[NSURL URLWithString:data[i]]];
            _rectimageView.tag = 600 + i;
            [self.scrollView addSubview:_rectimageView];
            UITapGestureRecognizer * tapOfImageView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewOnClick:)];
            [_rectimageView addGestureRecognizer:tapOfImageView];
        }
    
}
-(void)imageViewOnClick:(UITapGestureRecognizer *)tap{

    [ [NSNotificationCenter defaultCenter]postNotificationName:@"rectJump" object:[NSString stringWithFormat:@"%ld",tap.view.tag-600]];
}


#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.datas.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    MainRecTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainRecTCCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[MainRecTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    
    cell.alpha = 0.1;
    [cell showData:self.datas[indexPath.row]];
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/2.2, Screen_Width/2.2);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"qq");
   
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+(NSString *)getID{
    return @"MainRecTC";
}
@end
