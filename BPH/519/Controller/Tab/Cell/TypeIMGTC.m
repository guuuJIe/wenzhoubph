//
//  TypeIMGTC.m
//  519
//
//  Created by 陈 on 16/9/9.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TypeIMGTC.h"

@interface TypeIMGTC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UIView *imgGray;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIButton *allBtn;

@end

@implementation TypeIMGTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.375)];

    self.imgGray=[[UIView alloc]initWithFrame:self.img.bounds];
    self.imgGray.backgroundColor=[UIColor colorWithHexString:@"000000" andAlpha:0.3];
    
    self.titleLabel=[[UILabel alloc]init];
    self.titleLabel.textColor=[UIColor whiteColor];
    self.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];

    self.allBtn=[[UIButton alloc]init];
    [self.allBtn setBackgroundColor:[UIColor clearColor]];
    [self.allBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.allBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    self.allBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.allBtn.layer.borderWidth=Default_Line;
    self.allBtn.layer.masksToBounds=YES;
    
    [self addSubview:self.img];
    [self addSubview:self.imgGray];
    [self addSubview:self.titleLabel];
    [self addSubview:self.allBtn];
}

-(void)showData:(NSString *)title withImg:(NSString *)img withIndex:(NSInteger)index{
    NSString *titleStr=title;
    CGFloat titleWidth=[NSString sizeWithText:titleStr font:self.titleLabel.font maxSize:Max_Size].width;
    self.titleLabel.frame=CGRectMake((self.frame.size.width-titleWidth)/2, (self.img.frame.size.height-20-30-Default_Space)/2, titleWidth, 20);
    self.titleLabel.text=titleStr;
    
    NSString *allStr=@"查看全部";
    CGFloat allWidth=[NSString sizeWithText:allStr font:self.allBtn.titleLabel.font maxSize:Max_Size].width+Default_Space*2;
    self.allBtn.frame=CGRectMake((self.frame.size.width-allWidth)/2, self.titleLabel.frame.size.height+self.titleLabel.frame.origin.y+Default_Line, allWidth, 30);
    [self.allBtn setTitle:allStr forState:UIControlStateNormal];
    self.allBtn.tag = 900+index;
    [self.allBtn addTarget:self action:@selector(allBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:img]];
}

-(void)allBtnOnClick:(UIButton *)btn{
    
    [self.delegate allGoodOnClick:btn.tag-900];
}

+(CGFloat)getCellHeight{
    return Screen_Width*0.375;
}

+(NSString *)getID{
    return @"TypeIMGTC";
}
@end
