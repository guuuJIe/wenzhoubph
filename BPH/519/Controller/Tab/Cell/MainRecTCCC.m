//
//  MainAdvTCCC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainRecTCCC.h"

@interface MainRecTCCC()
@property(nonatomic,strong)UIImageView *img;

@end

@implementation MainRecTCCC
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.img=[[UIImageView alloc]initWithFrame:self.bounds];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.img];
}


-(void)showData:(NSString *)imgUrl{
    [self.img sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
}


+(NSString*)getID{
    return @"MainRecTCCC";
}

@end
