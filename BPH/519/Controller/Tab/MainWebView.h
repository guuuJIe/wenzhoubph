//
//  MainWebView.h
//  519
//
//  Created by Macmini on 16/12/20.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"
#import <JavaScriptCore/JavaScriptCore.h>
@protocol JSObjcDelegate <JSExport>
- (void)startLogin;
- (void)share;
@end
@interface MainWebView : BaseVC<JSObjcDelegate>

@property (nonatomic,copy)NSString * runjump;
@property(nonatomic,strong)NSString * webUrl;

@end
