//
//  Type.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TypeVC.h"
#import "SelectVC.h"
#import "TypeIMGTC.h"
#import "TypeTagTC.h"
#import "GoodListVC.h"
#import "MainModel.h"
#import "MainWebView.h"
#import "GoodInfoVC.h"
#import "ScoreStoreVC.h"
@interface TypeVC ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSInteger _num;
    NSString *_ids;
}
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,strong)NSMutableArray * typeMuArray;
@property (nonatomic,strong)NSMutableArray * subTypeArray;

@property (nonatomic,strong)UIButton * searchBtn;
@property (nonatomic,strong)UITextField * searchfield;
@property (nonatomic,strong)UILabel * typeNameLabel;
@property (nonatomic,strong)UIButton * moreBtn;

@property (nonatomic,strong)NSMutableArray * labelArr;
@end

@implementation TypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"我是分类页");
    // Do any additional setup after loading the view.
    
    _typeMuArray  = [NSMutableArray new];
    _subTypeArray = [NSMutableArray new];
    _labelArr = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshbySelfView:) name:@"refreshHome1" object:nil];
    
    _num = 0;
    [self initBar];
    [self initNewView];
    
    [self requestDataOfTypeVC];
}



-(void)refreshbySelfView:(NSNotification *)noti{
    MainModel * model = noti.object;
    [self viewControllerIsJumpBy:model.data withtype:model.type];
}
-(void)requestDataOfTypeVC{
    
    NSString * url = [NSString stringWithFormat:@"%@ctl=cate",FONPort];
    WeakSelf(self);
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        StrongSelf(self);
        for (NSDictionary * dict in dic[@"bcate_list"]) {
            CateModel * model = [[CateModel alloc]init];
            [model setValuesForKeysWithDictionary:dict];
            [self.typeMuArray addObject:model];
            
            NSMutableArray * subMuarr = [[NSMutableArray alloc]init];
            for (NSDictionary * subdic in dict[@"bcate_type"]) {
                CateModel * submodel = [[CateModel alloc]init];
                [submodel setValuesForKeysWithDictionary:subdic];
                [subMuarr addObject:submodel];
            }
            [self.subTypeArray addObject:subMuarr];
        }
        [self showDataOfType];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}


-(void)initBar{
    UIView * searview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
    searview.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = searview;
    
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 5, searview.frame.size.width-35, 30)];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.searchBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.searchBtn setBackgroundColor:[UIColor whiteColor]];
    [self.searchBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 0, 5, 0)];
    self.searchBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.searchBtn setImage:[UIImage imageNamed:@"搜索1"] forState:(UIControlStateNormal)];
    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    self.searchBtn.layer.cornerRadius = 5;
    self.searchBtn.layer.masksToBounds = YES;
    [searview addSubview:self.searchBtn];
    
}

-(void)onClickSearchBtn{
    SelectVC *vc=[[SelectVC alloc]init];
    [self tabHidePushVC:vc];
}

-(void)initNewView{
    self.view.backgroundColor=[UIColor colorWithWhite:0.9 alpha:1];
    //滑动view
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width/4, Screen_Height-self.barView.frame.size.height-self.statusBarView.frame.size.height-self.tabBarController.tabBar.frame.size.height)];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:self.tableView];
    
    UIView * collectionTitleView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.tableView.frame)+10, CGRectGetMaxY(self.barView.frame)+10 ,Screen_Width/4*3-20, 40)];
    collectionTitleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:collectionTitleView];
    self.typeNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 40)];
    _typeNameLabel.textColor =APPFourColor;
    _typeNameLabel.textAlignment = NSTextAlignmentLeft;
    _typeNameLabel.font = [UIFont systemFontOfSize:14];
    [collectionTitleView addSubview:_typeNameLabel];
    
    UIButton * moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetWidth(collectionTitleView.frame)-50, 0, 40, 40)];
    [moreBtn setTitle:@"全部" forState:(UIControlStateNormal)];
    moreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [moreBtn setTitleColor:APPThreeColor forState:(UIControlStateNormal)];
    [moreBtn addTarget:self action:@selector(mobtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [collectionTitleView addSubview:moreBtn];
    
    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 1;
    layout.minimumLineSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.tableView.frame)+10, CGRectGetMaxY(collectionTitleView.frame)+10, Screen_Width/4*3-20, Screen_Height-CGRectGetMaxY(collectionTitleView.frame)-20-self.statusBarView.frame.size.height-self.tabBarController.tabBar.frame.size.height-70*AdapterHeightScal) collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"catecollectionCellIde"];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=YES;
    [self.view addSubview:self.collectionView];
}

-(void)mobtnClick{
    GoodListVC * vc = [[GoodListVC alloc]init];
    vc.idStr = _ids;
    [self tabHidePushVC:vc];
}
-(void)showDataOfType{
   
    [self.tableView reloadData];
    [self.collectionView reloadData];
    NSInteger selectedIndex = 0;
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
    [self.tableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    CateModel * model = self.typeMuArray[0];
    self.typeNameLabel.text = model.name;
    _ids = model.ids;
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.typeMuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIde = @"catecellIde";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    CateModel * model = self.typeMuArray[indexPath.row];
    
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = APPBGColor;
    
    UILabel * celllabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    if (indexPath.row == 0) {
        celllabel.textColor = APPColor;
    }else{
        celllabel.textColor = APPFourColor;
    }
    celllabel.text = model.name;
    celllabel.textAlignment = NSTextAlignmentCenter;
    celllabel.font = [UIFont systemFontOfSize:15];
    [cell addSubview:celllabel];
    [self.labelArr addObject:celllabel];
    
    
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [cell addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
        make.height.offset(1);
    }];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    for (NSInteger i=0;i<_labelArr.count; i++) {
        UILabel * label = _labelArr[i];
        if (i == indexPath.row) {
            label.textColor = APPColor;
        }else{
            label.textColor = APPFourColor;
        }
    }
    
    CateModel * model = self.typeMuArray[indexPath.row];
    _typeNameLabel.text = model.name;
    _num = indexPath.row;
    _ids = model.ids;
    [self.collectionView reloadData];
}

#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.subTypeArray.count == 0) {
        return 0;
    }
    NSArray * arr = self.subTypeArray[_num];
    return arr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"catecollectionCellIde" forIndexPath:indexPath];
    if(cell==nil){
        cell=[[UICollectionViewCell alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }else{
        for (UIView * subView in cell.subviews) {
            [subView removeFromSuperview];
        }
    }

    cell.backgroundColor = [UIColor whiteColor];
    NSArray * arr = self.subTypeArray[_num];
    CateModel * model = arr[indexPath.row];
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, itemSize.width-20, itemSize.height-40)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.cate_img]];
    [cell addSubview:imageView];
    
    UILabel * nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame), CGRectGetWidth(cell.frame), 20)];
    nameLabel.text = model.name;
    nameLabel.textColor = APPFourColor;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont systemFontOfSize:13];
    [cell addSubview:nameLabel];
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((Screen_Width-Screen_Width/4-20-3)/3, 100);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * arr = self.subTypeArray[_num];
    CateModel * model = arr[indexPath.row];
    GoodListVC * goods = [GoodListVC new];
    goods.idStr = [NSString stringWithFormat:@"%@",model.ids];
    [self tabHidePushVC:goods];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark 数据跳转处理
-(void)viewControllerIsJumpBy:(NSDictionary *)dataArr withtype:(NSString *)type{
    
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    NSString * item;
    if ([type isEqualToString:@"0"]) {
        item = dataArr[@"url"];
        webView.webUrl = item;
        if(item.length>2){
            [self tabHidePushVC:webView];
        }
    }else
        if ([type isEqualToString:@"11"]) {
            item = dataArr[@"tid"];
            goods.idStr = [NSString stringWithFormat:@"%@",item];
            goods.selectstr = @"mainJumpGoods";
            [self tabHidePushVC:goods];
        }else
            if ([type isEqualToString:@"12"]) {
                item = dataArr[@"cate_id"];
                goods.idStr = [NSString stringWithFormat:@"%@",item];
                goods.selectstr = @"mainJumpGoods";
                [self tabHidePushVC:goods];
            }else
                if ([type isEqualToString:@"21"]) {
                    item = dataArr[@"item_id"];
                    GoodInfoVC * vc = [GoodInfoVC new];
                    vc.goodsId = item;
                    [self tabHidePushVC:vc];
                }else
                    if ([type isEqualToString:@"13"]) {
                        item = dataArr[@"data"];
                        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
                    }
}
@end
