//
//  TimeGoodVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TimeGoodVC.h"
#import "TimeGoodViewPageTC.h"
#import "TimeGoodTimeTC.h"
#import "TimeGoodItemTC.h"
#import "TimerGoodModel.h"
#import "GoodInfoVC.h"
@interface TimeGoodVC()<UITableViewDelegate,UITableViewDataSource,SDCycleScrollViewDelegate,TimerGoodsNowRobDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UITableView *tableView;
@property (strong, nonatomic)CountDown *countDown;
@property (strong,nonatomic)NSMutableArray *countDownArray;
@property(nonatomic,strong)NSMutableArray * timerMuArr;
@property(nonatomic,assign)NSInteger index;

@end

@implementation TimeGoodVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _timerMuArr = [NSMutableArray new];
    _index = 1;
    self.title = @"限时抢购";
    [self initView];
    [self initBar];
    [self requestDataOfTimer];
}

-(void)requestDataOfTimer{
    NSString * timerUrl = [NSString stringWithFormat:@"%@ctl=miaosha&page=%ld",FONPort,(long)_index];
    [RequestData requestDataOfUrl:timerUrl success:^(NSDictionary *dic) {
        if ([dic[@"status"] isEqual:@1]) {
            NSArray * dealArr = dic[@"qianggou_deal_list"];
            if (dealArr.count>0) {
                for (NSDictionary * subdic in dealArr) {
                    TimerGoodModel * model = [TimerGoodModel new];
                    [model setValuesForKeysWithDictionary:subdic];
                    [_timerMuArr addObject:model];
                }
                [self showData];
            }else{
                if (_index == 1) {
                    [self showData];
                }else{
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}


-(void)initView{
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, Screen_Height-self.barView.frame.size.height-self.barView.frame.origin.y)];
    [self.tableView registerClass:[TimeGoodTimeTC class] forCellReuseIdentifier:[TimeGoodTimeTC getID]];
    [self.tableView registerClass:[TimeGoodItemTC class] forCellReuseIdentifier:[TimeGoodItemTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.backgroundColor=APPBGColor;
    self.tableView.showsVerticalScrollIndicator=NO;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer= [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    self.tableView.mj_footer = footer;
    
    self.countDown = [[CountDown alloc] init];
    
    [self.view addSubview:self.tableView];
    
    
    
}
-(void)refreshData{
    _index = 1;
    [_timerMuArr  removeAllObjects];
    [self requestDataOfTimer];
}
-(void)loadData{
    _index++;
    [self requestDataOfTimer];
}

-(void)showData{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
     ///每秒回调一次
    [self.countDown countDownWithPER_SECBlock:^{
        [self updateTimeInVisibleCells];
    }];
}


#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.timerMuArr.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        TimeGoodTimeTC *cell=[tableView dequeueReusableCellWithIdentifier:[TimeGoodTimeTC getID] forIndexPath:indexPath];
        if(cell==nil){
            cell=[[TimeGoodTimeTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[TimeGoodTimeTC getID]];
        }
        [cell showData:[self getNowTimeWithString:self.endTimer]];
        return cell;
    }else{
        TimeGoodItemTC *cell=[tableView dequeueReusableCellWithIdentifier:[TimeGoodItemTC getID] forIndexPath:indexPath];
        if(cell==nil){
            cell=[[TimeGoodItemTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[TimeGoodItemTC getID]];
        }
        
        cell.delegate = self;
        TimerGoodModel * model = self.timerMuArr[indexPath.row-1];
        cell.model = model;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     if(indexPath.row==0){
        return 50+Default_Space*2;
    }else{
        return 140+Default_Line;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row>0) {
        TimerGoodModel * model = self.timerMuArr[indexPath.row - 1];
        [self nowRobWithIds:model.ids];
    }
    
}

#pragma mark viewpage delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
}

#pragma mark nowRobDelegate
-(void)nowRobWithIds:(NSString *)ids{
    GoodInfoVC * vc= [[GoodInfoVC alloc]init];
    vc.goodsId = ids;
    [self tabHidePushVC:vc];
}


#pragma mark 倒计时相关方法
//更新cell 可见 cell 数据
-(void)updateTimeInVisibleCells{
    NSArray  *cells = self.tableView.visibleCells; //取出屏幕可见ceLl
    for (UITableViewCell *cell in cells) {
        if([cell class]==[TimeGoodTimeTC class]){
            TimeGoodTimeTC *timeGoodTimeTC=(TimeGoodTimeTC *)cell;
            [timeGoodTimeTC showData:[self getNowTimeWithString:self.endTimer]];
        }else{
            continue;
        }
        
    }
}

//获取 index cell 数据
-(NSString *)getNowTimeWithString:(NSString *)aTimeString{
    
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 截止时间date格式
    NSDate  *expireDate = [formater dateFromString:aTimeString];
    NSDate  *nowDate = [NSDate date];
    // 当前时间字符串格式
    NSString *nowDateStr = [formater stringFromDate:nowDate];
    // 当前时间date格式
    nowDate = [formater dateFromString:nowDateStr];
    
    NSTimeInterval timeInterval =[expireDate timeIntervalSinceDate:nowDate];
    
    int days = (int)(timeInterval/(3600*24));
    int hours = (int)((timeInterval-days*24*3600)/3600);
    int minutes = (int)(timeInterval-days*24*3600-hours*3600)/60;
    int seconds = timeInterval-days*24*3600-hours*3600-minutes*60;
    
    NSString *dayStr;NSString *hoursStr;NSString *minutesStr;NSString *secondsStr;
    //天
    dayStr = [NSString stringWithFormat:@"%d",days];
    //小时
    hoursStr = [NSString stringWithFormat:@"%d",hours];
    //分钟
    if(minutes<Default_Space)
        minutesStr = [NSString stringWithFormat:@"0%d",minutes];
    else
        minutesStr = [NSString stringWithFormat:@"%d",minutes];
    //秒
    if(seconds < Default_Space)
        secondsStr = [NSString stringWithFormat:@"0%d", seconds];
    else
        secondsStr = [NSString stringWithFormat:@"%d",seconds];
    
    
    if (hours<=0&&minutes<=0&&seconds<=0) {
        return @"活动已经结束！";
    }
    if (days) {
        return [NSString stringWithFormat:@"%@天 %@小时 %@分 %@秒", dayStr,hoursStr, minutesStr,secondsStr];
    }
    return [NSString stringWithFormat:@"%@小时 %@分 %@秒",hoursStr , minutesStr,secondsStr];
}
@end
