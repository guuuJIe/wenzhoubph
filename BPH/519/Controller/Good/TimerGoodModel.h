//
//  TimerGoodModel.h
//  519
//
//  Created by Macmini on 16/12/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimerGoodModel : NSObject

@property(nonatomic,copy)NSString * buy_count;
@property(nonatomic,copy)NSString * current_price;
@property(nonatomic,copy)NSString * end_time;
@property(nonatomic,copy)NSString * icon;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * image;
@property(nonatomic,copy)NSString * max_bought;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * origin_price;
@property(nonatomic,copy)NSString * sub_name;
@property(nonatomic,copy)NSString * time_status;
@property(nonatomic,copy)NSString * buyper;
@end
