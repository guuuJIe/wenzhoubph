//
//  WSLTransitionTwo.m
//  TransferAnimation
//
//  Created by 王双龙 on 2018/4/13.
//  Copyright © 2018年 https://www.jianshu.com/u/e15d1f644bea All rights reserved.
//

#import "WSLTransitionAnimationTwo.h"
#import "NewMainViewController.h"
#import "BaseNC.h"
#import "GoodInfoVC.h"
#import "MainElseHotTC.h"
#import "MainHotTCCC.h"
#import "MainHotTC.h"
@implementation WSLTransitionAnimationTwo

//返回动画事件
- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.3;
}



//所有的过渡动画事务都在这个方法里面完成
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    switch (_transitionType) {
        case WSLTransitionTwoTypePresent:
            [self presentAnimation:transitionContext];
            break;
        case WSLTransitionTwoTypeDissmiss:
            [self dismissAnimation:transitionContext];
            break;
        case WSLTransitionTwoTypePush:
            [self pushAnimation:transitionContext];
            break;
        case WSLTransitionTwoTypePop:
            //            [self popAnimation:transitionContext];
            [self backAnimation:transitionContext];
            break;
    }
    
}

#pragma mark -- transitionType

- (void)presentAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    [transitionContext completeTransition:YES];
    
    //    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.55  initialSpringVelocity:1 / 0.55 options:0 animations:^{
    //    } completion:^(BOOL finished) {
    //    }];
}

- (void)dismissAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    [transitionContext completeTransition:YES];
}

- (void)pushAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    //通过viewControllerForKey取出转场前后的两个控制器，这里toVC就是转场后的VC、fromVC就是转场前的VC
    BaseVC *currentVC = (BaseVC *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    GoodInfoVC *toVC;
    if ([currentVC isKindOfClass:[GoodInfoVC class]]) {
        toVC = (GoodInfoVC *)currentVC;
    }else{
        //        toVC = nil;
        //        [transitionContext completeTransition:YES];
    }
    toVC.hidesBottomBarWhenPushed = true;
    NewMainViewController *fromVC;
    if ([[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey] isKindOfClass:[BaseNC class]]) {
        BaseNC *nv = (BaseNC *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        fromVC = nv.viewControllers.lastObject;
    }else if ([[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey] isKindOfClass:[NewMainViewController class]]){
        fromVC = (NewMainViewController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    }
    //这里有个重要的概念containerView，如果要对视图做转场动画，视图就必须要加入containerView中才能进行，可以理解containerView管理着所有做转场动画的视图
    UIView *containerView = [transitionContext containerView];
    
    MainHotTCCC *collCell;
    if ([fromVC.selectStr isEqualToString:@"1"]) {
        //获取点击的cell
        MainHotTC* cell = [fromVC.tableView cellForRowAtIndexPath:fromVC.currentIndexPath];
        
        collCell = (MainHotTCCC *)[cell.collectionView cellForItemAtIndexPath:cell.indexpath];
    }else if ([fromVC.selectStr isEqualToString:@"2"]){
        
        //获取点击的cell
        MainElseHotTC* cell = [fromVC.tableView cellForRowAtIndexPath:fromVC.currentIndexPath];
        
        collCell = (MainHotTCCC *)[cell.collectionView cellForItemAtIndexPath:cell.index];
    }
    
    
    
    
    
  //使用系统自带的snapshotViewAfterScreenUpdates:方法，参数为YES，代表视图的属性改变渲染完毕后截屏，参数为NO代表立刻将当前状态的视图截图
    UIView *imageViewCopy = [collCell.img snapshotViewAfterScreenUpdates:YES];
    imageViewCopy.frame = [collCell.img convertRect:collCell.img.bounds toView:containerView];
    
    
    
    BLog(@"%f %f ",imageViewCopy.frame.size.height,imageViewCopy.frame.size.width);
    //        collCell.img.hidden = YES;
    //    toVC.cycleScrollView.hidden = YES;
    
    //tempView 添加到containerView中，要保证在最上层，所以后添加
    [containerView addSubview:toVC.view];
    [containerView addSubview:imageViewCopy];
    
    
    //开始做动画
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        imageViewCopy.frame = [toVC.cycleScrollView convertRect:toVC.cycleScrollView.bounds toView:containerView];
        
    } completion:^(BOOL finished) {
        
        [imageViewCopy removeFromSuperview];
        
        //如果动画过渡取消了就标记不完成，否则才完成，这里可以直接写YES，如果有手势过渡才需要判断，必须标记，否则系统不会中断动画完成的部署，会出现无法交互之类的bug
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
    
}

- (void)backAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    //通过viewControllerForKey取出转场前后的两个控制器，这里toVC就是转场后的VC、fromVC就是转场前的VC
    
    NewMainViewController * toVC;
    GoodInfoVC * fromVC = (GoodInfoVC *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    if ([[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey] isKindOfClass:[BaseNC class]]) {
        BaseNC * na = (BaseNC *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        toVC = na.viewControllers.firstObject;
    }else if([[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey] isKindOfClass:[NewMainViewController class]]){
        
        toVC = (NewMainViewController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    }
    
    MainHotTCCC *collCell;
    if ([toVC.selectStr isEqualToString:@"1"]) {
        //获取点击的cell
        MainHotTC* cell = [toVC.tableView cellForRowAtIndexPath:toVC.currentIndexPath];
        
        collCell = (MainHotTCCC *)[cell.collectionView cellForItemAtIndexPath:cell.indexpath];
    }else if ([toVC.selectStr isEqualToString:@"2"]){
        
        //获取点击的cell
        MainElseHotTC* cell = [toVC.tableView cellForRowAtIndexPath:toVC.currentIndexPath];
        
        collCell = (MainHotTCCC *)[cell.collectionView cellForItemAtIndexPath:cell.index];
    }
    
    
    UIView* toView = toVC.view;
    UIView* fromView = fromVC.view;
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView insertSubview:toView belowSubview:fromView];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    //    fromView.frame = CGRectMake(0, StatusBarAndNavigationBarHeight, width, height);
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromView.frame = CGRectMake(width, StatusBarAndNavigationBarHeight, width, height);
        collCell.img.hidden = NO;
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
    
}

- (void)popAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    //通过viewControllerForKey取出转场前后的两个控制器，这里toVC就是转场后的VC、fromVC就是转场前的VC
    NewMainViewController * toVC;
    GoodInfoVC * fromVC = (GoodInfoVC *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    if ([[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey] isKindOfClass:[BaseNC class]]) {
        BaseNC * na = (BaseNC *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        toVC = na.viewControllers.firstObject;
    }else if([[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey] isKindOfClass:[NewMainViewController class]]){
        toVC = (NewMainViewController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    }
    
    //获取点击的cell
    MainHotTC * cell = [toVC.tableView cellForRowAtIndexPath:toVC.currentIndexPath];
    MainHotTCCC *collCell = (MainHotTCCC *)[cell.collectionView cellForItemAtIndexPath:cell.indexpath];
    
    UIView *containerView = [transitionContext containerView];
    
    //使用系统自带的snapshotViewAfterScreenUpdates:方法，参数为YES，代表视图的属性改变渲染完毕后截屏，参数为NO代表立刻将当前状态的视图截图
    UIView *imageViewCopy = [fromVC.cycleScrollView snapshotViewAfterScreenUpdates:NO];
    imageViewCopy.frame = [fromVC.cycleScrollView convertRect:fromVC.cycleScrollView.bounds toView:containerView];
    
    
    //    //设置初始状态
    //    fromVC.cycleScrollView.hidden = YES;
    [containerView addSubview:toVC.view];
    
    //背景过渡视图
    UIView * bgView = [[UIView alloc] initWithFrame:fromVC.view.bounds];
    bgView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:bgView];
    //imageViewCopy 添加到containerView中
    [containerView addSubview:imageViewCopy];
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        imageViewCopy.frame = [collCell.img convertRect:collCell.img.bounds toView:containerView];
        
        //        bgView.alpha = 0;
        //        NSLog(@"%f",collCell.img.frame.size.width);
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        //
        collCell.img.hidden = NO;
        
        //        //动画交互动作完成或取消后，移除临时动画文件
        [imageViewCopy removeFromSuperview];
        [bgView removeFromSuperview];
        
    }];
}


@end
