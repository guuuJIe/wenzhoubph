//
//  TimeGoodItemTC.m
//  519
//
//  Created by 陈 on 16/9/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TimeGoodItemTC.h"

@interface TimeGoodItemTC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,strong)CustomProgress *progressView;
@property(nonatomic,strong)UILabel *priceLabel;
@property(nonatomic,strong)UILabel *oldPriceLabel;
@property(nonatomic,strong)UIButton *btn;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UIView * line2;

@property(nonatomic,copy)NSString * ids;
@property(nonatomic,assign)int present;
@end

@implementation TimeGoodItemTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.line=[[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    self.img=[[UIImageView alloc] initWithFrame:CGRectMake(Default_Space, Default_Space*2, 100, 100)];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    
    self.nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.img.frame.origin.x+self.img.frame.size.width+Default_Space, Default_Space, Screen_Width-Default_Space-self.img.frame.size.width-Default_Space-Default_Space, 40)];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.textColor=APPFourColor;
    self.nameLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    
    self.infoLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.nameLabel.frame.origin.x, self.nameLabel.frame.origin.y+self.nameLabel.frame.size.height+5, self.nameLabel.frame.size.width, 20)];
    self.infoLabel.textColor=APPThreeColor;
    self.infoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.progressView = [[CustomProgress alloc] initWithFrame:CGRectMake(self.nameLabel.frame.origin.x, self.infoLabel.frame.origin.y+self.infoLabel.frame.size.height+Default_Space, self.infoLabel.frame.size.width, 15)];
    self.progressView.layer.cornerRadius=3;
    self.progressView.layer.borderColor=APPColor.CGColor;
    self.progressView.layer.borderWidth=Default_Line;
    self.progressView.layer.masksToBounds=YES;
    self.progressView.maxValue=100;
    self.progressView.bgimg.backgroundColor=[UIColor colorWithHexString:@"EF5350" andAlpha:0.5];
    self.progressView.leftimg.backgroundColor=APPColor;
    
    self.priceLabel=[[UILabel alloc]init];
    self.priceLabel.textColor=APPColor;
    self.priceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    
    self.oldPriceLabel=[[UILabel alloc]init];
    self.oldPriceLabel.textColor=APPThreeColor;
    self.oldPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_X];
    
    self.line2 = [[UIView alloc]init];
    self.line2.backgroundColor = APPThreeColor;
    [self.oldPriceLabel addSubview:self.line2];
    
    self.btn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-Default_Space-60, self.progressView.frame.size.height+self.progressView.frame.origin.y+Default_Space, 60, 20)];
    [self.btn setTitle:@"立即抢" forState:UIControlStateNormal];
    [self.btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btn setBackgroundColor:APPColor];
    self.btn.layer.cornerRadius=4;
    self.btn.layer.masksToBounds=YES;
    self.btn .titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.btn addTarget:self action:@selector(btnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.line];
    [self addSubview:self.img];
    [self addSubview:self.nameLabel];
    [self addSubview:self.infoLabel];
    [self addSubview:self.progressView];
    [self addSubview:self.priceLabel];
    [self addSubview:self.oldPriceLabel];
    [self addSubview:self.btn];
}

-(void)setModel:(TimerGoodModel *)model{
    NSLog(@"%d,%d,%f",[model.buy_count intValue],[model.max_bought intValue],[model.buy_count doubleValue]/[model.max_bought doubleValue]);
    
    _model = model;
    _ids = model.ids;
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.nameLabel.text = model.name;
    self.infoLabel.text = [NSString stringWithFormat:@"限量%@件",model.max_bought];
    [self.progressView setPresent:[model.buyper intValue]] ;
    self.priceLabel.text  = [NSString stringWithFormat:@"￥%.1lf",[model.current_price doubleValue]];
    self.oldPriceLabel.text = [NSString stringWithFormat:@"￥%@",model.origin_price];
    
    self.priceLabel.frame=CGRectMake(self.img.frame.origin.x+self.img.frame.size.width+Default_Space, self.btn.frame.origin.y, [NSString sizeWithText:self.priceLabel.text  font:self.priceLabel.font maxSize:Max_Size].width, 20);
    CGFloat width =[NSString sizeWithText:self.oldPriceLabel.text font:self.priceLabel.font maxSize:Max_Size].width;
    self.oldPriceLabel.frame=CGRectMake(self.priceLabel.frame.origin.x+self.priceLabel.frame.size.width+5, self.btn.frame.origin.y+2, width, 20);
    self.line2.frame = CGRectMake(0, 0, width, 1);
    self.line2.center = CGPointMake(self.oldPriceLabel.frame.size.width/2, self.oldPriceLabel.frame.size.height/2);
    
}


-(void)btnOnClick{
    [self.delegate nowRobWithIds:_ids];
}

+(NSString *)getID{
    return @"TimeGoodItemTC";
}
@end
