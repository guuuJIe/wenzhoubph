//
//  TimeGoodViewPageTC.m
//  519
//
//  Created by 陈 on 16/9/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TimeGoodViewPageTC.h"

@interface TimeGoodViewPageTC()
@property (nonatomic,strong)SDCycleScrollView *cycleScrollView;
@end

@implementation TimeGoodViewPageTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.cycleScrollView = [[SDCycleScrollView alloc]init];
    [self.cycleScrollView setAutoScrollTimeInterval:5];
    [self.cycleScrollView setAutoScroll:NO];
    [self.cycleScrollView setInfiniteLoop:YES];
    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@"pageControlCurrentDot"];
    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
    
    [self addSubview:self.cycleScrollView];
}

-(void)showImg:(NSArray *)imgs{
        self.cycleScrollView.delegate=self.delegate;
    [self.cycleScrollView setFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.5633)];
    [self.cycleScrollView setImageURLStringsGroup:imgs];
}

+(NSString *)getID{
    return @"TimeGoodViewPageTC";
}
@end
