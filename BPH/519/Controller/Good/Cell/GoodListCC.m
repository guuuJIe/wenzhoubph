//
//  GoodListCC.m
//  519
//
//  Created by 陈 on 16/9/17.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodListCC.h"

@interface GoodListCC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UIImageView *car;
@property(nonatomic,strong)UILabel *oldprice;
@property(nonatomic,strong)UILabel *buycountLabel;
@property(nonatomic,strong)UIView * bgview;
@property(nonatomic,strong)UIImageView * deleteimg;
@property(nonatomic,strong)UIView * deleteview;
@property(nonatomic,assign)NSInteger  index;
@end

@implementation GoodListCC

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.width)];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0, self.img.frame.size.height+self.img.frame.origin.y, self.frame.size.width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.line.frame.origin.y+self.line.frame.size.height+Default_Space, self.frame.size.width-20, 30)];
    self.title.font=[UIFont systemFontOfSize:FONT_SIZE_X];
    self.title.textColor=APPFourColor;
    self.title.lineBreakMode=NSLineBreakByTruncatingTail;
    self.title.numberOfLines=0;
    self.price=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.title.frame.origin.y+self.title.frame.size.height, [NSString sizeWithText:@"￥111.1" font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width, 20)];
    self.price.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.price.textColor=APPOneColor;
    self.oldprice = [[UILabel alloc]initWithFrame:CGRectMake(self.price.frame.size.width+self.price.frame.origin.x, self.price.frame.origin.y, [NSString sizeWithText:@"￥111" font:[UIFont systemFontOfSize:FONT_SIZE_X] maxSize:CGSizeMake(self.frame.size.width, 20)].width, 20)];
    self.oldprice.textColor=APPThreeColor;
    self.oldprice.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    
    _bgview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.oldprice.frame.size.width, 1)];
    _bgview.backgroundColor = APPThreeColor;
    _bgview.center = CGPointMake(self.oldprice.frame.size.width/2, self.oldprice.frame.size.height/2);
    
    
    self.buycountLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.oldprice.frame.size.width+self.oldprice.frame.origin.x, self.price.frame.origin.y, self.frame.size.width-self.oldprice.frame.size.width-self.oldprice.frame.origin.x-10, 20)];
    self.buycountLabel.textColor=APPFourColor;
    self.buycountLabel.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    self.buycountLabel.textAlignment = NSTextAlignmentRight;
    
    self.deleteview = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width-40, self.price.frame.origin.y, 40, self.frame.size.height-self.price.frame.origin.y)];
    UITapGestureRecognizer * imgTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(deleteCollectGoods)];
    [self.deleteview addGestureRecognizer:imgTap];
    [self addSubview:self.deleteview];
    
    
    self.deleteimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.deleteimg.center = CGPointMake(self.deleteview.frame.size.width/2, self.deleteview.frame.size.height/2-5);
    self.deleteimg.image = [UIImage imageNamed:@"delect_icon"];
    [self.deleteview addSubview:self.deleteimg];
    
    [self addSubview:self.img];
    [self addSubview:self.line];
    [self addSubview:self.title];
    [self addSubview:self.price];
    [self addSubview:self.oldprice];
    [self.oldprice addSubview:_bgview];
    [self addSubview:self.buycountLabel];
}

-(void)deleteCollectGoods{
    [self.delegate removeCollectGoodsIds:_index];
}

-(void)showDataOfTitle:(NSString *)title image:(NSString *)image withPrice:(NSString *)price  oldprice:(NSString *)oldPrice withCound:(NSString *)count buy_type:(NSString *)buy_type withDeal_score:(NSString *)score{
    self.deleteimg.hidden = YES;
    [self.img sd_setImageWithURL:[NSURL URLWithString:image]];
    self.title.text=title;
    if ([buy_type isEqualToString:@"1"]) {
        self.price.text = [NSString stringWithFormat:@"%@积分",score];
        self.oldprice.hidden = YES;
        _bgview.hidden = YES;
    }else{
        self.price.text=[NSString stringWithFormat:@"￥%@",price];
        self.oldprice.text = [NSString stringWithFormat:@"￥%@",oldPrice];
    }
    
    self.buycountLabel.text = [NSString stringWithFormat:@"%@人已购买",count];
    
    
    CGFloat priceWidth = [NSString sizeWithText:self.price.text font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    if (priceWidth>self.frame.size.width/3) {
        priceWidth = self.frame.size.width/3;
    }
    self.price.frame = CGRectMake(5,self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20);
    
    CGFloat moneyWidth = [NSString sizeWithText:self.oldprice.text font:[UIFont systemFontOfSize:FONT_SIZE_X] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    self.oldprice.frame = CGRectMake(priceWidth+Default_Space+5, self.price.frame.origin.y, moneyWidth+15, 20);
    
    self.buycountLabel.frame = CGRectMake(priceWidth+moneyWidth+Default_Space, self.price.frame.origin.y, self.frame.size.width-priceWidth-moneyWidth-Default_Space-10, 20);
    self.bgview.frame = CGRectMake(0, 0, self.oldprice.frame.size.width-10, 1);
    self.bgview.center = CGPointMake(self.oldprice.frame.size.width/2-5, self.oldprice.frame.size.height/2);
    
}

-(void)showDataForCollectVCName:(NSString *)name icon:(NSString *)icon withPrice:(NSString *)price oldprice:(NSString *)oldPrice withCound:(NSString *)count withcount:(NSInteger)index{
    _index = index;
    self.buycountLabel.hidden = YES;
    [self.img sd_setImageWithURL:[NSURL URLWithString:icon]];
    self.title.text=name;
    self.price.text=[NSString stringWithFormat:@"￥%.0lf",[price doubleValue]];
    self.oldprice.text = [NSString stringWithFormat:@"￥%.0lf",[oldPrice doubleValue]];
    
    CGFloat priceWidth = [NSString sizeWithText:self.price.text font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
//    if (priceWidth>self.frame.size.width/3) {
//        priceWidth = self.frame.size.width/3;
//    }
    self.price.frame = CGRectMake(5,self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20);
    
    CGFloat moneyWidth = [NSString sizeWithText:self.oldprice.text font:[UIFont systemFontOfSize:FONT_SIZE_X] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    self.oldprice.frame = CGRectMake(priceWidth+Default_Space+5, self.price.frame.origin.y, moneyWidth+15, 20);
    self.bgview.frame = CGRectMake(0, 0, self.oldprice.frame.size.width-10, 1);
    self.bgview.center = CGPointMake(self.oldprice.frame.size.width/2-5, self.oldprice.frame.size.height/2);
}

//guessgoods

-(void)showDataOfGuessLikeName:(NSString *)name icon:(NSString *)icon price:(NSString *)price withGuessGoodsId:(NSString *)goodsId{
    [self.img sd_setImageWithURL:[NSURL URLWithString:icon]];
    self.title.text = name;
    self.price.frame = CGRectMake(Default_Space,self.title.frame.origin.y+self.title.frame.size.height, self.frame.size.width, 20);
    self.price.text = [NSString stringWithFormat:@"￥%.02f",[price doubleValue]];
    self.bgview.hidden = YES;
    self.deleteimg.hidden = YES;
}



+(NSString *)getID{
    return @"GoodListCC";
}
@end
