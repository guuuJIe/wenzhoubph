//
//  GoodListCC.h
//  519
//
//  Created by 陈 on 16/9/17.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseCC.h"
#import "GoodInfoModel.h"
@protocol RemoveCollectGoodsDelegate <NSObject>

@optional

-(void)removeCollectGoodsIds:(NSInteger)index;

@end


@interface GoodListCC : BaseCC
@property (nonatomic,copy)NSString * goodsId;
@property(nonatomic,assign)id<RemoveCollectGoodsDelegate>delegate;


-(void)showDataForCollectVCName:(NSString *)name icon:(NSString *)icon withPrice:(NSString *)price  oldprice:(NSString *)oldPrice withCound:(NSString *)count withcount:(NSInteger)index;

-(void)showDataOfTitle:(NSString *)title image:(NSString *)image withPrice:(NSString *)price oldprice:(NSString *)oldPrice withCound:(NSString *)count buy_type:(NSString *)buy_type withDeal_score:(NSString *)score;

//goodinfo
-(void)showDataOfGuessLikeName:(NSString *)name icon:(NSString *)icon price:(NSString *)price withGuessGoodsId:(NSString *)goodsId;


+(NSString *)getID;
@end
