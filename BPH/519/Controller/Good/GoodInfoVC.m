//
//  GoodInfoVC.m
//  519
//
//  Created by 陈 on 16/9/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodInfoVC.h"
#import "GoodInfoEvaTC.h"
#import "GoodInfoMDTC.h"
#import "EvaluateVC.h"
#import "AddGoodVC.h"
#import "NewCarVC.h"
#import "MainVC.h"
#import "SDCollectionViewCell.h"
#import "GoodListCC.h"
#import "GoodInfoModel.h"
#import "PayResultVC.h"
#import "MainWebView.h"
#import "GoodstypeView.h"
#import "GoodsConponView.h"
#import "ImageBrowserView.h"
#import "VideoandPicMixShuffling.h"
#import "NewMainViewController.h"
#import "WSLTransitionAnimationTwo.h"
#import "WSLTransitionInteractiveTwo.h"
#import <WebKit/WebKit.h>
@interface GoodInfoVC()<UIScrollViewDelegate,SDCycleScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIWebViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,RemoveCollectGoodsDelegate,UINavigationControllerDelegate>
{
    NSString * _beginTime;
}
@property(nonatomic,strong)UIButton *backBtn;
@property (nonatomic,strong)UIButton *shareImgView;

@property(nonatomic,strong)UIScrollView * scrollView;
@property(nonatomic,strong)UIScrollView * oneScrollView;
@property(nonatomic,strong)UIView *moveMoreView;
@property(nonatomic,strong)WKWebView * twoWebView;
@property(nonatomic,strong)UIView *nowView;


@property(nonatomic,strong)SDCollectionViewCell * sdCollectCell;

@property(nonatomic,strong)UIView *priceView;
@property(nonatomic,strong)UILabel *priceLabel;
@property(nonatomic,strong)UILabel *oldPriceLabel;
@property(nonatomic,strong)UILabel *soldLabel;
@property(nonatomic,strong)CountDown *countDown;
@property(nonatomic,strong)UILabel *timeLabel;

@property(nonatomic,strong)UIView *goodInfoView;
@property (nonatomic,strong)UILabel *goodNameLabel;
@property (nonatomic,strong)UILabel *goodInfoLabel;

//@property(nonatomic,strong)UIView *goodTagView;

@property(nonatomic,strong)UIView *evaTagView;
@property(nonatomic,strong)UILabel *evaTagTitleLabel;
@property(nonatomic,strong)UIView *evaTagTagView;

@property(nonatomic,strong)UITableView *evaTableView;
@property(nonatomic,strong)NSMutableArray *evaData;

@property(nonatomic,strong)UIView *evaMoreView;
@property(nonatomic,strong)UILabel *evaMoreLabel;

@property(nonatomic,strong)UIView *payView;
@property(nonatomic,strong)UIView *kfView;
@property(nonatomic,strong)UIView *scView;
@property(nonatomic,strong)UIView *gwcView;
@property(nonatomic,strong)UIButton *jrgwcLabel;
@property(nonatomic,strong)UIButton *ljgmLabel;
@property(nonatomic,strong)UIImageView *scIcon;
@property(nonatomic,strong)UILabel *scTitle;

@property (nonatomic,strong)NSMutableArray * headImageMuArr;
@property (nonatomic,copy)NSString * priceStr;
@property (nonatomic,copy)NSString * oldPriceStr;
@property (nonatomic,copy)NSString * buyStr;
@property (nonatomic,strong)NSMutableArray * tagsMutableArr;
@property (nonatomic,strong)NSMutableArray * endAddressMuArr;
@property (nonatomic,strong)NSMutableArray * phoneMuArr;
@property (nonatomic,strong)NSMutableArray * headMuArray;

@property (nonatomic,strong)NSArray * goodsInfoMessageArr;
@property (nonatomic,copy)NSString * iconUrlStr;
@property (nonatomic,strong)NSMutableArray * dealNameMuarray;
@property (nonatomic,strong)NSMutableArray * attrListNameMuArray;
@property (nonatomic,strong)NSMutableArray * attrListIdMuArray;
@property (nonatomic,strong)NSMutableArray * attrListPriceArray;
@property (nonatomic,strong)UILabel * evaLabel;
@property (nonatomic,strong)UILabel * likeLabel;

@property (nonatomic,strong)NSArray * typeArr;
//new
@property(nonatomic,copy)NSString * unitPrice;
@property(nonatomic,assign)int nowTime;

//share
@property(nonatomic,copy)NSString * shareUrl;
@property(nonatomic,copy)NSString * shareImgUrl;

//guess like
@property(nonatomic,strong)UIView * guessView;
@property(nonatomic,strong)NSMutableArray * guessIconMuArr;
@property(nonatomic,strong)NSMutableArray * guessNameMuArr;
@property(nonatomic,strong)NSMutableArray * guessIdMuArr;
@property(nonatomic,strong)NSMutableArray * guessPriceMuArr;
@property(nonatomic,strong)UICollectionView * collectionView;
@property(nonatomic,assign)CGFloat collectionWidth;
@property(nonatomic,assign)CGFloat collectionHeight;
@property(nonatomic,copy)NSString * goodnum;
@property(nonatomic,copy)NSString * goodscore;

//时间
@property(nonatomic,copy)NSString * timeStatus;

@property(nonatomic,strong)GoodstypeView *typeView;

//优惠券
@property (nonatomic, strong)GoodsConponView *conponImg;
//测试408
@property (nonatomic, strong)NSDictionary *conponId;

@property (nonatomic) NSArray *imagesArr;

//视频图片轮播
@property (nonatomic) VideoandPicMixShuffling *videoView;

//动画过渡转场
@property (nonatomic, strong) WSLTransitionAnimationTwo * transitionAnimation;

//手势过渡转场
@property (nonatomic, strong) WSLTransitionInteractiveTwo * transitionInteractive;
@end

@implementation GoodInfoVC

- (instancetype)init{
    if (self == [super init]) {
        self.transitionAnimation.transitionType = WSLTransitionTwoTypePresent;
//        self.transitionInteractive.interactiveType = WSLTransitionTwoTypePop;
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商品详情";
    
    
    [self initView];
    [self initBar];
    [self requestDataOfGoodsInfo];
    
}
-(void)initBar{
//    [super initBar];
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton * shareBtn = [[UIButton alloc]initWithFrame:contentView.bounds];
    [shareBtn setImage:[UIImage imageNamed:@"share_white_icon"] forState:(UIControlStateNormal)];
    [shareBtn addTarget:self action:@selector(shareGood) forControlEvents:(UIControlEventTouchUpInside)];
    [contentView addSubview:shareBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
    if (self.navigationController.delegate == self) {
        self.navigationController.delegate = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.videoView.player.viewControllerDisappear = false;

}

- (void)viewDidDisappear:(BOOL)animated{
    self.navigationController.delegate = nil;
}

- (BOOL)shouldAutorotate {
    return self.videoView.player.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (self.videoView.player.isFullScreen) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskPortrait;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.videoView.player.isFullScreen) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    /// 如果只是支持iOS9+ 那直接return NO即可，这里为了适配iOS8
    return NO;
}


-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.videoView.player.viewControllerDisappear = true;
 
}
-(void)requestDataOfGoodsInfo{
    
    NSString * urlString = [NSString stringWithFormat:@"%@ctl=deal&id=%@",FONPort,self.goodsId];
    if ([self.isScroes isEqualToString:@"1"]) {
        urlString = [NSString stringWithFormat:@"%@&buy_type=1",urlString];
    }
    [MBProgressHUD showHUD];
    [RequestData requestDataOfUrl:urlString success:^(NSDictionary *dic) {
        UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectZero];
        NSString * oldAgent = [webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        if ([dic[@"status"] isEqual:@1]) {
            self.iconUrlStr = dic[@"icon"];
            self.dealNameMuarray = [NSMutableArray new];
            self.attrListIdMuArray = [NSMutableArray new];
            self.attrListPriceArray = [NSMutableArray new];
            self.attrListNameMuArray = [NSMutableArray new];
            self.guessIconMuArr = [NSMutableArray new];
            self.guessIdMuArr = [NSMutableArray new];
            self.guessNameMuArr = [NSMutableArray new];
            self.guessPriceMuArr = [NSMutableArray new];
            self.typeArr = dic[@"ref_items_list"];
            //商品优惠类型
            self.timeStatus = [NSString stringWithFormat:@"%@",dic[@"time_status"]];
            
            //商品类型，0非积分商品，1积分商品
            self.goodnum = dic[@"buy_type"];
            //商品积分
            self.goodscore = dic[@"return_score_show"];
            
            for (NSDictionary * subDic in dic[@"deal_attr"]) {
                [self.dealNameMuarray addObject:subDic[@"name"]];
                
                NSMutableArray * idMuarray = [NSMutableArray new];
                NSMutableArray * nameMuarray = [NSMutableArray new];
                NSMutableArray * priceMuarray = [NSMutableArray new];
                for (NSDictionary * thirdDic in subDic[@"attr_list"]) {
                    [idMuarray addObject:thirdDic[@"id"]];
                    [nameMuarray addObject:thirdDic[@"name" ]];
                    [priceMuarray addObject:thirdDic[@"price"]];
                }
                [self.attrListIdMuArray addObject:idMuarray];
                [self.attrListNameMuArray addObject:nameMuarray];
                [self.attrListPriceArray addObject:priceMuarray];
            }
            //headimage
            self.cycleScrollView.imageURLStringsGroup = dic[@"images"];
//            [self.videoView setWithIsVideo:TSDETAILTYPEVIDEO andDataArray:self.imgArray];
            self.imagesArr = dic[@"images"];
            //price
            self.priceStr = dic[@"current_price"];
            self.oldPriceStr = dic[@"origin_price"];
            self.conponId = dic[@"coupon"];
            self.buyStr = [NSString stringWithFormat:@"%@已购买",dic[@"buy_count"]];
            self.soldLabel.text = self.buyStr;
            self.goodNameLabel.text = dic[@"name"];
            self.goodInfoLabel.text = dic[@"brief"];
            
            self->_beginTime = [NSString stringWithFormat:@"%@",dic[@"begin_time"]];
            self.nowTime = [self->_beginTime intValue] - [dic[@"now_time"]intValue];
            if (self.nowTime < 0) {
                self.nowTime = [dic[@"end_time"] intValue] - [dic[@"now_time"] intValue];
                if (self.nowTime < 0) {
                    self.nowTime = 0;
                }
            }
            [self startLongLongStartStamp:self.nowTime withBeginTime:[dic[@"begin_time"] intValue]];
            
            self.tagsMutableArr = [NSMutableArray new];
                for (NSDictionary * tagsDic in dic[@"deal_tags"]) {
                    [self.tagsMutableArr addObject:tagsDic[@"v"]];
                }
            self.evaData = [NSMutableArray new];
            for (NSDictionary * subdic in dic[@"dp_list"]) {
                EvaEntity *entity=[[EvaEntity alloc]init];
                entity.avatar=subdic[@"user_avatar"];
                if ([subdic[@"user_name"] isEqual:[NSNull null]]) {
                    entity.user_name=@"test";
                }else{
                    entity.user_name=subdic[@"user_name"];
                }
                entity.point=[NSString stringWithFormat:@"%@",subdic[@"point"]];
                entity.create_time=subdic[@"create_time"];
                entity.content=subdic[@"content"];
                [self.evaData addObject:entity];
            }
            for (NSDictionary * subdic in dic[@"guess_goods"]) {
                [self.guessIconMuArr addObject:subdic[@"icon"]];
                [self.guessNameMuArr addObject:subdic[@"name"]];
                [self.guessIdMuArr addObject:subdic[@"id"]];
                [self.guessPriceMuArr addObject:subdic[@"current_price"]];
            }
            
            self.endAddressMuArr = [NSMutableArray new];
            self.phoneMuArr = [NSMutableArray new];
                for (NSDictionary * supplierDic in dic[@"supplier_location_list"]) {
                    [self.endAddressMuArr addObject:supplierDic[@"address"]];
                    [self.phoneMuArr addObject:supplierDic[@"tel"]];
                }
            NSString * webString = dic[@"description"];
            if (webString) {
                [self.twoWebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",webString] baseURL:nil];
            }
            NSLog(@"%@",dic[@"is_collect"]);
            if([dic[@"is_collect"] isEqual:@1]){
                self.scIcon.image = [UIImage imageNamed:@"yesshouchang"];
                self.scTitle.text = @"已收藏";
            }else{
                self.scIcon.image = [UIImage imageNamed:@"wodeshoucang_icon"];
                self.scTitle.text = @"收藏";
            }
           
            self.shareImgUrl = dic[@"icon"];
            self.shareUrl = dic[@"share_url"];
            
            [self showData];
           
            [MBProgressHUD dissmiss];
        }else{

            [MBProgressHUD dissmiss];
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
            
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        
        NSLog(@"%@",error);
    }];
}

-(void)popVC{
    [_countDown destoryTimer];
    if ([self.runjump isEqualToString:@"runjump"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"runjumpmain" object:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)initView{
    //滑动view
    //获取手机型号

    CGFloat navigationHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    if (statusHeight == 44) {
        //iphone X
        self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_SelfHeight-44-64-44)];
    }else{
        self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_SelfHeight-40-64)];
    }
    
    self.scrollView.delegate=self;
    self.scrollView.bounces=NO;
    self.scrollView.decelerationRate=0.0;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.tag=0;
    
    //拖动图文上部分
    self.oneScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
    self.oneScrollView.backgroundColor=APPBGColor;
    self.oneScrollView.bounces=NO;
    self.oneScrollView.decelerationRate=1.0;
    self.oneScrollView.showsVerticalScrollIndicator=NO;
    

    //拖动图文下部分
    self.twoWebView=[[WKWebView alloc]initWithFrame:CGRectMake(0, self.oneScrollView.frame.size.height,Screen_Width,  Screen_Height-64-40)];
    self.twoWebView.backgroundColor=APPBGColor;
    self.twoWebView.scrollView.bounces=NO;
    [self.twoWebView sizeToFit];
    self.twoWebView.scrollView.showsVerticalScrollIndicator=NO;
    self.twoWebView.scrollView.showsHorizontalScrollIndicator = NO;
    [self.scrollView addSubview:self.oneScrollView];
    [self.scrollView addSubview:self.twoWebView];
    
    
    self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width, self.oneScrollView.frame.size.height+self.twoWebView.frame.size.height);
    self.nowView=self.oneScrollView;
    
    //good view
//    self.videoView = [[VideoandPicMixShuffling alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.8)];
//    self.cycleScrollView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.8)];
//    [self.cycleScrollView setAutoScrollTimeInterval:5];
//    self.cycleScrollView.backgroundColor = [UIColor whiteColor];
//    [self.cycleScrollView setAutoScroll:NO];
//    [self.cycleScrollView setInfiniteLoop:YES];
//    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@""];
//    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
//    self.cycleScrollView.delegate=self;
  


    
    self.priceView=[[UIView alloc]initWithFrame:CGRectMake(0, self.cycleScrollView.frame.size.height, Screen_Width, 70)];
    self.priceView.backgroundColor=[UIColor whiteColor];
    
    self.priceLabel=[[UILabel alloc]init];
    self.priceLabel.textColor=APPColor;
    self.priceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_XL];
    
    self.oldPriceLabel=[[UILabel alloc]init];
    self.oldPriceLabel.textColor=APPThreeColor;
    self.oldPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.soldLabel=[[UILabel alloc]init];
    self.soldLabel.textColor=APPThreeColor;
    self.soldLabel.textAlignment = NSTextAlignmentRight;
    self.soldLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
   
    self.timeLabel=[[UILabel alloc]init];
    self.timeLabel.textColor=APPThreeColor;
    self.timeLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.countDown=[[CountDown alloc]init];
    
    [self.priceView addSubview:self.priceLabel];
    [self.priceView addSubview:self.oldPriceLabel];
    [self.priceView addSubview:self.soldLabel];
    [self.priceView addSubview:self.timeLabel];
    self.priceView.backgroundColor = [UIColor whiteColor];
    //商品类型
//    WeakSelf(self);
    self.typeView = [[GoodstypeView alloc] initWithFrame:CGRectMake(0, self.priceView.frame.origin.y+self.priceView.frame.size.height+Default_Line, Screen_Width, 50)];
    self.typeView.backgroundColor = [UIColor whiteColor];
    
//    self.typeView.clickBlock = ^(NSString *title) {
////        weakself.goodsId = title;
////        [weakself requestDataOfGoodsInfo];
//    };
    
    self.conponImg = [[GoodsConponView alloc] initWithFrame:CGRectMake(0, self.typeView.frame.origin.y+self.typeView.frame.size.height+Default_Line, Screen_Width, 5)];
    
    
    
    
    self.goodInfoView=[[UIView alloc]initWithFrame:CGRectMake(0, self.conponImg.frame.origin.y+self.conponImg.frame.size.height+Default_Line, Screen_Width, 100)];
    self.goodInfoView.backgroundColor=[UIColor whiteColor];
    
    self.goodNameLabel=[[UILabel alloc]init];
    self.goodNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.goodNameLabel.numberOfLines = 2;
    self.goodNameLabel.font=[UIFont boldSystemFontOfSize:FONT_SIZE_L];
    self.goodNameLabel.textColor=APPFourColor;
    
    
    self.goodInfoLabel=[[UILabel alloc]init];
    self.goodInfoLabel.textColor=APP9EColor;
    self.goodInfoLabel.font=[UIFont systemFontOfSize:12];
    self.goodInfoLabel.numberOfLines = 0;
    
    [self.goodInfoView addSubview:self.goodNameLabel];
    [self.goodInfoView addSubview:self.goodInfoLabel];

    
    self.evaTagView=[[UIView alloc] init];
    self.evaTagView.backgroundColor=[UIColor whiteColor];
    
    self.evaTagTitleLabel=[[UILabel alloc]init];
    self.evaTagTitleLabel.textColor=APPFourColor;
    self.evaTagTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    

    self.evaLabel = [[UILabel alloc]init];
    self.evaLabel.text = @"  评价";
    self.evaLabel.backgroundColor = [UIColor whiteColor];
    self.evaLabel.textColor = APPFourColor;
    self.evaLabel.textAlignment = NSTextAlignmentLeft;
    self.evaLabel.font = [UIFont boldSystemFontOfSize:FONT_SIZE_L];
    
    
    
    self.evaTableView=[[UITableView alloc]init];
    [self.evaTableView registerClass:[GoodInfoEvaTC class] forCellReuseIdentifier:[GoodInfoEvaTC getID]];
    self.evaTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.evaTableView.delegate=self;
    self.evaTableView.dataSource=self;
    self.evaTableView.showsVerticalScrollIndicator=NO;
    self.evaTableView.scrollEnabled=NO;
    
    self.evaMoreView=[[UIView alloc]init];
    self.evaMoreView.backgroundColor=[UIColor whiteColor];
    
    self.evaMoreLabel=[[UILabel alloc]init];
    self.evaMoreLabel.text=@"查看全部评价";
    self.evaMoreLabel.layer.cornerRadius=5;
    self.evaMoreLabel.layer.borderColor=APPColor.CGColor;
    self.evaMoreLabel.layer.borderWidth=Default_Line;
    self.evaMoreLabel.layer.masksToBounds=YES;
    self.evaMoreLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.evaMoreLabel.textAlignment=NSTextAlignmentCenter;
    self.evaMoreLabel.textColor=APPColor;
    self.evaMoreLabel.backgroundColor=[UIColor whiteColor];
    
    [self.evaMoreView addSubview:self.evaMoreLabel];
    
    
    //guess like
    self.guessView = [[UIView alloc]init];
    self.guessView.backgroundColor = APPBGColor;
    UILabel * guessLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 39)];
    guessLabel.text = @"  ";
    guessLabel.textColor = APPFourColor;
    guessLabel.backgroundColor = [UIColor whiteColor];
    self.likeLabel = guessLabel;
    guessLabel.font = [UIFont boldSystemFontOfSize:FONT_SIZE_L];
    [self.guessView addSubview:guessLabel];
    
    self.collectionWidth=(Screen_Width-5*4)/3;
    self.collectionHeight=self.collectionWidth+60;
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(self.collectionWidth,self.collectionHeight);
    layout.minimumInteritemSpacing =0;
    layout.minimumLineSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0,40,Screen_Width,400) collectionViewLayout:layout];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self.collectionView registerClass:[GoodListCC class] forCellWithReuseIdentifier:[GoodListCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.showsVerticalScrollIndicator=NO;
    self.collectionView.dataSource=self;
    [self.guessView addSubview:self.collectionView];

    
    self.moveMoreView=[[UIView alloc]init];
    self.moveMoreView.backgroundColor=APPBGColor;
    UILabel *moreLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.oneScrollView.frame.size.width, 50)  ];
    moreLabel.text=@"继续拖动查看图文详情";
    moreLabel.backgroundColor=[UIColor clearColor];
    moreLabel.textColor=APPFourColor;
    moreLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    moreLabel.textAlignment=NSTextAlignmentCenter;
    [self.moveMoreView addSubview:moreLabel];
    [self.oneScrollView addSubview:self.moveMoreView];
    
    
    [self.oneScrollView addSubview:self.cycleScrollView];
    [self.oneScrollView addSubview:self.priceView];
    [self.oneScrollView addSubview:self.typeView];
    [self.oneScrollView addSubview:self.conponImg];
    [self.oneScrollView addSubview:self.goodInfoView];
    [self.oneScrollView addSubview:self.evaLabel];
    [self.oneScrollView addSubview:self.evaTagView];
    [self.oneScrollView addSubview:self.evaTableView];
    [self.oneScrollView addSubview:self.evaMoreView];
    [self.oneScrollView addSubview:self.guessView];
   
    
    //支付view
    self.payView=[[UIView alloc]initWithFrame:CGRectMake(0, self.scrollView.frame.size.height+self.scrollView.frame.origin.y, Screen_Width, 64)];
    self.payView.backgroundColor=[UIColor whiteColor];
    
    self.kfView=[[UIView alloc]initWithFrame:CGRectMake(0, Default_Line, 44, 44-Default_Line)];
    self.kfView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer * kfTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(kfGoods)];
    [self.kfView addGestureRecognizer:kfTap];
    
    self.scView=[[UIView alloc]initWithFrame:CGRectMake(self.kfView.frame.size.width+Default_Line, Default_Line, 44, 44-Default_Line)];
    self.scView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer * scTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(scGoods)];
    [self.scView addGestureRecognizer:scTap];
    
    self.gwcView=[[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.scView.frame)+Default_Line, Default_Line, 44, self.scView.frame.size.height)];;
    self.gwcView.backgroundColor=[UIColor whiteColor];
    
    self.jrgwcLabel=[[UIButton alloc]initWithFrame:CGRectMake(self.gwcView.frame.origin.x+self.gwcView.frame.size.width+Default_Line, 0, (Screen_Width-44*3)/2, self.scView.frame.size.height)];
    self.jrgwcLabel.backgroundColor=APP33Color;
    [self.jrgwcLabel setTitle:@"加入购物车" forState:UIControlStateNormal];
    [self.jrgwcLabel setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    self.jrgwcLabel.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    [self.jrgwcLabel addTarget:self action:@selector(jrgwcLabelOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.ljgmLabel=[[UIButton alloc]initWithFrame:CGRectMake(self.jrgwcLabel.frame.origin.x+self.jrgwcLabel.frame.size.width, 0,(Screen_Width-44*3)/2, self.scView.frame.size.height)];
    self.ljgmLabel.backgroundColor=APPColor;
    [self.ljgmLabel setTitle:@"立即购买" forState:UIControlStateNormal];
    [self.ljgmLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.ljgmLabel.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
        [self.ljgmLabel addTarget:self action:@selector(ljgmLabelOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIImageView * kfimageV=[[UIImageView alloc]initWithFrame:CGRectMake((self.kfView.frame.size.width-20)/2, 2, 20, 20)];
    kfimageV.image=[UIImage imageNamed:@"1的副本"];
    UILabel * kfLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 22, self.scView.frame.size.width, 20)];
    kfLabel.textAlignment=NSTextAlignmentCenter;
    kfLabel.textColor=APPFourColor;
    kfLabel.text=@"客服";
    kfLabel.font=[UIFont systemFontOfSize:FONT_SIZE_X];
    [self.kfView addSubview:kfimageV];
    [self.kfView addSubview:kfLabel];
    
    
    _scIcon=[[UIImageView alloc]initWithFrame:CGRectMake((self.scView.frame.size.width-20)/2, 2, 20, 20)];
    _scIcon.image=[UIImage imageNamed:@"wodeshoucang_icon"];
    _scTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 22, self.scView.frame.size.width, 20)];
    _scTitle.textAlignment=NSTextAlignmentCenter;
    _scTitle.textColor=APPFourColor;
    _scTitle.text=@"收藏";
    _scTitle.font=[UIFont systemFontOfSize:FONT_SIZE_X];
    [self.scView addSubview:_scIcon];
    [self.scView addSubview:_scTitle];
    
    UIImageView *gwcIcon=[[UIImageView alloc]initWithFrame:CGRectMake((self.gwcView.frame.size.width-20)/2, 2, 20, 20)];
    gwcIcon.image=[UIImage imageNamed:@"gouwuche_black_icon"];
    UILabel *gwcTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 22, self.gwcView.frame.size.width, 20)];
    gwcTitle.textAlignment=NSTextAlignmentCenter;
    gwcTitle.textColor=APPFourColor;
    gwcTitle.text=@"购物车";
    gwcTitle.font=[UIFont systemFontOfSize:FONT_SIZE_X];
    UITapGestureRecognizer * gwcTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gwcTap)];
    [self.gwcView addGestureRecognizer:gwcTap];
    [self.gwcView addSubview:gwcIcon];
    [self.gwcView addSubview:gwcTitle];
    
    [self.payView addSubview:self.kfView];
    [self.payView addSubview:self.scView];
    [self.payView addSubview:self.gwcView];
    [self.payView addSubview:self.jrgwcLabel];
    [self.payView addSubview:self.ljgmLabel];
    
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.payView];
}


-(void)gwcTap{
    NewCarVC * vc = [NewCarVC new];
    vc.haveBackBtn=YES;
    [self tabHidePushVC:vc];
}

-(void)loadData{
    
    [self requestDataOfGoodsInfo];
}



-(void)showData{
 
    NSString * goodPrice;
    NSMutableAttributedString *priceStr;
    if ([_goodnum isEqualToString:@"1"]) {
        goodPrice = [NSString stringWithFormat:@"%@积分",_goodscore];
        priceStr = [[NSMutableAttributedString alloc] initWithString:goodPrice];
        self.jrgwcLabel.hidden = YES;
        self.ljgmLabel.frame = CGRectMake(self.gwcView.frame.origin.x+self.gwcView.frame.size.width+Default_Line, self.scView.frame.origin.y, Screen_Width/3*2, self.scView.frame.size.height);
        //价格布局
        self.priceLabel.frame=CGRectMake(Default_Space, 10, [NSString sizeWithText:priceStr.string font:self.priceLabel.font maxSize:Max_Size].width+Default_Space, 20);
        self.priceLabel.attributedText=priceStr;
    }else{
        if ([self.conponId isEqual:[NSNull null]]) {
            goodPrice = [NSString stringWithFormat:@"￥%.2lf",[self.priceStr doubleValue]];
            priceStr = [[NSMutableAttributedString alloc] initWithString:goodPrice];
            [priceStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FONT_SIZE_M] range:NSMakeRange(0, 1)];
            [priceStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FONT_SIZE_XL] range:NSMakeRange(1, priceStr.length-1)];
        }else{
            goodPrice = [NSString stringWithFormat:@"券后￥%.2lf",[self.priceStr doubleValue]];
             priceStr = [[NSMutableAttributedString alloc] initWithString:goodPrice];
        }
        
      
        
        NSMutableAttributedString *oldPriceStr=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",self.oldPriceStr] attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName : @(NSUnderlineStyleSingle)}];
        //价格布局
        self.priceLabel.frame=CGRectMake(Default_Space, 10, [NSString sizeWithText:priceStr.string font:self.priceLabel.font maxSize:Max_Size].width+Default_Space*2, 20);
        self.priceLabel.attributedText=priceStr;
        
        self.oldPriceLabel.frame = CGRectMake(CGRectGetMaxX(self.priceLabel.frame), self.priceLabel.frame.origin.y,[NSString sizeWithText:oldPriceStr.string font:self.oldPriceLabel.font maxSize:Max_Size].width, self.priceLabel.frame.size.height);
        self.oldPriceLabel.attributedText=oldPriceStr;
        
    }
    
    
   
    
    if ([_timeStatus isEqualToString:@"2"] && ![_beginTime isEqualToString:@"0"]) {
        UILabel * actionEndLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.gwcView.frame.origin.x+self.gwcView.frame.size.width+Default_Line, self.scView.frame.origin.y, Screen_Width/3*2, CGRectGetMaxY(self.ljgmLabel.frame))];
        actionEndLabel.backgroundColor =APPThreeColor;
        actionEndLabel.textColor = [UIColor whiteColor];
        actionEndLabel.textAlignment = NSTextAlignmentCenter;
        actionEndLabel.text = @"活动已结束";
        [self.payView addSubview:actionEndLabel];
        [self.jrgwcLabel setUserInteractionEnabled:NO];
        [self.ljgmLabel setUserInteractionEnabled:NO];
    }
    
    self.soldLabel.frame = CGRectMake(Screen_Width/2, self.priceLabel.frame.origin.y, Screen_Width/2-10, self.priceLabel.frame.size.height);
    
    //计时器
    if (_nowTime == 0) {
        self.timeLabel.frame =CGRectMake(Default_Space, self.priceLabel.frame.origin.y+self.priceLabel.frame.size.height, self.priceView.frame.size.width, 0);
        self.priceView.frame=CGRectMake(0, self.cycleScrollView.frame.size.height, Screen_Width, 40);
        
        
    }else{
        self.timeLabel.frame=CGRectMake(Default_Space, self.priceLabel.frame.origin.y+self.priceLabel.frame.size.height+Default_Space, self.priceView.frame.size.width, 20);
        self.priceView.frame=CGRectMake(0, self.cycleScrollView.frame.size.height, Screen_Width, 70);
    }
    
    
    
    BLog(@"高度呢 ======%f",self.typeView.origin.y+self.typeView.frame.size.height+self.priceView.frame.size.height+self.priceView.origin.y);
    
    if ([self.conponId isEqual:[NSNull null]]) {
        self.conponImg.frame = CGRectMake(0, 0, 0, 0);
        [self.ljgmLabel setTitle:@"立即购买" forState:0];
    }else{
        [self.conponImg setupData:self.conponId];
        self.conponImg.frame = CGRectMake(0, self.priceView.frame.size.height+self.priceView.origin.y, Screen_Width, 90);
        [self.ljgmLabel setTitle:@"领券购物" forState:0];
    }
    
    WeakSelf(self);
    self.conponImg.getClick = ^{
        StrongSelf(self);
        if (EMAIL) {
            NSString *reqUrl = [NSString stringWithFormat:@"%@ctl=yingxiao&act=youhuiquan&quan_id=%@",FONPort,self.conponId[@"id"]];
            [RequestData requestDataOfUrl:reqUrl success:^(NSDictionary *dic) {
                if ([dic[@"status"] isEqual:@1]) {
                    self.conponImg.userInteractionEnabled = false;
                    [self.conponImg setUIwithImageName:@"goodsConponSelected" andColor:@"#969696"];
                }else{
                    
                }
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
                
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
            }];
        }else{
            [self tabHidePushVC:[NewLoginVC new]];
        }
    };
    
    //商品信息
    CGFloat height = [self getHeightBytitle:_goodInfoLabel.text withSize:12];
    self.goodNameLabel.frame=CGRectMake(Default_Space, 10,self.goodInfoView.frame.size.width-20, [self getHeightBytitle:self.goodNameLabel.text withSize:16] );
    self.goodInfoLabel.frame=CGRectMake(Default_Space, self.goodNameLabel.frame.size.height+self.goodNameLabel.frame.origin.y,self.goodInfoView.frame.size.width-20, height+20);
    
    if (_goodInfoLabel.text.length>0) {
        self.goodInfoView.frame=CGRectMake(0, self.priceView.frame.origin.y+self.priceView.frame.size.height+Default_Line+self.conponImg.frame.size.height, Screen_Width, height+30+[self getHeightBytitle:self.goodNameLabel.text withSize:16]);
    }else{
        self.goodInfoView.frame=CGRectMake(0, self.priceView.frame.origin.y+self.priceView.frame.size.height+Default_Line+self.conponImg.frame.size.height, Screen_Width, [self getHeightBytitle:self.goodNameLabel.text withSize:16]+10);
    }
    
    CGFloat space = 0.0;
    if (self.typeArr.count>0) {
        
        
        
        
            //    self.typeView.goodsTypesArr = @[@{@"name":@"买一送一日日日热舞"},@{@"name":@"买一送一"},@{@"name":@"买一送一"},@{@"name":@"买一送一"},@{@"name":@"买一送一"},@{@"name":@"买一送一日日日热舞"}];
        self.typeView.goodsTypesArr = self.typeArr;
        self.typeView.frame = CGRectMake(0, self.goodInfoView.frame.size.height+self.goodInfoView.origin.y+Default_Space, Screen_Width, (self.typeView.index+1)*50+40);
        WeakSelf(self);
        self.typeView.clickBlock = ^(NSString *title) {
            weakself.goodsId = title;
            BLog(@"规格%@",title);
            [weakself requestDataOfGoodsInfo];
        };
        space = 10.0;
        self.typeView.goodsid = [self.goodsId integerValue];
        
    }else{
        self.typeView.frame = CGRectMake(0, 0, 0, 0);
        space = 0.0;
    }
    
    
    //评价
    self.evaLabel.frame = CGRectMake(0, self.typeView.frame.size.height+self.goodInfoView.frame.origin.y+self.goodInfoView.frame.size.height+Default_Space+space, Screen_Width,40);
    
    CGFloat evaTableViewHeight=0;
    for (int i=0;i<self.evaData.count;i++) {
        evaTableViewHeight=evaTableViewHeight+[self getEvaItemHeight:i];
    }
    
    self.evaTableView.frame=CGRectMake(0, self.evaLabel.frame.size.height+self.evaLabel.frame.origin.y+1, Screen_Width, evaTableViewHeight);
    [self.evaTableView reloadData];
    
    self.evaMoreView.frame=CGRectMake(0, self.evaTableView.frame.size.height+self.evaTableView.frame.origin.y, Screen_Width, 50);
    self.evaMoreLabel.frame=CGRectMake((self.evaMoreView.frame.size.width-([self.evaMoreLabel.text textSize:self.evaMoreLabel.font withWidth:Screen_Width].width+Default_Space*2))/2, Default_Space, [self.evaMoreLabel.text textSize:self.evaMoreLabel.font withWidth:Screen_Width].width+Default_Space*2, 30);
    self.evaMoreLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *evaMoreLabelTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(evaMoreLabelOnClick)];
    [self.evaMoreLabel addGestureRecognizer:evaMoreLabelTap];
   
    self.likeLabel.text = @"  猜你喜欢";
    self.guessView.frame = CGRectMake(0, self.evaMoreView.frame.size.height+self.evaMoreView.frame.origin.y+Default_Space, Screen_Width, self.collectionHeight*2+60);
    self.collectionView.frame = CGRectMake(0, 40, Screen_Width, self.collectionHeight*2+20) ;
    [self.collectionView reloadData];
    
    
    self.moveMoreView.frame=CGRectMake(0, self.guessView.frame.size.height+self.guessView.frame.origin.y, self.scrollView.frame.size.width, 50);
    self.oneScrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width,self.moveMoreView.frame.origin.y+self.self.moveMoreView.frame.size.height);
}


#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.guessIconMuArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    GoodListCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[GoodListCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[GoodListCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    cell.delegate = self;
    [cell showDataOfGuessLikeName:self.guessNameMuArr[indexPath.row]
                                      icon:self.guessIconMuArr[indexPath.row]
                                     price:self.guessPriceMuArr[indexPath.row]
                          withGuessGoodsId:self.guessIdMuArr[indexPath.row]];
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionWidth,self.collectionHeight);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,0,0,0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodInfoVC * goodsInfo = [GoodInfoVC new];
    goodsInfo.goodsId = [NSString stringWithFormat:@"%@",self.guessIdMuArr[indexPath.row]];
    
    [self tabHidePushVC:goodsInfo];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



#pragma mark scrollview delegate
//滑动结束时候 判断 所属view 显示
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if(scrollView.tag!=0){
        return;
    }
    
    if(offsetY==0){
        return;
    }
    
    if(self.nowView==self.oneScrollView){
        offsetY=offsetY+self.oneScrollView.frame.size.height;
    }
    
    if(offsetY>=self.scrollView.frame.size.height){
        [self scrollViewShow:self.twoWebView is:YES];
    }else{
        [self scrollViewShow:self.oneScrollView is:YES];
    }
}

//惯性滑动开始 的时候 滑动到指定显示的view 避免滑动过程 view 也滑动 体验不好
-(void)scrollViewWillBeginDecelerating: (UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if(scrollView.tag!=0){
        return;
    }
    
    if(offsetY==0){
        return;
    }
    
    [self scrollViewShow:self.nowView is:NO];
}

#pragma mark 滑动到那个view
-(void)scrollViewShow:(UIView *)view is:(BOOL)is{
    if(is&&self.nowView==view){
        return;
    }
    
    [self.scrollView setContentOffset:CGPointMake(0, view.frame.origin.y) animated:YES];
    self.nowView=view;
}

#pragma mark 轮播 delegate
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    ImageBrowserView *BrowserView = [[ImageBrowserView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height) ImageArr:self.imagesArr andTag:index];
    [BrowserView show];
}

#pragma mark 倒计时方法
-(void)refreshUIDay:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second{
    if ([_timeStatus isEqualToString:@"1"]) {
        self.timeLabel.text=[NSString stringWithFormat:@"%li天%li时%li分%li秒后结束",(long)day,(long)hour,(long)minute,(long)second];
    }
    if ([_timeStatus isEqualToString:@"2"]) {
        self.timeLabel.text = @"活动已结束";
        
        
    }
    if ([_timeStatus isEqualToString:@"3"]) {
        self.timeLabel.text=[NSString stringWithFormat:@"%li天%li时%li分%li秒后开始",(long)day,(long)hour,(long)minute,(long)second];
    }
}
//此方法用两个时间戳做参数进行倒计时
-(void)startLongLongStartStamp:(int)strtLL withBeginTime:(int)beginTime{
    if (beginTime == 0) {
        self.timeLabel.frame =CGRectMake(Default_Space, self.priceLabel.frame.origin.y+self.priceLabel.frame.size.height+10, self.priceView.frame.size.width, 0);
    }else{
        [self.countDown countDownWithStratTimeStamp:strtLL  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
            [self refreshUIDay:day hour:hour minute:minute second:second];
        }];
    }
    
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView==self.evaTableView){
        return self.evaData.count;
    }else{
        return 3;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView==self.evaTableView){
        EvaEntity *entity=[self.evaData objectAtIndex:indexPath.row];
        GoodInfoEvaTC *cell=[tableView dequeueReusableCellWithIdentifier:[GoodInfoEvaTC getID] forIndexPath:indexPath];
        if(cell==nil){
            cell=[[GoodInfoEvaTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[GoodInfoEvaTC getID]];
        }
        [cell showData:entity];
        
        return cell;
    }
    else{
        GoodInfoMDTC *cell=[tableView dequeueReusableCellWithIdentifier:[GoodInfoMDTC getID] forIndexPath:indexPath];
        if(cell==nil){
            cell=[[GoodInfoMDTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[GoodInfoMDTC getID]];
        }
        [cell showDataForAddress:self.endAddressMuArr[indexPath.row] withPhone:self.phoneMuArr[indexPath.row]];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView==self.evaTableView){
        return [self getEvaItemHeight:(int)indexPath.row];}
    else{
        return 70;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSString *allString = [NSString stringWithFormat:@"tel:%@",alertView.message];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
    }
}
//获取评价 item 高度
-(CGFloat)getEvaItemHeight:(int)i{
    EvaEntity *entity=[self.evaData objectAtIndex:i];
    return Default_Space+40+Default_Space+[entity.content textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space+[entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space;
}


#pragma mark bar
-(void)shareGood{
    //    分享
    if (![WXApi isWXAppInstalled]) {
        [MBProgressHUD showSuccess:@"当前设备未安装微信!" toView:self.view];
        return;
    }
    [UMUtil shareUM:self delegate:self title:self.goodNameLabel.text body:self.goodInfoLabel.text url:self.shareUrl img:nil urlImg:_shareImgUrl];
}



#pragma mark onclick
-(void)kfGoods{
    MainWebView * vc = [[MainWebView alloc]init];
    vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=%@&order_sn=",EMAIL,versio,[[UIDevice currentDevice] model],_goodsId];
    [self tabHidePushVC:vc];
    
    
//    QYSource *source = [[QYSource alloc] init];
//    source.title =  EMAIL;
//    source.urlString = @"https://8.163.com/";
//    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
//    sessionViewController.sessionTitle = @"泊啤汇客服";
//    sessionViewController.source = source;
//    sessionViewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:sessionViewController animated:YES];
//    if (EMAIL) {
//        sessionViewController.staffId = [SALES_OID intValue];
//    }else{
//        sessionViewController.groupId = 880528;
//    }
//    QYCommodityInfo *commodityInfo = [[QYCommodityInfo alloc] init];
//    commodityInfo.title = self.goodNameLabel.text;
//    commodityInfo.pictureUrlString = self.shareImgUrl;
//    commodityInfo.urlString = self.shareUrl;
//    if ([_goodnum isEqualToString:@"1"]) {
//        commodityInfo.note = [NSString stringWithFormat:@"%@积分",_goodscore];
//    }else{
//        commodityInfo.note = [NSString stringWithFormat:@"￥%.2lf",[self.priceStr doubleValue]];
//    }
//    commodityInfo.show = YES; //访客端是否要在消息中显示商品信息，YES代表显示,NO代表不显示
//    [QYCustomActionConfig sharedInstance].linkClickBlock = ^(NSString *linkAddress) {
//        MainWebView * vc = [[MainWebView alloc]init];
//        vc.webUrl = linkAddress;
//        [self tabHidePushVC:vc];
//    };
//    [[QYSDK sharedSDK] customUIConfig].bottomMargin = 2;
}

-(void)scGoods{
    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    NSString * scUrl = [NSString stringWithFormat:@"%@ctl=deal&act=add_collect&id=%@&email=%@&pwd=%@",FONPort,self.goodsId,email,pwd];
    [RequestData requestDataOfUrl:scUrl success:^(NSDictionary *dic) {
        
        if ([dic[@"status"] isEqual:@1]) {
            if ([dic[@"is_collect"] isEqual:@0]) {
                self.scIcon.image = [UIImage imageNamed:@"wodeshoucang_icon"];
                self.scTitle.text = @"收藏";
            }else if ([dic[@"is_collect"] isEqual:@1]) {
                self.scIcon.image = [UIImage imageNamed:@"yesshouchang"];
                self.scTitle.text = @"已收藏";
                [MBProgressHUD showSuccess:@"收藏成功" toView:self.view];
            }
            
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)evaMoreLabelOnClick{
    
    EvaluateVC * vc =[ EvaluateVC new];
    vc.evaluateId = self.goodsId;
    [self tabHidePushVC:vc];
}

-(void)ljgmLabelOnClick{
    AddGoodVC *vc=[[AddGoodVC alloc]init];
    vc.titleStr=@"立即购买";
    vc.goodsImageUrl = self.iconUrlStr;
    vc.goodsNameStr = self.goodNameLabel.text;
    vc.goodidArray = [self.attrListIdMuArray copy];
    vc.goodtitleArray = [self.dealNameMuarray copy];
    vc.goodpriceArray = [self.attrListPriceArray copy];
    vc.goodnameArray = [self.attrListNameMuArray copy];
    vc.goodtypeArray = self.typeArr;
    vc.goodsId = self.goodsId;
    vc.buy_type = self.goodnum;     //积分
    if([self.goodnum isEqualToString:@"1"]){
        vc.goodspriceStr = [NSString stringWithFormat:@"%@",self.self.goodscore];
    }else{
        vc.goodspriceStr = [NSString stringWithFormat:@"%@",self.priceStr];
    }
    [self tabHidePushVC:vc];
    
}

-(void)jrgwcLabelOnClick{
    NSLog(@"%@",self.typeArr);
    AddGoodVC *vc=[[AddGoodVC alloc]init];
    vc.titleStr=@"加入购物车";
    vc.goodsImageUrl = self.iconUrlStr;
    vc.goodsNameStr = self.goodNameLabel.text;
    vc.goodspriceStr = self.priceStr;
    vc.goodidArray = [self.attrListIdMuArray copy];
    vc.goodtitleArray = [self.dealNameMuarray copy];
    vc.goodpriceArray = [self.attrListPriceArray copy];
    vc.goodnameArray = [self.attrListNameMuArray copy];
    vc.goodtypeArray = self.typeArr;
    vc.goodsId = self.goodsId;
    vc.buy_type = self.goodnum;
    [self tabHidePushVC:vc];
}

//计算label高度
- (CGFloat)getHeightBytitle:(NSString *)title withSize:(NSInteger)size
{
     CGRect r = [title boundingRectWithSize:CGSizeMake(Screen_Width,10000) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]} context:nil];
    
    CGFloat height = r.size.height;
    return height;
}

-(NSArray *)imgArray
{
    return @[
             @"http://img.ptocool.com/3332-1518523974126-29",
             @"http://img.ptocool.com/3332-1518523974125-28",
             @"http://img.ptocool.com/3332-1518523974125-27",
             @"http://img.ptocool.com/3332-1518523974124-26"];
}

- (WSLTransitionAnimationTwo *)transitionAnimation{
    
    if (_transitionAnimation == nil) {
        _transitionAnimation = [[WSLTransitionAnimationTwo alloc] init];
    }
    return _transitionAnimation;
}

//- (WSLTransitionInteractiveTwo *)transitionInteractive{
//
//    if (_transitionInteractive == nil) {
//        _transitionInteractive = [[WSLTransitionInteractiveTwo alloc] init];
//        [_transitionInteractive addPanGestureForViewController:self];
//    }
//    return _transitionInteractive;
//}

#pragma mark -- UINavigationControllerDelegate

//返回处理push/pop动画过渡的对象
- (nullable id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                            animationControllerForOperation:(UINavigationControllerOperation)operation
                                                         fromViewController:(UIViewController *)fromVC
                                                           toViewController:(UIViewController *)toVC{
    
    if (operation == UINavigationControllerOperationPush) {
        self.transitionAnimation.transitionType = WSLTransitionTwoTypePush;
        self.tabBarController.tabBar.hidden = true;
        return self.transitionAnimation;
    }else{
        return nil;
    }
//    }else if (operation == UINavigationControllerOperationPop){
////        if ([fromVC isKindOfClass:[GoodInfoVC class]] && [toVC isKindOfClass:[NewMainViewController class]]) {
//
////             self.transitionAnimation.transitionType = WSLTransitionTwoTypePop;
////        }else{
////            self.transitionAnimation.transitionType = WSLTransitionTwoTypeDissmiss;
////        }
//
//      return self.transitionAnimation;
//    }
//
   
   
}

- (SDCycleScrollView *)cycleScrollView{
    if (!_cycleScrollView) {
        self.cycleScrollView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.8)];
        [self.cycleScrollView setAutoScrollTimeInterval:5];
        self.cycleScrollView.backgroundColor = [UIColor whiteColor];
        [self.cycleScrollView setAutoScroll:NO];
        [self.cycleScrollView setInfiniteLoop:YES];
        self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@""];
        self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
        self.cycleScrollView.delegate=self;
    }
    return _cycleScrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
