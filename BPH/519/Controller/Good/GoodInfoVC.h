//
//  GoodInfoVC.h
//  519
//
//  Created by 陈 on 16/9/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"



@interface GoodInfoVC : BaseVC<UINavigationControllerDelegate>

@property (nonatomic,copy)NSString * goodsId;
@property (nonatomic,copy)NSString * runjump;
@property (nonatomic,copy)NSString * isScroes;  //1:积分商品  0:普通商品

@property(nonatomic,strong)SDCycleScrollView *cycleScrollView;
@end
