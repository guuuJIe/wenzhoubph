//
//  GoodListVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodListVC.h"
#import "NewCarVC.h"
#import "GoodListCC.h"
#import "GoodInfoVC.h"
#import "ScreenTC.h"

@interface GoodListVC()<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UIButton *backBtn;
@property(nonatomic,strong)UIButton *carBtn;

@property(nonatomic,strong)UIView *sortView;
@property(nonatomic,strong)NSMutableArray *sortTitleArray;
@property(nonatomic,strong)NSMutableArray *sortBtnArray;
@property(nonatomic,strong)UIView *screenView;
@property(nonatomic,strong)UILabel *screenLabel;
@property(nonatomic,strong)UIImageView *screenImg;

@property(nonatomic,strong)UIView *screenInfoView;

@property(nonatomic,assign)CGFloat collectionWidth;
@property(nonatomic,assign)CGFloat collectionHeight;
@property(nonatomic,strong)UICollectionView *collectionView;

@property (nonatomic,strong)NSMutableArray * goodsImageArray;
@property (nonatomic,strong)NSMutableArray * goodsTitleArray;
@property (nonatomic,strong)NSMutableArray * goodsPriceArray;
@property (nonatomic,strong)NSMutableArray * goodsOldPriceArray;
@property (nonatomic,strong)NSMutableArray * goodsCountArray;
@property (nonatomic,strong)NSMutableArray * goodsBuyTypeArr;
@property (nonatomic,strong)NSMutableArray * goodsScoreArr;
@property (nonatomic,strong)NSArray * codeArray;

@property (nonatomic,strong)ScreenTC * screenCell;

@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)UIView * mlView;
@property (nonatomic,assign)NSInteger oldBtnIndex;
@property (nonatomic,assign)NSInteger pageIndex;
@property (nonatomic,strong)UIImageView * upImageView;
@property (nonatomic,strong)UIImageView * downImageView;

//筛选
@property (nonatomic,copy)NSString * firstString;
@property (nonatomic,copy)NSString * secondString;
@property (nonatomic,copy)NSString * thirdString;
@property (nonatomic,strong)NSMutableArray * selectMuArray;
@property (nonatomic,strong)NSMutableArray * tagMuArray;
@property (nonatomic,strong)NSMutableArray * titleidMuarr;
@property (nonatomic,strong)NSMutableDictionary * replaceMuDic;
@property (nonatomic,copy)NSString * replaceStr;


@property (nonatomic,strong)NSMutableArray * titleArray;
@property (nonatomic,strong)NSMutableArray * subBtnTitleArray;
@property (nonatomic,strong)NSMutableArray * allBtnTitleArray;
@property (nonatomic,strong)NSMutableArray * allBtnsArray;
@property (nonatomic,strong)NSMutableArray * idMuArray;


@property (nonatomic,strong)UITableView * tableView;

@end

@implementation GoodListVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"商品列表";
    [MBProgressHUD showHUD];
    _oldBtnIndex = 0;
    _pageIndex = 1;
    self.codeArray = @[@"buy_count",@"newest",@"price_asc",@"price_desc"];
    
    self.goodsImageArray = [NSMutableArray new];
    self.goodsPriceArray = [NSMutableArray new];
    self.goodsTitleArray = [NSMutableArray new];
    self.goodsOldPriceArray = [NSMutableArray new];
    self.goodsCountArray = [NSMutableArray new];
    self.goodsBuyTypeArr = [NSMutableArray new];
    self.goodsScoreArr = [NSMutableArray new];
    
    self.titleArray = [NSMutableArray new];
    self.subBtnTitleArray = [NSMutableArray new];
    self.allBtnTitleArray = [NSMutableArray new];
    self.allBtnsArray = [NSMutableArray new];
    
    self.selectMuArray = [NSMutableArray new];
    self.tagMuArray = [NSMutableArray new];
    self.titleidMuarr = [NSMutableArray new];
    self.replaceMuDic = [NSMutableDictionary new];
    
    self.idMuArray = [NSMutableArray new];
    
    [self initBar];
    [self initView];

    if (![_selectstr isEqualToString:@"mainJumpGoods"] && _selectstr &&![_selectstr isEqualToString:@"catJpmpGoodsList"] ) {
        //搜索
        [self selectData:1];
    }else if (_isFromWeb){
        //搜索
        [self selectData:1];
    }else{
        /**
         *  oldBtnIndex
         *  pageIndex  页数
         *  idStr : id
         */
        [self requestDataOfGood:_oldBtnIndex withPage:_pageIndex withidstr:self.idStr];
    }
    
}

-(void)selectData:(NSInteger )page{
    NSString * selectUrl = [NSString stringWithFormat:@"%@&ctl=goods&keyword=%@&page=%ld",FONPort,_selectstr,(long)page];
    
    if (_isFromWeb) {
        selectUrl = _url;
    }
    [RequestData requestDataOfUrl:selectUrl success:^(NSDictionary *dic) {
        
        NSArray * itemArr = dic[@"item"];
        if (itemArr.count>0) {
            for (NSDictionary * itemDic in dic[@"item"]) {
                [self.idMuArray addObject:itemDic[@"id"]];
                [self.goodsTitleArray addObject:itemDic[@"name"]];
                [self.goodsPriceArray addObject:itemDic[@"current_price"]];
                [self.goodsImageArray addObject:itemDic[@"image"]];
                [self.goodsOldPriceArray addObject:itemDic[@"origin_price"]];
                [self.goodsCountArray addObject:itemDic[@"buy_count"]];
                [self.goodsScoreArr addObject:itemDic[@"deal_score"]];
                [self.goodsBuyTypeArr addObject:itemDic[@"buy_type"]];
            }
            
            [self.tableView reloadData];
            [self showData];
            [MBProgressHUD dissmiss];
            
        }else{
            if (page == 1) {
                [self showData];
            }else{
                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}


-(void)requestDataOfGood:(NSInteger)inte withPage:(NSInteger)page withidstr:(NSString *)str{
    NSString * urlString;
    /**
     *  
     *  self.typeid = 13 : 积分
     */
    if ([self.selectstr isEqualToString:@"2"]) {
        urlString = [NSString stringWithFormat:@"%@ctl=goods&page=%ld&cate_id=%@",FONPort,(long)page,str];
    }else if([self.typeId isEqualToString:@"13"]){
        urlString = [NSString stringWithFormat:@"%@ctl=scores&page=%ld&cate_id=%@&buy_type=1",FONPort,(long)page,str];
    }else{
        urlString = [NSString stringWithFormat:@"%@ctl=goods&order_type=%@&page=%ld&cate_id=%@",FONPort,self.codeArray[inte],(long)page,str];
    }
    
    
     [RequestData requestDataOfUrl:urlString success:^(NSDictionary *dic) {
       
         NSArray * itemArr = dic[@"item"];
         if (itemArr.count>0) {
            for (NSDictionary * itemDic in itemArr) {
                [self.idMuArray addObject:itemDic[@"id"]];
                [self.goodsTitleArray addObject:itemDic[@"name"]];
                [self.goodsPriceArray addObject:itemDic[@"current_price"]];
                [self.goodsImageArray addObject:itemDic[@"icon"]];
                [self.goodsOldPriceArray addObject:itemDic[@"origin_price"]];
                [self.goodsCountArray addObject:itemDic[@"buy_count"]];
                [self.goodsBuyTypeArr addObject:itemDic[@"buy_type"]];
                [self.goodsScoreArr addObject:itemDic[@"deal_score"]];
            }
            [self.titleArray removeAllObjects];
            [self.allBtnsArray removeAllObjects];
            
            for (NSDictionary * secondDic in dic[@"filter_group"]) {
                
                [self.titleArray addObject:secondDic[@"name"]];
                [self.titleidMuarr addObject:secondDic[@"id"]];
                
                NSMutableArray * newArray = [NSMutableArray new];
                for (NSDictionary * thirdDic in secondDic[@"filter_list"]) {
                    [newArray addObject:thirdDic[@"name"]];
                    
                }
                [self.allBtnsArray addObject:newArray];
            }
            [self.tableView reloadData];
             
            [self showData];
         }else{
             if (page == 1) {
                [self showData];
             }else{
                 
                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
             }
         }
    } failure:^(NSError * error) {
        NSLog(@"%@",error);
    }];
}

-(void)initBar{
    [super initBar];
    self.title = @"商品列表";
    
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    self.carBtn=[[UIButton alloc]initWithFrame:contentView.bounds];
    [self.carBtn setBackgroundImage:[UIImage imageNamed:@"car_white_icon"] forState:UIControlStateNormal];
    [self.carBtn addTarget:self action:@selector(carBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:self.carBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
    
    self.carBtn.badge.backgroundColor=[UIColor clearColor];
    self.carBtn.badge.badgeColor=[UIColor whiteColor];
    self.carBtn.badge.textColor=APPFourColor;
    self.carBtn.badge.outlineWidth=0;
    self.carBtn.badge.font=[UIFont systemFontOfSize:FONT_SIZE_X];
    self.carBtn.badge.frame=CGRectMake(self.carBtn.frame.size.width-16, 0, 16, 16);
    self.carBtn.badge.userInteractionEnabled=NO;
}

-(void)popVC{
    [super popVC];
    if (_isFromWeb) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else
    if ([self.runjump isEqualToString:@"runjump"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"runjumpmain" object:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)initView{
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewOnClick)];
    viewTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:viewTap];

//    self.sortView=[[UIView alloc] initWithFrame:CGRectMake(0, self.barView.frame.origin.y+self.barView.frame.size.height, Screen_Width, 40)];
    self.sortView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.sortView];
    [self.sortView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.offset(0);
        make.height.offset(40);
    }];
    [self.view layoutIfNeeded];
    
    self.sortView.backgroundColor=[UIColor whiteColor];
    
    self.sortTitleArray=[[NSMutableArray alloc]initWithObjects:@"人气",@"新品",@"价格", nil];
    self.sortBtnArray=[[NSMutableArray alloc]init];
    for (int i=0; i<self.sortTitleArray.count; i++) {
        NSString *title=self.sortTitleArray[i];
        UIFont *font=[UIFont systemFontOfSize:FONT_SIZE_M];
        CGFloat width=Screen_Width/4;
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(i*width, 0, width, self.sortView.frame.size.height)];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:APPFourColor forState:UIControlStateNormal];
        [btn setTitleColor:APPColor forState:UIControlStateSelected];
        btn.titleLabel.font=font;
        btn.tag=i;
        [btn addTarget:self action:@selector(sortBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.sortBtnArray addObject:btn];
        [self.sortView addSubview:btn];
        if(i == self.sortTitleArray.count-1){
            self.upImageView = [[UIImageView alloc]initWithFrame:CGRectMake(((i+1)*width)-30, 10, 20, self.sortView.frame.size.height-20)];
            self.upImageView.image = [UIImage imageNamed:@"top_black_icon"];
            [self.sortView addSubview:self.upImageView];
            
            self.downImageView = [[UIImageView alloc]initWithFrame:CGRectMake(((i+1)*width)-30, 10, 20, self.sortView.frame.size.height-20)];
            self.downImageView.hidden = YES;
            self.downImageView.image = [UIImage imageNamed:@"bottom_black_icon"];
            [self.sortView addSubview:self.downImageView];
        }
    }
    
    self.screenView=[[UIView alloc]initWithFrame:CGRectMake(Screen_Width-(Screen_Width/4), 0,Screen_Width/4, self.sortView.frame.size.height)];
    self.screenView.backgroundColor=[UIColor whiteColor];
    self.screenLabel=[[UILabel alloc]init];
    self.screenLabel.textColor=APPFourColor;
    self.screenLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.sortView addSubview:self.screenView];
    
    self.screenImg=[[UIImageView alloc]init];
    self.screenImg.image=[UIImage imageNamed:@"bottom_black_icon"];
    [self.screenView addSubview:self.screenLabel];
    [self.screenView addSubview:self.screenImg];
    UITapGestureRecognizer *screenViewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(screenViewOnClick)];
    [self.screenView addGestureRecognizer:screenViewTap];
    
//    self.screenInfoView=[[UIView alloc]initWithFrame:CGRectMake(0, self.sortView.frame.size.height+self.sortView.frame.origin.y, Screen_Width, Screen_Height-84)];
  
    
    self.collectionWidth=(Screen_Width-5*3)/2;
    self.collectionHeight=self.collectionWidth+60;
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake(self.collectionWidth,self.collectionHeight);
    layout.minimumInteritemSpacing =0;
    layout.minimumLineSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, self.sortView.frame.origin.y+self.sortView.frame.size.height,Screen_Width, Screen_Height-self.sortView.frame.origin.y-self.sortView.frame.size.height) collectionViewLayout:layout];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sortView.mas_bottom).offset(0);
        make.left.right.bottom.offset(0);
    }];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[GoodListCC class] forCellWithReuseIdentifier:[GoodListCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.showsVerticalScrollIndicator=NO;
    self.collectionView.dataSource=self;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.collectionView.mj_header = header;
    
    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        _pageIndex = _pageIndex+1;
        [self addData];
    }];
    
    self.screenInfoView = [[UIView alloc] initWithFrame:CGRectZero];
    self.screenInfoView.backgroundColor=[UIColor colorWithRed:188/250.0 green:188/250.0 blue:188/250.0 alpha:1];
    self.screenInfoView.hidden=YES;
    [self.view addSubview:self.screenInfoView];
    [self.screenInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sortView.mas_bottom).offset(0);
        make.left.right.bottom.offset(0);
    }];
    
    [self createScreenInfo:self.screenInfoView];
    
    [self loadData];

}

-(void)refreshData{
    [self removeAllMuArr];
    _pageIndex = 1;
    if (![_selectstr isEqualToString:@"mainJumpGoods"] && _selectstr &&![_selectstr isEqualToString:@"catJpmpGoodsList"] ) {
        [self selectData:_pageIndex];
    }else{
        [self addGoodDataOrReplacrData:_oldBtnIndex];
    }
}
-(void)removeAllMuArr{
    [self.goodsTitleArray removeAllObjects];
    [self.goodsImageArray removeAllObjects];
    [self.goodsPriceArray removeAllObjects];
    [self.idMuArray removeAllObjects];
    [self.goodsCountArray removeAllObjects];
    [self.goodsOldPriceArray removeAllObjects];
    [self.goodsBuyTypeArr removeAllObjects];
    [self.goodsScoreArr removeAllObjects];
}

-(void)addData{
    
    if (![_selectstr isEqualToString:@"mainJumpGoods"] && _selectstr &&![_selectstr isEqualToString:@"catJpmpGoodsList"] ) {
        [self selectData:_pageIndex];
    }else{
        NSInteger goodsIndex = _oldBtnIndex;
        if (self.replaceStr.length>1) {
            [self requestDataOfReplaceBtn:_replaceStr withPage:_pageIndex withbtnIndex:goodsIndex];
        }else {
            if (_oldBtnIndex == 2 ) {
                if (self.downImageView.hidden == NO) {
                    goodsIndex = 3;
                }else{
                    goodsIndex = 2;
                }
            }
            [self requestDataOfGood:goodsIndex withPage:_pageIndex withidstr:_idStr];
        }
    }
}



-(void)loadData{
    [self showData];
}

-(void)showData{
    [MBProgressHUD dissmiss];
    [self.collectionView.mj_header endRefreshing];
    [self.collectionView.mj_footer endRefreshing];
    
    self.screenLabel.text=@"筛选";
    self.screenLabel.frame=CGRectMake((self.screenView.frame.size.width-[NSString sizeWithText:self.screenLabel.text font:self.screenLabel.font maxSize:Max_Size].width-20)/2, 10, [NSString sizeWithText:self.screenLabel.text font:self.screenLabel.font maxSize:Max_Size].width, 20);
    self.screenImg.frame=CGRectMake(self.screenLabel.frame.size.width+self.screenLabel.frame.origin.x,10,20, 20);
    
    [self.collectionView reloadData];
    
}

//筛选view
-(void)createScreenInfo:(UIView *)view{
    
//    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 2, Screen_Width, Screen_Height-100)];
   
    
    UIView * downView = [[UIView alloc]initWithFrame:CGRectZero];
    downView.backgroundColor = [UIColor colorWithRed:188/250.0 green:188/250.0 blue:188/250.0 alpha:1];
//    downView.center = CGPointMake(Screen_Width/2,view.frame.size.height-40);
    [view addSubview: downView];
    [downView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.bottom.offset(0);
        make.height.offset(40);
    }];
//    [self.view layoutIfNeeded];
    NSArray * downBtnTitle = @[@"重置",@"确定"];
    for (NSInteger i = 0; i<2; i++) {
        UIButton * tureRoReplacrBtn = [[UIButton alloc]initWithFrame:CGRectZero];
        [tureRoReplacrBtn setTitle:downBtnTitle[i] forState:(UIControlStateNormal)];
        if (i == 0) {
            [tureRoReplacrBtn setTitleColor:[UIColor colorWithRed:134/250.0 green:134/250.0 blue:134/250.0 alpha:1] forState:(UIControlStateNormal)];
            tureRoReplacrBtn.backgroundColor = [UIColor whiteColor];
        }if (i == 1) {
            [tureRoReplacrBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            tureRoReplacrBtn.backgroundColor= [UIColor colorWithRed:229/250.0 green:57/250.0 blue:53/250.0 alpha:1];
        }
        tureRoReplacrBtn.tag = 2000+i;
        tureRoReplacrBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [tureRoReplacrBtn addTarget:self action:@selector(tureOrReplaceClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [downView addSubview:tureRoReplacrBtn];
        [tureRoReplacrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(Screen_Width / 2 * i);
            make.width.offset(Screen_Width / 2);
            make.height.offset(40);
        }];
    }
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self.tableView registerClass:[ScreenTC class] forCellReuseIdentifier:[ScreenTC getID]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.bottom.equalTo(downView.mas_top);
    }];
    
}

-(void)tureOrReplaceClick:(UIButton *)btn{
    if (btn.tag == 2000) {
        for (UIButton * btn in self.selectMuArray) {
            btn.layer.borderColor = [UIColor colorWithRed:54/250.0 green:54/250.0 blue:54/250.0 alpha:1].CGColor;
            btn.selected = NO;
        }
        [self.selectMuArray removeAllObjects];
    }if (btn.tag == 2001) {
        self.replaceStr  = @"";
        for (UIButton * btn in self.selectMuArray) {
            NSInteger index = (btn.tag - 8000)/100;
            NSString * subStr = [NSString stringWithFormat:@"\"%@\":\"%@\"",self.titleidMuarr[index],btn.titleLabel.text];
            if ([self.replaceStr  isEqual: @""]) {
                self.replaceStr = [NSString stringWithFormat:@"%@",subStr];
            }else{
                self.replaceStr = [NSString stringWithFormat:@"%@,%@",self.replaceStr,subStr];
            }
            self.screenInfoView.hidden = YES;
        }
        _pageIndex = 1;
        [self removeAllMuArr];
        
        [self requestDataOfReplaceBtn:_replaceStr withPage:_pageIndex withbtnIndex:_oldBtnIndex];
        
    }
}

-(void)requestDataOfReplaceBtn:(NSString *)str withPage:(NSInteger)index withbtnIndex:(NSInteger)btnIndex{
    
   
    NSString * urlString = [NSString stringWithFormat:@"%@ctl=goods&page=%ld&f={%@}&order_type=%@&cate_id=%@",FONPort,(long)index,str,self.codeArray[btnIndex],self.idStr];
    
    [RequestData requestDataOfUrl:urlString success:^(NSDictionary *dic) {
         for (NSDictionary * itemDic in dic[@"item"]) {
             [self.goodsTitleArray addObject:itemDic[@"name"]];
             [self.goodsPriceArray addObject:itemDic[@"current_price"]];
             [self.goodsImageArray addObject:itemDic[@"image"]];
             [self.goodsOldPriceArray addObject:itemDic[@"origin_price"]];
             [self.goodsCountArray addObject:itemDic[@"buy_count"]];
             [self.idMuArray addObject:itemDic[@"id"]];
             [self.goodsScoreArr addObject:itemDic[@"deal_score"]];
             [self.goodsBuyTypeArr addObject:itemDic[@"buy_type"]];
        }
        [MBProgressHUD dissmiss];
        [self showData];
    } failure:^(NSError *error) {
         NSLog(@"%@",error);
    }];
    
}


#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.goodsTitleArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    GoodListCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[GoodListCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[GoodListCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    if (self.idMuArray.count > 0) {
        [cell showDataOfTitle:self.goodsTitleArray[indexPath.row]
                        image:self.goodsImageArray[indexPath.row]
                    withPrice:[NSString stringWithFormat:@"%@",self.goodsPriceArray[indexPath.row]]
                     oldprice:[NSString stringWithFormat:@"%@",self.goodsOldPriceArray[indexPath.row]]
                    withCound:[NSString stringWithFormat:@"%@",self.goodsCountArray[indexPath.row]]
                     buy_type:[NSString stringWithFormat:@"%@",self.goodsBuyTypeArr[indexPath.row]]
               withDeal_score:[NSString stringWithFormat:@"%@",self.goodsScoreArr[indexPath.row]]];
    }
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionWidth,self.collectionHeight);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodInfoVC * goodsInfo = [GoodInfoVC new];
    goodsInfo.goodsId = self.idMuArray[indexPath.row];
    if ([self.typeId isEqualToString:@"13"]) {
        goodsInfo.isScroes = @"1";
    }
    [self tabHidePushVC:goodsInfo];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark onclick delegate
-(void)carBtnOnClick{
    NewCarVC *vc=[[NewCarVC alloc] init];
    vc.haveBackBtn=YES;
    [self tabHidePushVC:vc];
}

-(void)sortBtnOnClick:(id)sender{
    self.screenInfoView.hidden = YES;
    [self removeAllMuArr];
    UIButton *btn=(UIButton *)sender;
    for (UIButton *itemBtn in self.sortBtnArray){

        if(itemBtn.tag==btn.tag){
            _pageIndex = 1;
            [self.goodsTitleArray removeAllObjects];
            [self.goodsImageArray removeAllObjects];
            [self.goodsPriceArray removeAllObjects];
            if (_oldBtnIndex != btn.tag) {
                _oldBtnIndex = btn.tag;
                [self addGoodDataOrReplacrData:_oldBtnIndex];
                
            }
            if (_oldBtnIndex == 2 &&itemBtn.selected == YES) {
                if (self.downImageView.hidden == YES) {
                    self.downImageView.hidden = NO;
                    self.upImageView.hidden = YES;
                    [self addGoodDataOrReplacrData:btn.tag+1];
                    
                }else{
                    self.downImageView.hidden = YES;
                    self.upImageView.hidden = NO;
                    [self addGoodDataOrReplacrData:btn.tag];
                    
                }
            }
            itemBtn.selected=YES;
        }else{
            itemBtn.selected=NO;
        }
    }
}

-(void)addGoodDataOrReplacrData:(NSInteger)index{
    if(_replaceStr.length>1){
        [self requestDataOfReplaceBtn:_replaceStr withPage:_pageIndex withbtnIndex:index];
    }else{
        [self requestDataOfGood:index withPage:_pageIndex withidstr:_idStr];
    }
}

-(void)viewOnClick{

}

-(void)screenViewOnClick{
    if(self.screenInfoView.hidden){
        self.screenInfoView.hidden=NO;
        self.screenImg.image=[UIImage imageNamed:@"top_black_icon"];
    }else{
        self.screenInfoView.hidden=YES;
        self.screenImg.image=[UIImage imageNamed:@"bottom_black_icon"];
    }
}

#pragma mark tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.titleArray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray * array = self.allBtnsArray[indexPath.row];
    
    return 65 + (array.count/5+1) * 50;
    

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    _screenCell =[tableView dequeueReusableCellWithIdentifier:[ScreenTC getID] forIndexPath:indexPath];
    if(_screenCell==nil){
        _screenCell=[[ScreenTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[ScreenTC getID]];
    }
    for (UIButton * btn in _screenCell.mlView.subviews) {
        [btn removeFromSuperview];
    }
    _screenCell.titleLabel.text = self.titleArray[indexPath.row];
    _screenCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGFloat width = _screenCell.mlView.frame.size.width/4;
    NSArray  * array = self.allBtnsArray[indexPath.row];
    NSMutableArray * mutableArray = [NSMutableArray new];
    for (NSInteger i = 0; i<array.count; i++) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(i%4 * width, i / 4 * 40+15, width-10, 28)];
        btn.layer.cornerRadius = 8;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = [UIColor colorWithRed:188/250.0 green:188/250.0 blue:188/250.0 alpha:1].CGColor;
        btn.layer.borderWidth = 1;
        btn.tag = 8000+100*indexPath.row+i;
        [mutableArray addObject:btn];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn setTitle:array[i] forState:(UIControlStateNormal)];
        [btn setTitleColor:[UIColor colorWithRed:54/250.0 green:54/250.0 blue:54/250.0 alpha:1] forState:(UIControlStateNormal)];
        [btn setTitleColor:[UIColor colorWithRed:229/250.0 green:57/250.0 blue:53/250.0 alpha:1] forState:(UIControlStateSelected)];
        [btn addTarget:self action:@selector(onClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [_screenCell.mlView addSubview:btn];
    }
    [_tagMuArray addObject:mutableArray];
    _screenCell.mlView.frame = CGRectMake(20, 40, Screen_Width-40, (array.count/5) * 50 + 20*(array.count/5+1));
    return _screenCell;
}

-(void)onClick:(UIButton *)btn{
    
    if (btn.selected == YES) {
        [self.selectMuArray removeObject:btn];
        btn.selected = NO;
        btn.layer.borderColor = [UIColor colorWithRed:188/250.0 green:188/250.0 blue:188/250.0 alpha:1].CGColor;
        
    }else{

        NSInteger index = (btn.tag-8000)/100;
        NSArray * array = _tagMuArray[index];
        for (UIButton * subbtn in array) {
            [self.selectMuArray removeObject:subbtn];
            subbtn.layer.borderColor = [UIColor colorWithRed:188/250.0 green:188/250.0 blue:188/250.0 alpha:1].CGColor;
            subbtn.selected = NO;
        }
        btn.layer.borderColor = [UIColor colorWithRed:229/250.0 green:57/250.0 blue:53/250.0 alpha:1].CGColor;
        btn.selected = YES;
        [self.selectMuArray addObject:btn];
        
    }
}

@end
