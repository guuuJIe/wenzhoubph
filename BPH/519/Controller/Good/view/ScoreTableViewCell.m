//
//  ScoreTableViewCell.m
//  519
//
//  Created by Macmini on 2017/12/15.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ScoreTableViewCell.h"
#import "ScoreCollectionViewCell.h"

@interface ScoreTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)UICollectionView * collectionView;
@end

@implementation ScoreTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(Screen_Width/2-0.5,Screen_Width/2-1+10);
    layout.minimumInteritemSpacing = 1;
    layout.minimumLineSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 1, Screen_Width, self.height) collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[ScoreCollectionViewCell class] forCellWithReuseIdentifier:@"scoreStoreIde"];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self addSubview:self.collectionView];
}

#pragma mark collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    ScoreCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"scoreStoreIde" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[ScoreCollectionViewCell alloc]initWithFrame:CGRectMake(0, 0, itemSize.width, itemSize.height)];
    }
    ScoreDateModel * model = self.dataArr[indexPath.row];
    cell.model = model;
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/2-0.5,Screen_Width/2-1+10);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ScoreDateModel * model = self.dataArr[indexPath.row];
    if (self.block) {
        self.block(model.Id);
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)setDataArr:(NSArray *)dataArr{
    _dataArr = dataArr;
    self.collectionView.frame = CGRectMake(0, 1, Screen_Width, (self.dataArr.count+1)/2*(Screen_Width/2+20));
    [_collectionView reloadData];
}

@end
