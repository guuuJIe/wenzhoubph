//
//  VideoandPicMixShuffling.h
//  519
//
//  Created by Macmini on 2019/5/25.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZFPlayer/ZFPlayer.h>
@class VideoandPicMixShuffling;

typedef enum : NSUInteger {
    TSDETAILTYPEVIDEO,//视屏
    TSDETAILTYPEIMAGE,//图片
} TSDETAILTYPE;


@protocol VideoPlaybackDelegate <NSObject>

- (void)videoView:(VideoandPicMixShuffling *)View didSelectItemAtIndexPath:(NSInteger)index;

@end

NS_ASSUME_NONNULL_BEGIN

@interface VideoandPicMixShuffling : UIView

@property (nonatomic,weak) id<VideoPlaybackDelegate> delegate;

@property (nonatomic,assign) TSDETAILTYPE type;

/*
 * isVideo  是否带视屏链接
 * array    数据
 */
-(void)setWithIsVideo:(TSDETAILTYPE)type andDataArray:(NSArray *)array;

//清除缓存（必须写）
-(void)clearCache;

@property (nonatomic, strong) ZFPlayerController *player;

@end

NS_ASSUME_NONNULL_END
