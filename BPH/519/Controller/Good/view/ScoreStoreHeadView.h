//
//  ScoreStoreHeadView.h
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//
typedef void(^scoreChangeBlock)(void);
typedef void(^scoreAdvBlock)(NSInteger index);
#import <UIKit/UIKit.h>

@interface ScoreStoreHeadView : UIView
@property (nonatomic,strong)UILabel * scoreLabel;
@property (nonatomic,copy)scoreChangeBlock block;
@property (nonatomic,copy)scoreAdvBlock advBlock;
@property (nonatomic,strong)SDCycleScrollView * cycle;
@end
