//
//  VideoandPicMixShuffling.m
//  519
//
//  Created by Macmini on 2019/5/25.
//  Copyright © 2019 519. All rights reserved.
//

#import "VideoandPicMixShuffling.h"
#import <ZFPlayer/ZFPlayer.h>
#import <ZFPlayer/ZFPlayerControlView.h>
#import <UIImageView+ZFCache.h>
#import <ZFPlayer/ZFAVPlayerManager.h>
#import "UIImage+Common.h"

static NSString *kVideoCover = @"https://upload-images.jianshu.io/upload_images/635942-14593722fe3f0695.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240";

@interface VideoandPicMixShuffling()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) NSArray *dataArray;



@property (nonatomic, strong) ZFPlayerControlView *controlView;

@property (nonatomic, strong) UIImageView *containerView;

@property (nonatomic, strong) ZFAVPlayerManager *playerManager;

@property (nonatomic, strong) UIButton *playBtn;

@property (nonatomic, strong) NSArray <NSURL *>*assetURLs;
@end

@implementation VideoandPicMixShuffling

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self initialControlUnit];
    }
    return self;
}




-(void)setWithIsVideo:(TSDETAILTYPE)type andDataArray:(NSArray *)array{
    self.dataArray = array;
    
    self.scrollView.contentSize = CGSizeMake(self.dataArray.count*self.frame.size.width, self.frame.size.height);
    self.type = type;
    
    for (int i = 0; i < _dataArray.count; i ++) {
        if (type == TSDETAILTYPEVIDEO) {
            if (i == 0) {
                self.containerView.frame = CGRectMake(0, 0, Screen_Width, Screen_Width*0.8);
                self.playBtn.frame = CGRectMake((CGRectGetWidth(self.containerView.frame)-44)/2, (CGRectGetHeight(self.containerView.frame)-44)/2, 44, 44);
                [self.containerView addSubview:self.playBtn];
                
                self.player = [ZFPlayerController playerWithPlayerManager:self.playerManager containerView:self.containerView];
                self.player.controlView = self.controlView;
                self.player.pauseWhenAppResignActive = false;
                WeakSelf(self)
                self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
                    
//                    [weakself setNeedsStatusBarAppearanceUpdate];
                };
                
                self.player.playerDidToEnd = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset) {
                    [weakself.player stop];
                };
                
                self.player.playerReadyToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
                    BLog(@"开始播放了");
                };
                
                self.assetURLs = @[[NSURL URLWithString:@"http://flv3.bn.netease.com/tvmrepo/2018/6/9/R/EDJTRAD9R/SD/EDJTRAD9R-mobile.mp4"]];
                self.player.assetURLs = self.assetURLs;
                
                [self.scrollView addSubview:self.containerView];
                
            }
            else{
                UIImageView * img = [[UIImageView alloc]initWithFrame:CGRectMake(i*self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
                img.userInteractionEnabled = YES;
                [img sd_setImageWithURL:[NSURL URLWithString:self.dataArray[i]] placeholderImage:[UIImage imageNamed:@"icon_video"]];
                [self.scrollView addSubview:img];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTapClick)];
                [img addGestureRecognizer:tap];
                
            }
            
            if (_dataArray.count > 1) {
             
            }
        }else{//全图片
            UIImageView * img = [[UIImageView alloc]initWithFrame:CGRectMake(i*self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
            [img sd_setImageWithURL:[NSURL URLWithString:self.dataArray[i]] placeholderImage:[UIImage imageNamed:@"icon_video"]];
            img.userInteractionEnabled = YES;
            [self.scrollView addSubview:img];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTapClick)];
            [img addGestureRecognizer:tap];
            
          
        }
    }
}

- (void)imgTapClick{

}

- (void)playClick:(UIButton *)sender{
   
    [self.player playTheIndex:0];
    
}

- (void)initialControlUnit{

    _playerManager = [[ZFAVPlayerManager alloc] init];
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.showsVerticalScrollIndicator = false;
    _scrollView.showsHorizontalScrollIndicator = false;
    _scrollView.userInteractionEnabled = true;
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.bounces = false;
    [self addSubview:_scrollView];
    self.scrollView.frame = CGRectMake(0, 0, Screen_Width, Screen_Width*0.8);

}

- (UIImageView *)containerView {
    if (!_containerView) {
        _containerView = [UIImageView new];
        [_containerView setImageWithURLString:kVideoCover placeholder:[UIImage imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)]];
        [self addSubview:_containerView];
    }
    return _containerView;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
        _controlView.fastViewAnimated = YES;
//        _controlView.autoHiddenTimeInterval = 5;
//        _controlView.autoFadeTimeInterval = 0.5;
    }
    return _controlView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"new_allPlay_44x44_"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _playBtn;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
