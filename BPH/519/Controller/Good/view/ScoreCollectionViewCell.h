//
//  ScoreCollectionViewCell.h
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodInfoModel.h"
@interface ScoreCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)ScoreDateModel * model;

@end
