//
//  ScoreStoreHeadView.m
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ScoreStoreHeadView.h"

@interface ScoreStoreHeadView()<SDCycleScrollViewDelegate>

@end

@implementation ScoreStoreHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = APPBGColor;
        [self createUI];
    }
    return self;
}
-(void)createUI{
    
    SDCycleScrollView * cycle = [[SDCycleScrollView alloc]init];
    cycle.delegate = self;
    self.cycle = cycle;
    [self addSubview:self.cycle];
    [self.cycle makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.left.mas_equalTo(self).with.offset(0);
        make.width.equalTo(self.width);
        make.height.equalTo(Screen_Width/375*160);
    }];
    
    UIView * leftView = [[UIView alloc]init];
    leftView.backgroundColor = [UIColor whiteColor];
    [self addSubview:leftView];
    [leftView makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cycle.mas_bottom).with.offset(0);
        make.left.mas_equalTo(self).with.offset(0);
        make.width.equalTo(self.width/2);
        make.height.equalTo(40);
    }];
    
    UIImageView  * leftImage = [[UIImageView alloc]init];
    leftImage.image = [UIImage imageNamed:@"icon_首页_积分"];
    [leftView addSubview:leftImage];
    [leftImage makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftView.mas_left).with.mas_offset(self.width/6);
        make.centerY.mas_equalTo(leftView.mas_centerY);
        make.height.and.width.equalTo(15);
    }];
    
    UILabel * leftLabel = [[UILabel alloc]init];
    leftLabel.textAlignment = NSTextAlignmentLeft;
    leftLabel.text = @"积分:";
    leftLabel.textColor = APPFourColor;
    leftLabel.font = [UIFont systemFontOfSize:14];
    [leftView addSubview:leftLabel];
    [leftLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImage.mas_right).with.mas_offset(10);
        make.centerY.equalTo(leftView.mas_centerY);
        make.height.equalTo(20);
    }];
    
    _scoreLabel = [[UILabel alloc]init];
    _scoreLabel.textColor = [UIColor redColor];
    _scoreLabel.text = @"2000";
    _scoreLabel.textAlignment = NSTextAlignmentLeft;
    _scoreLabel.font = [UIFont systemFontOfSize:14];
    [leftView addSubview:_scoreLabel];
    [_scoreLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftLabel.mas_right).with.mas_offset(10);
        make.centerY.equalTo(leftView.mas_centerY);
        make.height.equalTo(20);
    }];
    
    
    UIView * rightView = [[UIView alloc]init];
    rightView.backgroundColor = [UIColor whiteColor];
    [self addSubview:rightView];
    [rightView makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cycle.mas_bottom).with.offset(0);
        make.left.mas_equalTo(leftView.mas_right).with.offset(0);
        make.width.equalTo(self.width/2);
        make.height.equalTo(40);
    }];
    
    
    UIButton * infoBtn = [[UIButton alloc]init];
    [infoBtn setImage:[UIImage imageNamed:@"icon_兑换详情"] forState:(UIControlStateNormal)];
    [infoBtn setTitle:@"兑换记录" forState:(UIControlStateNormal)];
    [infoBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    infoBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    infoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    [infoBtn addTarget:self action:@selector(clickInfoBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [rightView addSubview:infoBtn];
    [infoBtn makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(rightView.mas_centerX);
        make.centerY.mas_equalTo(rightView.mas_centerY);
        make.height.equalTo(40);
    }];
    
    UIView * titleView = [[UIView alloc]init];
    titleView.backgroundColor = [UIColor whiteColor];
    [self addSubview:titleView];
    [titleView makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).with.offset(0);
        make.top.mas_equalTo(rightView.mas_bottom).with.offset(10);
        make.width.equalTo(self.width);
        make.height.equalTo(40);
    }];
    
    UIView * redView = [[UIView alloc]init];
    redView.backgroundColor = [UIColor redColor];
    [titleView addSubview:redView];
    [redView makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.mas_equalTo(titleView).with.offset(10);
        make.width.equalTo(3);
        make.height.equalTo(20);
    }];
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"热门商品";
    titleLabel.textColor = APPFourColor;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [titleView addSubview:titleLabel];
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(redView.mas_right).with.offset(10);
        make.top.mas_equalTo(titleView.mas_top).with.offset(10);
        make.width.equalTo(100);
        make.height.equalTo(20);
    }];
    
    UIView * lineview = [[UIView alloc]init];
    lineview.backgroundColor = APPBGColor;
    [self addSubview:lineview];
    [lineview makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self).with.offset(0);
        make.height.equalTo(1);
    }];
    
}
-(void)clickInfoBtn{
    if (self.block) {
        self.block();
    }
}


-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (self.advBlock) {
        self.advBlock(index);
    }
}



@end
