//
//  TelView.m
//  519
//
//  Created by Macmini on 2018/12/4.
//  Copyright © 2018年 519. All rights reserved.
//

#import "TelView.h"
@interface TelView()
@property (nonatomic,strong)UIButton * editBtn;
@end
@implementation TelView

- (instancetype)init{
    if (self = [super init]) {
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)]];
        [self setuplayout];
    }
    return self;
}

- (void)setuplayout{
    [self.tellbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(15);
        make.top.equalTo(15);
        make.bottom.equalTo(-15);
    }];
    
    [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.tellbl.mas_right).offset(5);
        make.centerY.equalTo(self.tellbl);
        make.right.equalTo(-10);
    }];
}

- (void)back{
    if (self.editBlock) {
        self.editBlock();
    }
}

- (UILabel *)tellbl{
    if (!_tellbl) {
        _tellbl = [UILabel new];
        _tellbl.font = [UIFont systemFontOfSize:15];
        _tellbl.textColor = RGB(142, 142, 142);
        _tellbl.text = @"1234546555";
        [self addSubview:_tellbl];
    }
    return _tellbl;
}

- (UIButton *)editBtn{
    if (!_editBtn) {
        _editBtn = [UIButton new];
        [_editBtn setImage:[UIImage imageNamed:@"edit"] forState:0];
        [self addSubview:_editBtn];
    }
    return _editBtn;
}

@end
