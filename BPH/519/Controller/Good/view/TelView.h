//
//  TelView.h
//  519
//
//  Created by Macmini on 2018/12/4.
//  Copyright © 2018年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TelView : UIView
@property (nonatomic,strong)UILabel * tellbl;
@property (nonatomic,copy)void(^editBlock)(void);
@end
