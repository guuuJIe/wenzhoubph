//
//  ScoreTableViewCell.h
//  519
//
//  Created by Macmini on 2017/12/15.
//  Copyright © 2017年 519. All rights reserved.
//
typedef void(^scoreGoodInfoBlock)(NSString * Id);
#import <UIKit/UIKit.h>

@interface ScoreTableViewCell : UITableViewCell
@property (nonatomic,strong)NSArray * dataArr;
@property (nonatomic,strong)scoreGoodInfoBlock block;
@end
