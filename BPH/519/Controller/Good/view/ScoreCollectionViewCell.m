//
//  ScoreCollectionViewCell.m
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ScoreCollectionViewCell.h"

@interface ScoreCollectionViewCell()

@property (nonatomic,strong)UIImageView * goodImageView;
@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)UIImageView * iconImageView;
@property (nonatomic,strong)UILabel * priceLabel;
@property (nonatomic,strong)UIButton * btn;
@end

@implementation ScoreCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self configUI];
    }
    return self;
}


-(void)configUI{
    self.goodImageView = [[UIImageView alloc]init];
    self.goodImageView.backgroundColor = [UIColor grayColor];
    [self addSubview:self.goodImageView];
    
    self.titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = APPFourColor;
    _titleLabel.text = @"反反复复付付ddddd";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    _titleLabel.numberOfLines = 0;
    [self addSubview:_titleLabel];
    
    
    _btn = [[UIButton alloc]init];
    [_btn setTitle:@"sssss" forState:(UIControlStateNormal)];
    [_btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [_btn setImage:[UIImage imageNamed:@"icon_积分"] forState:(UIControlStateNormal)];
    _btn.titleLabel.font = [UIFont systemFontOfSize:12];
    _btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self addSubview:_btn];
    
    [self.goodImageView makeConstraints:^(MASConstraintMaker *make) {
        make.width.and.height.mas_equalTo(self.width/3*2);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.mas_equalTo(self.mas_top).with.mas_offset(10);
    }];
    [self.titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImageView.mas_bottom).with.mas_offset(5);
        make.centerX.mas_equalTo(self.goodImageView.mas_centerX);
        make.height.equalTo(25);
        make.width.equalTo(self.frame.size.width-20);
    }];
    [self.btn makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_titleLabel.mas_bottom).with.mas_offset(5);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(20);
        make.width.equalTo(self.frame.size.width-50);
    }];
    
    
}

-(void)setModel:(ScoreDateModel *)model{
    _model = model;
    [self.goodImageView sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.titleLabel.text = model.name;
    [_btn setTitle:[NSString stringWithFormat:@"%@积分",model.deal_score] forState:(UIControlStateNormal)];
}


@end







