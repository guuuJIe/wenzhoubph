//
//  GoodListVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface GoodListVC : BaseVC
@property (nonatomic,copy)NSString * idStr;
@property(nonatomic,strong)NSString * selectstr;
@property (nonatomic,copy)NSString * runjump;
@property (nonatomic,copy)NSString * typeId;

@property (nonatomic,assign)BOOL isFromWeb;
@property (nonatomic,strong)NSString * url;
@end
