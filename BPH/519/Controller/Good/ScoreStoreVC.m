//
//  ScoreStoreVC.m
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ScoreStoreVC.h"
#import "ScoreStoreHeadView.h"
#import "ScoreCollectionViewCell.h"
#import "GoodInfoModel.h"
#import "GoodInfoVC.h"
#import "OrderListVC.h"
#import "ScoreTableViewCell.h"
#import "GoodListVC.h"
#import "MainWebView.h"
@interface ScoreStoreVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _page;
}
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,strong)UIScrollView * scrollView;
@property (nonatomic,strong)NSMutableArray * dataScore;
@property (nonatomic,strong)ScoreStoreHeadView * headView;
@property (nonatomic,strong)UITableView * tableView;

@property (nonatomic,strong)NSMutableArray * advdataScore;
@property (nonatomic,strong)NSMutableArray * advUrlData;
@property (nonatomic,strong)NSMutableArray * advtypeData;
@end

@implementation ScoreStoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _page = 0;
    self.dataScore = [NSMutableArray new];
    self.advdataScore = [NSMutableArray new];
    self.advUrlData = [NSMutableArray new];
    self.navigationItem.title = @"积分商城";
    self.view.backgroundColor = APPBGColor;
    
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton * btn = [[UIButton alloc]initWithFrame:contentView.bounds];
    [btn setImage:[UIImage imageNamed:@"back_white_icon"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    [contentView addSubview:btn];
    UIBarButtonItem * item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    item.width = -35;
    self.navigationItem.leftBarButtonItems = @[item,[[UIBarButtonItem alloc]initWithCustomView:contentView]];
    
    
    [self createScoreUI];
    [self requestDate];
    
    
}
-(void)popVC{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)createScoreUI{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_SelfHeight-Screen_NavBarHeight-Screen_StatusBarHeight) style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[ScoreTableViewCell class] forCellReuseIdentifier:@"scoreide"];
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
   _headView = [[ScoreStoreHeadView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 91+Screen_Width/375*160)];
    __weak typeof(self)weself = self;
    _headView.block = ^{
        [kUserDefaults setObject:@"1" forKey:@"isscore"];
        [kUserDefaults synchronize];
        [weself allOrderVIewOnClick:0];
    };
    _headView.advBlock = ^(NSInteger index) {
        [weself pushWebView:weself.advUrlData[index] withType:weself.advtypeData[index]];
        
    };
    self.tableView.tableHeaderView = _headView;
    
}

-(void)requestDate{
    NSString * scoreData = [NSString stringWithFormat:@"http://www.bphapp.com/cxb_api/index.php?ctl=scores&email=%@&pwd=%@",EMAIL,PWD];
    [RequestData requestDataOfUrl:scoreData success:^(NSDictionary *dic) {
        if ([dic[@"status"] isEqual: @1]) {
            
            for (NSDictionary * subdic in dic[@"advs"]) {
                [self.advdataScore addObject:subdic[@"img"]];
                [self.advtypeData addObject:subdic[@"type"]];
            }
            [self requestAdvPopData:dic[@"advs"]];
            _headView.cycle.imageURLStringsGroup = self.advdataScore;
            
            for (NSDictionary * subdic in dic[@"item"]) {
                ScoreDateModel * model = [[ScoreDateModel alloc]init];
                model.name = subdic[@"name"];
                model.icon = subdic[@"icon"];
                model.deal_score = [NSString stringWithFormat:@"%@",subdic[@"deal_score"]];
                model.Id = subdic[@"id"];
                [self.dataScore addObject:model];
            }
            _headView.scoreLabel.text = [NSString stringWithFormat:@"%@",dic[@"user_score"]];
            
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)requestAdvPopData:(NSArray *)advPopArr {
    for (NSDictionary * subdic in advPopArr) {
        
        NSString * ctlType = [NSString stringWithFormat:@"%@",subdic[@"type"]];
        if ([ctlType isEqualToString:@"0"]) {
            [_advUrlData addObject: subdic[@"data"][@"url"]];
        }else
            if ([ctlType isEqualToString:@"11"]) {
               [_advUrlData addObject: subdic[@"data"][@"tid"]];
            }else
                if ([ctlType isEqualToString:@"12"]) {
                    [_advUrlData addObject: subdic[@"data"][@"cate_id"]];
                }else
                    if ([ctlType isEqualToString:@"21"]) {
                        [_advUrlData addObject: subdic[@"data"][@"item_id"]];
                    }else
                        if ([ctlType isEqualToString:@"13"]) {
                            [_advUrlData addObject: subdic[@"data"][@"data"]];
                        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellide =@"scoreide";
    if (indexPath.row == 0) {
        ScoreTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellide];
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        if (cell == nil) {
            cell = [[ScoreTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellide];
        }
        cell.block = ^(NSString *Id) {
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = Id;
            [self.navigationController pushViewController:vc animated:YES];
        };
        cell.dataArr = self.dataScore;
        return cell;
    }
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellide];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"nomercellIde"];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return (self.dataScore.count+1)/2*(Screen_Width/2+10);
    }
    return 1;
}

-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        OrderListVC * vc = [OrderListVC new];
        vc.isScore = 1;
        Class class=[OrderListVC class];
        
        [vcArray addObject:class];
        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
        
    }
    
    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
    //    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    pageController.isscore = 1;
    pageController.payJumpPage = @"zz";
    [pageController initBar];
    
    if (EMAIL) {
        [self.navigationController pushViewController:pageController animated:YES];
    }else{
//        [self.navigationController pushViewController:[LoginVC new] animated:YES];
        [self.navigationController pushViewController:[NewLoginVC new] animated:YES];
    }
    
}

-(void)pushWebView:(NSString *)weburl withType:(NSString *)typeId{
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    //网页
    if ([typeId isEqualToString:@"0"]) {
        webView.webUrl = weburl;
        if(weburl.length>2){
            [self.navigationController pushViewController:webView animated:YES];
        }
    }
    //团购
    if ([typeId isEqualToString:@"11"]) {
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        
      [self.navigationController pushViewController:goods animated:YES];
    }
    //商品列表
    if ([typeId isEqualToString:@"12"]) {
        
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
       [self.navigationController pushViewController:goods animated:YES];
    }
    //商品详情
    if ([typeId isEqualToString:@"21"]) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = weburl;
        [self.navigationController pushViewController:vc animated:YES];
    }
    //积分
    if ([typeId isEqualToString:@"13"]) {
        
        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
    }
}



@end
