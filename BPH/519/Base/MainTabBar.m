//
//  MainTabBar.m
//  519
//
//  Created by Macmini on 17/6/10.
//  Copyright © 2017年 519. All rights reserved.
//

#import "MainTabBar.h"
#import "MainModel.h"
#import "UIView+Common.h"
@interface MainTabBar ()
@property(nonatomic, strong)NSMutableArray *tabbarBtnArray;
@property (nonatomic, strong) UIButton *middleBtn;
@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)NSMutableArray * arr;
@end

@implementation MainTabBar
-(NSMutableArray *)arr{
    if (!_arr) {
        _arr = [[NSMutableArray alloc]init];
    }
    return _arr;
}
-(instancetype)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addBtnImage:) name:@"addBarBtnImage" object:nil];
    }
    return self;
}
-(void)addBtnImage:(NSNotification *)noti{
    if (self.arr) {
        [self.arr removeAllObjects];
    }
    NSArray * arr = noti.object;
    //[shop_id isEqualToString:@"1"] &&
    if ( arr.count>0) {
        for (NSDictionary * subdic in arr) {
            MainModel * model = [[MainModel alloc]init];
            [model setValuesForKeysWithDictionary:subdic];
            [self.arr addObject:model];
        }
        MainModel * model = self.arr[0];
        [self.middleBtn setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.img]]] forState:(UIControlStateNormal)];
        self.titleLabel.text = model.name;
    }else{
//        [self.middleBtn setImage:[UIImage imageNamed:@"bph"] forState:UIControlStateNormal];
//        self.titleLabel.text = @"商城";
        
    }
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    UIView *bgview = [[UIView alloc] init];
    UIButton *sendBtn = [[UIButton alloc] init];
    [sendBtn setImage:[UIImage imageNamed:@"bph"] forState:UIControlStateNormal];
    [sendBtn addTarget:self action:@selector(didClickPublishBtn:) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.adjustsImageWhenHighlighted = NO;
    sendBtn.backgroundColor = [UIColor clearColor];
    sendBtn.bounds = CGRectMake(0, 0,Screen_Width/7, Screen_Width/7);
    sendBtn.center = CGPointMake(self.frame.size.width/2, 0);
//    [sendBtn setCornerRadius:Screen_Width/6/2 withShadow:true withOpacity:2];
    sendBtn.layer.cornerRadius =Screen_Width/7/2;
    sendBtn.layer.masksToBounds = YES;
//    sendBtn.layer.borderWidth = 4;
//    sendBtn.layer.borderColor = [UIColor clearColor].CGColor;
    
    bgview.frame = CGRectMake(0, 0, Screen_Width/6, Screen_Width/6);
    bgview.center = sendBtn.center;
    bgview.backgroundColor = [UIColor whiteColor];
    [bgview setCornerRadius:Screen_Width/6/2 withShadow:true withOpacity:0.71];
    
    UILabel * titleL = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, floor(Screen_Width/6), 15)];
    titleL.text = @"商城";
    int center_y = floor(CGRectGetHeight(self.frame)-7.5);
    if (self.frame.size.height == 83) {
        center_y = floor(CGRectGetHeight(self.frame)-40);
    }
    titleL.center =CGPointMake(self.frame.size.width/2, center_y);
    titleL.textColor = [UIColor blackColor];
    titleL.font = [UIFont systemFontOfSize:10];
    titleL.textAlignment = NSTextAlignmentCenter;
    if (!self.middleBtn) {
        [self addSubview:bgview];
        [self addSubview:sendBtn];
        [self addSubview:titleL];
        
        self.middleBtn = sendBtn;
        self.titleLabel = titleL;
    }
    [sendBtn setImagePositionWithType:SSImagePositionTypeTop spacing:4];
    // 其他位置按钮
    NSUInteger count = self.subviews.count;
    for (NSUInteger i = 0 , j = 0; i < count; i++)
    {
        UIView *view = self.subviews[i];
        Class class = NSClassFromString(@"UITabBarButton");
        if ([view isKindOfClass:class])
        {
            view.width = self.width / 5.0;
            view.x = self.width * j / 5.0;
            j++;
            if (j == 2)
            {
                j++;
            }
        }
    }
}
// 发布
- (void)didClickPublishBtn:(UIButton*)sender {
    if (self.block) {
        self.block(_arr);
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (self.isHidden == NO)
    {
        CGPoint newP = [self convertPoint:point toView:self.middleBtn];
        if ( [self.middleBtn pointInside:newP withEvent:event])
        {
            return self.middleBtn;
        }else
        {
            return [super hitTest:point withEvent:event];
        }
    }
    else
    {
        return [super hitTest:point withEvent:event];
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
