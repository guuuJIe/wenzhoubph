//
//  BaseNC.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import "BaseNC.h"
#import "UIImage+Common.h"
#import "GoodInfoVC.h"
@interface BaseNC ()<UINavigationBarDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UIPanGestureRecognizer *fullScreenPopPanGesture;

@end

@implementation BaseNC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19], NSForegroundColorAttributeName:[UIColor redColor]}];
//    self.interactivePopGestureRecognizer.delegate = self;
    [self addFullScreenPopPanGesture];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UMUtil mobclickAgentBeginLog:NSStringFromClass([self class])];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [UMUtil mobclickAgentEndLog:NSStringFromClass([self class])];
}

- (void)addFullScreenPopPanGesture{
    
    self.fullScreenPopPanGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self.interactivePopGestureRecognizer.delegate action:@selector(handleNavigationTransition:)];
    self.fullScreenPopPanGesture.delegate = self;
    [self.view addGestureRecognizer:self.fullScreenPopPanGesture];
    [self.interactivePopGestureRecognizer requireGestureRecognizerToFail:self.fullScreenPopPanGesture];
    self.interactivePopGestureRecognizer.enabled = NO;
}

- (void)handleNavigationTransition:(UIPanGestureRecognizer *)ges{
    
    [self popToBack];
    
}

- (void)popToBack
{
    [self popViewControllerAnimated:YES];
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0)
    {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    
    if (self.viewControllers.count > 0) {
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
        [btn addTarget:self action:@selector(popToBack) forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateHighlighted];
         btn.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    }
    
    [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    return [super popViewControllerAnimated:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer*)gestureRecognizer{
    if ([gestureRecognizer isEqual:self.fullScreenPopPanGesture]) {
        //获取手指移动后的相对偏移量
        CGPoint translationPoint = [self.fullScreenPopPanGesture translationInView:self.view];
        
        UIViewController *vc = self.viewControllers.lastObject;
        
        if ([vc isKindOfClass:[GoodInfoVC class]]) {
            return NO;
        }
        //向右滑动 && 不是跟视图控制器
        if (translationPoint.x > 0 && self.childViewControllers.count > 1) {
            return YES;
        }
        return NO;
    }
    return YES;
}


@end
