//
//  BaseVC.h
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController
@property(nonatomic,strong)UIView *statusBarView;
@property(nonatomic,strong)UIView *barView;
@property(nonatomic,strong)UILabel *barTitle;


#pragma mark 自定义bar 子类 需要调用，不需要不调用
-(void)initBar;


#pragma mark tab页面跳转子页面 tab 隐藏处理
-(void)tabHidePushVC:(BaseVC*)vc;

#pragma  mark 弹出页面
-(void)popVC;

@end
