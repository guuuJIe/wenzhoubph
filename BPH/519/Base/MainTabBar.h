//
//  MainTabBar.h
//  519
//
//  Created by Macmini on 17/6/10.
//  Copyright © 2017年 519. All rights reserved.
//

typedef void(^didMiddBtn)(NSArray * arr);
#import <UIKit/UIKit.h>

@interface MainTabBar : UITabBar

@property (nonatomic,copy) didMiddBtn  block;
@end
