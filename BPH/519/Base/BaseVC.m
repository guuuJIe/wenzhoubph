//
//  BaseVC.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置导航栏颜色、字体大小
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19], NSForegroundColorAttributeName:[UIColor whiteColor]}];
}
-(void)initBar{
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30,30)];
    UIButton * leftBtn=[[UIButton alloc]initWithFrame:contentView.bounds];
    [leftBtn setImage:[UIImage imageNamed:@"返回"] forState:(UIControlStateNormal)];
    [leftBtn setSelected:NO];
    [leftBtn addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:leftBtn];
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear: animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UMUtil mobclickAgentBeginLog:NSStringFromClass([self class])];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [UMUtil mobclickAgentEndLog:NSStringFromClass([self class])];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tab页面跳转子页面 tab 隐藏处理
-(void)tabHidePushVC:(BaseVC *)vc{
    
    
    
    //self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:nil];
    vc.hidesBottomBarWhenPushed=YES;

    [self.navigationController pushViewController:vc animated:YES];
}

#pragma  mark 弹出页面
-(void)popVC{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
