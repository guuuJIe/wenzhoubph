//
//  MainTabBarController.m
//  519
//
//  Created by Macmini on 17/6/10.
//  Copyright © 2017年 519. All rights reserved.
//
#define kDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#import "MainTabBarController.h"
#import "MainVC.h"
#import "NewMainViewController.h"
#import "TypeVC.h"
#import "CarVC.h"
#import "UserVC.h"
#import "MainWebView.h"
#import "GoodInfoVC.h"
#import "GoodListVC.h"
#import "SCJumpViewController.h"
#import "MainTabBar.h"
#import "MapNavigatorVC.h"
#import "MainWebView.h"
#import "ScoreStoreVC.h"
#import "StoreAddressVC.h"
#import "MainModel.h"
#import "NewCarVC.h"
#import "MapNavigatorVC.h"
@interface MainTabBarController ()
@property(nonatomic, weak)MainTabBar *mainTabBar;

@end

@implementation MainTabBarController
//单例
static MainTabBarController *controller = nil;
+(MainTabBarController *)sharedController{
    @synchronized(self){
        if(controller == nil){
            controller = [[self alloc] init];
            
        }
    }
    return controller;
}
+(instancetype)allocWithZone:(NSZone *)zone{
    @synchronized(self){
        if (controller == nil) {
            controller = [super allocWithZone:zone];
            return  controller;
        }
    }
    return nil;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    
    //去掉tabBar顶部线条
//    CGRect rect = CGRectMake(0, 0, Screen_Width, Screen_Height);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
//    CGContextFillRect(context, rect);
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    [self.tabBar setBackgroundImage:img];
//    [self.tabBar setShadowImage:img];
    
    [self SetupMainTabBar];
    [self SetupAllControllers];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    if (@available(iOS 13.0, *)) {

        [[UITabBar appearance] setUnselectedItemTintColor:APPFourColor];

    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)SetupMainTabBar{
    MainTabBar *tabBar = [[MainTabBar alloc] init];
    [self setValue:tabBar forKey:@"tabBar"];
    [tabBar setBlock:^(NSArray * arr){
       
        if(arr.count<1){
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"shop_id"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self setSelectedIndex:0];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshHome" object:nil];
        }else{
            MainModel * model = arr[0];
            if (self.selectedIndex == 0) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshHome" object:model];
            }
            if (self.selectedIndex == 1) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshHome1" object:model];
            }
            if (self.selectedIndex == 2) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshHome2" object:model];
            }
            if (self.selectedIndex == 3) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshHome3" object:model];
            }
        }
    }];
}

-(void)viewControllerIsJumpBy:(NSDictionary *)dataArr withtype:(NSString *)type{
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    NSString * item;
    if ([type isEqualToString:@"0"]) {
        item = dataArr[@"url"];
        webView.webUrl = item;
        if(item.length>2){
            [self.navigationController pushViewController:webView animated:YES];
        }
    }else
        if ([type isEqualToString:@"11"]) {
            item = dataArr[@"tid"];
            goods.idStr = [NSString stringWithFormat:@"%@",item];
            goods.selectstr = @"mainJumpGoods";
            [self.navigationController pushViewController:goods animated:YES];
        }else
            if ([type isEqualToString:@"12"]) {
                item = dataArr[@"cate_id"];
                goods.idStr = [NSString stringWithFormat:@"%@",item];
                goods.selectstr = @"mainJumpGoods";
                [self.navigationController pushViewController:goods animated:YES];
            }else
                if ([type isEqualToString:@"21"]) {
                    item = dataArr[@"item_id"];
                    GoodInfoVC * vc = [GoodInfoVC new];
                    vc.goodsId = item;
                    [self.navigationController pushViewController:vc animated:YES];
                }else
                    if ([type isEqualToString:@"13"]) {
                        item = dataArr[@"data"];
                        [self.navigationController pushViewController:[ScoreStoreVC new] animated:YES];
                    }
}
- (void)SetupAllControllers{
    NSArray *titles = @[@"首页",@"分类", @"购物车", @"我的"];
    NSArray *images = @[@"首页",@"分类", @"购物车", @"我的"];
    NSArray *selectedImages = @[@"首页颜色",@"分类颜色", @"购物车颜色", @"我的颜色"];

    NewMainViewController *main1 = [[NewMainViewController alloc] init];
    TypeVC *main2 = [[TypeVC alloc] init];
    NewCarVC *main3 = [[NewCarVC alloc] init];
    UserVC *main4 = [[UserVC alloc] init];
    NSArray *viewControllers = @[main1, main2, main3, main4];
    
    for (int i = 0; i < viewControllers.count; i++) {
        UIViewController *childVc = viewControllers[i];
        [self SetupChildVc:childVc title:titles[i] image:images[i] selectedImage:selectedImages[i]];
    }
}

- (void)SetupChildVc:(UIViewController *)VC title:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName{
    [VC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:APPFourColor} forState:UIControlStateNormal];
    VC.tabBarItem.title = title;
    [VC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName: APPColor} forState:UIControlStateSelected];
    VC.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    VC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNC *nav = [[BaseNC alloc] initWithRootViewController:VC];
    [self addChildViewController:nav];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    self.tabBarController.selectedIndex  = 1;
}
@end
