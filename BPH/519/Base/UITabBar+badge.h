//
//  UITabBar+badge.h
//  519
//
//  Created by Macmini on 2019/5/7.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITabBar (badge)


- (void)showBadgeOnItemIndex:(int)index;   //显示小红点

- (void)hideBadgeOnItemIndex:(int)index; //隐藏小红点



@end

NS_ASSUME_NONNULL_END
